/**
1. 개발자 : 김현섭
2. 개발일자 : 2018.09.12
3. 기능명 : 안드로이드 보안 업데이트로 인한 runtime 권한 호출
*/

var myclb;

function requestPermission(fnName, permissions, params){
    // request grant for multiple permissions

    let Permission = window.plugins.Permission;

    Permission.has(permissions, function(results) {
        // 권한이 없을때
        if (!results[permissions]) {
            Permission.request(permissions, function(results) {
                if( fnName == "phoneNumber"){
                    window.plugins.sim.getSimInfo(successPhoneInfoCallback, fnErrorCl);
                    return;
                }else if( fnName == "call"){
                    cordova.InAppBrowser.open("tel:" + params, '_system');
                    return;
                }else if( fnName == "sms"){
                    cordova.InAppBrowser.open("sms:" + params, '_system');
                    return;
                }else if( fnName == "camera"){

                    navigator.camera.getPicture(function(result){
                        console.log(result);
                        },function(error){
                            console.log(error);
                        },{
                        sourceType : Camera.PictureSourceType.CAMERA
                    });
                    return;
                }else if(fnName == "geolocation"){
                    requestLocationAccuracy();
                 }
            }, fnError);
        }else{
            if( fnName == "phoneNumber"){
                window.plugins.sim.getSimInfo(successPhoneInfoCallback, fnErrorCl);
                return;
            }else if( fnName == "call"){
                cordova.InAppBrowser.open("tel:" + params, '_system');
                return;
            }else if( fnName == "sms"){
                 cordova.InAppBrowser.open("sms:" + params, '_system');
                 return;
            }else if( fnName == "camera"){
                 navigator.camera.getPicture(function(result){
                    console.log(result);
                    },function(error){
                        console.log(error);
                    },{
                        sourceType : Camera.PictureSourceType.CAMERA
                    });
                 return;
            }else if(fnName == "geolocation"){
                requestLocationAccuracy();
            }
        }
    }, fnError);
}

// 전화번호 가져오기
function deviceGetFnPhoneNumber(){
    window.plugins.sim.getSimInfo(successPhoneInfoCallback, fnErrorCl);
}

function chomeGetFnPhoneNumber(){
    syncSetFcmKey("No Set Fcm Token", "Chome", "Web", "", "Google");
}

// 전화걸기 연동
function deviceExFnCall(phoneNum){
    var phoneNumber = phoneNum;
    var arrPermissions = ['android.permission.READ_PHONE_STATE', 'android.permission.CALL_PHONE' ];
    requestPermission("call", arrPermissions, phoneNumber);
}

// SMS 연동
function deviceExFnSMS(phoneNum){
    var phoneNumber = phoneNum;
    var arrPermissions = ['android.permission.READ_PHONE_STATE', 'android.permission.SEND_SMS' ];
    requestPermission("sms", arrPermissions, phoneNumber);
}

// 카메라 연동
function deviceExCamera(){
    var arrPermissions = ['android.permission.READ_EXTERNAL_STORAGE', 'android.permission.WRITE_EXTERNAL_STORAGE', 'android.permission.CAMERA'  ];
    requestPermission("camera", arrPermissions, null);
}

// 위치정보 연동
function deviceExGeoLocation(clb){

    myclb = clb;
//    console.log("deviceExGeoLocation");
    var arrPermissions = ['android.permission.ACCESS_COARSE_LOCATION', 'android.permission.ACCESS_FINE_LOCATION' ];
    requestPermission("geolocation", arrPermissions, null);
}

function deviceExGoogleMap(_lat, _lng){

    var geocoder;
    var longitude = _lng;
    var latitude = _lat;
    var infowindow = new google.maps.InfoWindow();

    var latLong = new google.maps.LatLng(latitude, longitude);

    var mapOptions = {
        center: latLong,
        zoom: 13,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    var map = new google.maps.Map(document.querySelector("#addrMap"), mapOptions);

    var marker = new google.maps.Marker({
          position: latLong,
          map: map,
          title: 'my location'
    });

    google.maps.event.addListener(map, 'click', codeCoordinate);

    function codeCoordinate(event) {

        Setmarker(event.latLng);
        //이벤트 발생 시 그 좌표에 마커를 생성합니다.
        
        // 좌표를 받아 reverse geocoding(좌표를 주소로 바꾸기)를 실행합니다.
        geocoder.geocode({'latLng' : event.latLng}, function(results, status) {
        
            if (status == google.maps.GeocoderStatus.OK)  {
                if (results[1]){
                    infowindow.setContent(results[1].formatted_address);
                    infowindow.open(map,marker[0]);
                    //infowindow로 주소를 표시합니다.
                }
            }        
        });
    }
  
    /* 
    클릭한 지점에 마커를 표시하는 함수입니다.
    그런데 이 함수를 잘 봐야 하는 것이 바로 marker를 생성하지 않고 marker라는 배열 안에 Marker 
    obejct  원소를 계속 추가하고 있습니다. 이는 매번 클릭할 때마다 새로운 마커를 생성하기 위함입니다. 
    */
      
      //입력 받은 주소를 지오코딩 요청하고 결과를 마커로 지도에 표시합니다.
      
    function codeAddress(event) {
      
       if (geocodemarker.length > 0) {
            for (var i=0;i<geocodemarker.length ;i++ ) {
                geocodemarker[i].setMap(null);
            }
            geocodemarker =[];
            geocodemarker.length = 0;
       }

       //이 부분도 위와 같이 주소를 입력할 때 표시되는 marker가 매번 새로 나타나게 하기 위함입니다.

        var address = document.getElementById("addr1").value;
        //아래의 주소 입력창에서 받은 정보를 address 변수에 저장합니다.



        //지오코딩하는 부분입니다.

        geocoder.geocode( {'address': address}, function(results, status) {

            //Geocoding이 성공적이라면,
            if (status == google.maps.GeocoderStatus.OK)  {

                alert(results.length + "개의 결과를 찾았습니다.");

                //결과의 개수를 표시하는 창을 띄웁니다. alert 함수는 알림창 함수입니다.

                for(var i=0;i<results.length;i++){

                    map.setCenter(results[i].geometry.location);

                    geocodemarker.push(new google.maps.Marker({
                        center: results[i].geometry.location,
                        position: results[i].geometry.location,
                        icon: GreenIcon,
                        map: map
                    }));
                }
                //결과가 여러 개일 수 있기 때문에 모든 결과를 지도에 marker에 표시합니다.
            }else{
                alert("Geocode was not successful for the following reason: " + status);

            }
        });
    }
}

// 위치정보 GPS 요청 함수
function clGeoLocation(){

    if( typeof myclb === 'function' ){
        myclb();
    }
    
    var options = {
        enableHighAccuracy: true,
        timeout: 5000,
        maximumAge: 0
    };

    var geo_id = navigator.geolocation.watchPosition(successGeoWatch, failGeoLoc ,
         // 위치정보 처리 옵션
         {
             maximumAge: 3000,           // 갱신주기(1/1000초 단위)
             timeout: 30000,             // 타임아웃(1/1000초 단위)
             enableHighAccuracy: true    // 높은 정확도 사용
         }
     ); // end watchPosition

     navigator.geolocation.getCurrentPosition(successGeoLoc, failGeoLoc , options);
}

function fnError(err){
    runWarningMessage(err, "danger", 1500, "up");


}

// 전화번호 조회 실패시 콜백함수
function fnErrorCl(err){
    // console( "fail: " + JSON.stringify(err) );
    runWarningMessage("핸드폰 정보를 읽어올수 없으므로 앱을 사용할 수 없습니다. 재실행하여 동의 후 사용하여 주시기 바랍니다", "danger", 2500, "up");
}

function successPhoneInfoCallback(res){
    
    resphoneNumber = res.phoneNumber;
    //resphoneNumber = null;
    localStorage.setItem("DEVICE_ID", res.deviceId);

    // 폰번호 null 체크
    var inputTel = null;
    if( isNull( resphoneNumber ) || resphoneNumber === "undefined" ){
        inputTel = localStorage.getItem("TEL");
    }else{
        inputTel = resphoneNumber ;
    }

    inputTel = getfilterNumber(inputTel);
    window.localStorage.setItem("TEL", inputTel);

    cordova.plugins.firebase.messaging.getToken().then(function(token) {
        var model               = device.model;
        var platform            = device.platform;
        var deviceVersion       = device.version;
        var deviceManufacturer  = device.manufacturer;

        syncSetFcmKey(token, model, platform, deviceVersion, deviceManufacturer);
    });

}


function successCall(res){
    alert("전화번호 걸기 성공 : " + res.phoneNumber);
}

function successGeoWatch(position){

    localStorage.setItem("LNG", position.coords.longitude);
    localStorage.setItem("LNG", position.coords.latitude);

//    // 위도
//    $('.list-group-item:eq(0) ').html(position.coords.longitude);
//    // 경도
//    $('.list-group-item:eq(1) ').html(position.coords.latitude);
//    // 고도
//    $('.list-group-item:eq(2) ').html(position.coords.altitude);
//    // 위경도 정확도 (미터 단위)
//    $('.list-group-item:eq(3) ').html(position.coords.accuracy);
//    // 고도 정확도 (미터 단위)
//    $('.list-group-item:eq(4) ').html(position.coords.altitudeAccuracy);
//    // 속도 (초당 미터 단위)
//    $('.list-group-item:eq(5) ').html(position.coords.speed);
//    // 방향 (속도가 없는 경우 값 없음)
//    $('.list-group-item:eq(6) ').html(position.coords.heading);
//    // 위치정보 수집 시각
    // $('.list-group-item:eq(7) span').html(position.timestamp);
}

// 위치정보 성공 콜백함수
function successGeoLoc(position){

    localStorage.setItem("LNG", position.coords.longitude);
    localStorage.setItem("LNG", position.coords.latitude);
//
//    // 위도
//    $('.list-group-item:eq(0) span').html(position.coords.longitude);
//    // 경도
//    $('.list-group-item:eq(1) span').html(position.coords.latitude);
//    // 고도
//    $('.list-group-item:eq(2) span').html(position.coords.altitude);
//    // 위경도 정확도 (미터 단위)
//    $('.list-group-item:eq(3) span').html(position.coords.accuracy);
//    // 고도 정확도 (미터 단위)
//    $('.list-group-item:eq(4) span').html(position.coords.altitudeAccuracy);
//    // 속도 (초당 미터 단위)
//    $('.list-group-item:eq(5) span').html(position.coords.speed);
//    // 방향 (속도가 없는 경우 값 없음)
//    $('.list-group-item:eq(6) span').html(position.coords.heading);
//    // 위치정보 수집 시각
//    $('.list-group-item:eq(7) span').html(position.timestamp);

    //deviceExGoogleMap(position.coords.latitude, position.coords.longitude);
}

// 위치정보 실패시 콜백함수
function failGeoLoc(err) {

    runWarningMessage("[" + err.code + "] " + err.message, "warning", 1500, "up");

    deviceExGoogleMap(106.63278, 32);
}

function onError(error) {
    console.error("The following error occurred: " + error);
    runWarningMessage("위치정보를 수신할수 없습니다. GPS 설정사용을 확인해주세요", "warning", 2000, "up");}

function handleLocationAuthorizationStatus(cb, status) {
    switch (status) {
        case cordova.plugins.diagnostic.permissionStatus.GRANTED:
            cb(true);
            break;
        case cordova.plugins.diagnostic.permissionStatus.NOT_REQUESTED:
            requestLocationAuthorization(cb);
            break;
        case cordova.plugins.diagnostic.permissionStatus.DENIED:
            cb(false);
            break;
        case cordova.plugins.diagnostic.permissionStatus.DENIED_ALWAYS:
            // Android only
            cb(false);
            break;
        case cordova.plugins.diagnostic.permissionStatus.GRANTED_WHEN_IN_USE:
            // iOS only
            cb(true);
            break;
    }
}

function requestLocationAuthorization(cb) {
    cordova.plugins.diagnostic.requestLocationAuthorization(handleLocationAuthorizationStatus.bind(this, cb), onError);
}
function ensureLocationAuthorization(cb) {

    cordova.plugins.diagnostic.getLocationAuthorizationStatus(handleLocationAuthorizationStatus.bind(this, cb), onError);
}

function requestLocationAccuracy(){
    ensureLocationAuthorization(function(isAuthorized){
        if(isAuthorized){
            cordova.plugins.locationAccuracy.canRequest(function(canRequest){
                if (canRequest) {
                    cordova.plugins.locationAccuracy.request(clGeoLocation , function (error) {
                            onError("Error requesting location accuracy: " + JSON.stringify(error));
                            if (error) {
                                // Android only
                                onError("error code=" + error.code + "; error message=" + error.message);
                                if (error.code !== cordova.plugins.locationAccuracy.ERROR_USER_DISAGREED) {
                                    if (window.confirm("Failed to automatically set Location Mode to 'High Accuracy'. Would you like to switch to the Location Settings page and do this manually?")) {
                                        cordova.plugins.diagnostic.switchToLocationSettings();
                                    }
                                }
                            }
                        }, cordova.plugins.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY // iOS will ignore this
                    );
                } else {
                    // On iOS, this will occur if Location Services is currently on OR a request is currently in progress.
                    // On Android, this will occur if the app doesn't have authorization to use location.
                    onError("Cannot request location accuracy");
                }
            });
        }else{
            onError("User denied permission to use location");
        }
    });
}

// Make the request
// start the check
function syncSetFcmKey(token, model, platform, deviceVersion, deviceManufacturer){

    tel                 = localStorage.getItem("TEL");
    deviceId            = localStorage.getItem("DEVICE_ID");
    var serviceToken    = new withWSDL("api/userm/syncInfo/0");

    // undefined 삭제
    if( isNull(deviceId) ){
        deviceId = tel;
    }

    //tel                 = null;
    serviceToken.success = function(_jsonData){

        var jsonData    = _jsonData;
        if( jsonData.error == 500){
            localStorage.setItem("FCM_KEY", null);
            runWarningMessage(jsonData.message, "danger", 1500, "up");
        }else{
            if( !isNull(jsonData.BRANCH_ID) ){
                localStorage.setItem("BRANCH_ID", jsonData.BRANCH_ID);
                localStorage.setItem("ID", jsonData.ID);
            }
            $(".myTel").html( localStorage.getItem("TEL") )
        }
        localStorage.setItem("FCM_KEY", token);
    };

    serviceToken.error = function(){
        alert("error");
    }

    // 디바이스ID를 못불러올 경우에는 전화번호로 대체시킴
    if (deviceId === "undefined"){
        deviceId  = tel;
    }

    // 전화번호가 세팅된 경우
    // 전화번호 수집 동의 체크 => 전화번호로 사용자인증 동기화
    if( !isNull( tel ) ){
        tel = getfilterNumber(tel);
        var option  = {
            "phoneNumber"   : getfilterNumber(tel),
            "fcm_token"     : token,
            "model"         : model,
            "platform"      : platform,
            "deviceVersion" : deviceVersion,
            "deviceManufacturer" : deviceManufacturer,
            "deviceId"      : deviceId,
            "CORP_ID"       : APP_CORP_ID,
            "BRANCH_ID"     : APP_BRANCH_ID,
            "LANG"          : localStorage.getItem("lang"),
            "IS_USE_SYS"    :"Y",
            "IS_USE_FCM"    :"Y",
            'ADDR1'         : localStorage.getItem("ADDR1"),
            'ADDR2'         : localStorage.getItem("ADDR2"),
            'area_id'       : localStorage.getItem("AREA_ID"),
            "app_version"   : localStorage.getItem("APP_VERSION"),
        };
        // console.log( JSON.parse(JSON.stringify(option)) );
        var telAgree = localStorage.getItem("TEL_AGREE_V1");

        // 1.전화번호 수동입력시 동의 확인
        if( isNull(telAgree) || telAgree != "Y"  ){
            modelView           = document.querySelector("#modalAgreeTel");
            btnClosePopTel      = document.querySelector(".btnClosePopTel");
            btnExitApp          = document.querySelector(".btnExitApp");
            modelView.show();

            var switchAgreePrivacy 	= document.querySelector("#switchAgreePrivacy");
            switchAgreePrivacy.onclick = function(){
                
                if( switchAgreePrivacy.checked ){
                    btnClosePopTel.disabled = false;
                }else{
                    btnClosePopTel.disabled = true;
                }
            };

            btnClosePopTel.onclick = function(){
                if( switchAgreePrivacy.checked ){
                    localStorage.setItem("TEL_AGREE_V1", "Y");
                    serviceToken.ajaxPutData(option);
                   
                }else{
                    var t_no_agree_msg  = myApp.i18Next.getTrasTxt("SYS", "NO_AGREE_MSG");
                    localStorage.setItem("TEL_AGREE_V1", null);
                    runWarningMessage(t_no_agree_msg, "warning", 1500, "up");
                    setTimeout(function(){
                        localStorage.setItem("TEL", null);
                        localStorage.setItem("ID", null);
                        location.reload();
                    }, 1500);
                }
                modelView.hide();
            }

            // 프로그램 종료
            btnExitApp.onclick = function(){
                navigator.app.exitApp();
            }
        }
        // 2. 전화번호동의 없을 경우 바로 인증하기
        else{
            serviceToken.ajaxPutData(option);
        }
    }
}
