// SPA (모바일)에서 Enter Key와 Esc키는 오류를 발생할 확률이 높으므로 사전감지하여 차단시킴
document.onkeydown = function(evt) {
	evt = evt || window.event;
	var isNotEscOrEnter = true;
	if ("key" in evt) {
		isNotEscOrEnter = !(evt.key == "Escape" || evt.key == "Esc" || evt.key == "Enter" );
		if( !isNotEscOrEnter ){
			evt.preventDefault();
		}
	}
	return true;
};


String.prototype.cut = function(len) {
	var str = this;
	var l = 0;
	for (var i=0; i<str.length; i++) {
		l += (str.charCodeAt(i) > 128) ? 2 : 1;
		if (l > len) return str.substring(0,i) + ".." ;
	}
	return str;
};

// 숫자 타입에서 천단위 , 추가에 대한 프로토 타입 선언
Number.prototype.format = function(){

	if(this==0) return 0;

	reg = /(^[+-]?\d+)(\d{3})/;
	n = (this + '');

	while (reg.test(n)) n = n.replace(reg, '$1' + ',' + '$2');

	lang = localStorage.getItem("lang");
	lang =  isNull(lang) ? "vn" : lang;

	if( lang.indexOf("vn") >= 0){
		n = this.toLocaleString('it-IT', {style : 'decimal', decimal : 'VND', minimumFractionDigits : 0});
		// console.log(n);
	}
	
	return n;
};

Number.prototype.currency = function(fNum){

	if(this==0) return 0;

	var reg = /(^[+-]?\d+)(\d{3})/;
	var n = (this + '');

	while (reg.test(n)) n = n.replace(reg, '$1' + ',' + '$2');

	var lang = localStorage.getItem("lang");

	//if( lang.indexOf("vn") >= 0){
	n = this.toLocaleString('it-IT', {style : 'currency', currency : 'VND', minimumFractionDigits : fNum});
	//}
	return n;
};

$.fn.serializeObject = function () {

	var result = {};
	var extend = function (i, element) {
	var node = result[element.name];
	if ('undefined' !== typeof node && node !== null) {
		if ($.isArray(node)) {
		node.push(element.value);
		} else {
		result[element.name] = [node, element.value];
		}
	} else {
		result[element.name] = element.value;
	}
	};

	$.each(this.serializeArray(), extend);
	return result;
};

// 숫자입력에 대한 천단위 컴마(,) 삽입
// 숫자의 범위 20억 제한 (MSSQL INTERGER 데이터 타입 크기 감안)
function formatValue(event){
	
	var inputId = event.target.name;
	var t_rqr	= myApp.i18Next.getTrasTxt("DIAG", "I_NUMBER");	// 숫자만 입력

	if( document.getElementById(inputId).value != null ) {
		var x = removeCommas(document.getElementById(inputId).value);
		
		if( !isNaN(x) ){
			// mssql interger 데이터 타입 크기 
			if( x > 2147483646){
				if( $("ons-toast").length == 0 ){
					runWarningMessage("범위가 너무 큽니다", "warning", 1000, "up");
				}
				document.getElementById(inputId).value = "";
			}else{
				document.getElementById(inputId).value = Number(x).format();	
			}
			
		}else{

			if( $("ons-toast").length == 0 ){
				runWarningMessage(t_rqr, "warning", 1000, "up");
			}
			document.getElementById(inputId).value = "";
		}
	}
}

// 콤마 제거
function removeCommas(str) {
	return(str.replace(/,/g,''));
}

// 입력에 대한 벨리데이션
// jQuery validation 은 form 기반 + one-input 인식이 안되서 우선 custom으로 제공
function fnValidationInput(pageId){

	var error_count = 0;
	var error_count_bef = 0;
	var error_select_count = 0;
	
	// 필수입력
	var t_rqr_input 	= myApp.i18Next.getTrasTxt("SYS", "RQR_INPUT");

	$('#' + pageId).find('ons-select').each(function(){
		
		$(this).css({"background" : "none" });
		$(this).find("label.red.error").remove();

		if( $(this).attr('required')) {
			
			var option = $(this).find("option:selected").val();
			if( option == "" ){
				error_select_count++;
				$(this).css({
					"border" : "1px solid #ff5e00",
				}).append("<label class='red error'>" + t_rqr_input + "</label>");    		
			}
		}
	});

	
	$('#' + pageId).find('ons-input').each(function(){

		error_count_bef = error_count;

		$(this).css({"background" : "none" });
		$(this).find("label.red.error").remove();

		if( $(this).attr('required') && $(this).attr('type') == "email") {

            if( $(this).val().length > 0 ){

                if( validateEmail( $(this).val() ) ){
                
                }else{
					var t_vali_email	= myApp.i18Next.getTrasTxt("AUTH", "VAL_EMAIL");
                    error_count++;
                    $(this).css({
                        "background" : "rgba(255, 0, 0, 0.2)",
                    }).append("<label class='red error'>" + t_vali_email + "</label>");
                }
            }
			
		}

		if( $(this).attr('required')) {
			
			if( $(this).val().length == 0 ){
				error_count++;
				$(this).css({
					// "background" : "rgba(255, 0, 0, 0.2)",
				}).append("<label class='red error'>필수 입력</label>");
			}
            
            if( $(this).attr('min-length')) {
			
                var minLength = $(this).attr('min-length');
                if( $(this).val().length < minLength ){
                    error_count++;
                    $(this).css({
                        "background" : "rgba(255, 0, 0, 0.2)",
                    }).append("<label class='red error'>최소 " + minLength + " 자리를 입력하세요</label>");
                    
                }
            }
    
            if( $(this).attr('max-length')) {
    
                var maxLength = $(this).attr('max-length');
                if( $(this).val().length > maxLength ){
                    error_count++;
                    $(this).css({
                        "background" : "rgba(255, 0, 0, 0.2)",
                    }).append("<label class='red error'>최대 " + maxLength + " 자리만 입력하세요</label>");
                    
                }
            }
		}

		if( error_count_bef == error_count){
			$(this).css({ "background" : "none" });
			$(this).find("label.red.error").remove();
		}
	});

	$('#' + pageId).find('textarea').each(function(){

		error_count_bef = error_count;

		$(this).css({"background" : "none" });
		$(this).find("label.red.error").remove();

		if( $(this).attr('required')) {
			
			if( $(this).val().length == 0 ){
				error_count++;
				$(this).css({
					// "background" : "rgba(255, 0, 0, 0.2)",
				}).append("<label class='red error'>" + t_rqr_input +"</label>");
			}
            
            if( $(this).attr('min-length')) {
			
                var minLength = $(this).attr('min-length');
                if( $(this).val().length < minLength ){
                    error_count++;
                    $(this).css({
                        // "background" : "rgba(255, 0, 0, 0.2)",
                    }).append("<label class='red error'>최소 " + minLength + " 자리를 입력하세요</label>");
                }
            }
    
            if( $(this).attr('max-length')) {
    
                var maxLength = $(this).attr('max-length');
                if( $(this).val().length > maxLength ){
                    error_count++;
                    $(this).css({
                        "background" : "rgba(255, 0, 0, 0.2)",
                    }).append("<label class='red error'>최대 " + maxLength + " 자리만 입력하세요</label>");
                }
            }
		}

		if( error_count_bef == error_count){
			$(this).css({ "background" : "none" });
			$(this).find("label.red.error").remove();
		}
	});

	// console.log(error_select_count, error_count);

	if( error_count > 0 || error_select_count > 0){

		if( $(document).find("ons-toast.warning").length == 0 ){
			runWarningMessage("입력화면의 필수값을 입력하세요", "warning", 1500, "up");
		}
		return false;
	}else{
		return true;
	}
}

function runWarningMessage(message, type, time, position){

	// up, bottom, left, right
	var _class ;
	var _type	 = type;
	var _postion = position;

	switch( position ){
		case	"" 	 	 : _postion = "none"; 	break;
		case	"up" 	 : _postion = "fall"; 	break;
		case 	"bottom" : _postion = "ascend"; break;
		default 		 : _postion = "fade"; 	break;
	}

	switch( _type ){
		case	"danger" 	: _class = "ban"; 		break;
		case 	"info" 		: _class = "info"; 		break;
		case 	"warning" 	: _class = "warning"; 	break;
		case 	"success" 	: _class = "check"; 	break;
		default 		 	: _class = ""; 			break;
	}

	// console.log( $(document).find("ons-toast."._type).length );
    if( $(document).find("ons-toast." + _type).length == 0 ){
		ons.notification.toast('<i class="fa fa-' + _class + '"></i> ' +  message , { timeout: time, animation: _postion , 'class' : _type });
    }
}

function getToday(){
	var date = new Date();
	var year  = date.getFullYear();
	var month = date.getMonth() + 1; // 0부터 시작하므로 1더함 더함
	var day   = date.getDate();

	if (("" + month).length == 1) { month = "0" + month; }
	if (("" + day).length   == 1) { day   = "0" + day;   }
	return "" + year + "-" + month + "-" + day;  
}

// 페이지내의 작성내용 클리어
function setClearPage(pageId){
	
	var page = $("#" + pageId);
	var bEdit = page.find("input[name='bEdit']").val();
	
	if( bEdit == "Y" || bEdit == "AUTO"){

		if( bEdit == "Y" ){
			if( !confirm("작성내용을 유지하시겠습니까?") ){
				bEdit = "AUTO";
			}
		}

		if( bEdit == "AUTO" ){
			page.find("ons-input").val("");
			page.find("ons-input label.error").remove();
			page.find("ons-select label.error").remove();

			page.find("ons-select").css("background", "none");
			page.find("ons-input").css("background", "none");
			page.find("input[name='bEdit']").val("N");
			page.find(".dynamicDom").remove();
		}
	}
}

// 페이지내의 입력란 수정여부
function fnStartEditCheck(pageId){
	var page = $("#" + pageId);
	page.find("input[name='bEdit']").val("N");
	page.find("ons-input").one("change", function(){
		page.find("input[name='bEdit']").val("Y");
	});

	page.find("ons-select").one("change", function(){
		page.find("input[name='bEdit']").val("Y");
	});
}

// 입력컨트롤에 대한 입력길이 제한
function setLimtedByInput(event){
	var target = event.target;
	var text	= $(target).val();
	var max		= $(target).attr("max-length");
	$(target).val( $(target).val().substr(0, (max) ));
}

function getBankList(eleId){

	var objBank = $("#" + eleId);

	if( objBank.find("option").length == 0){

		objBank.empty();
		var service	= new tnbWSDL();
		var WSDL_URL =  service.WSDL_URL + "/getBankList";

		$.ajax({
			type: "POST",
			contentType: "application/json",
			dataType: "json",
			url : WSDL_URL,
			beforeSend : function(){
				$(".page__content ons-progress-bar.progressStyle").remove();
				$(".page__content").prepend("<ons-progress-bar class='progressStyle' indeterminate></ons-progress-bar>");
			},
			success :  function(json){
							
	            var jsonData    = $.parseJSON(json.d);
	            if (jsonData === undefined) {
	                ons.notification.alert('서비스 호출에 실패하였습니다.');
	                return;
	            }

	            $select = $("<select class='select-input select-input--material select-input--underbar'>");
	            $select.append(  $("<option>", {
		            value   : "" ,
		            text    : "- 선택 -"
		        	})
	        	);
	            $.each(jsonData, function(){
	            	
	            	$select.append(  $("<option>", {
			            value   : this.BANKNM ,
			            text    : this.BANKNM 
			        	})
	            	);
	            });

	            objBank.append($select);
	            
			},
			error: function(x, t, m) {
				if(t==="timeout") {
					runWarningMessage("요청시간이 만료되었습니다. 다시 시도하세요", "warning", 3000, "up");
				} else {
					//$(".msg").text(  t  + " " + m ).fadeIn();
				}
			},
			complete: function() {
				 $(".page__content .progressStyle").fadeOut("slow");
			}
		
		});
	}
}

function isNull(obj){
	return (typeof obj !="undefined" && obj != null && obj !="" && obj!="null") ? false:true;
}

function validateEmail(email) {
	email = $.trim(email);
	var re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
	return re.test(email);
}

function validUrl(url){
	url = $.trim(url);
	var re = /^http(s)?:\/\/(www\.)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/;
	return re.test(url)
}

function getfilterNumber(str){

	if(!isNull(str)){
		var regExp = /[^(0-9)]/gi;
		return str.replace(regExp, "");
	}

	return str;
}

function ValidatePassWord(value){
	var pw = value;
	var num = pw.search(/[0-9]/g);
	var eng = pw.search(/[a-z]/ig);
	//var spe = pw.search(/[`~!@@#$%^&*|₩₩₩'₩";:₩/?]/gi);
   
	if(pw.length < 6 && pw.length > 20){
		// alert("6자리 ~ 20자리 이내로 입력해주세요.");
   		return false;
	}
   
	if(pw.search(/₩s/) != -1){
	 	// alert("비밀번호는 공백업이 입력해주세요.");
	 	return false;
   
	} 
	
	if(num < 0 || eng < 0 ){
	 	// alert("영문,숫자를 혼합하여 입력해주세요.");
	 	return false;
	}
	
	/*
	if(num < 0 || eng < 0 || spe < 0 ){
	 	// alert("영문,숫자,특수문자를 혼합하여 입력해주세요.");
	 	return false;
	}
	*/
	return true;
}


function checkLogin(){
    var IS_LOGIN_YN = localStorage.getItem("IS_LOGIN_YN");
    var USER_ID     = localStorage.getItem("USER_ID");
    var ID          = localStorage.getItem("ID");
    
    if( isNull(ID) || isNull(USER_ID) || isNull(IS_LOGIN_YN)  ){
        document.getElementById('appTabbar').setActiveTab(4);
		runWarningMessage("로그인 후 사용해주세요", "info", 1500, "up");
        return false;
    }else{
        return true;
    }
}

function getDayName(number, _type){
	
	dayNm = "";
	switch(number){
		case 0 :  dayNm = getLang("일", "Sun", "Sun");break;
		case 1 :  dayNm = getLang("월", "Mon", "Mon");break;
		case 2 :  dayNm = getLang("화", "Tue", "Tue");break;
		case 3 :  dayNm = getLang("수", "Wed", "Wed");break;
		case 4 :  dayNm = getLang("목", "Thu", "Thu");break;
		case 5 :  dayNm = getLang("금", "Fri", "Fri");break;
		case 6 :  dayNm = getLang("토", "Sat", "Sat");break;
		default : 
			dayNm = "Error";
		break;
	}

	if( _type == 1){
		dayNm += "요일";
	}else if( _type == 2){
		dayNm = "(" + dayNm + ")";
	}
	
	return dayNm;
}

String.prototype.toText = function() {
	var str = this;
	return isNull(str) ? "" : str;
};

/**
 * 문자열이 빈 문자열인지 체크하여 결과값을 리턴한다.
 * @param str       : 체크할 문자열
 */
function isEmpty(str){
		
	if(typeof str == "undefined" || str == null || str == "")
		return true;
	else
		return false ;
}
	
/**
 * 문자열이 빈 문자열인지 체크하여 기본 문자열로 리턴한다.
 * @param str           : 체크할 문자열
 * @param defaultStr    : 문자열이 비어있을경우 리턴할 기본 문자열
 */
function nvl(str, defaultStr){
		
	if(typeof str == "undefined" || str == null || str == "")
		str = defaultStr ;
	return str ;
}

function removePs(div_id) {
	var html = '<p>';
	$('#' + div_id + ' p').each(function(i, p) {
		html += $(p).html() + '</br>';
	});
	html += '</p>';
	$('#' + div_id).html(html);
}

function getLang(ko, vn, en){
	var lang 	= localStorage.getItem("lang");
	var value 	= "";

	switch(lang){
		case "ko-KR"	:  value = ko; break;
		case "vn-VN"	:  value = vn; break;
		case "en-EN"	:  value = en; break;
		default : 
			value = ko;
	} 
	if( isNull(value) ){
		return "";
	}else{
		return value;
	}
}

function getLangNow(){
	var lang 	= localStorage.getItem("lang");
	return (isNull(lang) ? "" : lang);
}

function chkTelNum(){
	
	var lang 	= localStorage.getItem("lang");
	if( isNull(lang)){
		showLang();
	}else{
		return true;
	}
}

// 지역코드 체크
function chkAreaId(){
	// 1. 전화번호 체크
	var area_id 	= localStorage.getItem("AREA_ID");
	if( isNull(area_id) ){
		if( chkTelNum() ){
			myApp.navigator.pushPage('html/auth/user_location.html',  { animation: "none" , data: {} }); 
		}
	}
}

function hasArea(){
	var area_id 	= localStorage.getItem("AREA_ID");
	if( isNull(area_id)){
		return false;
	}else{
		return true;
	}
}

// 전화번호 등록체크
function showChkTel_Diag(){

	inputTel = localStorage.getItem("TEL");
	if( isNull(inputTel) ){
		showPrompt();                     
		return false;
	}else{
		return true;
	}
}

function isAbleNumber(tel){

	var _tel        = tel ; 
	var prevNumber  = _tel.substr(0, 2);
	if(prevNumber == "82" || prevNumber == "01" ){
		return true;
	}else{
		return false;
	}
}

function loadjscssfile(filename, filetype){

    if (filetype=="js"){ //if filename is a external JavaScript file
        var fileref=document.createElement('script');
        fileref.setAttribute("type","text/javascript");
        fileref.setAttribute("src", filename);
    }
	else if (filetype=="css"){ //if filename is an external CSS file
        var fileref=document.createElement("link");
        fileref.setAttribute("rel", "stylesheet");
        fileref.setAttribute("type", "text/css");
        fileref.setAttribute("href", filename);
	}
	
    if (typeof fileref!="undefined"){
		if ( document.querySelectorAll("head > link[href='" + filename + "']").length == 0 ){
			document.getElementsByTagName("head")[0].appendChild(fileref);
		}

		if ( document.querySelectorAll("head > script[src='" + filename + "']").length == 0 ){
			document.getElementsByTagName("head")[0].appendChild(fileref);
		}
	}
}

function removejscssfile(filename, filetype){
    var targetelement=(filetype=="js")? "script" : (filetype=="css")? "link" : "none" //determine element type to create nodelist from
    var targetattr=(filetype=="js")? "src" : (filetype=="css")? "href" : "none" //determine corresponding attribute to test for
    var allsuspects=document.getElementsByTagName(targetelement)
    for (var i=allsuspects.length; i>=0; i--){ //search backwards within nodelist for matching elements to remove
    if (allsuspects[i] && allsuspects[i].getAttribute(targetattr)!=null && allsuspects[i].getAttribute(targetattr).indexOf(filename)!=-1)
        allsuspects[i].parentNode.removeChild(allsuspects[i]) //remove element by calling parentNode.removeChild()
    }
}

function copyClipBoard (that){

	var t_copy 	= myApp.i18Next.getTrasTxt("SYS", "COPY");
	var inp =document.createElement('input');
	document.body.appendChild(inp)
	inp.value =that.textContent
	inp.select();
	document.execCommand('copy',false);
	inp.remove();

	runWarningMessage("'" + that.textContent + "' " + t_copy, "info", 1500, "up");
}


function getParam(originUrl, sname) {

    var params = originUrl.substr(originUrl.indexOf("?") + 1);

    var sval = "";

    params = params.split("&");

    for (var i = 0; i < params.length; i++) {

        temp = params[i].split("=");

        if ([temp[0]] == sname) { 
			sval = temp[1]; 
		}
    }

    return sval;

}