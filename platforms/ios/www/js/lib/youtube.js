var done = false;

// 4. The API will call this function when the video player is ready.
function onPlayerReady(event) {
    event.target.playVideo();
}

// 5. The API calls this function when the player's state changes.
//    The function indicates that when playing a video (state=1),
//    the player should play for six seconds and then stop.

function onPlayerStateChange(event) {
    if (event.data == YT.PlayerState.PLAYING && !done) {
        // setTimeout(stopVideo, 6000);
        stopVideo();
        done = true;
    }
}
function stopVideo() {

    $('.yt_player_iframe').each(function(){
        this.contentWindow.postMessage('{"event":"command","func":"stopVideo","args":""}', '*');
    });
}