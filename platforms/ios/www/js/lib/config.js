function updateTime() {
    $('.sysTime').html(moment().format('YYYY-MM-DD HH:mm:ss'));
}

function conf_sys_onMessage(clb){

    var option = { 
        area_id : localStorage.getItem("AREA_ID"),
        id      : localStorage.getItem("ID"),
    };

    var service = new withWSDL("api/config/list");

    // 상품목록 서비스 바인딩
    service.success = function(_jsonData) {

        var jsonData	= _jsonData;
        var objUseyn	= jsonData.list;
        var feeConfig   = jsonData.feeList;
        var compay_conf = jsonData.compay_conf;

        var diff        = 0;
        var message     = "";
        if( objUseyn.length > 0 ){
            
            var ori_st_time;
            var end_dt;

            $(objUseyn).each(function(){
                item_nm 	= this.ITEM_NM;
                item_class	= this.ITEM_CLASS;
                item_value 	= this.ITEM_VALUE;

                switch(item_nm){
                    case "MESSAGE" 	:
                        message     = getLang( this.ITEM_VALUE, this.ITEM_VALUE_VN, this.ITEM_VALUE_EN);
                        break;

                    case "START_TM" :
                        ori_st_time = moment(item_value);
                        break;

                    case "TIME" 	:
                        minuts 	    = parseInt(item_value);
                        break;
                }
            })

            // 중단 종료시간
            end_dt 	= ori_st_time.add(minuts, 'minutes');

            diff = end_dt.diff(moment(), 'minutes');
            
            if( diff > 0){
                // sysModal.show();
                APP_SYS_ON_OFF = false;
                end_dt = end_dt.format("YYYY-MM-DD HH:mm");

                runWarningMessage(message + "<div style='text-align:center;'> Available After " + end_dt + " </div>" , "warning", 5000, "up");
                $("#basketPage input[name='CONFIG_START']").val(end_dt);

            }else{
                APP_SYS_ON_OFF = true;
            }
        }else{
            APP_SYS_ON_OFF = true;
            $("#basketPage input[name='CONFIG_START']").val("");
        };

        if( !isNull(feeConfig)){

            var config      = feeConfig;
            var min_amt     = isNull(config.MIN_AMT)        ? MIN_ORDER_AMT : config.MIN_AMT;

            // 최소주문금액 (30만동)
            MIN_ORDER_AMT       = min_amt;
            MIN_ORDER_WORD 		= parseInt(MIN_ORDER_AMT/10000);
            
            if( config.USE_YN   == "Y"){

                var min_amt     = isNull(config.MIN_AMT)        ? MIN_ORDER_AMT : config.MIN_AMT;
                var dlv_fee     = isNull(config.BASE_FEE)       ? BASE_DELV_FEE : config.BASE_FEE;
                var distance    = isNull(config.BASE_DISTANCE)  ? BASE_DISTANCE : config.BASE_DISTANCE;
                var add_fee     = isNull(config.ADDED_FEE)      ? ADDED_FEE     : config.ADDED_FEE;
                var add_dist    = isNull(config.ADDED_DISTANCE) ? ADDED_DISTANCE: config.ADDED_DISTANCE;
                var free_amt    = isNull(config.FREE_AMT)       ? FREE_DELV_AMT : config.FREE_AMT;
                var wpay_rate   = isNull(config.USE_POINT_RATE) ? WITHU_PAY_RATE: config.USE_POINT_RATE;
                
                // 최소주문금액
                MIN_ORDER_AMT   = min_amt;

                // 기본배달금액
                BASE_DELV_FEE 	= dlv_fee;

                // 기본거리
                BASE_DISTANCE   = distance;

                // 추가 거리
                ADDED_DISTANCE  = add_dist;

                // 추가 요금 (추가거리별)
                ADDED_FEE		= add_fee;

                // 무료배송금액
                FREE_DELV_AMT   = free_amt;

                
                // 위드유페이 (사용비율)
                WITHU_PAY_RATE  = wpay_rate;
                // console.log(MIN_ORDER_AMT, BASE_DELV_FEE, BASE_DISTANCE, ADDED_DISTANCE, ADDED_FEE, FREE_DELV_AMT);
            }
        }

        if( !isNull(compay_conf) ){
            IS_PAY_CASH 		= compay_conf.IS_PAY_CASH;
            IS_PAY_CARD 		= compay_conf.IS_PAY_CARD;
            IS_PAY_EPAY 		= compay_conf.IS_PAY_EPAY;
            IS_PAY_POINT 		= compay_conf.IS_PAY_POINT;
            IS_USE_TAX 			= compay_conf.IS_USE_TAX;
        }

        if( typeof clb === 'function' ){
            clb();
        }
    }
    // 상품목록 서비스 호출
    service.ajaxGetData(option);
}