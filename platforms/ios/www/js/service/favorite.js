var service	= new withWSDL();

window.serviceModel = {


    listFavorite : function(page, data, domain, option, callFavoriteList){

        var diagfavoriteRemove= page.querySelector("#favoriteRemove-dialog");

        var serviceFavoriteRemove	= new withWSDL('api/favorite/list/' + 0 );
        var jsonData        = data.list;
        var favoriteCount     = jsonData.length;

        if( favoriteCount == 0 ) {
            //console.log(page.querySelector(".emptyOrNoData.card"));
        }

        if( option.CLEAR == 1){
            $(page).find(".imageList ons-list-item").remove();
        }

        var htmlData    = [];
        
        if( favoriteCount > 0){

            $(page).find(".after-list").css("display", "none");

            $(diagfavoriteRemove).find(".btnDialogRemoveFavoriteOk").off("click");

            $(page).find(".imageList .emptyOrNoData").remove();
            $(page).find(".imageList ons-list-item").remove();

            sumAll = 0;
            sumEtc = 0;

            var img = "", imgSrc = "", rmk = "";
            
            $(jsonData).each(function(){

                imgSrc      = domain + "\\" + this.IMG_URL_SM;
                url         = validUrl(imgSrc) ? imgSrc : '' ;

                rmk         = getLang(this.RMK, this.RMK_VN, this.RMK_EN);
                rmk         = isNull(rmk) ? "" : rmk;

                sale		= isNull(this.EVENT_DISC_RATE) ? "" : this.EVENT_DISC_RATE.toFixed(1);
                qtySum		= isNull(this.QTY_SUM) || this.QTY_SUM < 0 ?  0 : this.QTY_SUM;


                unit        = isNull(this.UNIT_NM) ? "EA" : this.UNIT_NM;

                eventAmt    = isNull(this.EVENT_AMT) ? 0 : this.EVENT_AMT;
                orAmt       = isNull(this.OUT_UNIT_PRICE) ? 0 : this.OUT_UNIT_PRICE;
                saleText    = "";

                is_pay      = isNull(this.IS_PAY_COIN) ? "N" : this.IS_PAY_COIN ;

                if( !isNull(eventAmt) ){
                    if( !isNaN( parseInt(eventAmt) * parseInt(this.QTY ) ) ){

                        // 서비스상품은 최소주문금액에서 포함안시킴
                        if( this.IS_CONTAIN_MIN_AMT != "Y"){
                            sumEtc	+= parseInt(eventAmt) * parseInt(this.QTY );
                        }else{
                            sumAll	+= parseInt(eventAmt) * parseInt(this.QTY );
                        }
                        //console.log(sumAll);
                    }
                }

                // 상품개별 최수구매수량
                minQty      = isNull(this.MIN_ORDER_QTY) ? 0 : this.MIN_ORDER_QTY;

                // 브랜드명
                brandNm     = getLang(this.P_CORP_NAME, this.P_CORP_NAME_VN, this.P_CORP_NAME_EN);
                brandNm     = isNull(brandNm) ? "" : brandNm;

                st_none     = qtySum == 0 ? "st_none" : "";
                st_serv     = this.IS_CONTAIN_MIN_AMT != "Y" ? "st_service" : "";

                // 조리여부
                cooking     = this.IS_COOKING == 'C_Y' || this.IS_COOKING == 'C_H' ? "<div class='cooking'>" + this.COOK_MSG + "</div>" : "";

                // 음식준비시간 
                readyTm     = isNull(this.PRODUCT_READY_TM) ? "" : this.PRODUCT_READY_TM;
                foodReady   = "";
                if( this.IS_FOOD == 'Y' && !isNull(readyTm) ){
                    foodReady   = "<div class='orderTime green'><i class='fa fa-clock-o'></i> 음식조리소요시간 " + readyTm + "분 </div>" ;
                }

                // 판매상태(New, 할인, Hot 등이 있을 경우 출력)
                if( !isNull(this.CD_NM)){
                    sale_state  = "<span class='overTip bRed'>" + this.CD_NM + "</span>" ;
                }else{
                    sale_state  = "";
                }

                new_product     = "";
                if( this.IS_NEW == "Y"){
                    new_product = "<span class='bRed new overTip'>NEW</span>";
                }

                dd1             = this.DD1;     // 판매시작 (현시간부로 지난시간) 가능시간은 판매시작시간보다 지나야함 (양수)
                dd2             = this.DD2;     // 판매종료 (현시간부로 지난시간) 가능시간은 판매종료시간보다 남아야함 (음수)
                sale_timeout    = "";

                if( dd1 >= 0 && dd2 <= 0){
                    sale_timeout = "sale_on";
                }else{
                    sale_timeout = "sale_off";
                }

                if( sale != ""){
                    saleText    =   "<del class='ori_price'>" + parseInt(orAmt).currency() + "</del>" +
                                    "<span class='sale'>" + sale + "<span class='unit'>%</span></span>" ;
                }

                ytbCode     = this.YOUTUBE_CODE;
                ytbUseYn    = this.YOUTUBE_USE_YN;
                ytbIcon     = ""; 

                if( !isNull(ytbCode) && ytbUseYn == "Y" ){
                    ytbIcon     = "ytb";
                }

                st_msg      = getLang("최소주문액미포함", "Tổng số không bao gồm", "Total not included");
                st_service  = this.IS_CONTAIN_MIN_AMT != "Y" ? "<div class='isServiceProduct green'>" + st_msg + "</div>" : "";

                isOff = window.serviceModel.isOnOffHolyCheck(this.HOLY_DAYS, this.ORDER_STATRT_TM, this.ORDER_END_TM);

                corp_nm     = "";
                // 검색화면/장바구니일 경우 회사명 표기
                if( !isNull(option.isShowCorp) && option.isShowCorp ){
                    corp_nm     = getLang(this.CORP_NM, this.CORP_NM_VN, this.CORP_NM_EN);
                    corp_nm     = "<span class='gray corpnm'>" + corp_nm + "</span>";
                }

                p_name          = getLang(this.PRODUCT_NM, this.PRODUCT_NM_VN, this.PRODUCT_NM_EN);

                htmlData.push("<ons-list-item tappable data-minqty=" + minQty + " data-idx='" + this.IDX + "' data-product_cd='"+ this.PRODUCT_CD );
                htmlData.push("' data-ispay='" + is_pay + "' data-corp_cd='" + this.CORP_ID + "' data-idx='" + this.IDX + "' data-cate='" + this.P_CLS_IDX );
                htmlData.push("' data-p_corp_id='" +  this.PRODUCT_CORP_ID + "' class='listItem " + isOff + " " + st_serv + " " + st_none + " " + sale_timeout + "' >");
                htmlData.push("   <div class='left productImg " + ytbIcon + "' data-product_cd='"+ this.PRODUCT_CD +"' data-product_cate='" + this.P_CLS_IDX + "'>");
                htmlData.push("       <img class='list-item__thumbnail " + st_none+ "' src='"+ imgSrc +"' >" + new_product );
                htmlData.push("   </div>");
                htmlData.push("   <div class='center productInfo' data-product_cd='"+ this.REF_PRODUCT_CD +"' data-price='" + this.EVENT_AMT + "'>");

                htmlData.push("       <div class='list-item__title'>" + corp_nm  + "<span class='brand gray'>" + brandNm + "</span>");
                htmlData.push("           <span class='productName'>" + p_name + "</span>");
                htmlData.push(            sale_state);
                htmlData.push("       </div>");
                htmlData.push("       <div class='list-item__subtitle'>" + rmk + "</div>");
                htmlData.push("       <div class='pd_price' data-price='" + eventAmt + "'>");
                htmlData.push("			<span class='cp'>" + parseInt(eventAmt).currency() + "</span>");
                htmlData.push(            saleText);
                htmlData.push("       </div>");
                htmlData.push("       <div class='pd_etc'><span class='star'>" );
                htmlData.push("           <i class='fa fa-star'></i>" + this.STAR + "</span>");
                htmlData.push("           <span style='display:none;' class='stock' data-stock='" + qtySum + "'>재고수량 : " + Number(qtySum).format()  + "" + unit + " </span>" );
                htmlData.push("       </div>");
                htmlData.push(         cooking);
                htmlData.push(        st_service );
                htmlData.push(         foodReady );
				htmlData.push("	</div>");
				htmlData.push("	<div class='buttonArea right'>");
                htmlData.push("       <div class='list-item__subtitle'>");
                htmlData.push("           <ons-button class='btnRadius btnPlusQty' data-minqty=" + minQty + "></ons-button>");
                htmlData.push("       </div> ");
                
                htmlData.push("       <ons-button class='btnRmFavorite' data-name='" + p_name + "' data-idx='"+ this.IDX + "'><i class='fa fa-trash-o'></i>&nbsp;</ons-button>");
                htmlData.push("   </div>");
                htmlData.push("</ons-list-item>");
            });

        }else{
            if( $(page).find(".imageList .emptyOrNoData").length == 0 ){
                htmlData.push("<div class='emptyOrNoData card'><i><img src='img/svg/caution.svg'></i><span data-i18n='COMM:noMoreData'> 즐겨찾기에 등록된 상품이 없습니다. <span></div>");
            }
            $(page).find(".after-list").css("display", "none");
        }

      

        $(page).find(".imageList").append(htmlData.join(''));

        // 장바구니 삭제 여부 다이어로그 닫기
        $(diagfavoriteRemove).find(".btnDialogCancel").click(function(){
            diagfavoriteRemove.hide();
        });

        // 장바구니 삭제 여부 삭제 실행

        //$(diagfavoriteRemove).find(".btnDialogRemoveFavoriteOk").onclick = null;
        $(diagfavoriteRemove).find(".btnDialogRemoveFavoriteOk").on("click", function(){

            var idx = $(this).attr("data-idx");
            var option      = {
                idx : idx
            };
            serviceFavoriteRemove.ajaxDeleteData(option);
        });

        // 장바구니 삭제 성공 콜백
        serviceFavoriteRemove.success = function(_jsonData){

            var jsonData    = _jsonData;
            var data        = jsonData;
            // console.log(data.status);
            if(data.status == 200 ){
                runWarningMessage(data.message, "success", 1500, "up");
                diagfavoriteRemove.hide();

                // 필추가해야함 : click 이벤트 재귀호출 방지
                $(diagfavoriteRemove).find(".btnDialogRemoveFavoriteOk").off('click');

                // 장바구니 조회
                callFavoriteList();

            }else{
                runWarningMessage(data.message, "danger", 1500, "up");
            }
        };

        // 장바구니 삭제 버튼 => 다이어로그 활성화 (show)
        Array.prototype.forEach.call(page.querySelectorAll('.btnRmFavorite'), function(element) {
            element.onclick = function(target) {
                var idx     = $(this).data("idx");
                var name    = $(this).data("name");

                $(diagfavoriteRemove).find(".btnDialogRemoveFavoriteOk").attr("data-idx", idx);
                $(diagfavoriteRemove).find(".btnDialogRemoveFavoriteOk").attr("data-name", name);
                //alert(productCd);
                diagfavoriteRemove.show();
            }
        });

        // 버튼 클릭 증감 이벤트
        Array.prototype.forEach.call(page.querySelectorAll('.btnRadius'), function(element) {

            element.onclick = function(target) {

                var isAdult = $(element).parents('.listItem').data("adult");
                var t_sold  = myApp.i18Next.getTrasTxt("ORD", "SOLD_PRD");

                // 성인용 이상 상품 체크
                if( isAdult == "Y"){
                    if( IS_ADULT_AGREE  == false ){
                        adultDiag.show();
                        return false;
                    }
                }
                
                // 재고없음
                if( $(element).hasClass("st_none") ){
                    runWarningMessage(t_sold, "warning", 1000, "up");
                    return false;
                }

                $(element).prop("disabled", true);

                minQty      = $(this).data("minqty");
                corpCd      = $(element).parents('.listItem').data("p_corp_id");
                productCd   = $(this).parents(".listItem").find(".productInfo").data("product_cd");
                price		= $(this).parents('.listItem').find('.productInfo .pd_price').data("price");
                qty         = 1;

                isDetail    = false;
                stockQty    = $(this).parents(".listItem").find(".productInfo .stock").data("stock");

                if( isNull(stockQty) || stockQty <= 0 ){
                    runWarningMessage(t_sold, "warning", 1000, "up");
                    return false;
                }

                objSummaryQty   = $(".summaryQty");
                summaryQty      = parseInt(objSummaryQty.html());
                domQty          = $(element).parents(".buttonArea").find("ons-input");
                inputQty        = parseInt( domQty.val() );

                var t_min_ord_qty   = myApp.i18Next.getTrasTxt("BSK", "MIN_ORD_QTY");

                if( $(element).hasClass("btnMinusQty") ){
                    
                    // 최소주문수량과 일치할경우
                    if( inputQty == minQty && inputQty != 0){
                        runWarningMessage(t_min_ord_qty + " : " + minQty , "warning", 1500, "fade");
                        inputQty = 0;
                        summaryQty -= minQty;
                        qty         = minQty;
                        isEnable    = true;
                    }else{

                        if( inputQty < 0 ){
                            inputQty    = 0;
                            isEnable    = false;
                        }else{
                            if( inputQty > 0 ){
                                inputQty--;
                                summaryQty--;
                                isEnable    = true;

                            }else{
                                inputQty = 0;
                            }
                        }
                    }

                    objSummaryQty.html(summaryQty);
                    domQty.val( inputQty );
                    qty = qty * (-1);

                }else{

                    // 최소주문수량이 있을경우
                    if( minQty != 0 && inputQty == 0){
                        runWarningMessage(t_min_ord_qty + " : " + minQty , "warning", 1500, "fade");
                        inputQty    += minQty;
                        summaryQty  += inputQty;
                        qty         = minQty;
                    }else{
                        inputQty++;
                        summaryQty++;
                    }

                    objSummaryQty.html(summaryQty);
                    domQty.val( inputQty );
                }

                var data = {
                    ID					: localStorage.getItem("ID"),
                    TEL					: localStorage.getItem("TEL"),
                    PRODUCT_CORP_ID     : corpCd,
                    REF_PRODUCT_CD      : productCd,
                    QTY                 : qty,
                    now_qty             : inputQty,
                }

                // myApp.basket.addObjectItemFv(data);
                myApp.basket.addObjectItem(data);

                setTimeout(function(){
                    $(element).prop("disabled", false);
                }, 300)
            };
        });
    },

    // 주문버튼 활성화체크 메서드
    checkOrderButton : function(page){

        var objsumAll = $(page).find(".sumAll");
        var sumResult = !isNaN( getfilterNumber(objsumAll.html())) ? getfilterNumber(objsumAll.html()) : 0;
//        console.log(sumResult);

        var nStNone   = $(page).find("ons-list-item.st_none").length;
        var nHoly     = $(page).find("ons-list-item.off").length;
        var ntmOut    = $(page).find("ons-list-item.sale_off").length;

        var t_soldout   = myApp.i18Next.getTrasTxt("BSK", "WARN_SOLD_OUT");
        var t_closed    = myApp.i18Next.getTrasTxt("BSK", "WARN_CLOSED");
        var t_timeout   = myApp.i18Next.getTrasTxt("BSK", "WARN_TIMEOUT");
        var t_min_ord_qty   = myApp.i18Next.getTrasTxt("BSK", "MIN_ORD_QTY");

        if( nStNone > 0 ){
            runWarningMessage(t_soldout , "warning", 2000, "up");
        }

        if( nHoly > 0 ){
            runWarningMessage(t_closed , "warning", 2000, "up");
        }

        if( ntmOut > 0){
            runWarningMessage(t_timeout , "warning", 2000, "up");
        }

        var alertMsg    = $("<div style='text-align:right'></div>");
        var minQtyCnt   = 0;
        $(page).find("ons-list-item").each(function(){
            
            eachMinQty  = $(this).attr("data-minqty");
            qty         = $(this).find("ons-input.qty").val();
            pname       = $(this).find(".productName").text();
            
            // console.log(eachMinQty, qty, pname);
            if( eachMinQty > qty){
                minQtyCnt++;
                alertMsg.append( pname + " " + t_min_ord_qty + " : <span class='red'>" + eachMinQty + "</span> EA <br/>" );
            }
        })

    },

    setProductFavorite : function(page, favoriteData){

        // myApp.favorite.setItem("favorite", product);
        window.serviceModel.checkOrderButton(page);

        var data = {
            ID					: localStorage.getItem("ID"),
            TEL                 : localStorage.getItem("TEL"),
            PRODUCT_CORP_ID     : favoriteData.p_corp_id,
            REF_PRODUCT_CD      : favoriteData.productCd,
            QTY                 : favoriteData.qty,
            NOW_QTY             : favoriteData.now_qty
        }

        if( favoriteData.isEnable ){
            myApp.favorite.addObjectItem(data);
            // 모든 장바구니 카운터 동기화

            if( !isNull(favoriteData.summaryQty)){
                myApp.commonController.syncFavoriteQty(favoriteData.summaryQty);
            }
            return true;
        }else{
            return false;
        }
    }, 

    isOnOffHolyCheck : function(holy, sTm, eTm){

        result      = "";
        ostart      = isNull(sTm) && sTm.length > 5 ? "-" : sTm.substr(0, 5);
        estart      = isNull(eTm) && eTm.length    > 5 ? "-" : eTm.substr(0, 5);
        
        // 현재시각
        nowTime     = moment();

        // 영업시작시각
        stime       = moment(ostart, 'HH:mm');
        stTime      = moment()
        stTime.set({
            hour    : stime.get('hour'),
            minute  : stime.get('minute'),
        })

        // 영업종료시각
        etime       = moment(estart, 'HH:mm');
        etTime      = moment()
        etTime.set({
            hour    : etime.get('hour'),
            minute  : etime.get('minute'),
        })

        // 앞시간이 뒷시간보다 더 클 경우 다음날을 더해야함
        // - 가 나오면 뒷날에 하루를 더해준다
        daysDiff   = etTime.diff(stTime, 'minutes');

        holyDays   = holy;
        isHolyDay  = false;

        // 오늘 요일(1~7)
        todayNumber = moment().isoWeekday();
        
        // console.log(todayNumber);
        // console.log(holyDays);
        if( !isNull(holyDays) && holyDays.length > 0 ){
            var arrHoly = holyDays.replace(/'/g, '').split(",");  
            
            for(var i=0; i<=arrHoly.length; i++){

                nHoly   = parseInt(arrHoly[i]);
                //console.log(nHoly);

                if( todayNumber == nHoly){
                    isHolyDay = true;
                }
            }
        }
        
        //console.log(isHolyDay);
        if( daysDiff <= 0){
            etTime.add(1, 'days');
        }

        //console.log(stTime, etTime);
        sdiff       = nowTime.diff(stTime, "minutes");  // 현재시간보다 시작시간이 + 일 경우 주문못함
        ediff       = nowTime.diff(etTime, "minutes");  // 현재시간보다 

        // 시작시간이 지나고
        // 종료시간이 음수일 경우에 영업운영 
        // 쉬는날이 아닐경우
        if( ( sdiff >= 0 && ediff <= 0 ) && !isHolyDay){
            result = "";
        }else{
            result =  "off";
        }

        // 재검증 : 자정 이후면 한번 더 체크해야함
        // 마감날짜 불러오기
        etime2       = moment(estart, 'HH:mm');
        etTime2      = moment()
        etTime2.set({
            hour    : etime2.get('hour'),
            minute  : etime2.get('minute'),
        })

        newDiff = nowTime.diff(etTime2, "minutes"); 
        // console.log("etTime2", etTime2, newDiff);

        if( newDiff <= 0 && daysDiff <= 0 && !isHolyDay){
            result = "";
        }

        return result;
    }
}
