var service	= new withWSDL();

window.serviceModel = {

    login : function(page, data ){

        var jsonData    = data.user;
        var jsonDataM   = data.muser;

        var ErrorMsg    = data.Message;
        var hasError    = data.result;
        
        if( hasError.indexOf("success") >= 0 && Object.keys(jsonData).length !== 0 ){
            
            // null 값 체크 해서 공백으로 치환
            window.localStorage.setItem("IS_LOGIN_YN", "Y");
            
            window.localStorage.setItem("USER_ID",  jsonData.user_id);      // 사용자 아이디(KEY)
            window.localStorage.setItem("ID",       jsonData.id);           // 사용자 아이디
            window.localStorage.setItem("USER_NM",  jsonData.name);         // 사용자명
            window.localStorage.setItem("EMAIL",    jsonData.email);        // 이메일
            window.localStorage.setItem("TEL",      jsonData.phone_number); // 전화번호
            window.localStorage.setItem("BRANCH_ID",jsonDataM.BRANCH_ID);   // 지점코드
            window.localStorage.setItem("ADDR1",    jsonDataM.ADDR1);       // 주소1
            window.localStorage.setItem("ADDR2",    jsonDataM.ADDR2);       // 주소2

            window.localStorage.setItem("AREA_ID",  jsonDataM.area_id);       // AREA_ID
            window.localStorage.setItem("AREA_NM",  jsonDataM.AREA_NM);       // AREA_ID
            window.localStorage.setItem("CNTNT_ID", jsonDataM.CNTNT_ID);       // AREA_ID
            window.localStorage.setItem("NATION_ID",jsonDataM.NATION_ID);       // AREA_ID

            // window.localStorage.setItem("JOIN_DT",  jsonDataM.JOIN_DT);     // 가입일시
            window.localStorage.setItem("joinMemberData", null);            // 회원가입 정보

            localStorage.setItem("MY_COIN", Number(WITHU_PAY));
            WITHU_PAY = jsonDataM.COIN_CNT;

            // 아이디& 비밀번호 로그인 저장 설정
            // var isSaveIdPw = page.querySelector('#isSaveIdPw');
            // if( isSaveIdPw.checked ){
            //     window.localStorage.setItem("IS_SAVE_IDPW", true);
            //     window.localStorage.setItem("USER_ID", UserInfo.USER_ID);        // 로그인 아이디
            //     window.localStorage.setItem("USER_PW", user_pass );              // 로그인 비밀번호
            // }else{
            //     window.localStorage.setItem("IS_SAVE_IDPW", false);
            //     window.localStorage.setItem("USER_ID", "");
            //     window.localStorage.setItem("USER_PW" , "");
            // }

            // 자동로그인 설정
            // var isAutoLogin = document.getElementById('isAutoLogin');
            // if( isAutoLogin.checked ){
            //     window.localStorage.setItem("IS_AUTO_LOGIN", true);
            //     window.localStorage.setItem("LOGIN_ID", UserInfo.AGENT_ID);         // 로그인 아이디
            //     window.localStorage.setItem("USER_PW", UserInfo.LOGINPW);           // 로그인 비밀번호
            // }else{
            //     window.localStorage.setItem("IS_AUTO_LOGIN", false);
            // }
            IS_CHANGE_AREA = true;
            window.location.href = "index.html";
            
        }else{
            window.localStorage.setItem("IS_LOGIN_YN", "N");
            runWarningMessage("아이디와 비밀번호를 다시 한번 확인 후 다시 로그인해주세요", "danger", 2000, "up");
            page.querySelector("ons-input[name='PASS_WD']").focus();
        }
    },

    
    // 사용자 정보 조회
    userInfo : function(page, data ){
        
        
        var jsonData    = data.data;
        
        if( Object.keys(jsonData).length !== 0 ){
            
            WITHU_PAY = jsonData.COIN_CNT;

            var btnUpdateNick   = $(page).find("#btnSave");
            var btnChangePw     = $(page).find(".btnChangePw");
            var btnLogout       = $(page).find("#btnLogout");
            var btnOffService   = $(page).find("#btnOffService");
            var objsListSnsNo   = $(page).find(".item_sns_none");

            // 메인화면의 적립금
            $(".coinArea").show();
            $(".myCoin").html( Number(WITHU_PAY).format() );

            $(page).find("#coinQty").html( Number(WITHU_PAY).format() );
            $(page).find("#refferor").val( jsonData.REF_ID );
            $(page).find("#phone_number").val( jsonData.phone_number );

            if( !isNull(jsonData.REF_ID) && jsonData.REF_ID != "" && jsonData.REF_ID != 0) {
                $(page).find("#refferor").prop("readonly", true);
                $(page).find("#refferor input").prop("readonly", true);
                $(page).find("#btnReferer").prop("disabled", true);
            }

            var area_nm     = getLang(jsonData.AREA_NM, jsonData.AREA_NM_VN, jsonData.AREA_EN);
            var nation_nm   = getLang(jsonData.NATION_NM, jsonData.NATION_NM_VN, jsonData.NATION_NM_EN);

            $(page).find(".cityName").html(nation_nm + " " +  area_nm);

            // sns 로그인이 되어있을 경우
            var sns_type    = localStorage.getItem("SNS_TYPE");
            
            // sns_type = "KAKAO"; 
            if( !isNull( sns_type ) ){

                nick    = localStorage.getItem("NICK_NM");
                localStorage.setItem("USER_NM", nick);

                $(page).find("#email").val( localStorage.getItem("EMAIL") );
                $(page).find("#name").val( localStorage.getItem("USER_NM") );
                $(page).find("#name").attr("readonly", "readonly");
                $(page).find("#user_id").val( localStorage.getItem("USER_ID")  );
                btnUpdateNick.attr("disabled", "disabled").hide();
                btnChangePw.attr("disabled", "disabled").hide();
                // $(page).find("#user_id").hide();
                objsListSnsNo.hide();

                if( sns_type == "KAKAO" && btnLogout.find("img").length == 0 ){
                    btnLogout.prepend("<img width='20px' src='" + "img/svg/kakaotalk.svg"+ "' />&nbsp;");
                    btnLogout.css({
                        "background" : "#ffe812",
                        "color" : "	#000000",
                    })

                    btnOffService.prepend("<img width='20px' src='" + "img/svg/kakaotalk.svg"+ "' />&nbsp;");
                    btnOffService.css({
                        "background" : "#ffe812",
                        "color" : "	#000000",
                    })
                }else if( sns_type == "NAVER" && btnLogout.find("img").length == 0 ){
                    btnLogout.prepend("<img width='20px' src='" + "img/svg/_naver-logo1.png"+ "' />&nbsp;");
                    btnLogout.css({
                        "background" : "#2DB400",
                        "color" : "	#ffffff",
                    });

                    btnOffService.prepend("<img width='20px' src='" + "img/svg/_naver-logo1.png"+ "' />&nbsp;");
                    btnOffService.css({
                        "background" : "#2DB400",
                        "color" : "	#ffffff",
                    })
                }else if( sns_type == "APPLE" && btnLogout.find("img").length == 0 ){
                    btnOffService.hide({"animation" : "none"});
                }

                $(page).find(".cordovaButton").show();
                

            }else{
                $(page).find("#user_id").val( jsonData.user_id );
                $(page).find("#email").val( jsonData.email );
                $(page).find("#name").val( jsonData.name );

                $(page).find(".cordovaButton").hide();
            }
            
        }else{
            ons.notification.toast({ message: "데이터 조회 실패 " + ErrorMsg, timeout: 1000 });   
        }
          
    },

    // 코인획득 및 사용내역 목록 
    coinInfoList : function(page, data, option ){

        var jsonData    = data.list;
        var SummaryQty  = 0;
		
        var htmlData    = "";
        var cls         = "";
        var rmk			= "";
		var div			= "";
        var emptyMsg    = "<div class='emptyOrNoData card'><i><img src='img/svg/caution.svg'></i><span data-i18n='COMM:noMoreData'>더이상 데이터가 존재 하지 않습니다<span></div>";           

		if( option.new == true){
            $(page).find(".dataList").empty();
            $(page).find(".after-list").show();
		}

        if( jsonData.length > 0){

            // 현재코인 가능수량
            SummaryQty  = data.list[0].SUM_COIN;
            page.querySelector("#allQTY").value = Number(SummaryQty).format();

            var inText  = myApp.i18Next.getTrasTxt("COIN", "IN");
            var outText = myApp.i18Next.getTrasTxt("COIN", "OUT");

            $(jsonData).each(function(){

                if( !isNull(this.IS_INOUT)) {
                    rmk			= isNull(this.RMK) ? "" : this.RMK;
                    if( this.IS_INOUT == "O" ){
                        div	= outText;
                        cls = "blue" ;    
                    }else{
                        div	= inText;
                        cls = "red" ;
                    }
                    
                    htmlData += "<div class='card coinListItem noticeListItem listItem '>" +
                        "        <div class='left title' data-idx='" + this.IDX + "'>" +
                        "		    <span class='listContent ellipsis'>" + div  + "</span>" +
                        "		    <span class='listContent ellipsis'>" + this.IO_DT + "<br>" + rmk  + "</span>" +
                        "		    <span class='listContent ellipsis aRight " + cls + "'>" + (this.QTY).format() + "</span>" +
                        "        </div>" +
                        "</div>";
                } 
            });

            //console.log(htmlData);
            $(page).find(".dataList").append(htmlData);

            if( jsonData.length < 10){
                htmlData = emptyMsg;
                $(page).find(".dataList").append(htmlData);
                $(page).find(".after-list").hide();
            }

        }else{
            if( $(page).find(".dataList").find(".emptyOrNoData").length == 0){
                htmlData = emptyMsg;           
            }

            $(page).find(".dataList").append(htmlData);
            $(page).find(".after-list").hide();
        }
          
    },

    chkEmail : function(data){

        if( data === null || data === 'undefined'){
            return false;
        }

        var jsonData    = data.ResultDataSet.TB_CUSTOMER;
        // console.log(jsonData);
        var ErrorMsg    = data.Message;
        var hasError   = data.hasError;

        if( !hasError && Object.keys(jsonData).length !== 0 ){
            return true;
        }else{
            return false;
        }
    },

    loginKaKao : function(page, data, kakao ){

        var jsonData    = data.user;
        var jsonDataM   = data.muser;
        var sns_result  = data.result_sns;

        // console.log(jsonData);
        var ErrorMsg    = data.Message;
        var hasError    = data.result;
        
        if( hasError.indexOf("success") >= 0 && Object.keys(jsonData).length !== 0 ){
            
            // null 값 체크 해서 공백으로 치환
            localStorage.setItem("IS_LOGIN_YN", "Y");
            
            localStorage.setItem("USER_ID",  jsonData.user_id);      // 사용자 아이디(KEY)
            localStorage.setItem("ID",       jsonData.id);           // 사용자 아이디
            localStorage.setItem("USER_NM",  jsonData.name);         // 사용자명
            localStorage.setItem("EMAIL",    jsonData.email);        // 이메일
            localStorage.setItem("TEL",      jsonData.phone_number); // 전화번호
            
            localStorage.setItem("BRANCH_ID",jsonDataM.BRANCH_ID);   // 지점코드
            localStorage.setItem("ADDR1",    jsonDataM.ADDR1);       // 주소1
            localStorage.setItem("ADDR2",    jsonDataM.ADDR2);       // 주소2
            localStorage.setItem("MY_COIN", Number(WITHU_PAY));
            localStorage.setItem("joinMemberData", null);            // 회원가입 정보

            if( sns_result == "success"){
                localStorage.setItem("SNS_TYPE",   kakao.sns_type ); 
                localStorage.setItem("PROFILE_IMG",kakao.p_image ); 
                localStorage.setItem("NICK_NM",    kakao.nickname ); 
                localStorage.setItem("EMAIL",      kakao.email );

                // 연령대가 비공개일경우 구분
                if( kakao.has_age_range ){
                    localStorage.setItem("AGE_RANGE",  kakao.age_range);
                }else{
                    localStorage.setItem("AGE_RANGE",  null);
                }
            }

            location.href = "index.html";
            
        }else{
            var t_chk_agin  = myApp.i18Next.getTrasTxt("AUTH", "CHK_AGAIN");
            window.localStorage.setItem("IS_LOGIN_YN", "N");
            runWarningMessage(t_chk_agin, "warning", 2000, "up");
            page.querySelector("ons-input[name='PASS_WD']").focus();
        }
    },

    loginNaver : function(page, data, naver ){

        var jsonData    = data.user;
        var jsonDataM   = data.muser;
        var sns_result  = data.result_sns;

        // console.log(jsonData);
        var ErrorMsg    = data.Message;
        var hasError    = data.result;
        
        if( hasError.indexOf("success") >= 0 && Object.keys(jsonData).length !== 0 ){
            
            // null 값 체크 해서 공백으로 치환
            localStorage.setItem("IS_LOGIN_YN", "Y");
            
            localStorage.setItem("USER_ID",  jsonData.user_id);      // 사용자 아이디(KEY)
            localStorage.setItem("ID",       jsonData.id);           // 사용자 아이디
            localStorage.setItem("USER_NM",  jsonData.name);         // 사용자명
            localStorage.setItem("EMAIL",    jsonData.email);        // 이메일
            localStorage.setItem("TEL",      jsonData.phone_number); // 전화번호
            
            localStorage.setItem("BRANCH_ID",jsonDataM.BRANCH_ID);   // 지점코드
            localStorage.setItem("ADDR1",    jsonDataM.ADDR1);       // 주소1
            localStorage.setItem("ADDR2",    jsonDataM.ADDR2);       // 주소2
            localStorage.setItem("MY_COIN", Number(WITHU_PAY));
            localStorage.setItem("joinMemberData", null);            // 회원가입 정보

            if( sns_result == "success"){
                localStorage.setItem("SNS_TYPE",   naver.sns_type ); 
                localStorage.setItem("PROFILE_IMG",naver.p_image ); 
                localStorage.setItem("NICK_NM",    naver.nickname ); 
                localStorage.setItem("EMAIL",      naver.email );
                localStorage.setItem("AGE_RANGE",  naver.age_range);
            }

            location.href = "index.html";
            
        }else{
            var t_chk_agin  = myApp.i18Next.getTrasTxt("AUTH", "CHK_AGAIN");
            window.localStorage.setItem("IS_LOGIN_YN", "N");
            runWarningMessage(t_chk_agin, "warning", 2000, "up");
            page.querySelector("ons-input[name='PASS_WD']").focus();
        }
    },

    loginApple : function(page, data, apple ){

        var jsonData    = data.user;
        var jsonDataM   = data.muser;
        var sns_result  = data.result_sns;
        var jsonDataSns = data.user_sns;

        // console.log(jsonData);
        var ErrorMsg    = data.Message;
        var hasError    = data.result;
        
        if( hasError.indexOf("success") >= 0 && Object.keys(jsonData).length !== 0 ){
            
            // null 값 체크 해서 공백으로 치환
            localStorage.setItem("IS_LOGIN_YN", "Y");
            
            localStorage.setItem("USER_ID",  jsonData.user_id);      // 사용자 아이디(KEY)
            localStorage.setItem("ID",       jsonData.id);           // 사용자 아이디
            localStorage.setItem("USER_NM",  jsonData.name);         // 사용자명
            localStorage.setItem("EMAIL",    jsonData.email);        // 이메일
            localStorage.setItem("TEL",      jsonData.phone_number); // 전화번호
            
            localStorage.setItem("BRANCH_ID",jsonDataM.BRANCH_ID);   // 지점코드
            localStorage.setItem("ADDR1",    jsonDataM.ADDR1);       // 주소1
            localStorage.setItem("ADDR2",    jsonDataM.ADDR2);       // 주소2
            localStorage.setItem("MY_COIN", Number(WITHU_PAY));
            localStorage.setItem("joinMemberData", null);            // 회원가입 정보

            if( sns_result == "success"){

                localStorage.setItem("SNS_TYPE",   apple.sns_type ); 
                localStorage.setItem("PROFILE_IMG",apple.p_image ); 

                var nick = jsonDataSns.NICK_NAME;
                var email= jsonDataSns.SNS_EMAIL;

                if( nick == "null" || isNull(nick)){
                    nick = "";
                }

                if(email == "" || isNull(email)){
                    email = "";
                }
                localStorage.setItem("NICK_NM",    nick ); 
                localStorage.setItem("EMAIL",      email );

                // 연령대가 비공개일경우 구분
                if( apple.has_age_range ){
                    localStorage.setItem("AGE_RANGE",  apple.age_range);
                }else{
                    localStorage.setItem("AGE_RANGE",  null);
                }
            }

            location.href = "index.html";
            
        }else{
            var t_chk_agin  = myApp.i18Next.getTrasTxt("AUTH", "CHK_AGAIN");
            window.localStorage.setItem("IS_LOGIN_YN", "N");
            runWarningMessage(t_chk_agin, "warning", 2000, "up");
            page.querySelector("ons-input[name='PASS_WD']").focus();
        }
    }

}
