window.serviceModel = {

    setCategoryInProduct : function(page, data, corpData, holyData, domain, option, nth_cate){

        var jsonData        = data;
        var corpData        = corpData;
        var swiper_lnb      = null;
        var cateLenth       = 0;
        var selSlideNo      = nvl(nth_cate, 0);
        var youtubeHtml     = [];

        if( isNull(jsonData) ){
            cateLenth = 0;
        }else{
            cateLenth = jsonData.length;    
        }
        
        if( cateLenth == 0 ) {
            $(page).find(".CateMenu").empty();
        }

        // 아무조건없이 바인딩(카테고리 화면)
        if( option === null){
            // console.log(jsonData);
            var htmlData        = [];
            if( cateLenth > 0){
                // $(page).find(".slideMenu .card").off("click");
                $(jsonData).each(function(i){

                    isEnable    = this.USE_YN ? "enalble" : "btnDisabled";
                    corpCd      = this.CORP_ID;
                    branch      = this.BRANCHS;
                    corpNm      = this.CORP_NM;
                    cateCd      = this.P_CLS_IDX;
                    cateNm      = getLang(this.PROD_CLS_NAME, this.PROD_CLS_NAME_VN, this.PROD_CLS_NAME_EN);
                    imgSrc      = domain + "//" + this.IMG_URL_MD;
                    
                    ytbCode     = this.YOUTUBE_CODE;
                    youtubeUseYn= this.YOUTUBE_USE_YN;
                    ytbIcon     = ""; 

                    if( !isNull(ytbCode) && youtubeUseYn == "Y" ){
                        ytbIcon     = "ytb";
                    }

                    new_product     = "";
                    if( this.IS_NEW != "N"){

                        if( this.IS_NEW == "Y"){
                            new_product = "<span class='bRed new overTip'>" + "NEW" + "</span>";    
                        }else{
                            new_product = "<span class='bRed new overTip'>" + this.IS_NEW + "</span>";
                        }
                    }
                    
                    htmlData.push("<div class='card ispay btnCateMenu " + isEnable + " " + ytbIcon + "' data-cate_code='" + cateCd + "' data-corp_id='" + corpCd + "' data-nth=" + i + ">");
                    htmlData.push("     <span><img src='" + imgSrc + "' /></span>" + new_product );
                    htmlData.push("     <div class='cateName'>" + cateNm + "</div>");
                    htmlData.push("</div>");
                });

            }else{
                $(page).find(".after-list").css("display", "none");

				if( $(page).find(".imageList .emptyOrNoData").length == 0 ){
                    htmlData.push("<div class='emptyOrNoData card'><i><img src='img/svg/caution.svg'></i><span data-i18n='COMM:noMoreData'> 더이상 등록된 상품이 없습니다. <span></div>");
				}
            }

            $(page).find(".CateMenu").html(htmlData.join(''));

        } else {
            // 상품카테고리가 상단메뉴일때 바인딩
            if( jsonData.length > 0){
                $(option.root_class).each(function(index, item){
                    var root        = ".lnb ." + item;
                    var child       = option.child_clsss[index];
                    var htmlData    = [];
                    var corpCd      = "";
                    var branch      = "";
                    // 상단메뉴

                    $(jsonData).each(function(i){

                        isEnable    = this.IS_PROVE_CP == "Y" ? "enalble" : "btnDisabled";
                        corpCd      = this.CORP_ID;
                        branch      = this.BRANCHS;
                        corpNm      = getLang(this.CORP_NM, this.CORP_NM_VN, this.CORP_NM_EN);
                        cateCd      = this.P_CLS_IDX;
                        cateNm      = getLang(this.PROD_CLS_NAME, this.PROD_CLS_NAME_VN, this.PROD_CLS_NAME_EN);

                        imgSrc      = domain + "//" + this.IMG_URL_MD;
                        url         = validUrl(imgSrc) ? imgSrc : '' ;

                        htmlData.push("<div class='card btnCateMenu " + isEnable + " " + child + " ' data-cate_code='" + cateCd + "' data-corp_id='" + corpCd + "' data-nth=" + i + ">" );
                        htmlData.push("   <img src='" + imgSrc + "' />" );
                        htmlData.push("   <span class='cateName'>" + cateNm + "</span>" );
                        htmlData.push("</div>");

                    });
                    
                    $(page).find(root).html(htmlData.join(''));
                });
            }

            if( $(".ytbArea").length > 0){
                $(".ytbArea").remove();
            }

            var selCateData     = jsonData[nth_cate];
            if( !isNull(selCateData) ){
                // 유튜브 삭제
               
                youtubeCode     = selCateData.YOUTUBE_CODE;
                youtubeUseYn    = selCateData.YOUTUBE_USE_YN;
                if( !isNull(youtubeUseYn) && youtubeUseYn == "Y") {
                    youtubeHtml.push("<div class='ytbArea'>");
                    youtubeHtml.push('  <iframe class="yt_player_iframe" width="100%" height="250" src="https://www.youtube.com/embed/' + youtubeCode +  '?rel=0&enablejsapi=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');
                    youtubeHtml.push("</div>");
                    $(page).find(".listcard.listProduct").prepend(youtubeHtml.join(''));
                }
            }


            // 회사정보 바인딩 추가(주문가능시각, 전화번호)
            if( !isNull(corpData) ){
                
                telNumber   = isNull(corpData.TEL1) ? "-"   : corpData.TEL1;
                telValue    = isNull(corpData.TEL1) ? ""    : corpData.TEL1;

                ostart      = isNull(corpData.ORDER_STATRT_TM) && corpData.ORDER_STATRT_TM.length > 5 ? "-" : corpData.ORDER_STATRT_TM.substr(0, 5);
                estart      = isNull(corpData.ORDER_END_TM)    && corpData.ORDER_END_TM.length    > 5 ? "-" : corpData.ORDER_END_TM.substr(0, 5);
                
                // 현재시각
                nowTime     = moment();

                // 영업시작시각
                stime       = moment(ostart, 'HH:mm');
                stTime      = moment();
                stTime.set({
                    hour    : stime.get('hour'),
                    minute  : stime.get('minute'),
                });

                // 영업종료시각
                etime       = moment(estart, 'HH:mm');
                etTime      = moment();
                etTime.set({
                    hour    : etime.get('hour'),
                    minute  : etime.get('minute'),
                });

                // 앞시간이 뒷시간보다 더 클 경우 다음날을 더해야함
                // - 가 나오면 뒷날에 하루를 더해준다
                daysDiff   = etTime.diff(stTime, 'minutes');

                holyDays   = holyData;
                isHolyDay  = false;

                // 오늘 요일(1~7)
                todayNumber = moment().isoWeekday();
                strDays     = "";
                
                if( holyDays.length > 0 && !isNull(holyDays[0].HOLY_DAYS)){
                    arrHoly = holyDays[0].HOLY_DAYS.replace(/'/g, '').split(",");  
                    
                    for(var i=0; i<=arrHoly.length; i++){

                        nHoly   = parseInt(arrHoly[i]);
                        //console.log(nHoly);

                        if( todayNumber == nHoly){
                            isHolyDay = true;
                        }

                        // 휴일 요일 찍기
                        if( !isNull(nHoly)){
                            switch( nHoly){
                                case 1 :   strDays += getDayName(1, 2) + " ";  break;
                                case 2 :   strDays += getDayName(2, 2) + " ";  break;
                                case 3 :   strDays += getDayName(3, 2) + " ";  break;
                                case 4 :   strDays += getDayName(4, 2) + " ";  break;
                                case 5 :   strDays += getDayName(6, 2) + " ";  break;
                                case 6 :   strDays += getDayName(6, 2) + " ";  break;
                                case 7 :   strDays += getDayName(0, 2) + " ";  break;
                            }
                        }
                    }
                }
                
                //console.log(daysDiff);
                if( daysDiff <= 0){
                    etTime.add(1, 'days');
                }

                //console.log(stTime, etTime);
                sdiff       = nowTime.diff(stTime, "minutes");  // 현재시간보다 시작시간이 + 일 경우 주문못함
                ediff       = nowTime.diff(etTime, "minutes");  // 현재시간보다 
                var corpInfo= [];
                var t_op_t  = myApp.i18Next.getTrasTxt("PRD", "OPEN_TIME");
                var t_holy  = myApp.i18Next.getTrasTxt("PRD", "HOLY_DAY");
                var t_noHly = myApp.i18Next.getTrasTxt("PRD", "EVERY_OPEN");

                //corpInfo.push("<div class='anuance'>");
                corpInfo.push("  <img src='img/product/open_anaunce.PNG'/>" );
                corpInfo.push("   <div>" );
                corpInfo.push("       <h3>" + t_op_t + "</h3>" );
                corpInfo.push("       <span class='timeArea green'>" + ostart + "~" + estart + " ( " + t_holy +" : <span class='holydays'> " + t_noHly + " </span> )</span>");
                corpInfo.push("   </div>" );
                //corpInfo.push("</div>");


                // 시작시간이 음수일경우 영업중단
                // 종료시간이 음수일경우 영업중단
                if( ( sdiff >= 0 && ediff <= 0 ) && !isHolyDay){
                    $(page).find(".anuance").hide(); 
                    $(page).find("ons-list-item").removeClass("off");
                    
                }else{

                    $(page).find(".listcard .anuance").html(corpInfo.join(''));

                    if( strDays.length > 0 ){
                        $(page).find(".holydays").text(strDays);  
                    }
                    $(page).find("ons-list-item").addClass("off st_none");
                }

                // 재검증 : 자정 이후면 한번 더 체크해야함
                // 마감날짜 불러오기
                etime2       = moment(estart, 'HH:mm');
                etTime2      = moment();
                etTime2.set({
                    hour    : etime2.get('hour'),
                    minute  : etime2.get('minute'),
                });

                newDiff = nowTime.diff(etTime2, "minutes"); 

                if( newDiff <= 0 && daysDiff <= 0 && !isHolyDay){
                    $(page).find(".anuance").remove();    
                    $(page).find("ons-list-item").removeClass("off");
                }

            }else{
                $(page).find(".anuance").remove();
                $(page).find("ons-list-item").removeClass("off");
            }
        }

        // 상품 클래스(군) 변경되므로 ON 이벤트 할당시켜줘야함
        $(page).find(".slideMenu .card").off("click");
        $(page).find(".slideMenu .card").on("click",  function(){

            $(page).find(".after-list").show({ "animation": "none" });

            var cateCode    = $(this).data("cate_code");
            var nth         = $(this).data("nth");
            var cateName    = $(this).find(".cateName").text();

            swiper_lnb = null;
            localStorage.setItem("CORP_CLS", cateCode);

            // 상품검색 초기화
            $(page).find("#searchItem input[type='search']").val("");

            $(page).find("#list-node").empty();
            
            // console.log(jsonData);
            myApp.controllers.productListPage(page, nth);
            $(page).find("h4.cateName").html(cateName);

        });

        if( !isNull(swiper_lnb)){
			swiper_lnb.destroy();
        }

        // 슬라이드 배너
        var swiper_lnb  = null;
        swiper_lnb = new Swiper('#productListPage .swiper-container_lnb', {
            pagination: {
                el: '#productListPage .swiper-pagination',
                // type: 'fraction',
                // dynamicBullets: true,
                clickable: true,
            },
            init : true,
            slidesPerView : 'auto',
            freeMode: true,
            initialSlide : selSlideNo,
            // autoplay: true,
            // loop:isLoop,
        });

        $(page).find(".slideMenu .card[data-nth='" + selSlideNo + "']").addClass("slide-actived");
        
    },

    listCategory : function(page, data, domain, option, nth_cate){

        var jsonData        = data.list;        
        var navigation      = document.querySelector('#myNavigator');
        
        var corpData        = data.corp;
        var holyData        = data.holy;
        var cateLenth       = 0;
        // console.log(corpData);

        if( isNull(jsonData) ){
            cateLenth = 0;
        }else{
            cateLenth = jsonData.length;    
        }
        
        if( cateLenth == 0 ) {
            $(page).find(".CateMenu").empty();
        }

        // 아무조건없이 바인딩(카테고리 화면)
        if( option === null){
            // console.log(jsonData);
            var htmlData        = [];
            if( cateLenth > 0){
                // $(page).find(".slideMenu .card").off("click");
                $(jsonData).each(function(i){

                    isEnable    = this.USE_YN ? "enalble" : "btnDisabled";
                    corpCd      = this.CORP_ID;
                    branch      = this.BRANCHS;
                    corpNm      = getLang(this.CORP_NM, this.CORP_NM_VN, this.CORP_NM_EN);
                    cateCd      = this.P_CLS_IDX;
                    cateNm      = getLang(this.PROD_CLS_NAME, this.PROD_CLS_NAME_VN, this.PROD_CLS_NAME_EN);

                    imgSrc      = domain + "//" + this.IMG_URL_MD;
                    
                    ytbCode     = this.YOUTUBE_CODE;
                    youtubeUseYn= this.YOUTUBE_USE_YN;
                    ytbIcon     = ""; 

                    if( !isNull(ytbCode) && youtubeUseYn == "Y" ){
                        ytbIcon     = "ytb";
                    }

                    new_product     = "";
                    if( this.IS_NEW != "N"){

                        if( this.IS_NEW == "Y"){
                            new_product = "<span class='bRed new overTip'>" + "NEW" + "</span>";    
                        }else{
                            new_product = "<span class='bRed new overTip'>" + this.IS_NEW + "</span>";
                        }
                    }
                    
                    htmlData.push("<div class='card ispay btnCateMenu " + isEnable + " " + ytbIcon + "' data-cate_code='" + cateCd + "' data-corp_id='" + corpCd + "' data-nth=" + i + ">");
                    htmlData.push("     <span ><img src='" + imgSrc + "' /></span>" + new_product );
                    htmlData.push("     <div class='cateName'>" + cateNm + "</div>");
                    htmlData.push("</div>");
                });

            }else{
                $(page).find(".after-list").css("display", "none");

				if( $(page).find(".imageList .emptyOrNoData").length == 0 ){
                    htmlData.push("<div class='emptyOrNoData card'><i><img src='img/svg/caution.svg'></i><span data-i18n='COMM:noMoreData'> 더이상 등록된 상품이 없습니다. <span></div>");
				}
            }

            $(page).find(".CateMenu").html(htmlData.join(''));

            // 상품 클래스(군) 변경되므로 ON 이벤트 할당시켜줘야함
            $(page).find(".categoryBody.CateMenu .btnCateMenu").click(function(){
                
                corpId      = $(this).data("corp_id");
                cateCode    = $(this).data("cate_code");
                cateName    = $(this).find(".cateName").text();
                nth         = $(this).data("nth");

                if( !isNull(corpId) && !isNull(cateCode) ){
                    localStorage.setItem("CORP_CLS", cateCode);
                    navigation.pushPage("html/product/product_list.html", { animation: "none" , data: { corpId : corpId, cateCode: cateCode, cateName : cateName, nthCate : nth, clsdata : jsonData, corpData : corpData, holyData : holyData }});
                }
            });

            // 추가 : 카카오 혹은 sns를 통해 reffror로 연결된 회사+상품분류로 이동
            if( !isNull(REFFROR_CLS_ID) ){
                // 회사별 카테고리(분류) 순서
                
                // console.log(REFFROR_CLS_ID);
                $cMenu      = $(page).find(".btnCateMenu[data-cate_code='" + REFFROR_CLS_ID + "']");
                REFFROR_CLS_ID = null;

                corpId      = $cMenu.data("corp_id");
                cateCode    = $cMenu.data("cate_code");
                cateName    = $cMenu.find(".cateName").text();
                nth         = $cMenu.data("nth");

                if( !isNull(corpId) && !isNull(cateCode) ){
                    localStorage.setItem("CORP_CLS", cateCode);
                    setTimeout(function(){
                        navigation.pushPage("html/product/product_list.html", { animation: "none" , data: { corpId : corpId, cateCode: cateCode, cateName : cateName, nthCate : nth, clsdata : jsonData, corpData : corpData, holyData : holyData }});
                    }, 500);
                }
            }
            
            if( !isNull(corpData) && $(page).find(".corpInfo2").length == 0 ){
                // 회사정보 바인딩 추가(주문가능시각, 전화번호)

                $categoryTop    = $(page).find(".categoryBody").parents(".card");

                telNumber   = isNull(corpData.TEL1) ? "-" : corpData.TEL1;
                telValue    = isNull(corpData.TEL1) ? "" : corpData.TEL1;

                ostart      = isNull(corpData.ORDER_STATRT_TM) || corpData.ORDER_STATRT_TM.length < 5 ? "-" : corpData.ORDER_STATRT_TM.substr(0, 5);
                estart      = isNull(corpData.ORDER_END_TM)    || corpData.ORDER_END_TM.length    < 5 ? "-" : corpData.ORDER_END_TM.substr(0, 5);

                kakaoId     = isNull(corpData.KAKAO_ID) ? "-" : corpData.KAKAO_ID;

                var corpInfo  = [];

                corpInfo.push("<div class='corpInfo2'>");
                corpInfo.push("   <div><span class='lbl'><img src='img/svg/phone.svg'/></span>&nbsp;<a class='lblItem' href='tel:" + telValue + "'><span data-i18n='SYS:CALL'>" + "전화하기" + "</span></a></div>");
                corpInfo.push("   <div class='kakao_share'><span class='lbl'><img src='img/svg/kakao-talk.svg'/></span>&nbsp;<a class='lblItem' data-corpid='" + corpData.CORP_ID + "'><span data-i18n='SYS:KAKAO_SHARE'>" + "카카오톡공유" + "</span></a></div>");
                corpInfo.push("</div>");

                $categoryTop.prepend(corpInfo.join(''));

                $(page).find(".kakao_share").click(function(){
                
                    var corp_id     = $(this).find("a").data("corpid");
                    var svcComp = new withWSDL("api/company/list/" + corp_id);
                    // 상품군 목록 데이터 바인딩
                    svcComp.success = function(_jsonData) {
                        var jsonData    = _jsonData.list;
                        var data        = jsonData[0];

                        var plist       = _jsonData.product;

                        console.log(data, plist);
                        myApp.commonController.kakaoLinkCompany(data, plist);
                    };
    
                    svcComp.ajaxGetData({ "is_repmenu" : "Y"});
    
                });

            }
        } 
    },


    listProduct : function(page, data, domain, option){

        var jsonData    = data.list;
        // var holyData    = data.holy;
        var htmlData    = [];
        var emptyMsg    = [];
        var adultDiag   		= document.getElementById('adult-dialog');
        var diagAdultCancel 	= document.querySelector(".diagAdultCancel");
        var diagAdultOk 		= document.querySelector(".diagAdultOk");

        diagAdultCancel.onclick = function(){
            IS_ADULT_AGREE = false;
            adultDiag.hide({ "animation": "none" });
        };

        diagAdultOk.onclick = function(){
            IS_ADULT_AGREE = true;
            adultDiag.hide({ "animation": "none" });
        };

        emptyMsg.push("<div class='emptyOrNoData card'><i><img src='img/svg/caution.svg'></i><span data-i18n='COMM:noMoreData'> 더이상 등록된 상품이 없습니다. <span></div>");

        if( option.clear == 1 || option.clear == "1"){
            $(page).find(".imageList ons-list-item").remove();
        };

        if( jsonData.length > 0){

            $(page).find(".after-list").show({ "animation": "none" });

            // 데이터가 있을 경우 더이상 데이터 존재 하지 않는다는 메시지 삭제
            $(page).find(".emptyOrNoData").remove();

            var img = "", imgSrc = "", rmk = "";
            var t_cook_time     = myApp.i18Next.getTrasTxt("PRD", "COOK_TIME");
            var t_min           = myApp.i18Next.getTrasTxt("ORD", "MINUTS");

            $(jsonData).each(function(){

                imgSrc      = domain + "\\" + this.IMG_URL_MD;
                url         = imgSrc ;
                rmk         = getLang(this.RMK, this.RMK_VN, this.RMK_EN);
                
				sale		= isNull(this.EVENT_DISC_RATE) ? "" : this.EVENT_DISC_RATE.toFixed(1);
                qtySum		= isNull(this.QTY_SUM) || this.QTY_SUM < 0 ?  0 : this.QTY_SUM;
                stQty       = isNull(this.QTY_SUM) ?  0 : this.QTY_SUM;

                unit        = isNull(this.UNIT_NM) ? "EA" : this.UNIT_NM;

                eventAmt    = isNull(this.EVENT_AMT) ? 0 : this.EVENT_AMT;
                orAmt       = isNull(this.OUT_UNIT_PRICE) ? 0 : this.OUT_UNIT_PRICE;
                saleText    = "";

                // 최수구매수량
                minQty      = isNull(this.MIN_ORDER_QTY) ? 0 : this.MIN_ORDER_QTY;

                // 브랜드명
                brandNm     = getLang(this.P_CORP_NAME, this.P_CORP_NAME_VN, this.P_CORP_NAME_EN);
                // 재고가 없을 경우 img에 st_none 클래스 추가했습니다.
                st_none     = qtySum <= 0 ? "st_none" : "";
                st_serv     = this.IS_CONTAIN_MIN_AMT != "Y" ? "st_service" : "";

                // 회사명
                corp_nm     = "";
                cook_msg    = getLang(this.COOK_MSG, this.COOK_MSG_VN, this.COOK_MSG_EN);
                // 조리여부
                cooking     = this.IS_COOKING == 'C_Y' || this.IS_COOKING == 'C_H' ? "<div class='cooking'>" + cook_msg + "</div>" : "";

                // 음식준비시간 
                readyTm     = isNull(this.PRODUCT_READY_TM) ? "" : this.PRODUCT_READY_TM;

                // 성인용 여부 추가
                foodReady   = "";
                if( this.IS_FOOD == 'Y' && !isNull(readyTm) ){
                    foodReady   = "<div class='orderTime green'><i class='fa fa-clock-o'></i> " + t_cook_time + " " + readyTm + t_min + "</div>" ;
                }
                
                // 판매상태(New, 할인, Hot 등이 있을 경우 출력)
                if( !isNull(this.CD_NM)){
                    sale_state  = "<span class='overTip bRed'>" + this.CD_NM + "</span>" ;
                }else{
                    sale_state  = "";
                }
                
                new_product     = "";
                if( this.IS_NEW != "N"){
                    new_product = "<span class='bRed new overTip'>NEW</span>";
                }

                dd1             = this.DD1;     // 판매시작 (현시간부로 지난시간) 가능시간은 판매시작시간보다 지나야함 (양수)
                dd2             = this.DD2;     // 판매종료 (현시간부로 지난시간) 가능시간은 판매종료시간보다 남아야함 (음수)
                sale_timeout    = "";
                if( dd1 >= 0 && dd2 <= 0){
                    sale_timeout = "sale_on";
                }else{
                    sale_timeout = "sale_off";
                }

                if( sale != ""){
                    saleText    =   "<del class='ori_price'>" + parseInt(orAmt).currency() + "</del>" +
                                    "<span class='sale'>" + sale + "<span class='unit'>%</span></span>" ;
                }

                p_name          = getLang(this.PRODUCT_NM, this.PRODUCT_NM_VN, this.PRODUCT_NM_EN);

                // 검색화면/장바구니일 경우 회사명 표기
                if( !isNull(option.isShowCorp) && option.isShowCorp ){
                    corp_nm     = "<span class='gray corpnm'>" + getLang(this.CORP_NM, this.CORP_NM_VN, this.CORP_NM_EN) + "</span>";
                }

                ytbCode     = this.YOUTUBE_CODE;
                ytbUseYn    = this.YOUTUBE_USE_YN;
                ytbIcon     = ""; 

                if( !isNull(ytbCode) && ytbUseYn == "Y" ){
                    ytbIcon     = "ytb";
                }

                st_msg      = getLang("최소주문액<br/>미포함", "Tổng số<br/>không bao gồm", "Total<br/>not included");
                st_service  = this.IS_CONTAIN_MIN_AMT != "Y" ? "<div class='isServiceProduct'>" + st_msg + "</div>" : "";
                favorite    = isNull(this.FAV_IDX) ? "fa-heart-o" : "fa-heart";

                htmlData.push("<ons-list-item tappable data-stqty='" + stQty + "' data-product_cd='"+ this.PRODUCT_CD + "' data-corp_cd='" + this.CORP_ID + "'");
                htmlData.push(" data-p_corp_id='" +  this.CORP_ID + "' data-adult='" + this.IS_ADULT + "'");
                htmlData.push(" class='listItem " + st_none + " " + st_serv + " " + sale_timeout + "' >");                
                htmlData.push("     <div class='left productImg " + ytbIcon + " ' data-product_cd='"+ this.PRODUCT_CD +"' data-product_cate='" + this.P_CLS_IDX + "'>");
                htmlData.push("         <img class='list-item__thumbnail' src='"+ imgSrc +"' >" + new_product );
                htmlData.push("     </div>");
                htmlData.push("     <div class='center productInfo' data-product_cd='"+ this.PRODUCT_CD +"' >");
                htmlData.push("         <div class='list-item__title'>" + corp_nm + "<span class='brand gray'>" + brandNm + "</span>" + p_name);
                htmlData.push(                  sale_state );
                htmlData.push("         </div>" );
                htmlData.push("         <div class='list-item__subtitle'>" + rmk + "</div>");
                htmlData.push("         <div class='pd_price' data-price='" + eventAmt + "'>" );
                htmlData.push("			    <span class='cp'>" + parseInt(eventAmt).currency() + "</span>");
                htmlData.push(	                saleText );
                htmlData.push("         </div>" );
                htmlData.push("         <div class='pd_etc'><span class='star'><i class='fa fa-star'></i>" + this.STAR + "</span>" );
                htmlData.push("             <span style='display:none;' class='stock' data-stock='" + qtySum + "'>재고수량 : " + Number(qtySum).format()  + "" + unit + " </span></div>" );
                htmlData.push(        cooking );
                htmlData.push(        foodReady );
                htmlData.push("         </div>" );
                htmlData.push("	    <div class='buttonArea right'>" );
                htmlData.push("         <div class='list-item__subtitle'>" );
                htmlData.push("             <ons-button class='btnRadius btnMinusQty' data-minqty=" + minQty + "></ons-button>" );
                htmlData.push("             <ons-input input-id='qty' class='qty' type='number' value='0' readonly ></ons-input>" );
                htmlData.push("             <ons-button class='btnRadius btnPlusQty' data-minqty=" + minQty + "></ons-button>" );
                htmlData.push("         </div> " );
                htmlData.push(          st_service );
                htmlData.push("         <span class='heart'><i class='fa " + favorite + "'></i></span>" );
                htmlData.push("     </div>" );
                htmlData.push("</ons-list-item>");
            });

            $(page).find(".imageList").append(htmlData.join(''));
            
            if( jsonData.length < 15){
                htmlData = emptyMsg;
                $(page).find(".imageList").append(htmlData.join(''));
                $(page).find(".after-list").hide({ "animation": "none" });
            }
        }else{

            if( $(page).find(".imageList .emptyOrNoData").length == 0 ){
                htmlData = emptyMsg;
            }
            $(page).find(".imageList").append(htmlData.join(''));
            $(page).find(".after-list").hide({ "animation": "none" });
        }

        // 영업시간 안내가 있는것은 휴일이거나 영업시간이 끝난상태
        if( $(page).find(".anuance").is(":visible") > 0){
            $(page).find("ons-list-item").addClass("st_none off");
        }

        // 즐겨찾기
        Array.prototype.forEach.call(page.querySelectorAll('ons-list-item .heart'), function(element) {

            element.onclick = function(target) {
                corpCd      = $(element).parents('.listItem').data("p_corp_id");
                productCd   = $(this).parents(".listItem").find(".productInfo").data("product_cd");
                myApp.controllers.addFavorite(corpCd, productCd, this);
            };

        });

        // 추가 : 카카오 혹은 sns를 통해 reffror로 연결된 회사+상품분류로 이동
        if( !isNull(REFFROR_P_ID) ){
            $(page).find(".listItem .productInfo[data-product_cd='" + REFFROR_P_ID + "']").trigger("click");
            REFFROR_P_ID = null;
        }


        // 버튼 클릭 증감 이벤트
        Array.prototype.forEach.call(page.querySelectorAll('.btnRadius'), function(element) {

            element.onclick = function(target) {

                if( !showChkTel_Diag() ){
                    return false;
                }

                var isAdult = $(element).parents('.listItem').data("adult");
                var t_sold  = myApp.i18Next.getTrasTxt("ORD", "SOLD_PRD");
                
                // 성인용 이상 상품 체크
                if( isAdult == "Y"){
                    if( IS_ADULT_AGREE  == false ){
                        adultDiag.show( { "animation": "none" } );
                        return false;
                    }
                }
                
                // 재고없음
                if( $(element).hasClass("st_none") ){
                    runWarningMessage(t_sold, "warning", 1000, "up");
                    return false;
                }

                $(element).prop("disabled", true);

                minQty      = $(this).data("minqty");
                corpCd      = $(element).parents('.listItem').data("p_corp_id");
                productCd   = $(this).parents(".listItem").find(".productInfo").data("product_cd");
                price		= $(this).parents('.listItem').find('.productInfo .pd_price').data("price");
                qty         = 1;

                isDetail    = false;
                stockQty    = $(this).parents(".listItem").find(".productInfo .stock").data("stock");

                if( isNull(stockQty) || stockQty <= 0 ){
                    runWarningMessage(t_sold, "warning", 1000, "up");
                    return false;
                }

                objSummaryQty   = $(".summaryQty");
                summaryQty      = parseInt(objSummaryQty.html());
                domQty          = $(element).parents(".buttonArea").find("ons-input");
                inputQty        = parseInt( domQty.val() );

                var t_min_ord_qty   = myApp.i18Next.getTrasTxt("BSK", "MIN_ORD_QTY");

                if( $(element).hasClass("btnMinusQty") ){
                    // 최소주문수량과 일치할경우
                    if( inputQty == minQty && inputQty != 0){
                        runWarningMessage(t_min_ord_qty + " : " + minQty , "warning", 1500, "fade");
                        inputQty = 0;
                        summaryQty -= minQty;
                        qty         = minQty;
                        isEnable    = true;
                    }else{

                        if( inputQty < 0 ){
                            inputQty    = 0;
                            isEnable    = false;
                        }else{
                            if( inputQty > 0 ){
                                inputQty--;
                                summaryQty--;
                                isEnable    = true;

                            }else{
                                inputQty = 0;
                            }
                        }
                    }

                    objSummaryQty.html(summaryQty);
                    domQty.val( inputQty );
                    qty = qty * (-1);

                }else{

                    // 최소주문수량이 있을경우
                    if( minQty != 0 && inputQty == 0){
                        runWarningMessage(t_min_ord_qty + " : " + minQty , "warning", 1500, "bottom");
                        inputQty    += minQty;
                        summaryQty  += inputQty;
                        qty         = minQty;
                    }else{
                        inputQty++;
                        summaryQty++;
                    }

                    objSummaryQty.html(summaryQty);
                    domQty.val( inputQty );
                }

				var data = {
                    ID					: localStorage.getItem("ID"),
                    TEL					: localStorage.getItem("TEL"),
                    PRODUCT_CORP_ID     : corpCd,
                    REF_PRODUCT_CD      : productCd,
                    QTY                 : qty,
                    now_qty             : inputQty,
                };

                myApp.basket.addObjectItem(data);

                setTimeout(function(){
                    $(element).prop("disabled", false);
                }, 300);
            };
        });
    }

    ,viewProduct : function(page, data, domain, option, joProduct){

        var mySwiper    = null;

        var jsonData    = data.list;
        var htmlData    = "";
        var youtubeCode = null;
        var youtubeHtml = [];

        if( option.CLEAR == 1){
            $(page).find(".imageList ons-list-item").empty();
        }

        if( jsonData.length > 0){
            menuViewPopup   = page.querySelector("#menuViewPopup");
            youtubeCode     = jsonData[0].YOUTUBE_CODE;
            youtubeHtml.push("<div class='swiper-slide'>");
            youtubeHtml.push('  <iframe class="yt_player_iframe" width="100%" height="250" src="https://www.youtube.com/embed/' + youtubeCode +  '?rel=0&enablejsapi=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');
            youtubeHtml.push("</div>");
        }

        var objPopup    = $("#detailViewPopup");
        // var dots        = $(objPopup).find(".dots");
        var htmlData    = "";

        objPopup.find("#carouselProduct").empty();

        $(jsonData).each(function(){

            objSrc  = "<img src='" + domain + "\\" + this.FILE_URL + "' width='100%' height='167px' style='text-align:center;'/>" ;
            rmk     = isNull(this.RMK) ? "" : this.RMK;             // 비고
            pNm      = getLang(this.PRODUCT_NM, this.PRODUCT_NM_VN, this.PRODUCT_NM_EN);

            objPopup.find(".brandNm").html( this.P_CORP_NAME);      // 브랜드명
            objPopup.find(".productNm").html( pNm );                // 상품명

            htmlData += "<div class='swiper-slide'>" + objSrc  +  "</div>";
            objPopup.find(".heart").attr('onClick', "myApp.controllers.addFavorite('"+ this.CORP_ID +"', '" + this.PRODUCT_CD + "', null);");

            if( isNull(this.FAV_IDX) ){
                objPopup.find(".heart").html("<i class='fa fa-heart-o'></i>");
            }else{
                objPopup.find(".heart").html("<i class='fa fa-heart'></i>");
            }
            
            objPopup.find(".star").html(this.STAR + "<i class='fa fa-star'></i>");      // 별점
            objPopup.find(".currancy").html(parseInt(this.EVENT_AMT).currency());		// 금액
            objPopup.find(".dlvTime").html(this.PRODUCT_READY_TM);                      // 배달예상시간

            
            objPopup.find(".dlvTimeSt").html( this.ORDER_STATRT_TM);       // 주문시작
            objPopup.find(".dlvTimeEnd").html( this.ORDER_END_TM);         // 주문종료

            objPopup.find(".btnSetpointer").data("product_cd", this.PRODUCT_CD);        // 상품코드
            objPopup.find(".listItem").attr("data-orderid", this.ORDER_ID);             // 상품주문코드
            objPopup.find(".qtyCtrlArea .qty").val(joProduct.QTY);

            if( !isNull(this.CONTENT) ){
                objPopup.find(".content").html(this.CONTENT);
            }
        });

        $(objPopup).find(".swiper-wrapper").html(htmlData);
        if( !isNull(youtubeCode) && youtubeCode.length >= 10){
            $(objPopup).find(".swiper-wrapper").prepend(youtubeHtml.join(''));
        }

		if( !isNull(mySwiper)){
            mySwiper.destroy();
        }

        // 슬라이드 배너
        mySwiper = new Swiper('#detailViewPopup .swiper-container', {
			pagination: {
				el: '#detailViewPopup .swiper-pagination',
				type: 'fraction',
				dynamicBullets: true,
            },
            init : true,
            slidesPerView : 'auto',
			// autoplay: true,
			/*
			{
				 delay: 2000,
				 disableOnInteraction: false,
			},
            */
           on : {
                slideChange : function (){
                    // When Change slide then  stop youtube  : 중요
                    $('.yt_player_iframe').each(function(){
                        this.contentWindow.postMessage('{"event":"command","func":"stopVideo","args":""}', '*');
                    });
                }
            },
			navigation: {
				nextEl: '#detailViewPopup .swiper-button-next',
				prevEl: '#detailViewPopup .swiper-button-prev',
			},
		});

		this.prdPlusMinus(page, joProduct);

    },

	prdPlusMinus : function(page, joProduct){

        // 휴일일 경우 버튼 비활성화
        // if( joProduct.IS_HOLY){
        //     $(page).find(".qtyCtrlArea ons-button").prop("disabled", true);
        // }else{
        //     $(page).find(".qtyCtrlArea ons-button").prop("disabled", false);
        // }

		// 버튼 클릭 증감 이벤트
		var objSumAmt		= $(page).find(".sumAll");
        var objTotalAmt		= $(page).find(".total");
		var objCoinQty		= $(page).find("#useCoin").val(0);
		var myCoin			= localStorage.getItem("MY_COIN");
		var objMyCoin		= $(page).find(".myCoin");

		var objSumAll	    = $(page).find(".sumAll");      // 상품합계
        var objSumEtc       = $(page).find(".sumEtc");      // 기타상품합계
        var objMyCoin       = $(page).find(".myCoin");      // 보유코인
        var objUseCoin      = $(page).find("#coin");        // 코인사용
        var objTotalAmt		= $(page).find(".total");       // 계산금액

        var sumAll			= parseInt(objSumAmt.html());
		var myCoins			= parseInt( getfilterNumber(objMyCoin.html()) );
		var tempCoin		= parseInt(objCoinQty.val());   // 사용코인
		var totalAmt		= parseInt(objTotalAmt.html());

        Array.prototype.forEach.call(document.querySelectorAll('.viewModal .btnRadius'), function(element) {

            element.onclick = function(target) {

                if( !showChkTel_Diag() ){
                    return false;
                }

                var corpCd      = joProduct.CORP_ID;
                var user_id     = joProduct.USER_ID;
                var cateCode    = joProduct.CATEGORY_CD;
                var productCd   = joProduct.PRODUCT_CD;
                var p_corp_id   = joProduct.P_CORP_ID;
                var idx         = $(this).parents('.listItem').data("idx");
				var price		= joProduct.PRICE;
                var qty         = 1;
                var stQty       = joProduct.STOCK_QTY;

                // var stockQty    = $(this).parents(".listItem").find(".productInfo[data-product_cd='" +productCd+ "'] .stock").data("stock");
                var stockQty    = stQty;

                if( isNull(stockQty) ){
                    stockQty    = $(this).parents(".listItem").find(".productInfo .stock").data("stock");
                }

                if( isNull(stockQty) || stockQty <= 0 ){
                    ons.notification.toast('<i class="fa fa-warning"></i> ' + ' SOLD OUT', { timeout: 500, animation: 'fall', 'class' : 'warning' });
                    return false;
                }

                var isEnable		= false;
                var objSummaryQty	= null;
				var objListQtyInput = $(page).find("ons-list-item[data-product_cd='" + productCd + "'] .buttonArea ons-input input");

                // 탭바의 장바구니 숫자와 페이지에서의 숫자가 다름
                if( $(page).find(".summaryQty").length > 0 ){
                    objSummaryQty = $(page).find(".summaryQty");
                }else{
                    objSummaryQty = $("ons-tab .notification");
                }

				var objSummaryQty   = $(".summaryQty");
                var summaryQty      = parseInt(objSummaryQty.html());
                var domQty          = $(element).parents(".qtyCtrlArea").find("ons-input");
                var inputQty        = parseInt( domQty.val() );


                if( $(this).hasClass("btnMinusQty") ){

                    if( inputQty < 0 ){
                        inputQty    = 0;
                        isEnable    = false;
                    }else{
                        if( inputQty > 0 ){
                            inputQty--;
                            summaryQty--;
                            isEnable    = true;
                            // 계산금액반영
                            if( (totalAmt - price) >= 0){
                                objTotalAmt.html( totalAmt - price );
                                objSumAll.html( sumAll - price );
                            }
                        }else{
                            inputQty = 0;
                        }
                    }

					// 리스트의 수량
					objListQtyInput.val(inputQty);

					// 상세화면 수량
                    domQty.val( inputQty );
                    qty = qty * (-1);

                }else{
                    isEnable    = true;
                    inputQty++;
                    summaryQty++;

					// 총금액반영
					objSumAll.html( sumAll + price);
					// 계산금액반영
                    objTotalAmt.html( totalAmt + price);

                    objListQtyInput.val(inputQty);
                    domQty.val( inputQty );
                }

                var data = {
                    ID					: localStorage.getItem("ID"),
                    TEL					: localStorage.getItem("TEL"),
                    PRODUCT_CORP_ID     : p_corp_id,
                    REF_PRODUCT_CD      : productCd,
                    QTY                 : qty,
                    now_qty             : inputQty,
                }
                
                myApp.basket.addObjectItem(data);

                // 모든 장바구니 카운터 동기화
                myApp.commonController.syncBasketQty(summaryQty);
				return;
            };
        });
	},

	// 댓글 목록 바인딩
	// 사용자별 댓글 중복 입력 방지
    listComment : function(page, data, pOption){

		var modal       = document.querySelector('#detailViewPopup');
		var dialogC		= page.querySelector('#comment-dialog');
        var jsonData    = data.list;
        var objList     = $(modal).find("#list-comment");
        var htmlData    = [];
		var product_cd	= "";

        objList.empty();
        
        if( jsonData.length > 0){

            $(jsonData).each(function(i){

				product_cd	= this.PRODUCT_CD;

                htmlData.push("<div class='listItem listData cmt" + i + "' data-userid='" + this.USER_ID + "' data-product_cd='" + this.PRODUCT_CD + "' data-idx='" + this.IDX + "'>");
                htmlData.push("    <div class='left title' >");
                htmlData.push("	   <span class='listName'><i class='fa fa-commenting-o'></i> " + this.name  + "</span>");
                htmlData.push("	   <span class='listContent red'><i class='fa fa-star'></i> " + this.PRODUCT_STARTS  + "</span>");
                htmlData.push("       <span class='right txtRight'>" + this.CREATED_AT + "</span>");
                htmlData.push("    </div>");
                htmlData.push("    <div class='center listContent ellipsis'>" + this.PRODUCT_COMMENT + "</div>");
                htmlData.push("</div>");
            });
            objList.html(htmlData.join(''));

        }else{
            
            if(objList.find(".emptyOrNoData").length == 0){
                htmlData.push("<div class='emptyOrNoData card'><i><img src='img/svg/caution.svg'></i><span data-i18n='COMM:noMoreData'>더이상 데이터가 존재 하지 않습니다<span></div>");
            }
			objList.html(htmlData.join(''));
        }


		objList.off("click", ".listItem", null );
		objList.on("click", ".listItem", function(){
			if( $(this).data('userid') == localStorage.getItem("USER_ID") ){
				dialogC.show({ "animation": "none" });
			};
		});

        $(modal).find(".after-list").css("display", "none");

		// 다이어로그 닫기
		$(dialogC).on("click", ".btnDialogCancelComment", function(){
			dialogC.hide( { animation : "none"} );
		});

		// 댓글삭제
		$(dialogC).on("click", ".btnDialogRemoveComment", function(){

			// 별점/댓글 삭제
			var service		= new withWSDL("api/order/list/" + '');
			var data		= {
				CORP_ID     : localStorage.getItem("CORP_ID"),
				USER_ID     : localStorage.getItem("USER_ID"),
				PRODUCT_CD  : product_cd,
			}

			service.success = function(_jsonData){
				// var jsonData    = $.parseJSON(_jsonData);
				dialogC.hide( { animation : "none"} );
				objList.find(".listData[data-userid='" + data.USER_ID + "']" ).remove();
			};

			service.ajaxGetData(data);
		});

		// 별점, 댓글을 등록할 수 있는 지 확인 (구매 이력 확인)
		var service4	= new withWSDL("api/order/list");
		service4.success = function(_jsonData){

			var jsonData    = _jsonData.list;
			var userid		= localStorage.getItem("USER_ID");
            var objList     = $(page).find("#list-comment");
            
            var t_cnt_write = myApp.i18Next.getTrasTxt("BSK", "CANT_WRITE");
            var t_cmmt_input= myApp.i18Next.getTrasTxt("PRD", "COMMET_INP");

			$(modal).find(".btnSetpointer").prop("disabled", false);

			// 구매이력 확인
			if( $(jsonData).length ==  0){
                $(modal).find("#PRODUCT_COMMENT").prop("readonly", true);
                $(modal).find("#PRODUCT_COMMENT").attr("placeholder", t_cnt_write);
                $(modal).find(".btnAddComment").prop("disabled", true);
			}else{
                $(modal).find("#PRODUCT_COMMENT").prop("readonly", false);
                $(modal).find("#PRODUCT_COMMENT").attr("placeholder", t_cmmt_input);
                $(modal).find(".btnAddComment").prop("disabled", false);
            }

			$(jsonData).each(function(){

				$(modal).find(".btnAddComment").attr("data-orderid",  this.ORDER_IDX);
                if( !isNull(userid) ){
                    if( userid.trim().indexOf( this.USER_ID ) >= 0 ){
    
                        // 댓글 중복 방지 : 버튼 비활성화
                        objList.find(".listItem").each(function(){
                            if( userid.trim().indexOf( $(this).data("userid") ) >= 0 ){
                                $(modal).find(".btnSetpointer").prop("disabled", true);
                                return false;
                            }
                        });
                    }
                }
			});
		}

		var chkData = {
            "tel"           : localStorage.getItem("TEL"),
			"corp_id"       : localStorage.getItem("CORP_ID"),
			"user_id"       : localStorage.getItem("USER_ID"),
			"product_cd"	: pOption.PRODUCT_CD,
		};

		service4.ajaxGetData(chkData);
    },     
}