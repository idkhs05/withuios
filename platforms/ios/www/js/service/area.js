var service	= new withWSDL();

window.serviceModel = {

    setAreaInfo : function(page, data, option){

        var $select1= $(page).find("ons-select[name='sCntnt'] select");
        var cntnt   = data.cntnt;
        var nation  = data.nation;
        var city    = data.city;
        var user    = data.user;

        var cntnt_id    = option.cntnt_id;
        var selected    = "";
        var msg_sel_cntnt   = myApp.i18Next.getTrasTxt("LOCATION", "SEL_CNTNT");
        
        if( !isNull(cntnt) && cntnt.length >= 0 ){
            
            $select1.empty();
            $select1.append("<option value=''>" + msg_sel_cntnt + "</option>");

            $(cntnt).each(function(){
                var id          = this.ID;
                var cntnt_nm    = getLang(this.CNTNT_NM, this.CNTNT_NM_VN, this.CNTNT_NM_EN);
                
                if( this.ID == cntnt_id){
                    selected    = "selected";
                }else{
                    selected    = "";
                }
                $select1.append("<option value='" + id + "' " + selected + ">" + cntnt_nm + "</option>");
            });
        }

        this.setNationInfo(page, data, option);
        this.setCityInfo(page, data, option);  
    },

    setNationInfo : function(page, data, option) {

        var nation      = data.nation;
        var $select2    = $(page).find("ons-select[name='sNation'] select");
        var selected    = "";
        var nation_id   = option.nation_id;
        var msg_sel_nation  = myApp.i18Next.getTrasTxt("LOCATION", "SEL_NATION");

        if( !isNull(nation) && nation.length >= 0 ){

            $select2.empty();
            $select2.append("<option value=''>" + msg_sel_nation + "</option>");
                
            $(nation).each(function(){
                var id          = this.ID;
                var nation_nm    = getLang(this.NATION_NM, this.NATION_NM_VN, this.NATION_NM_EN);
                
                if( this.ID == nation_id){
                    selected    = "selected";
                }else{
                    selected    = "";
                }
                $select2.append("<option value='" + id + "' " + selected + ">" + nation_nm + "</option>");
            });
        }else{
            // $select2.html("<option value=''>" + msg_sel_nation + "</option>");
        }
    },

    setCityInfo : function(page, data, option) {
        
        var city        = data.city;
        var $select3    = $(page).find("ons-select[name='sCity'] select");
        var selected    = "";
        var city_id     = option.area_id;
        var msg_sel_city  = myApp.i18Next.getTrasTxt("LOCATION", "SEL_CITY");

        if( !isNull(city) && city.length >= 0 ){
            $select3.empty();
            $select3.append("<option value=''>" + msg_sel_city + "</option>");
            $(city).each(function(){
                var id          = this.AREA_ID;
                var city_nm    = getLang(this.AREA_NM, this.AREA_NM_VN, this.AREA_NM_EN);
                
                if( this.AREA_ID == city_id){
                    selected    = "selected";
                }else{
                    selected    = "";
                }
                $select3.append("<option value='" + id + "' " + selected + ">" + city_nm + "</option>");
            });
        }else{
            $select3.html("<option value=''>" + msg_sel_city + "</option>");
        }
    }
};
