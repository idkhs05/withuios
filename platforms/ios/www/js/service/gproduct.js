window.serviceModel = {

    listGrpProduct : function(page, data, domain, option){

        var jsonData    = data.list;
        var holyData    = data.holy;
        var htmlData    = [];

        var navigation          = document.querySelector('#myNavigator');
        var adultDiag   		= document.getElementById('adult-dialog');
        var diagAdultCancel 	= document.querySelector(".diagAdultCancel");
        var diagAdultOk 		= document.querySelector(".diagAdultOk");

        diagAdultCancel.onclick = function(){
            IS_ADULT_AGREE = false;
            adultDiag.hide();
        }

        diagAdultOk.onclick = function(){
            IS_ADULT_AGREE = true;
            adultDiag.hide();
        }

        var emptyMsg    = "<div class='emptyOrNoData card'><i><img src='img/svg/caution.svg'></i><span data-i18n='COMM:noMoreData'> 더이상 등록된 상품이 없습니다. <span></div>";

        if( option.clear == 1 || option.clear == "1"){
            $(page).find(".grpList ons-list-item").remove();
            $(page).find(".emptyOrNoData ").remove();

        }

        if( jsonData.length > 0){
            // 데이터가 있을 경우 더이상 데이터 존재 하지 않는다는 메시지 삭제
            $(page).find(".anuance").hide();
            $(page).find(".emptyOrNoData ").remove();

            //let menuViewPopup = page.querySelector("#menuViewPopup");

            var img = "", imgSrc = "", rmk = "";
            var lang_apply      = myApp.i18Next.getTrasTxt("GB","APP_AMT");
            var lang_target     = myApp.i18Next.getTrasTxt("GB","TAR_AMT");
            var lang_myapply    = myApp.i18Next.getTrasTxt("GB","MY_APPLY");

            var t_remain        = myApp.i18Next.getTrasTxt("GB", "D_DAY_RMN");
            var t_before        = myApp.i18Next.getTrasTxt("GB", "D_DAY_BFR");
            var t_after         = myApp.i18Next.getTrasTxt("GB", "D_DAY_AFT");
            var t_deadline      = myApp.i18Next.getTrasTxt("GB", "DEADLINE");

            $(jsonData).each(function(){

                imgSrc      = domain + "\\" + this.IMG_URL_MD;
                url         = imgSrc ;
                // rmk         = this.RMK.toText();

                mTitle      = getLang(this.MAIN_TITLE, this.MAIN_TITLE_VN, this.MAIN_TITLE_EN);
                sTitle      = getLang(this.SUB_TITLE, this.SUB_TITLE_VN, this.SUB_TITLE_EN);
                // 브랜드명
                pCorpName   = getLang(this.P_CORP_NAME, this.P_CORP_NAME_VN, this.P_CORP_NAME_EN);
            
                pName       = getLang(this.PRODUCT_NM, this.PRODUCT_NM_VN, this.PRODUCT_NM_EN);
                
                pclsNam     = isNull(this.PROD_CLS_NAME) ? "" : this.PROD_CLS_NAME;

                custNm      = isNull(this.CUST_NM) ? "WITHU" : this.CUST_NM;

                price       = isNull(this.P_PRICE) ? 0 : this.P_PRICE;
                qty         = isNull(this.P_QTY) ? 0 : this.P_QTY;

                totAmt      = price * qty;
                totAmt      = isNaN(totAmt) ? 0 : totAmt ;
                
                sd_qty      = isNull(this.ORD_QTY_SUM) ? 0 : this.ORD_QTY_SUM;;
                
                totSdAmt    = price * sd_qty;

                percent     = ( totSdAmt / totAmt ) * 100;
                percent     = isFinite(percent) ? percent : 0;
                percent     = isNaN(percent) ? 0 : Math.round(percent) ;

                saleText    = "";

                // 재고가 없을 경우 img에 st_none 클래스 추가했습니다.
                // st_none     = qtySum <= 0 ? "st_none" : "";
                st_serv     = this.IS_CONTAIN_MIN_AMT != "Y" ? "st_service" : "";

                foodReady   = "";
                
                // 판매상태(New, 할인, Hot 등이 있을 경우 출력)
                if( !isNull(this.CD_NM)){
                    sale_state  = "<span class='overTip bRed'>" + this.CD_NM + "</span>" ;
                }else{
                    sale_state  = "";
                }

                // 자기가 주문여부 
                ordCount    = this.ORD_CNT;
                ordMsg      = "";
                // expandable  = "";
                if( !isNull(ordCount) && ordCount > 0){
                    ordMsg  = "<span class='orderTip'>" + lang_myapply + " - " + ordCount + " </span>" ;
                }

                new_product     = "";
                if( this.IS_NEW != "N"){
                    new_product = "<span class='bRed new overTip'>NEW</span>";
                }
                
                end_sign        = "";
                dd1             = this.DDAY;     // 판매시작 (현시간부로 지난시간) 가능시간은 판매시작시간보다 지나야함 (양수)
                dd2             = this.DMIN;     // 판매종료 (현시간부로 지난시간) 가능시간은 판매종료시간보다 남아야함 (음수)

                sale_timeout    = "";
                
                if( dd1 >= 0 && dd2 >= 0){
                    sale_timeout = "sale_on";
                    dday         = Math.abs(dd1) + t_remain ;   // "n일 남음"
                    
                    if( dd1 <= 1 ){
                        end_sign = "<span class='bOrange endsign overTip blinking'>" + t_deadline + "</span>";
                    }

                }else{
                    sale_timeout = "sale_off";
                    if( dd1 == 0){
                        dd1 = 1;
                    }
                    dday         = Math.abs(dd1) + t_after;     // "n일 지남"
                }

                sd1             = this.DD1;     // 판매시작 (현시간부로 지난시간) 가능시간은 판매시작시간보다 지나야함 (양수)
                sd2             = this.DD2;     // 판매종료 (현시간부로 지난시간) 가능시간은 판매종료시간보다 남아야함 (음수)
                sale_timeout2    = "";

                if( sd1 < 0){
                    sale_timeout2 = "gready_on";
                    dday         = Math.ceil(Math.abs(sd1)/60/24) + t_before ;  // "n일 후 시작"
                }else{
                    sale_timeout2 = "gsale_off";
                }

                ytbCode     = this.YOUTUBE_CODE;
                youtubeUseYn= this.YOUTUBE_USE_YN;
                ytbIcon     = ""; 

                if( !isNull(ytbCode) && youtubeUseYn == "Y" ){
                    ytbIcon     = "ytb";
                }

                htmlData.push("<ons-list-item tappable data-product_cd='"+ this.PRODUCT_CD + "' data-corp_cd='" + this.CORP_ID + "" );
                htmlData.push("' data-idx='" + this.ID + "' data-adult='" + this.IS_ADULT );
                htmlData.push("' class='" + st_serv + " " + sale_timeout + " " + sale_timeout2 + "'>");
                htmlData.push("   <div class='left productImg " + ytbIcon + "' data-product_cd='"+ this.PRODUCT_CD +"'>"  );
                htmlData.push("       <img class='list-item__thumbnail' src='"+ imgSrc + "' >" + new_product +  ordMsg + end_sign );
                htmlData.push("   </div>" );
                htmlData.push("   <div class='center' data-product_cd='"+ this.PRODUCT_CD +"' >");
                htmlData.push("       <div class='list-item__title'>" + mTitle + "</div>" );
                htmlData.push("       <div class='list-item__subtitle'>" );
                htmlData.push("           <span class='blue'>" + pCorpName + "</span>" );
                htmlData.push("           <span class='pName'>" + pName + "</span>" );
                htmlData.push("       </div>" );
                htmlData.push("       <div class='list-item__subtitle'>" );
                htmlData.push("           <span class=''>" + lang_apply + " " + Number(price).currency() + "</span> "  );
                htmlData.push("       </div>" );
                htmlData.push("       <div class='list-item__subtitle'>" );
                htmlData.push("           <span class=''>" + lang_target + " " + Number(totAmt).currency() + "</span> "  );
                htmlData.push("       </div>" );
                htmlData.push("       <div class='progressbar list-item__subtitle'>" );
                htmlData.push("           <ons-progress-bar value='" + percent + "' secondary-value='100'></ons-progress-bar>" );
                htmlData.push("       </div>" );
                htmlData.push("       <div class='list-item__subtitle innerSubTitle'>" );
                htmlData.push("           <span class='subSummary percenter orange bold'>" + percent + "%</span>" );
                htmlData.push("           <span class='subSummary mdays'>" + dday + "</span>" );
                htmlData.push("       </div>  " );
                htmlData.push("   </div>" );
                htmlData.push("</ons-list-item>");
                
            });

            $(page).find(".grpList").append(htmlData.join(''));
            $(page).find(".after-list").show();

            if( jsonData.length < 15){
                $(page).find(".grpList").append(emptyMsg);
                $(page).find(".after-list").hide();
            }
        }else{

            if( $(page).find(".grpList .emptyOrNoData").length == 0 ){
                $(page).find(".grpList").append(emptyMsg);
            }else{

            }
            
            $(page).find(".after-list").hide();
        }


        // 영업시간 안내가 있는것은 휴일이거나 영업시간이 끝난상태
        // if( $(page).find(".anuance").length > 0){
        //     $(page).find("ons-list-item").addClass("st_none off");
        // }

        // 상품 클래스(군) 변경되므로 ON 이벤트 할당시켜줘야함
        $(page).find(".grpList > ons-list-item").click(function(){
                
            data        = { 
                corpId      : $(this).data("corp_cd"),
                id          : $(this).data("idx"), 
                productCd   : $(this).data("product_cd")
            };
            
            if( !isNull(data.corpId) && !isNull(data.productCd) && !isNull(data.id) ){
                navigation.pushPage("html/grp/grp_product_detail.html", { data: data, animation : "none"} );
            }
        });

        if( !isNull(REFFROR_G_ID)){
            data        = { 
                id          : REFFROR_G_ID, 
            };
            REFFROR_G_ID    = null;
            navigation.pushPage("html/grp/grp_product_detail.html", { data: data, animation : "none"} );
        }
    }

    ,viewGrpProduct : function(page, data, domain, option, joProduct){

        var modal           = page.querySelector('ons-modal');
        var menuViewPopup   = page.querySelector("#menuViewPopup");
        var navigation      = document.querySelector('#myNavigator');
        // 슬라이드 배너
        var mySwiper        = null;

        var jsonCount   = data.list.length;
        var jsonData    = data.list;
        var jsonImage   = data.imgList;
        var htmlData    = "";
        var youtubeCode = null;
        var youtubeHtml = [];
        var youtubeUseYn= "N";

        var t_before        = myApp.i18Next.getTrasTxt("GB", "D_DAY_BFR");

        //console.log(jsonData);
        if( option.CLEAR == 1){
            $(page).find(".imageList ons-list-item").empty();
        }
        //console.log(jsonData.MAIN_TITLE);
        
        if( !isNull(jsonData) ){

            d = jsonData;

            youtubeCode     = d.YOUTUBE_CODE;
            youtubeUseYn    = d.YOUTUBE_USE_YN;
            youtubeHtml.push("<div class='swiper-slide'>");
            youtubeHtml.push('  <iframe class="yt_player_iframe" width="100%" height="300" src="https://www.youtube.com/embed/' + youtubeCode +  '?rel=0&enablejsapi=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>');
            youtubeHtml.push("</div>");

            mainTitle   = getLang(d.MAIN_TITLE, d.MAIN_TITLE_VN, d.MAIN_TITLE_EN);
            subTitle    = getLang(d.SUB_TITLE, d.SUB_TITLE_VN, d.SUB_TITLE_EN);

            $(page).find(".mTitle").html("<i class='fa fa-legal'></i> " + mainTitle );
            $(page).find(".sTitle").html(subTitle);

            if( d.DDAY >= 0 && d.DMIN >= 0){
                sale_timeout = "sale_on";
            }else{
                sale_timeout = "sale_off";
            }

            sd1             = d.DD1;     // 판매시작 (현시간부로 지난시간) 가능시간은 판매시작시간보다 지나야함 (양수)
            sd2             = d.DD2;     // 판매종료 (현시간부로 지난시간) 가능시간은 판매종료시간보다 남아야함 (음수)
            sale_timeout2    = "";

            if( sd1 < 0){
                sale_timeout2 = "gready_on";
                dday          = Math.ceil(Math.abs(sd1)/60/24) + t_before ;
            }

            $(page).find(".btnRadius").addClass(sale_timeout).addClass(sale_timeout2);

            minQty  = isNaN(d.MIN_ORDER_QTY) ? 1 : d.MIN_ORDER_QTY;
            $(page).find(".btnRadius").attr("data-minqty", minQty);

            

            pCorpName   = getLang(d.P_CORP_NAME, d.P_CORP_NAME_VN, d.P_CORP_NAME_EN);
            pName       = getLang(d.PRODUCT_NM, d.PRODUCT_NM_VN, d.PRODUCT_NM_EN);            
            
            $(page).find(".productName").attr("data-product_cd", d.PRODUCT_CD);
            $(page).find(".productName").html("<span class='blue'>" + pCorpName + "</span>" + 
                                              "<span class='pName'>" + pName + "</span>" + 
                                              "<span class='pPrice'>" + Number(d.P_PRICE).currency() + "</span>"
                                              );
            // DDAY, DMIN;
            daysNum = moment(d.END_DT).day();
            dayName = getDayName(daysNum, 2);
            
            daysNum3 = moment(d.START_DT).day();
            dayName3 = getDayName(daysNum3, 2);

            // 주문가능기간
            $(page).find(".endDate").attr("data-endDate", d.END_DT);
            $(page).find(".endDate").text(  moment(d.START_DT).format("YY-MM-DD") + dayName3 + ", " + moment(d.START_DT).format("HH:mm") + " ~ " + 
                                            moment(d.END_DT).format("YY-MM-DD") + dayName + ", " + moment(d.END_DT).format("HH:mm")
                                        );

            // 배송시작일시
            delv_dt  = isNull(d.DELV_START_DT) ? moment().format("YY-MM-DD HH:mm") : d.DELV_START_DT;
            daysNum1 = moment(delv_dt).day();
            dayName1 = getDayName(daysNum1, 2);

            // 배송종료일시
            delv_dtEnd  = isNull(d.DELV_END_DT) ? moment().format("YY-MM-DD HH:mm") : d.DELV_END_DT;
            daysNum2 = moment(delv_dtEnd).day();
            dayName2 = getDayName(daysNum2, 2);
            $(page).find(".delvDate").attr("data-delvdate", delv_dt);
            $(page).find(".delvDate").attr("data-delvdateend", delv_dtEnd);
            $(page).find(".delvDate").text( moment(delv_dt).format("YY-MM-DD") + dayName1 + ", " + moment(delv_dt).format("HH:mm") + " ~ " + 
                                            moment(delv_dtEnd).format("YY-MM-DD") + dayName2 + ", " + moment(delv_dtEnd).format("HH:mm")
                                           );


            // 남은시간
            // console.log("d.END_DT : " +  d.END_DT);
            window.serviceModel.getCountDowntimer(page, moment(d.END_DT));

            pSum    = d.P_SUM;
            pSum    = isNaN(pSum) ? 0 : pSum;
            sSum    = d.P_PRICE * parseInt(d.ORD_QTY_SUM) ;
            sSum    = isNaN(sSum) ? 0 : sSum;

            percent = ( sSum / pSum ) * 100;
            percent = isNaN(percent) ? 0 : Math.round(percent);
            
            cooking     = d.IS_COOKING == 'C_Y' || d.IS_COOKING == 'C_H' ? d.COOK_MSG : "-";
            // console.log(d.P_PRICE);
            $(page).find(".sum").attr("data-price", d.P_PRICE);
            $(page).find(".sum").text( Number(pSum).currency() );
            t_now   = myApp.i18Next.getTrasTxt("GB", "MEM");
            t_req   = myApp.i18Next.getTrasTxt("GB", "REQ_MEM");
                
            $(page).find(".percent").html( percent + "% <span class='now'>(" + t_now + " : " + Number(sSum).currency() + ")</span>" );

            // 구매자수
            $(page).find(".number").text( d.ORD_QTY_CNT + t_req);

            $(page).find(".origin").text( isNull(d.ORIGIN_NAME) ? "-" : d.ORIGIN_NAME);
            $(page).find(".cust").text( isNull(d.CUST_NM) ? "-" : d.CUST_NM );
            $(page).find(".color").text( isNull(d.COLOR) ? "-" : d.COLOR);
            $(page).find(".size").text( isNull(d.SIZE) ? "-" : d.SIZE);
            $(page).find(".food").text( isNull(cooking) ? "-" : cooking);

            // 상품설명
            remark  = getLang(d.REMARK1, d.REMARK2, d.REMARK3);
            $(page).find(".content").html( remark );
            removePs("gContent");
            
            $(page).find(".barArea").html(
                "<div class='progressbar'>" + 
                "    <ons-progress-bar value=" + percent + " secondary-value='100'></ons-progress-bar>"  + 
                "</div>"
            );
        }

        var objSlide    = $(page).find(".swiper-container");
        var htmlData    = "";
        objSlide.find(".swiper-wrapper").empty();
        $(jsonImage).each(function(){
            objSrc   = "<img src='" + domain + "\\" + this.FILE_URL + "' width='100%' data-id='" + this.IDX + "' style='padding:0;text-align:center;'/>" ;
            htmlData += "<div class='swiper-slide'>" + objSrc  +  "</div>";
        });
        $(objSlide).find(".swiper-wrapper").html(htmlData);

        if( !isNull(youtubeCode) && youtubeUseYn == "Y"){
            $(objSlide).find(".swiper-wrapper").prepend(youtubeHtml.join(''));
        }

        if( $(objSlide).find(".swiper-slide").length == 0 ){
            $(objSlide).find(".swiper-wrapper").html("<div class='swiper-slide'><img src='' /></div>");
        }

		if( !isNull(mySwiper)){
            mySwiper.destroy();
        }

        // 슬라이드 배너
        mySwiper = new Swiper('#grpProductViewPage .swiper-container', {
			pagination: {
				el: '#grpProductViewPage .swiper-pagination',
				type: 'fraction',
            },
            init : true,
            slidesPerView : 'auto',
			//autoplay: true,
			/*
			{
				 delay: 2000,
				 disableOnInteraction: false,
			},
            */
           on : {
                slideChange : function (){
                    // When Change slide then  stop youtube  : 중요
                    $('.yt_player_iframe').each(function(){
                        this.contentWindow.postMessage('{"event":"command","func":"stopVideo","args":""}', '*');
                    });
                }
            },
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
        });

        

		 // 버튼 클릭 증감 이벤트
         Array.prototype.forEach.call(page.querySelectorAll('.btnRadius'), function(element) {

            element.onclick = function(target) {

                var t_ready = myApp.i18Next.getTrasTxt("ORD", "READY_PRD");
                var t_ended = myApp.i18Next.getTrasTxt("ORD", "ENDED_PRD");
                var isAdult     = $(element).parents('.listItem').data("adult");
            
                // 성인용 이상 상품 체크
                if( isAdult == "Y"){
                    if( IS_ADULT_AGREE  == false ){
                        adultDiag.show();
                        return false;
                    }
                }

                // 시작여부
                if( $(element).hasClass("gready_on") ){
                    runWarningMessage(t_ready, "warning", 1000, "up");
                    return false;
                }
                
                // 재고없음
                if( $(element).hasClass("st_none") ){
                    runWarningMessage(t_ended, "warning", 1000, "up");
                    return false;
                }

                minQty      = $(this).data("minqty");
                corpCd      = $(element).parents('.listItem').data("p_corp_id");
                
                productCd   = $(this).parents(".listItem").find(".productInfo").data("product_cd");
                price		= $(this).parents('.listItem').find('.productInfo .pd_price').data("price");
                qty         = 1;

                isDetail    = false;
               

                domQty          = $(element).parents(".qtyCtrlArea").find("ons-input");
                inputQty        = parseInt( domQty.val() );

                if( $(element).hasClass("btnMinusQty") ){
                    
                    // 최소주문수량과 일치할경우
                    if( inputQty == minQty && inputQty != 0){
                       
                        inputQty = 0;
                        
                        qty         = minQty;
                        isEnable    = true;
                    }else{

                        if( inputQty < 0 ){
                            inputQty    = 0;
                            isEnable    = false;
                        }else{

                            //console.log(totalAmt , price);
                            if( inputQty > 0 ){
                                inputQty--;
                                isEnable    = true;

                            }else{
                                inputQty = 0;
                            }
                        }
                    }

                    domQty.val( inputQty );
                    qty = qty * (-1);

                }else{

                    // 최소주문수량이 있을경우
                    if( minQty != 0 && inputQty == 0 && minQty > 1){
                        runWarningMessage("최수주문수량 : " + minQty + " 개", "info", 1500, "up");
                        inputQty    += minQty;   
                        qty         = minQty;
                    }else{
                        inputQty++;
                        
                    }
                    // summaryQty++;
                    
                    //myApp.basket.addObjectItem("basket", data);
                    domQty.val( inputQty );
                
                }

                window.serviceModel.checkGrpOrderButton(page);
                // myApp.basket.setItem("basket", product);
				/*
                var data = {
                    CORP_ID             : corpCd,
                    USER_ID             : localStorage.getItem("USER_ID"),
                    PRODUCT_CORP_ID     : corpCd,
                    REF_PRODUCT_CD      : productCd,
                    QTY                 : qty,
                    SAVED               : "S",
                }
				*/

				var data = {
                    ID					: localStorage.getItem("ID"),
                    TEL					: localStorage.getItem("TEL"),
                    PRODUCT_CORP_ID     : corpCd,
                    REF_PRODUCT_CD      : productCd,
                    QTY                 : qty,
                    now_qty             : inputQty,
                }

                
                // console.log(data);
                //return false;
                // myApp.basket.addObjectItem(data);

                // setTimeout(function(){
                //     $(element).prop("disabled", false);
                // }, 500)
            };
            
        });

    },

	prdPlusMinus : function(page, joProduct){


        // 휴일일 경우 버튼 비활성화
        // console.log(joProduct.IS_HOLY);
        if( joProduct.IS_HOLY){
            $(page).find(".qtyCtrlArea ons-button").prop("disabled", true);
        }else{
            $(page).find(".qtyCtrlArea ons-button").prop("disabled", false);
        }

		// 버튼 클릭 증감 이벤트

		var objSumAmt		= $(page).find(".sumAll");
        var objTotalAmt		= $(page).find(".total");
		var objCoinQty		= $(page).find("#useCoin").val(0);
		var myCoin			= localStorage.getItem("MY_COIN");
		var objMyCoin		= $(page).find(".myCoin");

		var objSumAll	    = $(page).find(".sumAll");      // 상품합계
        var objSumEtc       = $(page).find(".sumEtc");      // 기타상품합계
        var objMyCoin       = $(page).find(".myCoin");      // 보유코인
        var objUseCoin      = $(page).find("#coin");        // 코인사용
        var objTotalAmt		= $(page).find(".total");       // 계산금액

        var sumAll			= parseInt(objSumAmt.html());
		var myCoins			= parseInt( getfilterNumber(objMyCoin.html()) );
		var tempCoin		= parseInt(objCoinQty.val());   // 사용코인
		var totalAmt		= parseInt(objTotalAmt.html());

        Array.prototype.forEach.call(page.querySelectorAll('.viewModal .btnRadius'), function(element) {

            element.onclick = function(target) {

                var corpCd      = joProduct.CORP_ID;
                var user_id     = joProduct.USER_ID;
                var cateCode    = joProduct.CATEGORY_CD;
                var productCd   = joProduct.PRODUCT_CD;
                var p_corp_id   = joProduct.P_CORP_ID;
                var idx         = $(this).parents('.listItem').data("idx");
				var price		= joProduct.PRICE;
                var qty         = 1;
                var stQty       = joProduct.STOCK_QTY;

                // var stockQty    = $(this).parents(".listItem").find(".productInfo[data-product_cd='" +productCd+ "'] .stock").data("stock");
                var stockQty    = stQty;

                if( isNull(stockQty) ){
                    stockQty    = $(this).parents(".listItem").find(".productInfo .stock").data("stock");
                }

                // console.log(stockQty);

                if( isNull(stockQty) || stockQty <= 0 ){
                    ons.notification.toast('<i class="fa fa-warning"></i> ' + ' 재고없음', { timeout: 500, animation: 'fall', 'class' : 'warning' });
                    return false;
                }

                var isEnable		= false;
                var objSummaryQty	= null;
				var objListQtyInput = $(page).find("ons-list-item[data-product_cd='" + productCd + "'] .buttonArea ons-input input");

                // 탭바의 장바구니 숫자와 페이지에서의 숫자가 다름
                if( $(page).find(".summaryQty").length > 0 ){
                    objSummaryQty = $(page).find(".summaryQty");
                }else{
                    objSummaryQty = $("ons-tab .notification");
                }

				var objSummaryQty   = $(".summaryQty");
                var summaryQty      = parseInt(objSummaryQty.html());
                var domQty          = $(element).parents(".qtyCtrlArea").find("ons-input");
                var inputQty        = parseInt( domQty.val() );


                if( $(this).hasClass("btnMinusQty") ){

                    if( inputQty < 0 ){
                        inputQty    = 0;
                        isEnable    = false;
                    }else{

                        //console.log(totalAmt , price);
                        if( inputQty > 0 ){
                            inputQty--;
                            summaryQty--;
                            isEnable    = true;
                            // 계산금액반영
                            if( (totalAmt - price) >= 0){

                                // console.log(totalAmt ,price )
                                objTotalAmt.html( totalAmt - price );
                                objSumAll.html( sumAll - price );
                            }
                        }else{
                            inputQty = 0;
                        }

                    }

					// 리스트의 수량
					objListQtyInput.val(inputQty);

					// 상세화면 수량
                    domQty.val( inputQty );
                    qty = qty * (-1);

                }else{
                    isEnable    = true;
                    inputQty++;
                    summaryQty++;

					// 총금액반영
					objSumAll.html( sumAll + price);
					// 계산금액반영
                    objTotalAmt.html( totalAmt + price);

                    var cart		= $('.myBasket');
                    var locLeft		= cart.offset().left;
                    var loctop		= cart.offset().top;
                    var imgtodrag   = $(element).parents('.popBody').find("img").eq(0);

                    if( imgtodrag.length == 0){
                        imgtodrag = $("#carousel").find("img").eq(0);
                        isDetail = true;
                    }

                    if (imgtodrag) {
                        var imgclone = imgtodrag.clone().offset({

								top: imgtodrag.offset().top,
								right: imgtodrag.offset().right
							}).css({
								'opacity'	: '0.5',
								'position'	: 'fixed',
								'height'	: '150px',
								'width'		: '150px',
								'z-index'	: '100'
							}).appendTo($('body')).animate({
								'top'		: parseFloat(loctop) + 10,
								'right'		: parseInt(locLeft),
								'width'		: 75,
								'height'	: 75
						}, 1000, 'easeInOutExpo');
                        // console.log(cart.offset().top, cart.offset().left);

                        //console.log(summaryQty);

                        imgclone.animate({ 'width': 0, 'height': 0 }, function () {
                            $(this).detach()
                        });

						// 리스트의 수량
						objListQtyInput.val(inputQty);

                        domQty.val( inputQty );

                    }
                }

                var data = {
                    ID					: localStorage.getItem("ID"),
                    TEL					: localStorage.getItem("TEL"),
                    PRODUCT_CORP_ID     : p_corp_id,
                    REF_PRODUCT_CD      : productCd,
                    QTY                 : qty,
                    now_qty             : inputQty,
                }

                // console.log(data);
                //return false;
                myApp.basket.addObjectItem(data);

                // 모든 장바구니 카운터 동기화
                myApp.commonController.syncBasketQty(summaryQty);

				return;

            };
        });

	},

	// 댓글 목록 바인딩
	// 사용자별 댓글 중복 입력 방지
    listComment : function(page, data, pOption){

		var modal       = page.querySelector('ons-modal');
		var dialogC		= page.querySelector('#comment-dialog');
        var jsonData    = data.list;
        var objList     = $(page).find("#list-comment");
        var htmlData    = "";
		var product_cd	= "";

        objList.empty();

        if( jsonData.length > 0){

            htmlData    = "";
            $(jsonData).each(function(i){

				product_cd	= this.PRODUCT_CD;

                htmlData +="<div class='listItem listData cmt" + i + "' data-userid='" + this.USER_ID + "' data-product_cd='" + this.PRODUCT_CD + "' data-idx='" + this.IDX + "'>"
                         + "    <div class='left title' >"
                         + "	   <span class='listName'><i class='fa fa-commenting-o'></i>" + this.name  + "</span>"
                         + "	   <span class='listContent red'><i class='fa fa-star'></i>" + this.PRODUCT_STARTS  + "</span>"
                         + "       <span class='right txtRight'>" + this.CREATED_AT + "</span>"
                         + "    </div>"
                         + "    <div class='center listContent ellipsis'>" + this.PRODUCT_COMMENT + "</div>"
                         + "</div>";
            });
            objList.html(htmlData);

        }else{
            if(objList.find(".emptyOrNoData").length == 0){
                htmlData = "<div class='emptyOrNoData card'><i><img src='img/svg/caution.svg'></i><span data-i18n='COMM:noMoreData'>더이상 데이터가 존재 하지 않습니다<span></div>";
            }
			objList.html(htmlData);
        }


		objList.off("click", ".listItem", null );
		objList.on("click", ".listItem", function(){
			if( $(this).data('userid') == localStorage.getItem("USER_ID") ){
				dialogC.show();
			};
		});

        $(page).find(".after-list").css("display", "none");

		// 다이어로그 닫기
		$(dialogC).on("click", ".btnDialogCancelComment", function(){
			dialogC.hide();
		});

		// 댓글삭제
		$(dialogC).on("click", ".btnDialogRemoveComment", function(){


			// 별점/댓글 삭제
			var service		= new withWSDL("api/order/list/" + '');
			var data		= {

				CORP_ID     : localStorage.getItem("CORP_ID"),
				USER_ID     : localStorage.getItem("USER_ID"),
				PRODUCT_CD  : product_cd,
			}

			service.success = function(_jsonData){
				var jsonData    = $.parseJSON(_jsonData);
				dialogC.hide();
				objList.find(".listData[data-userid='" + data.USER_ID + "']" ).remove();
			};


			service.ajaxGetData(data);

		});

		// 별점, 댓글을 등록할 수 있는 지 확인 (구매 이력 확인)
		var service4	= new withWSDL("api/order/list");
		service4.success = function(_jsonData){

			var jsonData    = _jsonData.list;
			var userid		= localStorage.getItem("USER_ID");
			var objList     = $(page).find("#list-comment");

			$(modal).find(".btnSetpointer").prop("disabled", false);

			// 구매이력 확인
			// console.log( $(jsonData).length );
			if( $(jsonData).length ==  0){
                runWarningMessage('구매입력이 없어서 입력할수 없습니다', "warning", 1000, "up")
                $(page).find(".btnAddComment").prop("disabled", true);
			}else{
                $(page).find(".btnAddComment").prop("disabled", false);
            }


			$(jsonData).each(function(){

				$(modal).find(".btnAddComment").attr("data-orderid",  this.ORDER_IDX);

                if( !isNull(userid) ){

                    if( userid.trim().indexOf( this.USER_ID ) >= 0 ){
    
                        // 댓글 중복 방지 : 버튼 비활성화
                        objList.find(".listItem").each(function(){
                            if( userid.trim().indexOf( $(this).data("userid") ) >= 0 ){
                                $(modal).find(".btnSetpointer").prop("disabled", true);
                                return false;
                            }
                        });
                    }
                }
			});
		}

		var chkData = {
            "tel"           : localStorage.getItem("TEL"),
			"corp_id"       : localStorage.getItem("CORP_ID"),
			"user_id"       : localStorage.getItem("USER_ID"),
			"product_cd"	: pOption.PRODUCT_CD,
			//"BRANCH_ID"         : "BRANCH_01"
		}

		service4.ajaxGetData(chkData);
    },

    // 주문버튼 활성화체크 메서드
    checkGrpOrderButton : function(page){

        var t_ended_prd = myApp.i18Next.getTrasTxt("ORD", "ENDED_PRD");
        var t_min_qty   = myApp.i18Next.getTrasTxt("BSK", "MIN_ORD_QTY");
        
        var nQty = $(page).find("#qty").val();
        nQty = !isNaN( getfilterNumber(nQty)) ? getfilterNumber(nQty) : 0;
        nQty = parseInt(nQty);

        var minQty  = $(page).find(".btnRadius").attr("data-minqty");
        minQty      = isNaN(minQty) ? 0 : parseInt(minQty);
        
        var ntmOut  = $(page).find(".sale_timeoff").length;
        

        if( ntmOut > 0){
            runWarningMessage(t_ended_prd , "warning", 2000, "bottom");
        }

        var alertMsg    = $("<div style='text-align:right'></div>");        
        if( minQty > qty){
            alertMsg.append( t_min_qty + " : <span class='red'>" + minQty + "</span>개" );
        }

        // 최소수량 및 판매기간 정상일경우
        if( nQty >= minQty && ntmOut == 0 && nQty > 0){
            $(page).find(".btnGrpOrder").prop("disabled", false);
            $(page).find(".minOrderMsg").hide();
            return true;
        }else{
            $(page).find(".btnGrpOrder").prop("disabled", true);
            $(page).find(".minOrderMsg").show();
            return false;   
        }
    },

    getCountDowntimer: function(page, dday){
        
        
        var countDownDate = moment(dday).valueOf();
        // console.log("countDownDate" + countDownDate)
        var x = setInterval(function() {

            var now   = moment().valueOf();
            // console.log("console : " + now + ", countDownDate : " + countDownDate);
            
            var distance = countDownDate - now;
            var days    = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours   = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);
        
            function showRemaining() {
                if(days < 0) {
                    days = 0;
                    hours = 0;
                    minutes = 0;
                    seconds = 0;
                    clearInterval(x);
                }

                if( days <= 0 && hours <= 0 && minutes <= 0 && seconds <= 0 ){
                    $(page).find(".btnGrpOrder").prop("disabled", true);
                    $(page).find(".btnGrpOrder").addClass("sale_timeoff");
                }

                $(page).find('.ctDay').html(days);
                $(page).find('.ctTime').html(hours);
                $(page).find('.ctMin').html(minutes);
                $(page).find('.ctSec').html(seconds);
            }
            showRemaining();
        }, 1000);

    }
}
