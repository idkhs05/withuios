window.serviceModel = {

    listGrpOrder : function(page, data, domain, option){
        var dialogRemove    = page.querySelector("#removeOrder-dialog");
        var jsonData		= data.list;
        var reg_dt          = "";
        var orderId         = "";

        if( option.clear == 1 || option.clear == "1"){
            $(page).find(".grpList div").remove();
        }else{
            $(page).find("grpList .emptyOrNoData").remove();
        }

        if( jsonData.length > 0){

            // NOTE: jquery 두번 실행 방지로 click event off 시킴            
            $(page).find(".grpList").off("click", ".btnOrderRemove");
            $(page).find(".after-list").show();

            var lang_cal_am     = myApp.i18Next.getTrasTxt("ORD","AMT");
            var lang_cal_amt    = myApp.i18Next.getTrasTxt("ORD","CAL_AMT");
            var lang_reg_dt     = myApp.i18Next.getTrasTxt("ORD","REG_DT");

            var lang_delv       = myApp.i18Next.getTrasTxt("ORD","DELV_NM");
            var lang_addr       = myApp.i18Next.getTrasTxt("ORD","DELV_ADDR");
            var lang_dp_dt      = myApp.i18Next.getTrasTxt("ORD","DPT_DT");
            var lang_list       = myApp.i18Next.getTrasTxt("ORD","LIST");
            var lang_detail     = myApp.i18Next.getTrasTxt("ORD","DETAIL");
            var lang_cncl       = myApp.i18Next.getTrasTxt("ORD","CNCL");

            $(jsonData).each(function(){    
                
                var htmlData    = [];
                corpDiv     = this.CORP_DIV;
                isEnable    = this.IS_PROVE_YN ? "enalble" : "btnDisabled";
                corpCd      = this.CORP_ID;
                corpNm      = getLang(this.CORP_NM, this.CORP_NM_VN, this.CORP_NM_EN);
                productNm   = getLang(this.PRODUCT_NM, this.PRODUCT_NM_VN, this.PRODUCT_NM_VN);
                
                totalAmt    = isNaN(this.TOTAL_AMT) ? 0 : parseInt(this.TOTAL_AMT);
                reg_dt      = moment(this.CREATED_AT).format("YY-MM-DD HH:mm");
                imgSrc      = domain + "/" + this.IMG_URL_SM;
                url			= validUrl(this.URL) ? " class='exLink' data-url='" + this.URL + "'" : '' ;
                orderId     = this.GORDER_IDX;

                qty         = isNull( this.QTY )  ? 0 : this.QTY ;
                unit        = isNull( this.PRICE ) ? 0 : this.PRICE ;
                status      = this.ORDER_STATE;

                dName       = this.D_NAME ;
                dTelNo      = this.D_TEL ;

                s_corp_nm   = getLang( this.CORP_NM, this.CORP_NM_VN, this.CORP_NM_EN);
                dName       = isNull( dName  ) ? s_corp_nm : "" ;
                dTelNo      = isNull( dTelNo ) ? this.TEL1 : "";

                dMinuts     = this.WISH_DT;

                status      = this.ORDER_STATE;
                memo        = isNull( this.MEMO) ? "" : this.MEMO;
                var wpay        = isNull( this.PAY_OPTION_COIN) ? 0 : this.PAY_OPTION_COIN ;
                var cash        = isNull( this.PAY_OPTION_CASH) ? 0 : this.PAY_OPTION_CASH ;
                var card        = isNull( this.PAY_OPTION_CARD) ? 0 : this.PAY_OPTION_CARD ;
                var calAmt      = cash + card - wpay;

                addr1       = isNull( this.ADDR1) ? "" : this.ADDR1; 
                addr2       = isNull( this.ADDR2) ? "" : this.ADDR2; 
                
                htmlData.push("<div class='card dataList " + orderId + "' data-orderid='" + orderId + "' data-status='" + status + "' data-branchid='" + this.BRANCH_ID + "'>");
                htmlData.push("    <div class='top'>");
                htmlData.push("        <div class='list-item__title'><span class='sublabel'><i class='fa fa-angle-right'></i> " + lang_cal_am + " : </span> <strong class='totAmt'>" + Number(totalAmt).currency() + "</strong>");
                htmlData.push("        <span class='list-item__title_date'><span class='sublabel'><i class='fa fa-angle-right'></i> " + lang_reg_dt + " : </span> " + reg_dt + "</span></div>");
                htmlData.push("    </div>");
                htmlData.push("    <div class='center'>");
                htmlData.push("        <div class='delvAmt'><span class='subtitle'>CASH </span> <span class='amtOption'>" + Number(cash).currency() + "</span>" );
                htmlData.push("                             <span class='subtitle'>CARD </span> <span class='amtOption'>" + Number(card).currency() + "</span>" + "</div>" );
                htmlData.push("        <hr>" );
                htmlData.push("        <div class='delvAmt'><span class='subtitle'>&nbsp;</span> <span class='amtOption'>&nbsp;</span><span class='subtitle'>" + lang_cal_amt + "</span> <span class='amtOption green'>" + Number(calAmt).currency() + "</span></div>" );
                htmlData.push("        <h5><i class='fa fa-map-signs'></i> " + lang_detail + "</h5>" );
                htmlData.push("        <div class='orderDetailInfo'>");
                htmlData.push("             <div class='memo'><span class='subtitle'>Memo.</span> <span class=''>" + memo + "</span></div>" );
                htmlData.push("             <div class='delvInfo'><span class='subtitle'>" + lang_delv + "</span> <span><strong class='totAmt'>" + dName + "</strong></span><span class='delvTel'>" + " <i class='fa fa-phone'></i> <a href='tel:" + dTelNo + "'>" + dTelNo + "</a></span></div>" );
                htmlData.push("             <div class='delvTime'><span class='subtitle'>" + lang_dp_dt + "</span> <span> <span class='delvTm'>" + dMinuts + "</span>" + "</div>" );
                htmlData.push("             <div class='adddrInfo'><span class='subtitle'>"+ lang_addr + "</span> <span class='delvddr'>" + addr2 + ", " + addr1 + "</span>" + "</div>" );
                htmlData.push("        </div>");
                htmlData.push("        <h5><i class='fa fa-list'></i> " + lang_list + "</h5>" );
                htmlData.push("        <ul class='orderSummary'></ul>" );
                htmlData.push("    </div>" );
                htmlData.push("        <div class='right'>" );
                htmlData.push("		        <div class='segment'>" );
                htmlData.push("			        <ons-button class='new' modifier='large--cta'></ons-button>" );
                htmlData.push("			        <ons-button class='ing' modifier='large--cta'></ons-button>" );
                htmlData.push("			        <ons-button class='end' modifier='large--cta'></ons-button>" );
                htmlData.push("		        </div>" );
                htmlData.push("        </div>" );
                htmlData.push("</div>" );

                htmlData.push("<div class='card bottom " + orderId + "'>" );
                htmlData.push("    <ons-button class='btnUndoGrpOrder' modifier='cta' data-orderid='" + orderId + "' >" + lang_cncl + "</ons-button>" );
                htmlData.push("</div>");

                $(page).find(".grpList").append(htmlData.join(''));

                var orderDt = moment(reg_dt);
                // HH:24시간 설정 / hh:12시간 설정
                var nowDt	= moment().format("YY-MM-DD HH:mm:ss");
                var diffDt	= Math.abs(moment.duration(orderDt.diff(nowDt)).asMinutes()) ;
                //console.log( diffDt );
                $objUndoBtn	    = $(page).find(".grpList .card.bottom." + orderId + " > .btnUndoGrpOrder");
                //$objRefundBtn	= $(page).find(".card.bottom." + orderId + " > .btnRefund");
                $objDeleteBtn   = $(page).find(".grpList [data-orderid='" + orderId + "']  .btnOrderRemove");
                // $objLocBtn      = $(page).find(".grpList [data-orderid='" + orderId + "']  .btnOrderMap");
                $statusName     = $(page).find(".grpList [data-orderid='" + orderId + "']  .statusName");
                //status = "ORDER_DELV_ING"
                // $objLocBtn.hide();

                // 주문 후 종료시 취소불가(정확히 3분안으로 수정)
                // || status.indexOf("ING")

                dday    = isNaN(this.DDAY) ? 0 : this.DDAY;
                dmin    = isNaN(this.DMIN) ? 0 : this.DMIN; 

                // 주문가능일자 인경우 취소가능
                if( dday >= 0 && dmin >= 0) {
                    $objUndoBtn.prop('disabled',false); // 취소가능
                }else{
                    $objUndoBtn.prop('disabled',true);  // 취소불가
                }

                // 새주문일경우(주문접수중)
                if( status.indexOf("NEW") >= 0 ){
                    $statusName.append(" <img src='img/button/ajax-loader.gif' />");   

                    $(page).find(".grpList [data-orderid='" + orderId + "'] .segment .new").html(this.ORD_STATUS_NM2);
                }

                // 주문접수중일 경우 배달자 정보 설정
                if( status.indexOf("ORD_ING") >= 0 ){
                    $(page).find(".grpList [data-orderid='" + orderId + "'] .delvInfo").hide();
                }else{
                    $(page).find(".grpList [data-orderid='" + orderId + "'] .delvInfo").show();
                }

                // 취소 불가 (중/완료일경우는 disabled)
                if( status.indexOf("CNCL") >= 0 ){
                    $objUndoBtn.prop('disabled',true);
                }

                // 배달시 배달원 위치 조회
                if( status.indexOf("DELV_ING") >= 0 ){
                    $statusName.append("<i class='winphone-128x2' />");
                    // $objLocBtn.show();
                }

                // 상태값 현지화
                statNm  = getLang(this.ORD_STATUS_NM, this.ORD_STATUS_NM2, this.ORD_STATUS_NM3);

                // NOTE: (주문완료, 환불완료, 주문취소완료) 시 삭제 가능
                if( status.indexOf("END") >= 0){
                    $objDeleteBtn.prop('disabled',false);
                    $objDeleteBtn.show();
                    window.serviceModel.setOrderSts(page, orderId, status, statNm, this.DLV_END_TM, null, this.DELV_CORP_ID);

                }else{
                    $objDeleteBtn.prop('disabled',true);
                    $objDeleteBtn.hide();
                    window.serviceModel.setOrderSts(page, orderId, status, statNm, this.RECEPTION_DT, dMinuts, this.DELV_CORP_ID);
                }

                var htmlData1   = [];
                // 브랜드명
                p_corpNm    = getLang(this.P_CORP_NAME, this.P_CORP_NAME_VN, this.P_CORP_NAME_EN);
                g_pName     = getLang(this.GIFT_PRODUCT_NM, this.GIFT_PRODUCT_NM_VN, this.GIFT_PRODUCT_NM_EN);
                g_corpNm    = getLang(this.GIFT_CORP_NM, this.GIFT_CORP_NM_VN, this.GIFT_CORP_NM_EN);
                
                htmlData1.push("<li class='listItem' data-product_cd='" + this.PRODUCT_CD + "' data-sale_corp_id='" + this.CORP_ID + "' data-qty='" + this.QTY + "' data-price='" + this.PRICE + "' data-unit='" + this.UNIT+ "'>");
                htmlData1.push("    <span class='item__subtitle itemName'>" );
                htmlData1.push("        <span class='item_corpInfo'><span class='saleCorpNm'>" + corpNm + "</span></span>" );
                htmlData1.push("        " + p_corpNm + " " + productNm + " <b class='red'> ("+ qty + ")</b>" + "</span>" );
                // htmlData1.push("        <img class='item_prod' width='20px' src='" + domain + "/" + this.IMG_URL_SM + "' />" + p_corpNm + " " + productNm + " <b class='red'> ("+ qty + ")</b>" + "</span>" );
                htmlData1.push("    </span>" );
                htmlData1.push("    <span class='itemQty' >" + parseInt(unit).currency() + " </span>");
                htmlData1.push("</li>");
                           
                if( !isNull(this.GIFT_ID) ){
                    htmlData1.push("<li class='listItem' data-product_cd='" + this.GIFT_PRODUCT_CD + "' data-sale_corp_id='" + this.CORP_ID + "' data-qty='" + this.GIFT_QTY + "'>" );
                    htmlData1.push("    <span class='item__subtitle itemName'>" );
                    htmlData1.push("        <span class='item_corpInfo'><span class='saleCorpNm'>" + g_corpNm + "</span></span>" );
                    htmlData1.push("        " + g_pName + " <b class='red'> ("+ this.GIFT_QTY + ")</b>" + "</span>" );
                    htmlData1.push("    </span>" );
                    htmlData1.push("    <span class='itemQty' >" + parseInt(0).currency() + " </span>" );
                    htmlData1.push("</li>");
                }

                $(page).find(".grpList .dataList." + orderId + " .orderSummary").append(htmlData1.join(''));

            });

            if( jsonData.length < 30){
                noneMessage = "<div class='emptyOrNoData card'><i><img src='img/svg/caution.svg'></i><span data-i18n='COMM:noMoreData'> 더이상 등록된 주문내역이 없습니다. <span></div>";
                $(page).find(".grpList").append(noneMessage);
                $(page).find(".after-list").hide();
            }

            // 클릭 : 주문내역 삭제하기 버튼 다이어로그 열기
            $(page).find(".grpList").on("click", ".btnOrderRemove", function(){

                var orderId = $(this).closest(".dataList").data("orderid");
                var objList = $(this).closest(".dataList." + orderId );
                var status  = objList.data("status");

                $(dialogRemove).attr("data-orderid", orderId);
                if( status.indexOf("END") >= 0 ){
                    dialogRemove.show();
                }
            });

        }else{

            if( $(page).find(".grpList .emptyOrNoData").length == 0 ){
                noneMessage = "<div class='emptyOrNoData card'><i><img src='img/svg/caution.svg'></i><span data-i18n='COMM:noMoreData'> 더이상 등록된 주문내역이 없습니다. <span></div>";
            }

            $(page).find(".grpList").append(noneMessage);
            $(page).find(".after-list").css("display", "none");
        }
    },

    // 상태값에 대한 세그먼트 상태 설정
    setOrderSts : function(page, orderId, status, statusName, rcDt, dMinuts, delvCorpId){

        var lang_ready     = myApp.i18Next.getTrasTxt("ORD","READY");
        var lang_ing       = myApp.i18Next.getTrasTxt("ORD","ING");
        var lang_complete  = myApp.i18Next.getTrasTxt("ORD","COMPLETE");

        switch(status){

            case "ORD_NEW" : 
            case "ORD_ING" : 
                $(page).find(".grpList [data-orderid='" + orderId + "'] .segment .new").html(statusName).addClass("blink_me").addClass("chLine");                
                $(page).find(".grpList [data-orderid='" + orderId + "'] .segment .ing").html(lang_ing);
                $(page).find(".grpList [data-orderid='" + orderId + "'] .segment .end").html(lang_complete);

                page.querySelector("[data-orderid='" + orderId + "'] .segment .ing").setAttribute('disabled', true);
                page.querySelector("[data-orderid='" + orderId + "'] .segment .end").setAttribute('disabled', true);

                window.serviceModel.grpDeliveryUpdateTime(page, orderId, rcDt, dMinuts, delvCorpId);
            break;

            case "ORD_DELV_ING" : 
                $(page).find(".grpList [data-orderid='" + orderId + "'] .segment .new").html(lang_ready);
                $(page).find(".grpList [data-orderid='" + orderId + "'] .segment .ing").html("<img height='30px' src='img/map/bycle1.png'/>" + statusName).addClass("blink_me").addClass("chLine");
                $(page).find(".grpList [data-orderid='" + orderId + "'] .segment .end").html(lang_complete);

                page.querySelector(".grpList [data-orderid='" + orderId + "'] .segment .new").disabled;
                page.querySelector(".grpList [data-orderid='" + orderId + "'] .segment .end").disabled;

                window.serviceModel.grpDeliveryUpdateTime(page, orderId, rcDt, dMinuts, delvCorpId);
            break;

            case "ORD_DELV_END" : 
            case "ORD_CNCL_NEW" : 
            case "ORD_CNCL_END" : 
            case "ORD_RECV_END" : 
                $(page).find(".grpList [data-orderid='" + orderId + "'] .segment .new").html(lang_ready);
                $(page).find(".grpList [data-orderid='" + orderId + "'] .segment .ing").html(lang_ing);
                $(page).find(".grpList [data-orderid='" + orderId + "'] .segment .end").html(statusName).addClass("chLine");

                page.querySelector(".grpList [data-orderid='" + orderId + "'] .segment .new").disabled;
                page.querySelector(".grpList [data-orderid='" + orderId + "'] .segment .ing").disabled;
                
                window.serviceModel.grpDeliveryEndTime(page, orderId, rcDt);
            break;

            default : 
                
                page.querySelector(".grpList [data-orderid='" + orderId + "'] .segment .new").disabled;
                page.querySelector(".grpList [data-orderid='" + orderId + "'] .segment .ing").disabled;
                page.querySelector(".grpList [data-orderid='" + orderId + "'] .segment .end").disabled;
                

        }   
    }, 

    // 배달자 도착시간 정보 표기 (WITH_00 가 아닐경우외부로 간주)
    grpDeliveryUpdateTime(page, orderId, rcDt, dMinuts, delvCorpId) {

        // delvCorpId 가 null 일경우 아직 접수전 상태임
        if( !isNull(delvCorpId) ){
            if( delvCorpId != APP_CORP_ID){
                var extDelving  = myApp.i18Next.getTrasTxt("ORD", "EXT_DELVING");
                $(page).find(".grpList [data-orderid='" + orderId + "'] .delvTime span.green").text(extDelving).addClass("blink_me");
                return;
            }
        }
    }, 

    // 도착완료시간
    grpDeliveryEndTime(page, orderId, endDt){
        var complete_time  = myApp.i18Next.getTrasTxt("ORD", "COMP_TIME");        
        $(page).find(".grpList [data-orderid='" + orderId + "'] .delvTime span.green").text(complete_time);
    }
}
