window.serviceModel = {

    listAddr : function(page, data, option){
        
        var jsonData    = data.list;
        var htmlData    = "";
        var city        = "";
        var district    = "";
        var building    = "";
        var branch_id   = "";
        var branch_nm   = "";

        var t_local     = myApp.i18Next.getTrasTxt("LOCATION", "LOCAL_MSG");
        var t_buiding   = myApp.i18Next.getTrasTxt("LOCATION", "CITY_MSG");
        var t_store     = myApp.i18Next.getTrasTxt("LOCATION", "STORE_MSG");

        if( jsonData.length > 0){

            // 시를 변경시 하단의 select 초기화
            
            if( option.level == 2 ){
                $(page).find("#district select").html("<option value=''><span>" + t_local + "</span></option>");
                $(page).find("#building select").html("<option value=''><span>" + t_buiding + "</span></option>");
                $(page).find("#branch   select").html("<option value=''><span>" + t_store + "</span></option>");
                $(page).find("#addr1").val("");
            }

            // 구를 변경시 하단의 select 초기화
            if( option.level == 3 ){
                $(page).find("#building select").html("<option value=''><span>" + t_buiding + "</span></option>");
                $(page).find("#branch   select").html("<option value=''><span>" + t_store + "</span></option>");
                $(page).find("#addr1").val("");
            }

            // 건물을 변경시 하단의 select 초기화
            if( option.level == 4 ){
                $(page).find("#branch   select").html("<option value=''><span>" + t_store + "</span></option>");
                $(page).find("#addr1").val("");
            }

            $(jsonData).each(function(){
                
                if( option.level == 1){

                    city        = this.CITY;
                    htmlData    = "<option value='" + city + "'>" +
                                  "   <span class=''>" + city  + "</span>" +
                                  "</option>";
                                  
                    $(page).find("#city select").append(htmlData);
                    
                    $(page).find("#city ons-select").removeAttr("disabled");
                    $(page).find("#district select option[value !='']").remove();
                    $(page).find("#building select option[value !='']").remove();
                    $(page).find("#branch   select option[value !='']").remove();

                    if( jsonData.length == 1){
                        $(page).find("#city select").val(city);
                        $(page).find("#city select").trigger("change");
                    }

                }else if( option.level == 2){

                    district    = this.DISTRICT;
                    htmlData    = "<option value='" + district + "'>" +
                                  "   <span class=''>" + district  + "</span>" +
                                  "</option>";
                    $(page).find("#district select").append(htmlData);
                    $(page).find("#district ons-select").removeAttr("disabled");
                    $(page).find("#building select option[value !='']").remove();
                    $(page).find("#branch   select option[value !='']").remove();

                }else if( option.level == 3){

                    building    = this.BUILDING_NM;
                    htmlData    = "<option value='" + building + "'>" +
                                  "   <span class=''>" + building  + "</span>" +
                                  "</option>";
                    $(page).find("#building select").append(htmlData);
                    $(page).find("#building ons-select").removeAttr("disabled");
                    $(page).find("#branch   select option[value !='']").remove();

                }else if( option.level == 4){

                    branch_id   = this.BRANCH_ID;
                    branch_nm   = this.BRANCH_NM
                    htmlData    = "<option value='" + branch_id + "'>" +
                                  "   <span class=''>" + branch_nm  + "</span>" +
                                  "</option>";
                    $(page).find("#branch ons-select").removeAttr("disabled");
                    $(page).find("#branch   select").append(htmlData);

                }else if( option.level == 5){

                    var addr1 = !isNull(this.ADDR1) ? this.ADDR1 : "";
                    var addr2 = !isNull(this.ADDR2) ? this.ADDR2 : "";

                    $(page).find("#addr1").val(addr1 + " " + addr2);
                    $(page).find("#message").val(this.RMK);
                }
            });

        }else{
            var t_nodata = myApp.i18Next.getTrasTxt("COMM", "noData");
            runWarningMessage(t_nodata, "warning", 1500, "up");
        }
  
    }
}