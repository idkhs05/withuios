
window.serviceModel = {

    listOrder : function(page, data, domain, option){

        var dialogRemove    = document.querySelector("#removeOrder-dialog");
        var jsonData		= data.list;

        var reg_dt          = "";
        var orderId         = "";
        var oldOrderId      = "";

        if( option.clear == 1 || option.clear == "1"){            
            $(page).find(".commonList").empty();
        }else{
            $(page).find(".commonList .emptyOrNoData").remove();
        }

        if( jsonData.length > 0){
            // NOTE: jquery 두번 실행 방지로 click event off 시킴            
            // $(page).find(".commonList").off("click", ".btnOrderMap");
            $(page).find(".commonList").off("click", ".btnOrderRemove");
            $(page).find(".after-list").show();

            var lang_cal_am     = myApp.i18Next.getTrasTxt("ORD","AMT");
            var lang_reg_dt     = myApp.i18Next.getTrasTxt("ORD","REG_DT");

            var lang_delv       = myApp.i18Next.getTrasTxt("ORD","DELV_NM");
            var lang_dlev_amt   = myApp.i18Next.getTrasTxt("ORD","DLV_AMT");
            var lang_added_amt  = myApp.i18Next.getTrasTxt("ORD","ADD_DELV_AMT");
            var lang_cal_amt    = myApp.i18Next.getTrasTxt("ORD","CAL_AMT");

            var lang_dp_dt      = myApp.i18Next.getTrasTxt("ORD","DPT_DT");
            var lang_addr       = myApp.i18Next.getTrasTxt("ORD","DELV_ADDR");
            var lang_bonus      = myApp.i18Next.getTrasTxt("COIN","BONUS");

            var lang_minuts     = myApp.i18Next.getTrasTxt("ORD","MINUTS");
            var lang_abtTm      = myApp.i18Next.getTrasTxt("ORD","ABOUT_TIME");

            var lang_cncl       = myApp.i18Next.getTrasTxt("ORD","CNCL");
            var lang_copy       = myApp.i18Next.getTrasTxt("ORD","COPY");
            
            var lang_list       = myApp.i18Next.getTrasTxt("ORD","LIST");
            var lang_detail     = myApp.i18Next.getTrasTxt("ORD","DETAIL");

            $(jsonData).each(function(){
                
                //console.log(jsonData.length);
                var htmlData    = [];
                corpCd      = this.CORP_ID;
                corpNm      = getLang(this.CORP_NM, this.CORP_NM_VN, this.CORP_NM_EN);
                productNm   = getLang(this.PRODUCT_NM, this.PRODUCT_NM_VN, this.PRODUCT_NM_EN);
                
                totalAmt    = isNaN(this.TOTAL_AMT) ? 0 : parseInt(this.TOTAL_AMT);
                reg_dt      = moment(this.CREATED_AT).format("YY-MM-DD HH:mm");
                imgSrc      = domain + "/" + this.IMG_URL_SM;
                // url			= validUrl(this.URL) ? " class='exLink' data-url='" + this.URL + "'" : '' ;
                oldOrderId  = this.ORDER_IDX

                qty         = isNull( this.QTY )  ? 0 : this.QTY ;
                unit        = isNull( this.UNIT ) ? 0 : this.UNIT ;
                status      = this.ORDER_STATE;
                
                dName       = isNull( this.D_NAME ) ? "" : this.D_NAME ;
                dTelNo      = this.D_TEL;

                s_corp_nm   = getLang( this.SALE_CORP_NM, this.SALE_CORP_NM_VN, this.SALE_CORP_NM_EN);
                dName       = isNull( dName  ) ? s_corp_nm : "" ;
                dTelNo      = isNull( dTelNo ) ? this.TEL1 : "";
                dTelNo      = isNull( dTelNo ) ? "" : dTelNo;
                
                dMinuts     = isNull( this.DELV_TIME) ? "0" : Math.ceil(this.DELV_TIME);
                memo        = isNull( this.MEMO) ? "" : this.MEMO;
                baseDelvFee = isNull( this.BASE_DELV_FEE) ? 0 : this.BASE_DELV_FEE ;
                addedFee    = isNull( this.ADDED_DELV_FEE) ? 0 : this.ADDED_DELV_FEE ;
                addedDist   = isNull( this.ADDED_DELV_DIST) ? 0 : this.ADDED_DELV_DIST ;

                var wpay        = isNull( this.PAY_OPTION_COIN) ? 0 : this.PAY_OPTION_COIN ;
                var cash        = isNull( this.PAY_OPTION_CASH) ? 0 : this.PAY_OPTION_CASH ;
                var card        = isNull( this.PAY_OPTION_CARD) ? 0 : this.PAY_OPTION_CARD ;
                var calAmt      = cash + card + baseDelvFee + addedFee - wpay;

                addr1       = isNull( this.ADDR1) ? "" : this.ADDR1; 
                addr2       = isNull( this.ADDR2) ? "" : this.ADDR2; 
                if( orderId != oldOrderId ){
                    status      = this.ORDER_STATE;
                    //totAmt    = 0;
                    orderId   = oldOrderId;
                    htmlData.push("<div class='card dataList " + orderId + "' data-orderid='" + orderId + "' data-status='" + status + "' data-branchid='" + this.BRANCH_ID + "'>");
                    htmlData.push("    <div class='top'>");
                    htmlData.push("        <div class='list-item__title'><span class='sublabel'><i class='fa fa-angle-right'></i> " + lang_cal_am + " : </span><strong class='totAmt'> " + Number(totalAmt).currency() + "</strong>" );
                    htmlData.push("        <span class='list-item__title_date'><span class='sublabel'><i class='fa fa-angle-right'></i> " + lang_reg_dt + " : </span> " + reg_dt + "</span></div>" );
                    htmlData.push("        <div class='f_right'><ons-button modifier='cta' class='btnOrderRemove'><i class='fa fa-trash-o'></i>&nbsp;</ons-button></div>" );
                    htmlData.push("    </div>" );
                    htmlData.push("    <div class='center'>" );
                    
                    htmlData.push("        <div class='delvAmt'><span class='subtitle'>CASH</span> <span class='amtOption'>" + Number(cash).currency() + "</span>" );
                    htmlData.push("                             <span class='subtitle'>CARD</span> <span class='amtOption'>" + Number(card).currency() + "</span>" + "</div>" );
                    htmlData.push("        <div class='delvAmt'><span class='subtitle'>" + lang_dlev_amt + "</span> <span class='amtOption'>" + Number(baseDelvFee).currency() + "</span>" );
                    htmlData.push("                             <span class='subtitle'>" + lang_bonus    + "</span> <span class='amtOption'>" + Number(wpay).currency() + "</span>" + "</div>" );
                    htmlData.push("        <hr>" );
                    htmlData.push("        <div class='delvAmt'><span class='subtitle'>" + lang_added_amt+ "</span> <span class='amtOption'>" + Number(addedFee).currency() + "</span>");
                    htmlData.push("                             <span class='subtitle'>" + lang_cal_amt + "</span> <span class='amtOption green'>" + Number(calAmt).currency() + "</span></div>" );

                    htmlData.push("        <h5><i class='fa fa-map-signs'></i> " + lang_detail + "</h5>" );
                    htmlData.push("        <div class='orderDetailInfo'>");
                    htmlData.push("             <div class='memo'><span class='subtitle'>Memo. </span> <span class=''>" + memo + "</span></div>" );
                    htmlData.push("             <div class='delvInfo'><span class='subtitle'>" + lang_delv + "</span> <span><strong class='totAmt'>" + dName + "</strong></span><span class='delvTel'>" + " <a href='tel:" + dTelNo + "'>" + dTelNo + "</a> <i class='fa fa-phone'></i></span></div>" );
                    htmlData.push("             <div class='delvTime'><span class='subtitle'>" + lang_dp_dt + "</span> <span class='delvTm'></span> <span class='amtOption'> (" + lang_abtTm + " <b class='red'>" + dMinuts + "</b> " + lang_minuts + " ) </span>" + "</div>" );
                    htmlData.push("             <div class='adddrInfo'><span class='subtitle'>"+ lang_addr + "</span> <span class='delvddr'>" + addr2 + ", " + addr1 + "</span>" + "</div>" );
                    htmlData.push("        </div>");
                    htmlData.push("        <h5><i class='fa fa-list'></i> " + lang_list + "</h5>" );
                    htmlData.push("        <ul class='orderSummary'></ul>" );
                    htmlData.push("    </div>" );
                    htmlData.push("        <div class='right'>" );
                    htmlData.push("		        <div class='segment'>" );
                    htmlData.push("			        <ons-button class='new' modifier='large--cta'></ons-button>" );
                    htmlData.push("			        <ons-button class='ing' modifier='large--cta'></ons-button>" );
                    htmlData.push("			        <ons-button class='end' modifier='large--cta'></ons-button>" );
                    htmlData.push("		        </div>" );
                    htmlData.push("         </div>" );
                    htmlData.push("</div>" );
                    htmlData.push("<div class='card bottom " + orderId + "'>" );
                    htmlData.push("    <ons-button class='btnUndoOrder' modifier='cta' data-orderid='" + orderId + "' >" + lang_cncl + "</ons-button>" );
                    htmlData.push("    <ons-button class='btnReOrder'   modifier='cta' data-orderid='" + orderId + "' >" + lang_copy + "</ons-button>" );
                    htmlData.push("</div>");

                    $(page).find(".commonList").append(htmlData.join(''));

					var orderDt = moment(reg_dt);
					// HH:24시간 설정 / hh:12시간 설정
					var nowDt	= moment();
					var diffDt	= Math.abs(moment.duration(orderDt.diff(nowDt)).asMinutes()) ;
					//console.log( diffDt );
                    $objUndoBtn	    = $(page).find(".commonList .card.bottom." + orderId + " > .btnUndoOrder");
                    //$objRefundBtn	= $(page).find(".card.bottom." + orderId + " > .btnRefund");
                    $objDeleteBtn   = $(page).find(".commonList [data-orderid='" + orderId + "']  .btnOrderRemove");
                    // $objLocBtn      = $(page).find(".commonList [data-orderid='" + orderId + "']  .btnOrderMap");
                    $statusName     = $(page).find(".commonList [data-orderid='" + orderId + "']  .statusName");
					//status = "ORDER_DELV_ING"
                    // $objLocBtn.hide();

                    // 주문 후 2분이 지나면 취소불가(정확히 3분안으로 수정)
                    // || status.indexOf("ING")
                    if( diffDt > 2.9 ) {
                        $objUndoBtn.prop('disabled',true);  // 취소불가
                    }else{
                        $objUndoBtn.prop('disabled',false); // 취소가능
                    }

                    // 새주문일경우(주문접수중)
                    if( status.indexOf("NEW") >= 0 ){
                        $statusName.append(" <img src='img/button/ajax-loader.gif' />");   
                        $(page).find(".commonList [data-orderid='" + orderId + "'] .segment .new").html(this.STATUS_NM);
                    }

                    // 주문접수중일 경우 배달자 정보 설정
                    if( status.indexOf("ORD_ING") >= 0 ){
                        $(page).find(".commonList [data-orderid='" + orderId + "'] .delvInfo").hide();
                    }else{
                        $(page).find(".commonList [data-orderid='" + orderId + "'] .delvInfo").show();
                    }

                    // 취소 불가 (중/완료일경우는 disabled)
                    if( status.indexOf("CNC") >= 0 ){
                        $objUndoBtn.prop('disabled',true);
                    }

                    // 배달시 배달원 위치 조회
                    if( status.indexOf("DELV_ING") >= 0 ){
                        $statusName.append("<i class='winphone-128x2' />");
                        $objUndoBtn.prop('disabled',true);
                        // $objLocBtn.show();
                    }

                    // 상태값 현지화
                    statNm  = getLang(this.STATUS_NM, this.STATUS_NM_VN, this.STATUS_NM_EN);

                    // NOTE: (주문완료, 환불완료, 주문취소완료) 시 삭제 가능
                    if( status.indexOf("END") >= 0){
                        $objDeleteBtn.prop('disabled',false);
                        $objDeleteBtn.show( { animation : "none" } );
                        window.serviceModel.setOrderSts(page, orderId, status, statNm, this.DLV_END_TM, null, this.DELV_CORP_ID);

                    }else{
                        $objDeleteBtn.prop('disabled',true);
                        $objDeleteBtn.hide();
                        window.serviceModel.setOrderSts(page, orderId, status, statNm, this.RECEPTION_DT, dMinuts, this.DELV_CORP_ID);
                    }
                }
                
                sCorpNm      = getLang(this.SALE_CORP_NM, this.SALE_CORP_NM_VN, this.SALE_CORP_NM_EN);
                var htmlOrder  = [];
                // 브랜드명
                p_corpNm    = getLang(this.P_CORP_NAME, this.P_CORP_NAME_VN, this.P_CORP_NAME_EN);
                p_corpNm    = isNull(p_corpNm) ? "" : p_corpNm;

                htmlOrder.push("<li class='listItem' data-product_cd='" + this.PRODUCT_CD + "' data-sale_corp_id='" + this.SALE_CORP_ID + "' data-qty='" + this.QTY + "' data-stock='" + this.SUM_QTY + "' data-unit='" + this.UNIT+ "'>" );
                // htmlOrder.push("    <i class='fa fa-angle-right'></i>");
                htmlOrder.push("    <span class='item__subtitle itemName'>" );
                htmlOrder.push("        <span class='item_corpInfo'><span class='saleCorpNm'>" + sCorpNm + "</span></span>" );
                htmlOrder.push("        " + p_corpNm + " " + productNm + " <b class='red'> ("+ qty + ")</b>" + "</span>" );
                // htmlOrder.push("        <img class='item_prod' width='20px' src='" + domain + "/" + this.IMG_URL_SM + "' />" + p_corpNm + " " + productNm + " <b class='red'> ("+ qty + ")</b>" + "</span>" );
                htmlOrder.push("        <span class='itemQty' >" + parseInt(unit).currency() );
                htmlOrder.push("        <ons-button class='btnReOrderItem' modifier='cta' data-product_cd='" + this.PRODUCT_CD + "' data-sale_corp_id='" + this.SALE_CORP_ID + "' data-qty='" + this.QTY + "' data-stock='" + this.SUM_QTY + "'>" );
                htmlOrder.push( "           <svg version='1.1' id='cart' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' viewBox='0 0 98 104.2' style='enable-background:new 0 0 98 104.2;' xml:space='preserve'>" );
                htmlOrder.push( "               <path id='wheel2' class='st0' d='M38.7,77.2c-7.5,0-13.5,6-13.5,13.5s6,13.5,13.5,13.5s13.5-6,13.5-13.5C52.2,83.3,46.1,77.3,38.7,77.2z M38.7,95.2c-2.5,0-4.5-2-4.5-4.5s2-4.5,4.5-4.5s4.5,2,4.5,4.5C43.2,93.2,41.2,95.2,38.7,95.2z'/>" );
                htmlOrder.push( "               <path id='wheel1' class='st0' d='M76.3,77.2c-7.5,0-13.5,6-13.5,13.5s6,13.5,13.5,13.5s13.5-6,13.5-13.5C89.7,83.3,83.7,77.3,76.3,77.2z M76.3,95.2c-2.5,0-4.5-2-4.5-4.5s2-4.5,4.5-4.5s4.5,2,4.5,4.5C80.8,93.2,78.7,95.2,76.3,95.2z'/>" );
                htmlOrder.push( "               <path id='cartBody' class='st0' d='M96.4,25.9c-1.2-1.4-2.9-2.2-4.7-2.2h-67L20.9,3.7C20.5,1.5,18.7,0,16.5,0h-12C2,0,0,2,0,4.5S2,9,4.5,9h8.3l5.7,30.2l5,29.1c0.5,3.1,3.1,5.4,6.2,5.5h55.7c3.1-0.1,5.8-2.4,6.2-5.4l6.4-37.1C98.2,29.3,97.6,27.4,96.4,25.9zM83,64.8H32L27.7,40c0-0.1,0-0.2,0-0.2c0,0,0-0.1,0-0.2l-0.4-2.2l-0.8-4.7h62L83,64.8z'/>" );
                htmlOrder.push( "           </svg>" );
                htmlOrder.push("        </ons-button>" );
                htmlOrder.push("    </span>" );
                htmlOrder.push("</li>");

                // console.log(htmlData); CORP_IMG, SALE_CORP_NM

                $(page).find(".commonList .dataList." + orderId + " .orderSummary").append(htmlOrder.join(''));
                //$(page).find(".dataList." + orderId + " .totAmt").text(totAmt.currency());

            });

            
            if( jsonData.length < 10){
                noneMessage = "<div class='emptyOrNoData card'><i><img src='img/svg/caution.svg'></i><span data-i18n='COMM:noMoreData'><span></div>";
                $(page).find(".commonList").append(noneMessage);
                $(page).find(".after-list").hide();
            }

            // 클릭 : 주문내역 삭제하기 버튼 다이어로그 열기
            $(page).find(".commonList").on("click", ".btnOrderRemove", function(){

                var orderId = $(this).closest(".dataList").attr("data-orderid");
                var objList = $(this).closest(".dataList." + orderId );
                var status  = objList.data("status");

                $(dialogRemove).attr("data-orderid", orderId);
                if( status.indexOf("END") >= 0 ){
                    dialogRemove.show( { animation : "none" } );
                }
            });
            
        }else{

            if( $(page).find(".commonList .emptyOrNoData").length == 0 ){
                noneMessage = "<div class='emptyOrNoData card'><i><img src='img/svg/caution.svg'></i><span data-i18n='COMM:noMoreData'> 더이상 등록된 주문내역이 없습니다. <span></div>";
            }

            $(page).find(".commonList").append(noneMessage);
            $(page).find(".after-list").hide();
        }
    }, 

    // 상태값에 대한 세그먼트 상태 설정
    setOrderSts : function(page, orderId, status, statusName, rcDt, dMinuts, delvCorpId){

        var lang_ready     = myApp.i18Next.getTrasTxt("ORD","READY");
        var lang_ing       = myApp.i18Next.getTrasTxt("ORD","ING");
        var lang_complete  = myApp.i18Next.getTrasTxt("ORD","COMPLETE");
        
        switch(status){

            case "ORD_NEW" : 
            case "ORD_ING" : 
                $(page).find(".commonList [data-orderid='" + orderId + "'] .segment .new").html(statusName).addClass("blink_me").addClass("chLine");                
                $(page).find(".commonList [data-orderid='" + orderId + "'] .segment .ing").html(lang_ing);
                $(page).find(".commonList [data-orderid='" + orderId + "'] .segment .end").html(lang_complete);

                page.querySelector(".commonList [data-orderid='" + orderId + "'] .segment .ing").setAttribute('disabled', true);
                page.querySelector(".commonList [data-orderid='" + orderId + "'] .segment .end").setAttribute('disabled', true);

                // 접수준비중일때는 예상도착시간을 표기하지 않음
                // window.serviceModel.deliveryUpdateTime(page, orderId, rcDt, dMinuts, delvCorpId);
            break;

            case "ORD_DELV_ING" : 
                $(page).find(".commonList [data-orderid='" + orderId + "'] .segment .new").html(lang_ready);
                $(page).find(".commonList [data-orderid='" + orderId + "'] .segment .ing").html("<img height='30px' src='img/map/bycle1.png'/>" + statusName).addClass("blink_me").addClass("chLine");
                $(page).find(".commonList [data-orderid='" + orderId + "'] .segment .end").html(lang_complete);

                page.querySelector(".commonList [data-orderid='" + orderId + "'] .segment .new").disabled;
                page.querySelector(".commonList [data-orderid='" + orderId + "'] .segment .end").disabled;

                window.serviceModel.deliveryUpdateTime(page, orderId, rcDt, dMinuts, delvCorpId);
            break;

            case "ORD_DELV_END" : 
            case "ORD_CNCL_NEW" : 
            case "ORD_CNCL_END" : 
            case "ORD_RECV_END" : 
                $(page).find(".commonList [data-orderid='" + orderId + "'] .segment .new").html(lang_ready);
                $(page).find(".commonList [data-orderid='" + orderId + "'] .segment .ing").html(lang_ing);

                $(page).find(".commonList [data-orderid='" + orderId + "'] .segment .end").html(statusName).addClass("chLine");

                page.querySelector(".commonList [data-orderid='" + orderId + "'] .segment .new").disabled;
                page.querySelector(".commonList [data-orderid='" + orderId + "'] .segment .ing").disabled;
                
                window.serviceModel.deliveryEndTime(page, orderId, rcDt);
            break;

            default : 
                
                page.querySelector(".commonList [data-orderid='" + orderId + "'] .segment .new").disabled;
                page.querySelector(".commonList [data-orderid='" + orderId + "'] .segment .ing").disabled;
                page.querySelector(".commonList [data-orderid='" + orderId + "'] .segment .end").disabled;
                

        }   
    }, 

    // 배달자 도착시간 정보 표기 (WITH_00 가 아닐경우외부로 간주)
    deliveryUpdateTime(page, orderId, rcDt, dMinuts, delvCorpId) {

        // delvCorpId 가 null 일경우 아직 접수전 상태임
        if( !isNull(delvCorpId) ){
            if( delvCorpId != APP_CORP_ID){
                
                var extDelving  = myApp.i18Next.getTrasTxt("ORD", "EXT_DELVING");
                $(page).find(".commonList [data-orderid='" + orderId + "'] .delvTime span.green").text(extDelving).addClass("blink_me");
                return;
            }
        }
        
        if( dMinuts >= 0){
            dMinuts = parseInt(dMinuts);
            $(page).find(".commonList [data-orderid='" + orderId + "'] .delvTm").html( 
                moment(rcDt).add(dMinuts, 'minutes').format('MM-DD HH:mm') + "~" +
                moment(rcDt).add(( dMinuts + 15 ), 'minutes').format('HH:mm')
            );
        }
        
    }, 

    // 도착완료시간
    deliveryEndTime(page, orderId, endDt){        
        
        var complete_time  = myApp.i18Next.getTrasTxt("ORD", "COMP_TIME");
        $(page).find(".commonList [data-orderid='" + orderId + "'] .delvTime span.green").text(complete_time);
        $(page).find(".commonList [data-orderid='" + orderId + "'] .delvTm").html(moment(endDt).format('MM-DD HH:mm'));
    }
}
