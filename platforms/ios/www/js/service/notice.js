var service	= new withWSDL();

window.serviceModel = {

    listNotice : function(page, data, domain){
        
        var jsonData    = data.list;
        var htmlData    = [];
        var imgSrc      = "";
        var idx         = "";
        var div         = "";
        var noneMessage = "<div class='emptyOrNoData card'><i><img src='img/svg/caution.svg'></i><span data-i18n='COMM:noMoreData'> 더이상 데이터가 존재 하지 않습니다<span></div>";

        if( jsonData.length > 0){

            $(page).find(".after-list").show();

            $(jsonData).each(function(){
                
                idx         = this.IDX;
                div         = this.BRD_CLS_CODE;
                imgSrc   = domain + "\\" + this.FILE_URL;
                img      = "";
                if(!isNull(this.FILE_URL)){
                    img = "<img src='" + imgSrc + "' width='100%' >" ;
                }
                
                ytbCode     = this.YOUTUBE_CODE;
                ytbIcon     = ""; 

                if( !isNull(ytbCode) ){
                    ytbIcon     = "<img class='ytbIco' src='/img/logo/youtube_icon25x25.png' >";
                }
                
                htmlData.push("<div class='card container listData' data-div='" + div + "' data-idx='" + idx + "' >" + img + ytbIcon);
                htmlData.push("     <div class='card listItem'>" );
                htmlData.push("         <div class='left title' data-seq='" + this.IDX + "'>" );
                htmlData.push("             <div class='list-item__title orange'>" + this.TITLE + "</div>" );
                htmlData.push("             <div class='list-item__subtitle'>" );
                htmlData.push("                 <span class='blue'>" + moment(this.CREATED_AT).format("YYYY-MM-DD HH:mm") + "</span>" );
                htmlData.push("             </div>" );
                htmlData.push("         </div>" );
                htmlData.push("         <div class='right txtRight cnts'>" + this.CONTENT + "</div>");
                htmlData.push("     </div>" );
                htmlData.push("</div>");
            });
            $(page).find(".dataList").append(htmlData);

            if( jsonData.length < 10){
                $(page).find(".dataList").append(noneMessage);
                $(page).find(".after-list").hide();
            }

        }else{
            
            if($(page).find(".dataList").find(".emptyOrNoData").length == 0){
                htmlData.push(noneMessage);
            }
            $(page).find(".dataList").append(htmlData.join(''));
            $(page).find(".after-list").hide();
        }

        // 디테일 페이지 이동
        Array.prototype.forEach.call(page.querySelectorAll(".dataList .listData"), function(ele) {
            ele.onclick = function() {
                var div     = $(this).data("div");
                var idx     = $(this).data("idx");
                var data    = { "DIV" : div,  "IDX" : idx }
                myApp.navigator.pushPage('html/notice/detail.html', { data : data });  
            };
        });
    },

    listEvent : function(page, data, domain){
        
        var jsonData    = data.list;
        var htmlData    = [];
        var noneMessage = "<div class='emptyOrNoData card'><i><img src='img/svg/caution.svg'></i><span data-i18n='COMM:noMoreData'> 더이상 데이터가 존재 하지 않습니다<span></div>";

        if( jsonData.length > 0){
            
            var imgSrc      = "";
            var idx         = "";
            var div         = "";

            $(jsonData).each(function(){
                
                idx         = this.IDX;
                div         = this.BRD_CLS_CODE;


                imgSrc   = domain + "\\" + this.FILE_URL;
                img      = "";
                if(!isNull(this.FILE_URL)){
                    img = "<img src='" + imgSrc + "' width='100%' />" ;
                }

                ytbCode     = this.YOUTUBE_CODE;
                ytbIcon     = ""; 

                if( !isNull(ytbCode) ){
                    ytbIcon     = "<img class='ytbIco' src='/img/logo/youtube_icon25x25.png' >";
                }
                
                htmlData.push("<div class='card container listData' data-div='" + div + "' data-idx='" + idx + "' >" + img + ytbIcon);
                htmlData.push("     <div class='card listItem'>");
                htmlData.push("         <div class='left title' data-seq='" + this.IDX + "'>");
                htmlData.push("             <div class='list-item__title orange'>" + this.TITLE + "</div>");
                htmlData.push("             <div class='list-item__subtitle'>");
                htmlData.push("                 <span class='blue'>" + moment(this.CREATED_AT).format("YYYY-MM-DD HH:mm") + "</span>");
                htmlData.push("             </div>");
                htmlData.push("         </div>");
                htmlData.push("         <div class='right txtRight cnts'>" + this.CONTENT + "</div>");
                htmlData.push("     </div>");
                htmlData.push("</div>");    
            });

            $(page).find(".dataList").append(htmlData.join(''));

            if( jsonData.length < 10){
                $(page).find(".dataList").append(noneMessage);
                $(page).find(".after-list").hide();
            }

        }else{
            if($(page).find(".dataList").find(".emptyOrNoData").length == 0){
                htmlData.push(noneMessage);
            }
            $(page).find(".dataList").append(htmlData.join(''));
            $(page).find(".after-list").css("display", "none");
        }

        // 디테일 페이지 이동
        Array.prototype.forEach.call(page.querySelectorAll(".dataList .listData"), function(ele) {
            ele.onclick = function() {
                var div     = $(this).data("div");
                var idx     = $(this).data("idx");
                var data    = { "DIV" : div,  "IDX" : idx }
                myApp.navigator.pushPage('html/notice/detail.html', { data : data });  
            };
        });
  
    },


    detailNotice : function(page, data, domain){
        
        var jsonData    = data.list;
        var htmlData    = "";
        var imgSrc      = "";

        var youtubeCode = null;
        var youtubeHtml = "";

        if( jsonData.length > 0){

            $(page).find(".pTit").html(jsonData[0].TITLE);
            $(page).find(".date").html(jsonData[0].CREATED_AT);
            $(page).find(".mCt").html(jsonData[0].CONTENT);
            $(page).find(".hTitle").html( jsonData[0].BRD_CLS_CODE == "notice" ? "공지사항" : "이벤트" );

            youtubeCode     = jsonData[0].YOUTUBE_CODE;
            if( !isNull(youtubeCode) ){
                youtubeHtml =   '<iframe class="yt_player_iframe" width="100%" height="250" src="https://www.youtube.com/embed/' + youtubeCode +  '?rel=0&enablejsapi=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
            }
            
            htmlData    = youtubeHtml;
            
            if( jsonData[0].FILE_URL !== null && typeof jsonData[0].FILE_URL !== undefined) {
                imgSrc      = domain + "\\" + jsonData[0].FILE_URL;
                htmlData += "<div class=''>" +
                                "<img src='" + imgSrc + "' width='100%' />" + 
                            "</div>";
            }
            
            $(page).find(".iCt").append(htmlData);
        }        
    },

    // 한인소식 조회 PDF 링크 ()
    listNews : function(page, data, domain){
        
        var jsonData    = data.list;
        var htmlData    = "";
        var imgSrc      = "";
        var idx         = "";
        var div         = "";

        if( jsonData.length > 0){
            $(jsonData).each(function(){
                
                idx         = this.IDX;
                div         = this.BRD_CLS_CODE;

                htmlData += "<div class='card container listData' data-div='" + div + "' data-idx='" + idx + "' >" +
                                "<div class='card listItem'>" +
                                "    <div class='left title' data-seq='" + this.IDX + "'>" +
                                "        <i class='fa fa-newspaper-o'></i>" + 
                                "        <div class='listContent list-item__title orange'>" + this.TITLE + "</div>" + 
                                "    </div>" +
                                "    <div class='right txtRight newsFrame'>" + this.LINK_ADDR + "</div>" +
                                "</div>" + 
                            "</div>";
            });

            $(page).find(".dataList").append(htmlData);
        }else{
            if($(page).find(".dataList").find(".emptyOrNoData").length == 0){
                //htmlData += "<div class='emptyOrNoData card'><i class='fa fa-commenting-o'></i>&nbsp;<span data-i18n='COMM:noMoreData'>더이상 데이터가 존재 하지 않습니다<span></div>";
                htmlData = "<div class='emptyOrNoData card'><i><img src='img/svg/caution.svg'></i><span data-i18n='COMM:noMoreData'> 더이상 데이터가 존재 하지 않습니다<span></div>";
            }
            $(page).find(".dataList").append(htmlData);
            $(page).find(".after-list").css("display", "none");
        }

        $(page).find('.newsFrame').each(function(){
            $(this).find("iframe").css({
                "width" : "100%",
                "height" : "auto",
            })
        });
    },

    // 메인 배너 동적 바인딩
    listBanner : function(page, data, domain){
        var jsonData    = data.list;
        // console.log(jsonData);
        if( jsonData.length > 0){
            
            $(jsonData).each(function(){

                if(this.IS_BANNER_YN == "Y"){
                 
                    var htmlData    = "";
                    var idx         = this.IDX;
                    var div         = this.BRD_CLS_CODE;
                    var isTop       = this.IS_TOP_BOTTOM ;
                    var title       = this.TITLE;
                    var imgSrc      = "";
                    var objImg      = "";

                    //if( !isNull( this.FILE_URL ) ){
                        
                    imgSrc  = domain + "\\" + this.FILE_URL;
                    objImg  = "<img src='" + imgSrc + "' width='100%' data-div='" + div + "' data-idx='" + idx + "' data-title='" + title + "' />" ;
                    //}
                    
                    htmlData += "<div class='swiper-slide goEvent'>" + objImg + "</div>";
                    
                    if( isTop.indexOf("Y") >= 0){
                        $(page).find(".topBanner").append(htmlData);
                    }else if( isTop.indexOf("N") >= 0 ) {
                        $(page).find(".bottomBanner").append(htmlData);
                    }
                    else if( isTop.indexOf("F") >= 0  ){

                        var EventData = [];

                        EventData.push("<div class='goEvent'>");
                        EventData.push("    <img src='" + imgSrc + "' width='100%'  data-div='" + div + "' data-idx='" + idx + "' data-title='" + title + "' />" );
                        EventData.push("    <div class='imgText'>" + title + "</div>" );
                        EventData.push("    <div class='imgText content'>" + this.CONTENT + "</div>");
                        EventData.push("</div>");
                        
                        $(page).find(".bt_banner").append(EventData.push(''));
                    }
                }
            });

            // 상단 배너 클릭 And 상세페이지로 이동
            Array.prototype.forEach.call(page.querySelectorAll(".goEvent > img"), function(ele) {
                ele.onclick = function() {

                    let div		= $(this).data("div");
                    let idx		= $(this).data("idx");
					let	title	= $(this).data("title");
                    let data    = { "DIV" : div ,  "IDX" : idx, "title" : title }
                    myApp.navigator.pushPage('html/notice/detail.html', { data : data });  
                };
            });

            // 메인 슬라이드 배너
            let swiper = new Swiper('.swiper-container', {
                pagination: {
                    el: '.swiper-pagination',
                    type: 'fraction',
                    dynamicBullets: true,
                },
                autoplay: {
                    delay: 3000,
                    // disableOnInteraction: false,
                },
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
            });
        }
    },

    // 팝업 동적 바인딩
    listPopup : function(page, data, domain){
        var jsonData    = data.list;
        var modalAdPop  = document.querySelector('#adPopup');
        var isPopYn     = false;

        if( jsonData.length > 0){
            
            var popHtml = [];
            $(jsonData).each(function(i){

                if( this.IS_M_POP_YN == "Y"){

                    if (isPopYn == false ){
                        popHtml.push("<ons-carousel id='carousel' swipeable auto-scroll overscrollable initial-index='0'>");
                        isPopYn = true;
                    }
                    
                    var idx        = this.IDX;
                    var div        = this.BRD_CLS_CODE;
                    var imgSrc     = domain + "/" + this.FILE_URL;
                    var title      = this.TITLE;
                    var ctt        = this.CONTENT;
                   
                    // if( !isNull(this.YOUTUBE_CODE) ) {
                    //         // youtube api를 이용해서 개발해야함 : 2019-08-20 하아....too tired..
                    //         // popHtml +=  "<ons-carousel-item class='carousel-item' >" +
                    //         //             '   <iframe width="320" height="240" src="https://www.youtube.com/embed/' + this.YOUTUBE_CODE +  '" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>' +
                    //         //             "</ons-carousel-item>";
                    // }else{

                    if( !isNull(imgSrc)){
                        popHtml.push("<ons-carousel-item class='carousel-item goEvent'>");
                        popHtml.push("    <img src='" + imgSrc + "' height='205px' data-div='" + div + "' data-idx='" + idx + "' data-title='" + title + "' />");
                        popHtml.push("</ons-carousel-item>");
                    }else{
                        popHtml.push("<ons-carousel-item class='carousel-item goEvent'>" );
                        popHtml.push("    <div class='popUpBody'>" );
                        popHtml.push("       <h4>" + title + "</h4>" );
                        popHtml.push("       <div>" + ctt + "</div>" );
                        popHtml.push("   </div>" );
                        popHtml.push("</ons-carousel-item>");
                    }
                    // }
                }
            });

            if(isPopYn){
                popHtml.push("</ons-carousel>");
                popHtml.push("<a class='left carousel-control carousel-control-left' href='#myCarousel'><i class='fa fa-angle-left'></i></a>");
                popHtml.push("<a class='right carousel-control carousel-control-right' href='#myCarousel'><i class='fa fa-angle-right'></i></a>");
    
                $(page).find("#adPopup .popBody").append(popHtml.join(''));
            }

            // 상단 배너 클릭 And 상세페이지로 이동
            Array.prototype.forEach.call(page.querySelectorAll(".goEvent > img"), function(ele) {
                ele.onclick = function() {

                    let div		= $(this).data("div");
                    let idx		= $(this).data("idx");
                    let	title	= $(this).data("title");
                    let data    = { "DIV" : div ,  "IDX" : idx, "title" : title }
                    myApp.navigator.pushPage('html/notice/detail.html', { data : data });
                };
            });
        }

        if( isPopYn && jsonData.length > 0 ){
            const carousel = page.querySelector('#carousel');
            carousel.addEventListener('postchange', function () {
                var index = carousel.getActiveIndex();
                const dots = page.querySelectorAll('.dot');
                for (dot of dots) {
                    dot.innerHTML = dot.id === 'dot' + index ? '&#9679;' : '&#9675;';
                }
            });

            Array.prototype.forEach.call(page.querySelectorAll('.dot'), function(element) {
                element.onclick = function(event){
                    carousel.setActiveIndex(Number(event.target.id.slice(-1)));
                }
            });

            // 메인팝업 좌우 슬라이드 버튼
            page.querySelector(".carousel-control-left").onclick = function(){ carousel.prev(); }
            page.querySelector(".carousel-control-right").onclick = function(){ carousel.next(); }

            modalAdPop.show();

        }else{
            modalAdPop.hide();
        }

        // 화면 팝업 오늘 하루 보이지 않기
        Array.prototype.forEach.call(page.querySelectorAll('.btnClosePopup'), function(element) {
            element.onclick = function() {
                var isChecked = $(this).parents("ons-modal").find("input:checkbox").is(":checked");
                if(isChecked){
                    setCookie( "DsafeNoOpen1", "1" , 1);
                }
                $(this).parents("ons-modal").hide();
                // $(this).parents("ons-modal").find(".popBody").empty();
            };
        });
    }
}
