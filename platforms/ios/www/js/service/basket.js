var service	= new withWSDL();

window.serviceModel = {


    listBasket : function(page, data, domain, option, callBasektList){

        var diagbasketRemove    = document.querySelector("#basketRemove-dialog");
        var serviceBasketRemove	= new withWSDL('api/basket/list/' + 0 );

        var jsonData        = data.list;
        var jsonCoin        = data.coin;
        var jsonNxtDt       = data.nextdt;
        var nextDt          = "";
        // 예약가능시간 : 쉬는업체가 있을 경우 제일 빠른 예약시간을 할당
        if( !isNull(jsonNxtDt)){
            var nextDt      = isNull(jsonNxtDt[0].NEXT_DT) ? "" : jsonNxtDt[0].NEXT_DT;
        }
        $(page).find("input[name='NEXT_DT']").val(nextDt);

        var objSumAll	    = $(page).find(".sumAll");      // 상품합계
        var objSumEtc       = $(page).find(".sumEtc");      // 기타상품합계
        var objTotalAmt		= $(page).find(".total");       // 계산금액
        
        var sumAll          = 0;
        var sumEtc          = 0;
        var delvAmt         = BASE_DELV_FEE; 
        var basketCount     = jsonData.length;
        var t_st_out_msg    = myApp.i18Next.getTrasTxt("BSK", "ST_LIMIT_MSG");
        WITHU_PAY           = jsonCoin;
        
        $(page).find(".myCoin").text(parseInt(WITHU_PAY).format());

        if( option.CLEAR == 1){
            $(page).find(".imageList ons-list-item").remove();
        }

        var htmlData    = [];
        
        if( basketCount > 0){

            $(page).find(".after-list").css("display", "none");
            $(diagbasketRemove).find(".btnDialogRemoveBasketOk").off("click");

            $(page).find(".imageList .emptyOrNoData").remove();
            $(page).find(".imageList ons-list-item").remove();

            sumAll = 0;
            sumEtc = 0;

            var imgSrc = "", rmk = "";
            var t_cook_time = myApp.i18Next.getTrasTxt("PRD", "COOK_TIME");
            var t_min       = myApp.i18Next.getTrasTxt("ORD", "MINUTS");

            $(jsonData).each(function(){

                imgSrc      = domain + "\\" + this.IMG_URL_SM;
                url         = validUrl(imgSrc) ? imgSrc : '' ;
                rmk         = getLang(this.RMK, this.RMK_VN, this.RMK_EN);

                sale		= isNull(this.EVENT_DISC_RATE) ? "" : this.EVENT_DISC_RATE.toFixed(1);
                qtySum		= isNull(this.QTY_SUM) || this.QTY_SUM < 0 ?  0 : this.QTY_SUM;


                unit        = isNull(this.UNIT_NM) ? "EA" : this.UNIT_NM;

                eventAmt    = isNull(this.EVENT_AMT) ? 0 : this.EVENT_AMT;
                orAmt       = isNull(this.OUT_UNIT_PRICE) ? 0 : this.OUT_UNIT_PRICE;
                saleText    = "";

                is_pay      = isNull(this.IS_PAY_COIN) ? "N" : this.IS_PAY_COIN ;

                clsCss  = "";
                bskMsg  = "";
                
                if( qtySum < this.QTY ){
                    clsCss  = "css-st-bg";
                    bskMsg  = "<div class='st_msg red'>" + t_st_out_msg + "</div>"
                }

                if( !isNull(eventAmt) ){
                    if( !isNaN( parseInt(eventAmt) * parseInt(this.QTY ) ) ){

                        // 서비스상품은 최소주문금액에서 포함안시킴
                        if( this.IS_CONTAIN_MIN_AMT != "Y"){
                            sumEtc	+= parseInt(eventAmt) * parseInt(this.QTY );
                        }else{
                            sumAll	+= parseInt(eventAmt) * parseInt(this.QTY );
                        }
                    }
                }

                // 상품개별 최수구매수량
                minQty      = isNull(this.MIN_ORDER_QTY) ? 0 : this.MIN_ORDER_QTY;

                // 브랜드명
                brandNm     = getLang(this.P_CORP_NAME, this.P_CORP_NAME_VN, this.P_CORP_NAME_EN);

                st_none     = qtySum == 0 ? "st_none" : "";
                st_serv     = this.IS_CONTAIN_MIN_AMT != "Y" ? "st_service" : "";

                cook_msg    = getLang(this.COOK_MSG, this.COOK_MSG_VN, this.COOK_MSG_EN);
                // 조리여부
                cooking     = this.IS_COOKING == 'C_Y' || this.IS_COOKING == 'C_H' ? "<div class='cooking'>" + cook_msg + "</div>" : "";

                // 음식준비시간 
                readyTm     = isNull(this.PRODUCT_READY_TM) ? "" : this.PRODUCT_READY_TM;
                foodReady   = "";
                if( this.IS_FOOD == 'Y' && !isNull(readyTm) ){
                    foodReady   = "<div class='orderTime green'><i class='fa fa-clock-o'></i> " + t_cook_time + " " + readyTm + t_min + "</div>" ;
                }

                // 판매상태(New, 할인, Hot 등이 있을 경우 출력)
                if( !isNull(this.CD_NM)){
                    sale_state  = "<span class='overTip bRed'>" + this.CD_NM + "</span>" ;
                }else{
                    sale_state  = "";
                }

                new_product     = "";
                if( this.IS_NEW == "Y"){
                    new_product = "<span class='bRed new overTip'>NEW</span>";
                }

                dd1             = this.DD1;     // 판매시작 (현시간부로 지난시간) 가능시간은 판매시작시간보다 지나야함 (양수)
                dd2             = this.DD2;     // 판매종료 (현시간부로 지난시간) 가능시간은 판매종료시간보다 남아야함 (음수)
                sale_timeout    = "";

                if( dd1 >= 0 && dd2 <= 0){
                    sale_timeout = "sale_on";
                }else{
                    sale_timeout = "sale_off";
                }

                if( sale != ""){
                    saleText    =   "<del class='ori_price'>" + parseInt(orAmt).currency() + "</del>" +
                                    "<span class='sale'>" + sale + "<span class='unit'>%</span></span>" ;
                }

                st_msg      = myApp.i18Next.getTrasTxt("PRD", "NO_CONTAIN");
                st_service  = this.IS_CONTAIN_MIN_AMT != "Y" ? "<div class='isServiceProduct'>" + st_msg + "</div>" : "";
                favorite    = isNull(this.FAV_IDX) ? "fa-heart-o" : "fa-heart";

                isOff = window.serviceModel.isOnOffHolyCheck(this.HOLY_DAYS, this.ORDER_STATRT_TM, this.ORDER_END_TM);

                corp_nm     = "";
                // 검색화면/장바구니일 경우 회사명 표기
                if( !isNull(option.isShowCorp) && option.isShowCorp ){

                    corp_nm     = getLang(this.CORP_NM, this.CORP_NM_VN, this.CORP_NM_EN);
                    corp_nm     = "<span class='gray corpnm'>" + corp_nm + "</span>";
                }

                p_name          = getLang(this.PRODUCT_NM, this.PRODUCT_NM_VN, this.PRODUCT_NM_EN);

                ytbCode     = this.YOUTUBE_CODE;
                ytbUseYn    = this.YOUTUBE_USE_YN;
                ytbIcon     = ""; 

                if( !isNull(ytbCode) && ytbUseYn == "Y" ){
                    ytbIcon     = "ytb";
                }
                
                htmlData.push("<ons-list-item tappable data-minqty=" + minQty + " data-idx='" + this.IDX + "' data-product_cd='"+ this.PRODUCT_CD );
                htmlData.push("' data-ispay='" + is_pay + "' data-corp_cd='" + this.CORP_ID + "' data-idx='" + this.IDX + "' data-cate='" + this.P_CLS_IDX );
                htmlData.push("' data-p_corp_id='" +  this.PRODUCT_CORP_ID + "' class='listItem " + isOff + " " + st_serv + " " + st_none + " " + sale_timeout + " " + clsCss + "' >");
                htmlData.push("   <div class='left productImg " + ytbIcon + " ' data-product_cd='"+ this.PRODUCT_CD +"' data-product_cate='" + this.P_CLS_IDX + "'>");
                htmlData.push("       <img class='list-item__thumbnail " + st_none+ "' src='"+ imgSrc +"' >" + new_product );
                htmlData.push("   </div>");
                htmlData.push("   <div class='center productInfo' data-product_cd='"+ this.REF_PRODUCT_CD +"' data-price='" + this.EVENT_AMT + "'>");

                htmlData.push("       <div class='list-item__title'>" + corp_nm  + "<span class='brand gray'>" + brandNm + "</span>");
                htmlData.push("           <span class='productName'>" + p_name + "</span>");
                htmlData.push(            sale_state);
                htmlData.push("       </div>");
                htmlData.push("       <div class='list-item__subtitle'>" + rmk + "</div>");
                htmlData.push("       <div class='pd_price' data-price='" + eventAmt + "'>");
                htmlData.push("			<span class='cp'>" + parseInt(eventAmt).currency() + "</span>");
                htmlData.push(            saleText);
                htmlData.push("       </div>");
                htmlData.push("       <div class='pd_etc'><span class='star'>" );
                htmlData.push("           <i class='fa fa-star'></i>" + this.STAR + "</span>");
                htmlData.push("           <span style='display:none;' class='stock' data-stock='" + qtySum + "'>재고수량 : " + Number(qtySum).format()  + "" + unit + " </span>" );
                htmlData.push("       </div>");
                htmlData.push(         bskMsg);
                htmlData.push(         cooking);
                htmlData.push(         foodReady );
				htmlData.push("	</div>");
				htmlData.push("	<div class='buttonArea right'>");
                htmlData.push("       <div class='list-item__subtitle'>");
                htmlData.push("           <ons-button class='btnRadius btnMinusQty' data-minqty=" + minQty + "></ons-button>");
                htmlData.push("           <ons-input input-id='qty' data-product_cd='"+ this.PRODUCT_CD +"' class='qty' type='number' value=" + this.QTY + " readonly ></ons-input>");
                htmlData.push("           <ons-button class='btnRadius btnPlusQty' data-minqty=" + minQty + "></ons-button>");
                htmlData.push("       </div> ");
                htmlData.push(        st_service );
                htmlData.push("       <span class='heart'><i class='fa " + favorite + "'></i></span>" );
                htmlData.push("       <ons-button class='btnRmBasket' data-name='" + this.PRODUCT_NM + "' data-idx='"+ this.IDX + "'><i class='fa fa-trash-o'></i>&nbsp;</ons-button>");
                htmlData.push("   </div>");
                htmlData.push("</ons-list-item>");
            });

        }else{
            if( $(page).find(".imageList .emptyOrNoData").length == 0 ){
                htmlData.push("<div class='emptyOrNoData card'><i><img src='img/svg/caution.svg'></i><span data-i18n='COMM:noMoreData'> 장바구니에 등록된 상품이 없습니다. <span></div>");
            }
            $(page).find(".after-list").css("display", "none");
        }

        // 계산금액
        /*
        var objSumAll	    = $(page).find(".sumAll");      // 상품합계
        var objSumEtc       = $(page).find(".sumEtc");      // 기타상품합계
        var objMyCoin       = $(page).find(".myCoin");      // 보유코인
        var objUseCoin      = $(page).find("#coin");        // 코인사용
        var objTotalAmt		= $(page).find(".total");       // 계산금액
        */

        sumAll  = parseInt(sumAll);
        sumEtc  = parseInt(sumEtc);

        // 상품합계
        objSumAll.html( Number(sumAll).currency(0) );
        // 기타상품 합계
        objSumEtc.html( Number(sumEtc).currency(0) );
        
        // TODO 기본배송비 추가
        // 단, 배달비 제외 합계금액이 무료배송비용이상 일 경우 배달비 면제
        if ( (sumAll + sumEtc) >= FREE_DELV_AMT ){
            delvAmt = 0;
        }

        $(page).find(".baseDelvAmt").html( Number(delvAmt).currency(0) );

        objTotalAmt.html( Number(sumAll + sumEtc + delvAmt).currency(0) );

        // 상단 전체 합계
        $(page).find(".topTopAmt").text( Number(sumAll + sumEtc + delvAmt).currency(0) )  ;

        $(page).find(".imageList").append(htmlData.join(''));

        // 장바구니 삭제 여부 다이어로그 닫기
        $(diagbasketRemove).find(".btnDialogCancel").click(function(){
            diagbasketRemove.hide( { "animation": "none" } );
        });

        // 장바구니 삭제 여부 삭제 실행
        $(diagbasketRemove).find(".btnDialogRemoveBasketOk").on("click", function(){

            var idx = $(this).attr("data-idx");
            var option      = {
                idx : idx
            };
            
            serviceBasketRemove.ajaxDeleteData(option);
        });

        // 장바구니 삭제 성공 콜백
        serviceBasketRemove.success = function(_jsonData){

            var jsonData    = _jsonData;
            var data        = jsonData;
            if(data.status == 200 ){
                runWarningMessage(data.message, "success", 1500, "up");
                diagbasketRemove.hide( { "animation": "none" } );

                // 필추가해야함 : click 이벤트 재귀호출 방지
                $(diagbasketRemove).find(".btnDialogRemoveBasketOk").off('click');

                // 장바구니 조회
                callBasektList();

            }else{
                runWarningMessage(data.message, "danger", 1500, "up");
            }
        };

        // 즐겨찾기
        Array.prototype.forEach.call(page.querySelectorAll('ons-list-item .heart'), function(element) {

            element.onclick = function(target) {
                corpCd      = $(element).parents('.listItem').data("p_corp_id");
                productCd   = $(this).parents(".listItem").find(".productInfo").data("product_cd");
                myApp.controllers.addFavorite(corpCd, productCd, this);
            }

        });

        // 장바구니 삭제 버튼 => 다이어로그 활성화 (show)
        Array.prototype.forEach.call(page.querySelectorAll('.btnRmBasket'), function(element) {
            element.onclick = function(target) {
                var idx     = $(this).data("idx");
                var name    = $(this).data("name");

                $(diagbasketRemove).find(".btnDialogRemoveBasketOk").attr("data-idx", idx);
                $(diagbasketRemove).find(".btnDialogRemoveBasketOk").attr("data-name", name);
                //alert(productCd);
                diagbasketRemove.show({ "animation": "none" });
            }
        });

        // 장바구니 버튼 활성화 여부 : 최소주문금액 체크
        this.checkOrderButton(page)
        
        // 상품 추가/삭제 버튼 클릭 증감 이벤트
        Array.prototype.forEach.call(page.querySelectorAll('.btnRadius'), function(element) {

            element.onclick = function(target) {

                if( !showChkTel_Diag() ){
                    return false;
                }
                
                // 재고없음
                if( $(element).hasClass("st_none") ){
                    ons.notification.toast('<i class="fa fa-warning"></i> ' + ' SOLD OUT', { timeout: 500, animation: 'fall', 'class' : 'warning' });
                    return false;
                }

                $(element).prop("disabled", true);

                var cateCode    = $(this).parents('.listItem').data("cate");
                var productCd   = $(this).parents(".listItem").find(".productInfo").data("product_cd");
                var p_corp_id   = $(this).parents(".listItem").data("p_corp_id");
                
				var isService   = $(this).closest('.listItem.st_service').length > 0 ? "Y" : "N";
                var price		= $(this).parents('.listItem').find('.productInfo .pd_price').data("price");
                var minQty      = $(this).data("minqty");
                var qty         = 1;

                var isEnable    = false;
                var objSummaryQty  = null;

                if( cateCode === undefined){
                    cateCode = $("#carousel").data("cate");
                    isDetail = true;
                }

                // 탭바의 장바구니 숫자와 페이지에서의 숫자가 다름
                if( $(page).find(".summaryQty").length > 0 ){
                    objSummaryQty = $(page).find(".summaryQty");
                }else{
                    objSummaryQty = $("ons-tab .notification");
                }

                var summaryQty      = parseInt( objSummaryQty.html() );
                var domQty          = $(this).closest(".listItem").find("ons-input");
                var inputQty        = parseInt( getfilterNumber( domQty.val() ) );
                var totalAmt		= parseInt( getfilterNumber( $(page).find(".total").html()) );
				var sumAll			= parseInt( getfilterNumber( $(page).find(".sumAll").html()) );
                var sumEtc          = parseInt( getfilterNumber( $(page).find(".sumEtc").html()) );

                var delvBaseAmt     = parseInt( getfilterNumber( $(page).find(".baseDelvAmt").html()) );
                
                var t_min_ord_qty   = myApp.i18Next.getTrasTxt("BSK", "MIN_ORD_QTY");
                if( $(this).hasClass("btnMinusQty") ){

                    // 최소주문수량과 일치할경우
                    if( inputQty == minQty && inputQty != 0){
                        runWarningMessage(t_min_ord_qty + " : " + minQty , "warning", 1500, "fade");
                        inputQty    = 0;
                        summaryQty -= minQty;
                        
                        qty         = minQty;
                        isEnable    = true;
                    }else{

                        if( inputQty < 0 ){
                            inputQty    = 0;
                            isEnable    = false;
                        }else{

                            if( inputQty > 0 ){
                                inputQty--;
                                summaryQty--;
                                isEnable    = true;

                            }else{
                                inputQty = 0;
                            }
                        }
                    }
                    
                    price      = price * qty;

                    // 계산금액반영 (서비스상품일 경우 구분하여 계산해준다)
                    if( (totalAmt - price) >= 0){

                        if( isService == "Y"){
                            objSumEtc.html( Number(sumEtc - price).currency(0) );
                            objTotalAmt.html( Number(totalAmt - price).currency(0) );
                        }else{
                            //console.log(totalAmt ,price )
                            objSumAll.html( Number(sumAll - price).currency(0) );
                            objTotalAmt.html( Number(totalAmt - price).currency(0) );
                        }
                    }

                    domQty.val( inputQty );
                    qty = qty * (-1);

                }else{

                    // 최소주문수량이 있을경우
                    if( minQty != 0 && inputQty == 0){
                        runWarningMessage(t_min_ord_qty + " : " + minQty , "warning", 1500, "bottom");
                        inputQty    += minQty;
                        summaryQty  += inputQty;
                        qty         = minQty;
                    }else{
                        inputQty++;
                        summaryQty++;
                    }

                    price       = price * qty;

                    isEnable    = true;

                    if( isService == "Y"){
                        // 서비스상품반영
                        objSumEtc.html( Number(sumEtc + price).currency(0) );
                    } else{
                        // 총금액반영
                        objSumAll.html( Number(sumAll + price).currency(0)) ;
                    }

					// 계산금액반영
                    objTotalAmt.html( Number(totalAmt + price).currency(0) );

                    isDetail = true;
                    domQty.val( inputQty );
                    
                }

                var obj = {
                    p_corp_id   : p_corp_id,
                    productCd   : productCd,
                    qty         : qty,
                    isEnable    : isEnable,
                    summaryQty  : summaryQty,
                    now_qty     : inputQty
                }

                window.serviceModel.setProductBasket(page, obj );

                var cal_sumAll			= parseInt( getfilterNumber( $(page).find(".sumAll").html()) );
                var cal_sumEtc          = parseInt( getfilterNumber( $(page).find(".sumEtc").html()) );
                var delvAmt             = BASE_DELV_FEE;

                // console.log(delvAmt);
                
                // 합계(일반 + 서비스 상품) 이 무료배송일 경우 배달비 면제
                if ( (cal_sumAll + cal_sumEtc) >= FREE_DELV_AMT ){
                    delvAmt = 0;
                }
                // 계산된 배달비 입력
                $(page).find(".baseDelvAmt").html( Number(delvAmt).currency(0) );
                
                var cal_sum   = cal_sumAll + cal_sumEtc + delvAmt;

                // 상단 : 전체합계
                $(page).find(".topTopAmt").text( Number(cal_sum).currency(0) )  ;

                // 하단 : 지불금액 합계
                objTotalAmt.html( Number(cal_sum).currency(0) );

                setTimeout(function(){
                    $(element).prop("disabled", false);
                    // var sumAll2 = parseInt( getfilterNumber( $(page).find(".sumAll").html()) );
                    // $(page).find(".topTopAmt").text( Number(sumAll2 + delvAmt).currency())  ;
                }, 500)

            };
        });
    },

    // 주문버튼 활성화체크 메서드
    checkOrderButton : function(page){

        // 기본배송비 포함
        var objsumAll   = $(page).find(".sumAll");
        var sumResult   = !isNaN( getfilterNumber(objsumAll.html())) ? getfilterNumber(objsumAll.html()) : 0;
        var nStNone     = $(page).find("ons-list-item.st_none").length;
        var nHoly       = $(page).find("ons-list-item.off").length;
        var ntmOut      = $(page).find("ons-list-item.sale_off").length;
        var next_dt     = $(page).find("input[name='NEXT_DT']").val();

        var t_soldout   = myApp.i18Next.getTrasTxt("BSK", "WARN_SOLD_OUT");
        var t_closed    = myApp.i18Next.getTrasTxt("BSK", "WARN_CLOSED");
        var t_timeout   = myApp.i18Next.getTrasTxt("BSK", "WARN_TIMEOUT");

        $(page).find(".min_amt").text(MIN_ORDER_WORD);
        
        if( nStNone > 0 ){
            runWarningMessage(t_soldout , "warning", 2000, "up");
        }

        if( nHoly > 0 ){
            runWarningMessage(t_closed , "warning", 2000, "up");
        }

        if( ntmOut > 0){
            runWarningMessage(t_timeout , "warning", 2000, "up");
        }

        if( sumResult >= MIN_ORDER_AMT && nStNone == 0 && nHoly == 0 && ntmOut == 0){
            $(page).find(".btnOrder").prop("disabled", false);
            $(page).find(".minOrderMsg").hide( { "animation": "none" });
        }else{
            $(page).find(".btnOrder").prop("disabled", true);
            $(page).find(".minOrderMsg").show( { "animation": "none" } );
        }

        // 예약가능시간이 있을 경우 허용 
        // 최소금액 조건  
        if( sumResult >= MIN_ORDER_AMT && next_dt != "" && !isNull(next_dt) ) {
            $(page).find(".btnOrder").prop("disabled", false);
            $(page).find(".minOrderMsg").hide( { "animation": "none" });
        }

    },

    setProductBasket : function(page, basketData){

        // myApp.basket.setItem("basket", product);
        window.serviceModel.checkOrderButton(page);

        var data = {
            ID					: localStorage.getItem("ID"),
            TEL                 : localStorage.getItem("TEL"),
            PRODUCT_CORP_ID     : basketData.p_corp_id,
            REF_PRODUCT_CD      : basketData.productCd,
            QTY                 : basketData.qty,
            NOW_QTY             : basketData.now_qty
        }

        if( basketData.isEnable ){
            myApp.basket.addObjectItem(data);
            // 모든 장바구니 카운터 동기화

            if( !isNull(basketData.summaryQty)){
                myApp.commonController.syncBasketQty(basketData.summaryQty);
            }
            return true;
        }else{
            return false;
        }
    }, 

    isOnOffHolyCheck : function(holy, sTm, eTm){

        result      = "";
        ostart      = isNull(sTm) && sTm.length > 5 ? "-" : sTm.substr(0, 5);
        estart      = isNull(eTm) && eTm.length    > 5 ? "-" : eTm.substr(0, 5);
        
        // 현재시각
        nowTime     = moment();

        // 영업시작시각
        stime       = moment(ostart, 'HH:mm');
        stTime      = moment()
        stTime.set({
            hour    : stime.get('hour'),
            minute  : stime.get('minute'),
        })

        // 영업종료시각
        etime       = moment(estart, 'HH:mm');
        etTime      = moment()
        etTime.set({
            hour    : etime.get('hour'),
            minute  : etime.get('minute'),
        })

        // 앞시간이 뒷시간보다 더 클 경우 다음날을 더해야함
        // - 가 나오면 뒷날에 하루를 더해준다
        daysDiff   = etTime.diff(stTime, 'minutes');

        holyDays   = holy;
        isHolyDay  = false;

        // 오늘 요일(1~7)
        todayNumber = moment().isoWeekday();
        
        if( !isNull(holyDays) && holyDays.length > 0 ){
            var arrHoly = holyDays.replace(/'/g, '').split(",");  
            
            for(var i=0; i<=arrHoly.length; i++){

                nHoly   = parseInt(arrHoly[i]);

                if( todayNumber == nHoly){
                    isHolyDay = true;
                }
            }
        }
        
        if( daysDiff <= 0){
            etTime.add(1, 'days');
        }

        sdiff       = nowTime.diff(stTime, "minutes");  // 현재시간보다 시작시간이 + 일 경우 주문못함
        ediff       = nowTime.diff(etTime, "minutes");  // 현재시간보다 

        // 시작시간이 지나고
        // 종료시간이 음수일 경우에 영업운영 
        // 쉬는날이 아닐경우
        if( ( sdiff >= 0 && ediff <= 0 ) && !isHolyDay){
            result = "";
        }else{
            result = "off";
        }

        // 재검증 : 자정 이후면 한번 더 체크해야함
        // 마감날짜 불러오기
        etime2       = moment(estart, 'HH:mm');
        etTime2      = moment()
        etTime2.set({
            hour    : etime2.get('hour'),
            minute  : etime2.get('minute'),
        })

        newDiff = nowTime.diff(etTime2, "minutes"); 

        if( newDiff <= 0 && daysDiff <= 0 && !isHolyDay){
            result = "";
        }

        return result;
    }
}
