

window.serviceModel = {

    listMessage : function(page, data, domain, option){
        
        var modal       = page.querySelector('ons-modal');

        var jsonData    = data.list;
        var htmlData    = "";
        var idx         = "";

        if( option.clear == 1 || option.clear == "1"){
            $(page).find(".imageList ons-list-item").remove();
        }

        if( !isNull(jsonData) && jsonData.length > 0 ){

            $(page).find(".emptyOrNoData ").remove();

            $(jsonData).each(function(){
                
                idx         = this.id;
                title       = this.msg_title.trim();
                body        = this.msg_body.trim();
                is_all      = this.pusher_to;

                notice      = getLang("전체", "tất cả", "ALL")
                notice      = is_all == "ALL" ? "<span class='red'>[" + notice + "]</span> " : "";

                htmlData += "<div class='card listItem listData' data-id='" + idx + "'>" +
                            "    <div class='left title' >" +
                            "	     <span class='listContent ellipsis titleText'>" + notice + title  + "</span>" +
                            "    </div>" +
                            "    <span class='center bodyText' style='display:none;'>" +  body + "</span>" +
                            "    <div class='right txtRight'>" + this.created_at + "</div>" +
                            "</div>";
            });
            $(page).find(".dataList").append(htmlData);

            if( jsonData.length < 15){
                htmlData = "<div class='emptyOrNoData card'><i><img src='img/svg/caution.svg'></i><span data-i18n='COMM:noMoreData'> 더이상 수신된 메시지가 없습니다. <span></div>";
                $(page).find(".dataList").append(htmlData);
                $(page).find(".after-list").hide();
            }

        }else{
            if( $(page).find(".dataList .emptyOrNoData").length == 0 ){
                htmlData = "<div class='emptyOrNoData card'><i><img src='img/svg/caution.svg'></i><span data-i18n='COMM:noMoreData'> 더이상 수신된 메시지가 없습니다. <span></div>";
            }
            $(page).find(".dataList").append(htmlData);
            $(page).find(".after-list").hide();
        }

        $(page).off("click", ".listItem", null );
        $(page).on("click", ".listItem", function() {

            title   = $(this).find(".titleText").html();
            content = $(this).find(".bodyText").html();
            cdate   = $(this).find(".txtRight").html();

            $(page).find(".msgTitle").html(title);
            $(page).find(".view_summary").html(content);
            modal.show({ "animation": "lift" });
        });
       

        page.querySelector(".btnClosePopup").onclick = function() {

            $(page).find(".msgTitle").html("");
            $(page).find(".view_summary").html("");
            modal.hide({ "animation": "none" });
        };

    },
}