var service	= new withWSDL();

window.serviceModel = {

    // 전화번호군 목록
    listTelClass : function(page, data, option){
        
        var jsonData    = data.list;
        var htmlData    = "";
        var idx         = "";
        var cname       = "";

        if( option.is_search){
            $(page).find(".dataList").empty();
        }

        if( jsonData.length > 0){
            $(jsonData).each(function(){
                
                idx         = this.TEL_CLASS_IDX;
                cname       = this.TEL_CLASS_NM;
                //telnames    = this.TEL_NAMES.length > 0 ? this.TEL_NAMES + " 등" : "";
                telCount    = this.CNT;

                htmlData += "<ons-list-item tappable class='listItem' data-idx='" + idx + "'>" +
                        "        <div class='center'>" + 
                        "            <label class='list-item__title'>" + cname + "(" +telCount+ ")</label>" +
                        "		     <span class='list-item__subtitle'> " + "한국대사관, 호치민 총영사관 등 5개"  + "</span>" +
                        "        </div>" +
                        " </ons-list-item>";
            });
            $(page).find(".dataList").append(htmlData);
        }else{
            if( $(page).find(".dataList").find(".emptyOrNoData").length == 0){
                //htmlData += "<div class='emptyOrNoData card'><i class='fa fa-commenting-o'></i>&nbsp;<span data-i18n='COMM:noMoreData'>더이상 데이터가 존재 하지 않습니다<span></div>";
                htmlData = "<div class='emptyOrNoData card'><i><img src='img/svg/caution.svg'></i><span data-i18n='COMM:noMoreData'> 더이상 등록된 데이터가 없습니다. <span></div>";
            }
            $(page).find(".dataList").append(htmlData);
            $(page).find(".after-list").css("display", "none");
        }

        // 클릭 And 상세페이지로 이동
        Array.prototype.forEach.call(page.querySelectorAll(".listItem"), function(ele) {
            ele.onclick = function() {

                let idx    = $(this).data("idx");
                let title   = $(this).find(".list-item__title").text();
                let data    = { "idx" : idx, "title" : title }
                myApp.navigator.pushPage('html/etc/telList.html', { data : data });  
            };
        });
          
    },

    // 전화번호군에 대한 전화번호 목록
    listTelList : function(page, data, option){
        
        var jsonData    = data.list;
        var htmlData    = "";
        var idx         = "";
        var cname       = "";
        var tel1        = "";
        var tel2        = "";

        //alert(option.is_search);
        if( option.is_search){
            $(page).find(".dataList").empty();
        }

        if( jsonData.length > 0){
            $(jsonData).each(function(){
                
                idx     = this.TEL_IDX;
                cname   = this.AGENCY_NM;
                tel1    = this.TEL1;
                tel2    = this.TEL2;

                htmlData += "<ons-list-item tappable class='listItem' data-idx='" + idx + "'>" +
                        "        <div class='center'>" + 
                        "            <label class='list-item__title'>" + cname + "</label>" +
                        "		     <span class='list-item__subtitle tel' ><a href='tel:" + tel1  + "'>" + tel1 + "</a></span>" +
                        "		     <span class='list-item__subtitle tel' ><a href='tel:" + tel2  + "'>" + tel2 + "</a></span>" +
                        "		     <span class='list-item__subtitle addr'> " + this.ADDR1 + ", " + this.ADDR2  + " </span>" +
                        "        </div>" +
                        " </ons-list-item>";
            });
            $(page).find(".dataList").append(htmlData);
        }else{
            if( $(page).find(".dataList").find(".emptyOrNoData").length == 0){
                //htmlData += "<div class='emptyOrNoData card'><i class='fa fa-commenting-o'></i>&nbsp;<span data-i18n='COMM:noMoreData'>더이상 데이터가 존재 하지 않습니다<span></div>";
                htmlData = "<div class='emptyOrNoData card'><i><img src='img/svg/caution.svg'></i><span data-i18n='COMM:noMoreData'> 더이상 등록된 데이터가 없습니다. <span></div>";
            }
            $(page).find(".dataList").append(htmlData);
            $(page).find(".after-list").css("display", "none");
        }

        
    }
}
