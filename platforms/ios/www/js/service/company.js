window.serviceModel = {

    // 회사조회 : 메인 화면 및 카테고리 메뉴에 포함
    listCompany : function(page, data, domain, option){
        
        var jsonData		= data.list;
		var corpDiv			= "";
		var isEnable		= "";
		var corpCd			= "";
		var corpNm			= "";
		var imgSrc			= "";
		var url				= "";
        var isProve			= "";
        
        if( jsonData.length > 0){

            var section = 0;
            var nth     = -1;

            $(jsonData).each(function(){
            
                var htmlData= [];
                corpDiv     = this.CORP_DIV;
                isProve     = this.IS_PROVE_CP;

                isEnable    = isProve.toUpperCase() == "Y" ? "enalble" : "btnDisabled";

                // 메뉴 분류 추가
                mkind       = this.CORP_M_KIND;
                corpCd      = this.CORP_ID;
                corpNm      = getLang(this.CORP_NM, this.CORP_NM_VN, this.CORP_NM_EN);
                imgSrc      = domain + "//" + this.IMG_URL_MD;
                is_url_acc  = this.IS_ACCESS_URL;

                url			= validUrl(this.URL) ? " class='exLink' data-url='" + this.URL + "'" : '' ;
                is_url_acc  = is_url_acc == "Y" ? "<i class='external fa fa-external-link-square'></i>" : "";

                proved      = isProve == "Y" ? "" : "ready";
                new_product     = "";
                if( this.IS_NEW != "N"){
                    if( this.IS_NEW == "Y"){
                        new_product = "<span class='bRed new overTip'>" + "NEW" + "</span>";    
                    }
                }

                if( corpDiv.indexOf("MAIN") >= 0 ){
                    nth++;
                }

                if( section != mkind){
                    etcInfo = "";
                    if( mkind == "ETC"){
                        etcInfo = "(" + myApp.i18Next.getTrasTxt("COMM", "EXT_LINK") + ")";
                    }
                    if( $("div[data-kind='" + mkind + "']").length == 0){
                        htmlData.push("<p>" + mkind +  etcInfo + " </p>");
                        htmlData.push("<div class='CateMenu Main' data-kind='" + mkind + "'></div>");                            
                        $(page).find("section.buttonMenu").append(htmlData.join(''));
                    }
                    section = mkind;
                }
                var CorpData    = [];
                CorpData.push("<div class='card btnCorp " + isEnable + "' data-nth='"+ nth +"' data-corp_id='" + corpCd + "' data-div='"+ corpDiv +"' " + url + ">");
                CorpData.push("<span class='" + proved + "'><img src='" + imgSrc + "' " + url + " />" +  is_url_acc + "</span>" + new_product);
                CorpData.push("<label>" + corpNm + "</label></div>");  
                $(page).find("div[data-kind='" + section + "']").append(CorpData.join(''));        
            });


            // 상단메뉴 바인딩
            var active      = "active";
            var nth = 0;     
            var tabHtml = [];
            
            tabHtml.push("<ons-tabbar id='cateTabbar' position='top' swipeable animation='none' >");
            $(jsonData).each(function(i, d){
                corpDiv     = d.CORP_DIV;
                isEnable    = d.IS_USE_CP == "Y" ? "enalble" : "btnDisabled";
                corpCd      = d.CORP_ID;
                corpNm      = getLang(d.CORP_NM, d.CORP_NM_VN, d.CORP_NM_EN);
                
                if( corpDiv.indexOf("MAIN") >= 0){
                    if(nth == 0){
                        active = "active";
                    }else{
                        active = "";
                    }
                    tabHtml.push("<ons-tab " + active+ " data-nth='"+ (nth++) +"' data-corpId='" + corpCd + "' label='" + corpNm + "' class='corpTab " + isEnable + "' " + "page='html/product/category_1.html?corpid=" + corpCd + "' ></ons-tab>") ; 
                }
            });

            tabHtml.push("</ons-tabbar>");
            $("#categoryIndexPage #cateTabbar").remove();
            $("#categoryIndexPage").append(tabHtml.join(''));
            localStorage.setItem("CATE_NTH", 0);

            // 메뉴 클릭(동적 바인드)
            $(page).find(".buttonMenu .btnCorp.enalble").click(function(){
                var div		= $(this).data("div");

                if( div == "MAIN"){
                    // 회사별 카테고리(분류) 순서
                    var nth         = $(this).data("nth");
                    var url         = $(this).data("url");
                    
                    if( !isNull(url) ){
                        return;
                    }

                    localStorage.setItem("CATE_NTH", nth);
                    document.querySelector('#appTabbar').setActiveTab(1);

                    if( isNull(nth)){
                        localStorage.setItem("CATE_NTH", nth);
                    }
                    // cateTabbar 버그로 인한 setTimeout 설정 : 19-10-21
                    document.querySelector('#cateTabbar').setActiveTab(nth);
                }
            });

            // 추가 : 카카오 혹은 sns를 통해 reffror로 연결된 회사로 이동
            if( !isNull(REFFROR_CORP_ID) ){
                // 회사별 카테고리(분류) 순서
                var nth         = $(page).find(".btnCorp[data-corp_id='" + REFFROR_CORP_ID + "']").data("nth");
                REFFROR_CORP_ID = null;

                if( nth != 0 && isNull(nth) ){
                    return;
                }

                localStorage.setItem("CATE_NTH", nth);
                setTimeout(function(){
                    document.querySelector('#appTabbar').setActiveTab(1);
                    document.querySelector('#cateTabbar').setActiveTab(nth);
                }, 500);
            }

        }else{     
            
            $("#categoryIndexPage #cateTabbar").remove();       
            var noneMessage =   "<div class='emptyOrNoData card mainEmpty'>" + 
                                "   <i><img src='img/svg/caution.svg'></i>" + 
                                "   <span data-i18n='LOCATION:NO_COMP_MSG'></span>" + 
                                "   <span class='orange goLocSetting' data-i18n='LOCATION:MY_AREA_MSG'></span>" + 
                                "</div>";
            
            $(page).find(".companyListMain").html(noneMessage);

        }

        // 추가 : 카카오 혹은 sns를 통해 reffror로 연결된 공구로 이동
        if( !isNull(REFFROR_G_ID) ){
            document.querySelector('#appTabbar').setActiveTab(2);
        }

        // 구역아이디(지역설정) 설정 바로 이동
        Array.prototype.forEach.call(document.querySelectorAll('.goLocSetting'), function(ele) {
            ele.onclick = function() {
                myApp.navigator.pushPage('html/auth/user_location.html',  { animation: "none" , data: jsonData }); 
            };    
        });
    }, 
}
