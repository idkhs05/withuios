withWSDL = function  ( url ) {
	this.params = Array();
    //this.DOMAIN	= "http://192.168.0.3:8888/";

    this.DOMAIN	= "https://withuvn.net/";

    
	this.WSDL_URL	=  this.DOMAIN + url;
	this.type		= 'GET';
    
	$.ajaxSetup({
		url 	: this.WSDL_URL,
		type	: this.type,
		dataType: 'json',
        data 	: this.data,
		cache	: true ,
		timeout	: 10000,
		beforeSend : function(){

		},
		error: function(x, t, m) {

			if(t==="timeout") {
                message = myApp.i18Next.getTrasTxt("SYS", "DELAY_CONN"); //연결이 지연되어 데이터를 불러오지 못했습니다
                runWarningMessage(message, "warning", 2500, "bottom");
            }

            if (x.readyState == 4) {
                // HTTP error (can be checked by XMLHttpRequest.status and XMLHttpRequest.statusText)
            } else if (x.readyState == 0) {
                // Network error (i.e. connection refused, access denied due to CORS, etc.)
                message = myApp.i18Next.getTrasTxt("SYS", "CHK_CONN"); //인터넷연결상태를 확인 후 재실행 해주세요
                runWarningMessage(message, "warning", 2500, "bottom");
            
            } else {
                // something weird is happening
            }

            // 핸드폰 동기화 + 회사조회 오류 에러 
            if( x.status == 501){
                try{
                    var errors = $.parseJSON(x.responseText);
                    var ul = [];
                    ul.push("<ul class='toastList'>");
                    $.each(errors.message, function (key, val) {
                        ul.push("<li>" +  val + "</li>");
                    });
                    ul.push("</ul>");
                    runWarningMessage(ul.join(''), "warning", 2500, "up");
                    
                }catch(error){
                   console.log(error);
                }
            }

            // 서버로부터 validation 오류처리
            if( x.status == 422){
                try{
                    var errors = $.parseJSON(x.responseText);
                    var ul = [];
                    ul.push("<ul class='toastList'>");
                    $.each(errors.message, function (key, val) {
                        ul.push("<li>" +  val[0] + "</li>");
                        $("#" + key).css("border", "1px solid red").focus();
                    });
                    ul.push("</ul>");
                    runWarningMessage(ul.join(''), "danger", 2500, "up");
                    
                }catch(error){

                }
            }

            if( x.status == 401){
                try{
                    var errors = $.parseJSON(x.responseText);
                    var ul = $("<div><ul class='toastList'></ul></div>");

                    $.each(errors.message, function (key, val) {
                        ul.find("ul").append("<li>" +  val +  "</li>");
                    });
                    ul.find("li").css("list-style", "none");
                    runWarningMessage(ul.html(), "warning", 2500, "up");
                    
                }catch(error){
                    console.log(error);
                }
            }

            if( x.status == 402){
                try{
                    var errors = $.parseJSON(x.responseText);

                    var option  = errors.option;

                    // 장바구니 확인 옵션
                    if( option == "confirm"){
                        var txt     = myApp.i18Next.getTrasTxt("BSK", "CART_EMPTY");
                        runWarningMessage(txt, "info", 2500, "bottom");
                    }

                    var ul = $("<div><ul class='toastList'></ul></div>");

                    $.each(errors.message, function (key, val) {
                        ul.find("ul").append("<li>" +  val +  "</li>");
                    });

                    ul.find("li").css("list-style", "none");
                    runWarningMessage(ul.html(), "warning", 2500, "up");
                
                }catch(error){
                    console.log(error);
                }
            }

            if( x.status == 500){
                
                try{
                    var errors = $.parseJSON(x.responseText);
                    var ul = $("<div><ul class='toastList'></ul></div>");

                    $.each(errors.message, function (key, val) {
                        ul.find("ul").append("<li>" +  val +  "</li>");
                    });

                    ul.find("li").css("list-style", "none");
                    runWarningMessage(ul.html(), "warning", 2500, "up");
                    
                }catch(error){
                    console.log(error);
                }
            }

            if( m.indexOf("Internal Server Error") >= 0){
                message = myApp.i18Next.getTrasTxt("SYS", "UNSAFE_CONN"); 
                runWarningMessage(message, "warning", 1000, "up");
            }
        },
        
		complete: function() {
			$(".page__content .progressStyle").fadeOut("fast");
			
			$("img").on("error", function(){
				var uri = "img/404.PNG"; 
				$(this).attr('src', uri);
            });
            
            myApp.i18Next.init();
		}
	});
}

withWSDL.prototype = {

	ajaxGetData : function(data, clb){			

		$.ajax({
			data: data,
//			async : false,
			success: this.success,
			url : this.WSDL_URL
		});

		if(typeof clb === 'function'){
			clb();
		}
	},

	ajaxPutData : function(data){			
		data._method =  "PUT";
		$.ajax({
			data: data, success: this.success, type : 'POST'
		});
	},

	ajaxPostData : function(data){
		data._method =  "POST";
		$.ajax({
			data: data, success: this.success, type : 'POST' 
		});
	},

	ajaxDeleteData : function(data){			
		
		data._method =  "DELETE";
		$.ajax({
			data: data, success: this.success, type : 'POST' 
		});
	},

}
