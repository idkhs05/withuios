myApp.grpControllers = {

    // 공구상품목록
    grpProductlistPage: function(page, corpId) {

        page.onShow = function() {

            chkTelNum();
            var option = {
                start   : 0,
                orderby : "tb_grp_product.VIEW_ORDER",
                order   : "ASC",
                use_yn  : "Y",
                isadult : 'N',
                adult_yn: 'N',
                clear   : 1,
                file_div: "GPRODUCT",
                area_id : localStorage.getItem("AREA_ID"),
            };
            
            // 검색대 전환
            $(page).find(".searchProduct").click(function(){
                $(page).find(".showTitleProduct").hide();
                $(page).find(".showSearchProduct").show();
                $(page).find("#searchItem input[type='search']").focus();
            })

            var service = new withWSDL("api/gproduct/list");

            // 상품목록 서비스 바인딩
            service.success = function(_jsonData) {

                var jsonData    = _jsonData;
                
                if (typeof serviceModel.listGrpProduct === 'undefined') {
                    $.getScript("js/service/gproduct.js", function() {
                        serviceModel.listGrpProduct(page, jsonData, service.DOMAIN, option);
                    });
                } else {
                    serviceModel.listGrpProduct(page, jsonData, service.DOMAIN, option);
                }

                // 페이지 로딩 감추기
                $(page).find(".pageLoading").hide();
            };

            option.orderby  = "tb_grp_product.VIEW_ORDER";
            option.file_div = "GPRODUCT";
            option.userTel  = localStorage.getItem("TEL");
            
            // 상품 카테고리 목록 서비스 호출
            service.ajaxGetData(option);

            //  상품 카테고리 이벤트 등록
            page.onInfiniteScroll = function(done) {

                if ( $(page).find(".emptyOrNoData.card").length == 0  ) {
                    setTimeout(function() {
                    // $(page).find(".after-list").show();
                        option.start += 15;
                        option.clear = "0";
                        option.orderby = "tb_grp_product.VIEW_ORDER";
                        // 이벤트 목록 호출
                        service.ajaxGetData(option);
                        done();
                    }, 200)
                }
            };   
        }
    },

    // 공구상품상세조회
    grpProductViewPage : function(page, data) {

        myApp.i18Next.changeLang();

        var data = page.pushedOptions.data;

        var option = {
            corp_id     : data.corpId,
            id          : data.id,
            product_cd  : data.productCd            
        };

        var end_dt  = "";
        var service = new withWSDL("api/gproduct/list/" + option.id);
        var vData   = null;

        // 상품목록 서비스 바인딩
        service.success = function(_jsonData) {

            var jsonData    = _jsonData;
            var end_dt      = jsonData.list.END_DT;
            vData           = jsonData; // 카카오링크용
            
            if (typeof serviceModel.viewGrpProduct === 'undefined') {
                $.getScript("js/service/gproduct.js", function() {
                    serviceModel.viewGrpProduct(page, jsonData, service.DOMAIN, option);
                });
            } else {
                serviceModel.viewGrpProduct(page, jsonData, service.DOMAIN, option);
            }

            // 페이지 로딩 감추기
            $(page).find(".pageLoading").hide();
        };

        service.ajaxGetData(option);

        // 클릭 : 상품주문하기
        $(page).find(".btnGrpOrder").click(function() {

            var objProduct = {
                CORP_ID         : localStorage.getItem("CORP_ID"),
                USER_ID         : localStorage.getItem("USER_ID"),
                BRANCH_ID       : localStorage.getItem("BRANCH_ID"),
                PRODUCT_CD      : option.product_cd,
                PRODUCT_NM      : $(page).find(".pName").text(),
                GP_ID           : option.id,
                DELV_CORP_ID    : null,
                ORDER_STATE     : "ORD_NEW",
                ORDER_DT        : null,
                DLV_WISH_TIME   : null, // page.find("#select-productQty option:selected").val().trim()

                ORDER_TEL       : localStorage.getItem("TEL"),
                ADDR1           : localStorage.getItem("ADDR1"),
                ADDR2           : localStorage.getItem("ADDR2"),
                
                MEMO            : null, //page.querySelector("#req_text").value
                PAY_OPTION      : null, ///page.find("ons-checkbox[name='payway']:checked")

                DELV_START_DT   : $(page).find(".delvDate").attr("data-delvdate") ,
                DELV_END_DT     : $(page).find(".delvDate").attr("data-delvdateend") ,
                // PAY_OPTION_COIN : parseInt($(page).find("#useCoin").val().replace(/[^0-9]/g, "")),
                DELV_USER_ID    : null,
                RECEPTION_DT    : null,
                DLV_READY_TM    : null,
                DLV_DOING_TM    : null,
                DLV_END_TM      : null,
                GIFT_ID         : null,
                PRICE           : $(page).find(".sum").attr("data-price"),
                QTY             : $(page).find("#qty").val(),
                END_DT          : end_dt,
                
            };
            // console.log({ TB_ORDER_INFO: objProduct, TB_ORDER_DETAIL: arrProduct });
            myApp.navigator.pushPage('html/grp/grp_order_detail.html', { data: { TB_GORDER_INFO: objProduct} });

        });

        $(page).find(".kakao_share").click(function(){
            myApp.commonController.kakaoLinkGroupBuying(vData);
        });
    },

    // 주문하기 페이지 2(상세)
	grpOrderDetailPage : function(page){

        myApp.i18Next.changeLang();

        // 주문확인 모달 창
        var orderConfirmPopup= page.querySelector("#orderConfirmPopup");
        var data             = page.pushedOptions.data;
        var userid           = localStorage.getItem("ID");

        // 로그인 여부에 따라 사은품 필수 처리 (적립금 포함)
        if( isNull( userid ) ){
            $(page).find("#giftItem").attr("required");
            $(page).find(".giftMessage").hide();
        }else{
            $(page).find("#giftItem").removeAttr("required");
            $(page).find(".giftMessage").show();
        }

        // 전화면으로 부터 구매정보와 구매상품목록정보를 가져온다
        var productData		= null;
        var prodcutDetail	= null;
        var obj			    = null;     // 증정품
        var taxData         = null;
        var timeCheck       = false;

        // 구매버튼 클릭시 첫 팝업여부 한번만 실행되도록 체크 하기위한 카운트
        var nPopFirst       = 0;

        // 주소검색
        $(page).find("#goAddr").click(function(){
            localStorage.setItem("ADDR_ACTIVE_TAB", 0);
            localStorage.setItem("ADDR_POP_PAGE", 1);
            myApp.navigator.pushPage('html/config/config_addr_tabbar.html');
        });

        // 사은품 정보 가져오기(합계금액 대비)
        var gOption     = null;
        var disabledt   = [];
        var allowTimes  =  [
            '08:00', '09:00', '10:00', '11:00',  '12:00', '13:00', '14:00',  '15:00', 
            '16:00',  '17:00', '18:00', '19:00', '20:00', '21:00' , '22:00', '23:00'
        ];

        page.onShow = function() {

            timeCheck       = false;
            // 팝업 카운트 초기화
            productData		= null;
            prodcutDetail	= null;
            obj			    = new Object();
            taxData         = null;

            nPopFirst       = 0;
            productData		= data.TB_GORDER_INFO;            
            taxData         = null;

            var svcDelvDateList 	= new withWSDL("api/gproduct/getDisbledList1");
            svcDelvDateList.success = function(_jsonData){
                var jsonData = _jsonData;
                disabledt = jsonData.list;

                $(disabledt).each(function(i){

                    time     = this.HR;
                    
                    if( allowTimes.indexOf(time) >= 0){
                        console.log(time);
                        allowTimes.splice(allowTimes.indexOf(time),1);
                    }
                
                });

                timeCheck           = false;
                $(page).find(".reserv").show();

                var reservTime      = moment(productData.DELV_START_DT).format("YYYY-MM-DDTHH:mm");
                var reservEndTime   = moment(productData.DELV_END_DT).format("YYYY-MM-DDTHH:mm");

                productData.DLV_WISH_TIME       = reservTime;
                
                var lang    = getLang("ko", "vi", "en") ;
                $.datetimepicker.setLocale(lang);
                // $.datetimepicker.setDateFormatter('moment');

                $('#AddrTime3').datetimepicker({
                    format:'Y-m-d H:00',
                    formatDate:'Y-m-d H:i',
                    // inline:true,
                    minDateTime : reservTime,
                    maxDateTime : reservEndTime,                    
                    allowTimes: allowTimes,
                    step:60,
                    hours12: false,
                    // defaultDate: reservTime,
                    closeOnWithoutClick :false,
                    // onChangeDateTime : changeDate,
                    // onChangeDateTime : changeDate,
                    onSelectTime : function(){
                        timeCheck = true;
                    },
                    
                });
            };

            svcDelvDateList.ajaxGetData(
                {
                    "gp_id" : productData.GP_ID
                }
            );
            
            
            $(page).find("#AddrTime3").change(function(){ 

                selectedDateTime = $(this).val();    
                $(disabledt).each(function(i){

                    days     = this.DAYS ;
                    dayTime  = moment(days).format("YYYY-MM-DD HH:mm");

                    if( selectedDateTime == dayTime ){
                        timeCheck = false;
                        return;        
                    }
                });
                
                if( !timeCheck ){
                    // "달력 내 우측 시간대를 정확히 선택하여 입력해주세요"
                    var t_msg_timecheck     = myApp.i18Next.getTrasTxt("GB", "INPUT_TIME_CAL");
                    runWarningMessage(t_msg_timecheck, "warning", 2000, "up");
                }
            });
            // 주문완료 버튼 활성화
            
            // 배송메시지 알림(배송내용이 있을경우만 알림)
            areaMessage     = window.localStorage.getItem("AREA_MSG");
            if( !isNull(areaMessage) && areaMessage.length > 0 ){
                runWarningMessage(areaMessage, "info", 3500, "up");
            }

            gOption = {
                amt_level   :  parseInt( (productData.QTY * productData.PRICE) )
            };

            // 세금계산서 비활성화 및 초기화
            $(page).find(".taxItem").css("display", "none");
            $(page).find("#corpname").removeAttr("required");
            $(page).find("#corpemail").removeAttr("required");
            $(page).find("#tax_number").removeAttr("required");

            // 주소 초기화
            ADDR1 = isNull(localStorage.getItem("ADDR1")) ? "" : localStorage.getItem("ADDR1") ;
            ADDR2 = isNull(localStorage.getItem("ADDR2")) ? "" : localStorage.getItem("ADDR2") ;

            $(page).find("#dlvaddr").val( ADDR1 );
            $(page).find("#dlvaddrDetail").val( ADDR2 );

            
            $giftSelect = $(page).find("#giftItem select");
            var serviceGift     = new withWSDL("api/gift/list");
            serviceGift.success = function(_jsonData){

                var jsonData    = _jsonData;
            
                if( jsonData.recordsTotal > 0){
                    selmsg  = myApp.i18Next.getTrasTxt("ORDD", "ORD_GIFT");
                    $giftSelect.html("<option value=''>" + selmsg + "</option>");

                    $(jsonData.list).each(function(){
                        pName   = getLang(this.PRODUCT_NM, this.PRODUCT_NM_VN, this.PRODUCT_NM_EN);
                        $giftSelect.append("<option data-id='"+ this.ID + "' data-corp_id='" + this.CORP_ID + "' data-qty='" + this.QTY + "' value='" + this.PRODUCT_CD + "'>" + pName + "</option>")
                    });

                }else{
                    // 사은품이 없을 경우 필수값 제외
                    var none    = myApp.i18Next.getTrasTxt("COMM", "NONE");
                    $giftSelect.html("<option value=''>" + none + "</option>");
                    $giftSelect.removeAttr("required");
                    $(page).find("#giftItem").removeAttr("required");
                    $(page).find(".giftMessage").show();
                }
            };

            // 증정품(사은품) 조회
            serviceGift.ajaxGetData(gOption);
        }
       

        page.onHide = function(){
            nPopFirst       = 0;
            productData		= null;
            prodcutDetail	= null;
            taxData         = null;
        }

        // 결제방식 체크 및 금액 계산 체크
        function checkRadio(){

            var tot_amt     = parseInt( productData.PRICE * productData.QTY);

            Array.prototype.forEach.call(page.querySelectorAll("ons-radio[name='payway']"), function(ele){

                if(ele.checked){
                    if( ele.value == "CASH"){
                        productData.PAY_OPTION = "CASH";
                        productData.PAY_OPTION_CARD = null;
                        productData.PAY_OPTION_CASH = tot_amt;
                        productData.PAY_OPTION_PAY = null;
                    }else if(ele.value == "CARD"){
                        productData.PAY_OPTION = "CARD";
                        productData.PAY_OPTION_CARD = tot_amt;
                        productData.PAY_OPTION_CASH = null;
                        productData.PAY_OPTION_PAY = null;
                    }else{
                        productData.PAY_OPTION = "EPAY";
                        productData.PAY_OPTION_CARD = null;
                        productData.PAY_OPTION_CASH = null;
                        productData.PAY_OPTION_PAY = tot_amt;
                    }
                    return true;
                }
            });
        }

        // 세금계산서 체크 및 계산서 필수값 체크
        function checkTax(){

            var t_corpnm    = myApp.i18Next.getTrasTxt("ORDD", "CORP_NM");
            var t_email     = myApp.i18Next.getTrasTxt("ORDD", "CORP_EMAIL");
            var t_tax_num   = myApp.i18Next.getTrasTxt("ORDD", "TAX_NUM");
            var t_addr      = myApp.i18Next.getTrasTxt("CONF", "addr1");

            $(page).find("#corpname").attr("placeholder", t_corpnm);
            $(page).find("#corpemail").attr("placeholder", t_email);
            $(page).find("#taxnumber").attr("placeholder", t_tax_num);
            $(page).find("#taxaddr").attr("placeholder", t_addr);

            var isTaxChecked = page.querySelector("ons-checkbox[name='chkReqTax']").checked;

            if(isTaxChecked){
                $(page).find("#corpname").attr("required",true);
                //console.log( $(page).find("#corpname").prop("required") );
                $(page).find("#corpemail").attr("required",true);
                $(page).find("#tax_number").attr("required",true);
                $(page).find("#taxaddr").attr("required",true);

                taxData  = {

                    "TAX_COMP_NM"       : $(page).find("#corp_name").val(),
                    "TAX_CORP_EMAIL"    : $(page).find("#corp_email").val(),
                    "TAX_DUTY_NO"       : $(page).find("#taxnumber").val(),
                    "TAX_ADDR"          : $(page).find("#taxaddr").val()
                }

            }else{

                $(page).find("#corpname").removeAttr("required");
                $(page).find("#corpemail").removeAttr("required");
                $(page).find("#tax_number").removeAttr("required");
                $(page).find("#taxaddr").removeAttr("required");

                taxData  = null;
            }
        }

        // 현금 / 카드 사용여부 (체크박스체크 및 데이터 바인딩)
        Array.prototype.forEach.call(page.querySelectorAll("ons-checkbox[name='payway']"), function(element){
            element.onclick = function(){
                checkRadio();
            }
        });

        // 계산서 신청시 필수체크 및 계산서 데이터 값 바인딩
        page.querySelector("ons-checkbox[name='chkReqTax']").onclick = function(){

            $(page).find(".taxItem").toggle();

            var t_corpnm    = myApp.i18Next.getTrasTxt("ORDD", "CORP_NM");
            var t_email     = myApp.i18Next.getTrasTxt("ORDD", "CORP_EMAIL");
            var t_tax_num   = myApp.i18Next.getTrasTxt("ORDD", "TAX_NUM");
            var t_addr      = myApp.i18Next.getTrasTxt("CONF", "addr1");

            $(page).find("#corpname").attr("placeholder", t_corpnm);
            $(page).find("#corpemail").attr("placeholder", t_email);
            $(page).find("#taxnumber").attr("placeholder", t_tax_num);
            $(page).find("#taxaddr").attr("placeholder", t_addr);

            if( this.checked ){
                $(page).find("#corpname").attr("required",true);
                $(page).find("#corpemail").attr("required",true);
                $(page).find("#tax_number").attr("required",true);
                $(page).find("#taxaddr").attr("required",true);
            }else{
                $(page).find("#corpname").removeAttr("required");
                $(page).find("#corpemail").removeAttr("required");
                $(page).find("#tax_number").removeAttr("required");
                $(page).find("#taxaddr").removeAttr("required");
            }
        }
		// 주소 바인딩
		page.querySelector("#dlvaddr").value		= localStorage.getItem("ADDR1");
		page.querySelector("#phone_number").value	= localStorage.getItem("TEL");

        // 주문확인 모달창 닫기 클릭 이벤트
        Array.prototype.forEach.call(page.querySelectorAll(".btnCloseOrderPopup"), function(element){
            element.onclick = function(){
                orderConfirmPopup.hide({ "animation" : "fade" });
            }
        });

		// console.log(data.TB_ORDER_INFO);
		// 주문하기 모달창 보이기
		$(page).find(".btnGrpOrderComplete").click(function(){
            
            // 주문하기 버튼 비활성화
            // 중복 주문 방지
            // ---------- 예약 배달시간 밸리데이션 시간 -----------------------------------------------
            // 예약배송시간 설정 확인
            if( !timeCheck ){
                runWarningMessage("예약배송시간설정을 확인하여 주세요", "warning", 2500, "up");
                $(page).find("#AddrTime3").addClass("warning");
                return false;
            }else{
                $(page).find("#AddrTime3").removeClass("warning");
            }
            // ---------- 예약 배달시간 밸리데이션 완료 -----------------------------------------------
            $(page).find("#orderConfirmPopup .orerList").empty();

            checkTax();
            checkRadio();

            withDatetime = moment( $(page).find("#AddrTime3").val()).format("YYYY-MM-DD HH:mm");
            productData.DLV_WISH_TIME       = withDatetime;

            // console.log(withDatetime);
            // productData.DLV_WITH_TIME_END   = page.querySelector(".t_end").value;
           
            if( !fnValidationInput("orderDetailPage")){
                return false;
            }

            // 주문마스터 정보 (주소,메모)
            productData.ADDR1			= page.querySelector("#dlv_addr").value;
            productData.ADDR2			= page.querySelector("#dlvaddrDetail").value; ;
            productData.MEMO			= page.querySelector("#req_text").value;

            // 증정품 추가
            giftProductCd    = $(page).find("#giftItem select option:selected").val();
            if( !isNull(giftProductCd) ){
                obj.ID          = $(page).find("#giftItem select option:selected").attr("data-id");
                obj.CORP_ID		= localStorage.getItem("CORP_ID");
                obj.USER_ID		= localStorage.getItem("USER_ID");
                obj.BRANCH_ID	= localStorage.getItem("BRANCH_ID");
                obj.PRODUCT_CD	= giftProductCd;
                obj.SALE_CORP_ID= $(page).find("#giftItem select option:selected").attr("data-corp_id");
                obj.DIV         = null;
                obj.BARCODE		= null;
                obj.PRODUCT_NM	= $(page).find("#giftItem select option:selected").text();
                obj.DISPLAY_NO	= null;
                obj.QTY			= $(page).find("#giftItem select option:selected").attr("data-qty");
                obj.UNIT		= 0;
                obj.RMK         = "사은품";
                obj.IS_OUT_YN	= "Y";
            }else{
                obj = new Object();
            }

            // ----------------- 주문 직전 전체 확인 사항 팝업 내용 --------------------
            // 총금액
            $(page).find(".sValue").text( Number( ( productData.QTY * productData.PRICE )  ).currency(0));

            // 배달시간
            $(page).find(".tValue").text( productData.DLV_WISH_TIME );

            // 배달주소
            $(page).find(".aValue").text( productData.ADDR1 + " " + productData.ADDR2 );

            // 위드유 현금
            $(page).find(".cashValue").text( Number(productData.PAY_OPTION_CASH).currency(0) );

            // 위드유 이페이
            $(page).find(".coinValue").text( Number(productData.PAY_OPTION_PAY).currency(0) );

            // 결제
            $(page).find(".pValue").text( productData.PAY_OPTION );

            // 메모
            $(page).find(".pMemo").text( productData.MEMO );

            // 계산서 여부
            if( !isNull(taxData)){
                var t_corpnm    = myApp.i18Next.getTrasTxt("ORDD", "CORP_NM");
                var t_email     = myApp.i18Next.getTrasTxt("ORDD", "CORP_EMAIL");
                var t_tax_num   = myApp.i18Next.getTrasTxt("ORDD", "TAX_NUM");
                var t_tax_addr  = myApp.i18Next.getTrasTxt("ORDD", "TAX_ADDR");
                
                $(page).find(".taxValue1").text( t_corpnm + " : " + taxData.TAX_COMP_NM );
                $(page).find(".taxValue2").text( t_email  + " : " + taxData.TAX_CORP_EMAIL );
                $(page).find(".taxValue3").text( t_tax_num + " : " + taxData.TAX_DUTY_NO );
                $(page).find(".taxValue4").text( t_tax_addr + " : " + taxData.TAX_ADDR );
                $(page).find(".summaryTax").show();
            }else{
                $(page).find(".summaryTax").hide();
            }

            $list = $(page).find("#orderConfirmPopup .orerList");

            // 주문상품 목록 보여주기            
            pli         =   "<div class='summaryLi'>" +
                            "   <span class='left'>"    + productData.PRODUCT_NM + "</span>" +
                            "   <span class='center'>"  + Number(productData.PRICE).currency() + "</span>" +
                            "   <span class='right'>"   + Number(productData.QTY).format() + "</span>" +
                            "</div>";

            $list.append(pli);
            
            // 증점품이 있을 경우 보여주기
            if( !isNull(obj) && !isNull(obj.ID) ){

                // 증정품도 보여주기
                gift    =   "<div class='summaryLi'>" +
                            "   <span class='left'>"    + obj.PRODUCT_NM + "(" + obj.RMK + ")" + "</span>" +
                            "   <span class='center'>"  + Number(obj.UNIT).currency(0) + "</span>" +
                            "   <span class='right'>"   + Number(obj.QTY).format() + "</span>" +
                            "</div>";

                $list.append(gift);

                productData.GIFT_ID = obj.ID;
            }

            // 1차단계 줄임
            orderConfirmPopup.show();

        });

        // 취소 하기
        $(page).find(".btnCancelOrder").click(function(){
            orderConfirmPopup.hide();
        });

        // 주문완료 하기
        $(page).find(".btnExcuteOrder").click(function(){


            obj = null;
            
            // 주문을 위한 웹서비시즈
            var serviceGrpOrder    = new withWSDL("api/gorder/list");

            // 주문정보 요청 성공
            serviceGrpOrder.success = function(_jsonData){
                var jsonData    = _jsonData;
                // console.log(jsonData);

                if( jsonData.status == "success"){
                    orderConfirmPopup.hide();
                    runWarningMessage(jsonData.message, "success", 1500, "up");

                    myApp.navigator.popPage( { animation : "none" } );
                    myApp.navigator.popPage( { animation : "none" } );
                    
                    // 공동구매로 이동
                    document.getElementById('appTabbar').setActiveTab(2);
                    

                }else{
                    runWarningMessage(jsonData.message.PRODUCT_NM, "danger", 1500, "up");
                }
            };

            serviceGrpOrder.ajaxPostData(
            {
                'TB_GORDER_INFO'    : productData,
                'TB_TAX_INFO'       : taxData
            });
        });
	},
}