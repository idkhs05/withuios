myApp.commonController = {

	common : function(){

		var globalTimeout = null; 

		// 장바구니 페이지 이동
		Array.prototype.forEach.call(document.querySelectorAll('.btnGoCart'), function(element) {
			element.onclick = function() {
				myApp.navigator.pushPage('html/product/basket.html', { animation : "none" } );
			};
		});
		
		var svcNewCount 	= new withWSDL("api/order/list/" + localStorage.getItem("TEL"));
		svcNewCount.success = function(_jsonData){
            var jsonData = _jsonData;
			$(".summaryQty").text( jsonData.bsk);
			$(".myBasket .notification").text( jsonData.bsk);
			$(".myOrder .notification").text( jsonData.ordr);
			$(".grpTabbar .notification").text( jsonData.gpr);

			var grpNum 	= jsonData.gpr;
			grpNum 	= isNull(grpNum) ? 0 : grpNum;
			grpNum	= isNaN(grpNum) ? 0 : grpNum;
			
			if( parseInt(grpNum) > 0){
				$(".grpTabbar .tabbar__label").addClass("color_ripple");
			}else{
				$(".grpTabbar .tabbar__label").removeClass("color_ripple");
			}
        };
        svcNewCount.ajaxGetData({
            "area_id" : localStorage.getItem("AREA_ID")
        });
		
		
        // jQuerySerializeObject
        $.fn.serializeObject = function () {
            var o = {};
            var a = this.serializeArray();
            $.each(a, function () {
                if (o[this.name] !== undefined) {
                    if (!o[this.name].push) {
                        o[this.name] = [o[this.name]];
                    }
                    o[this.name].push(this.value || '');
                } else {
                    o[this.name] = this.value || '';
                }
            });
            return o;
		};
		
		return true;
        
	},

	syncBasketQty  : function(summaryQty) {
		// 탭바의 장바구니 숫자와 페이지에서의 숫자가 다름
		$(document).find(".summaryQty").each(function(){ $(this).html(summaryQty); });
		$(document).find("ons-tab.myBasket .notification").html(summaryQty);
	},

	chkLogin	: function(){
		
		var isLoginYn	= localStorage.getItem("IS_LOGIN_YN");
		if( isLoginYn != "Y"){
			document.getElementById('appTabbar').setActiveTab(5);
		}
	},

	chkUserduplication : function(option, value, cb){
		
		var option ;
		var service	= new withWSDL("api/user/list");
		// 로그인 서비스 호출
		service.success = function(_jsonData){
			var jsonData    =_jsonData;
			cb(jsonData.recordsTotal);
		};

		if( option == 'user_id'){
			option = {
				'user_id'	: value,
			};
		}else{
			option = {
				'email'		: value
			};
		}

		service.ajaxGetData(option);   

	}, 

	// 지역소속 체크
	chkArea : function(){
		
		var area_id 	= localStorage.getItem("AREA_ID");
		if( isNull(area_id) ){
			
		}
	},

	// 장바구니에 다른 회사상품 유무 체크
	chkCartProduct : function(objProducts){

		let count	= 0;
		if( objProducts.length > 0){
			let d_value = objProducts[0].SALE_CORP_ID;

			// 초기값과 다르면 false
			$(objProducts).each(function(i, d){
				if( d_value != d.SALE_CORP_ID ) {
					count++;
				}
			});
		}
		
		if( count > 0){
			return false;
		}else{
			return true;
		}
	},

	setYoutube : function(page, ele_id, code, use_yn){

		var tag = page.createElement('script');
        tag.src = "https://www.youtube.com/iframe_api";
        var firstScriptTag = page.getElementsByTagName('ons-modal')[0];
		firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
		
		var player;

        function onYouTubeIframeAPIReady() {
            player = new YT.Player('gangnamStyleIframe', {
//                height: '315',            // <iframe> 태그 지정시 필요없음
//                width: '560',             // <iframe> 태그 지정시 필요없음
//                videoId: '9bZkp7q19f0',   // <iframe> 태그 지정시 필요없음
//                playerVars: {             // <iframe> 태그 지정시 필요없음
//                    controls: '2'
//                },
                events: {
                    'onReady': onPlayerReady,               // 플레이어 로드가 완료되고 API 호출을 받을 준비가 될 때마다 실행
                    'onStateChange': onPlayerStateChange    // 플레이어의 상태가 변경될 때마다 실행
                }
            });
        }
        function onPlayerReady(event) {
            console.log('onPlayerReady 실행');
 
            // 플레이어 자동실행 (주의: 모바일에서는 자동실행되지 않음)
            event.target.playVideo();
        }
        var playerState;
        function onPlayerStateChange(event) {
            playerState = event.data == YT.PlayerState.ENDED ? '종료됨' :
                    event.data == YT.PlayerState.PLAYING ? '재생 중' :
                    event.data == YT.PlayerState.PAUSED ? '일시중지 됨' :
                    event.data == YT.PlayerState.BUFFERING ? '버퍼링 중' :
                    event.data == YT.PlayerState.CUED ? '재생준비 완료됨' :
                    event.data == -1 ? '시작되지 않음' : '예외';
 
            console.log('onPlayerStateChange 실행: ' + playerState);
        }

	}, 

	kakaoLinkProduct : function(data){
		var data 	= data;
		var content = data.CONTENT.replace(/(<([^>]+)>)/gi, "");

		let listHeaderLink	= {
			iosExecutionParams		: 'corpid=' + data.CORP_ID + '&clsid=' + data.P_CLS_IDX + '&pid=' + data.PRODUCT_CD,
			androidExecutionParams	: 'corpid=' + data.CORP_ID + '&clsid=' + data.P_CLS_IDX + '&pid=' + data.PRODUCT_CD
		};

		let listButtons1	= {
			title: '구매하기',
			link: {
				iosExecutionParams		: 'corpid=' + data.CORP_ID + '&clsid=' + data.P_CLS_IDX + '&pid=' + data.PRODUCT_CD,
				androidExecutionParams	: 'corpid=' + data.CORP_ID + '&clsid=' + data.P_CLS_IDX + '&pid=' + data.PRODUCT_CD
			},
		};

		let feedContent = {
			title	: data.CORP_NM  + "\n" + data.PRODUCT_NM,
			link	: listHeaderLink,
			desc 	: Number(data.EVENT_AMT).currency() + "\n" + content,
			imageURL: APP_URL + "/" + data.FILE_URL,
		};

		let feedTemplate = {
			
			content :  feedContent,
			buttons : [listButtons1]
		};

		KakaoCordovaSDK.sendLinkFeed( feedTemplate,
			function(response){
				// console.log(response);
				// console.log(JSON.stringify(response));
			},
			function(error) {
				console.log("fail");
				// console.log(JSON.stringify(error));
			}
		);

	},

	kakaoLinkGroupBuying : function(data){
		var data 	= data.list;

		let listHeaderLink	= {
			iosExecutionParams		: 'gid=' + data.ID,
			androidExecutionParams	: 'gid=' + data.ID,
		};

		let listButtons1	= {
			title: '구매하기',
			link: {
				iosExecutionParams		: 'gid=' + data.ID,
				androidExecutionParams	: 'gid=' + data.ID,
			},
		};

		let feedContent = {
			title	: "[공구]" + data.CORP_NM  + "\n" + data.MAIN_TITLE,
			link	: listHeaderLink,
			desc 	: data.SUB_TITLE,
			imageURL: APP_URL + "/" + data.IMG_URL_ORIGIN,
		};

		let feedTemplate = {
			
			content :  feedContent,
			buttons : [listButtons1]
		};

		KakaoCordovaSDK.sendLinkFeed( feedTemplate,
			function(response){
				// console.log(response);
				// console.log(JSON.stringify(response));
			},
			function(error) {
				console.log("fail");
				// console.log(JSON.stringify(error));
			}
		);

	},

	kakaoLinkCompany : function(option, list){

		var img 	= option.IMG_URL_ORIGIN;
		var corp_nm = option.CORP_NM;
		var content	= option.CONTENT;
		var corpid 	= option.CORP_ID;
		content 	= content.replace(/(<([^>]+)>)/gi, "");

		var stt_delv 	= option.ORDER_STATRT_TM.substr(0, 5);
		var end_delv 	= option.ORDER_END_TM.substr(0, 5);

		let listHeaderLink	= {
			iosExecutionParams		: 'corpid=' + corpid,
			androidExecutionParams	: 'corpid=' + corpid,
		};

		let listButtons1	= {
			title: '더보기',
			link: {
				iosExecutionParams		: 'corpid=' + corpid,
				androidExecutionParams	: 'corpid=' + corpid,
			},
		};

		let arrObj 	= [{
			title	: ("배달시간 " + stt_delv + "-" + end_delv),
			desc	: content,
			link	: listHeaderLink,
			imageURL: APP_URL + "/" + img,
		}];

		if( list.length > 0 ){
			$(list).each(function(i, d){
				var obj 	= {
					title	: d.PRODUCT_NM,
					desc	: Number(d.EVENT_AMT).currency(),
					link	: {
						iosExecutionParams		: 'corpid=' + corpid + "&clsid=" + d.P_CLS_IDX,
						androidExecutionParams	: 'corpid=' + corpid + "&clsid=" + d.P_CLS_IDX,	
					},
					imageURL: APP_URL + "/" + d.IMG_URL_MD,
				};
	
				arrObj.push(obj);
			});

			let feedTemplate = {
				headerTitle: corp_nm,
				headerLink: listHeaderLink,
				contents: arrObj,
				buttonTitle: '더보기',
				buttons: [listButtons1]
			};
	
	
			KakaoCordovaSDK.sendLinkList( feedTemplate,
				function(response){
					// console.log(JSON.stringify(response));
				},
				function(error) {
					console.log("fail list");
					console.log(JSON.stringify(error));
				}
			);
		}else{

			let feedContent = {
				title	: corp_nm,
				link	: listHeaderLink,
				desc 	: ("배달시간 " + stt_delv + "-" + end_delv) + "\n" + content,
				imageURL: APP_URL + "/" + img,
			};

			let feedTemplate = {
				
				content :  feedContent,
				buttons : [listButtons1]
			};

			KakaoCordovaSDK.sendLinkFeed( feedTemplate,
				function(response){
					// console.log(response);
					console.log(JSON.stringify(response));
				},
				function(error) {
					console.log("fail feed");
					console.log(JSON.stringify(error));
				}
			);

		}
	}
}
