myApp.configControllers = {

    // (배달)주소설정 페이지
    cfAddrPage : function(page){
        myApp.i18Next.changeLang();
        page.onShow = function(){

            var activeTab   = localStorage.getItem("ADDR_ACTIVE_TAB");
            if ( !isNull(activeTab)){
                var tabbr = document.getElementById("addrTabbar");
                // 온센 UI 버그로 인한 setTimeOut 적용
                // console.log(tabbr);
                setTimeout(function(){
                    tabbr.setActiveTab(activeTab);
                }, 100);
            }
            localStorage.setItem("ADDR_ACTIVE_TAB", null);
        }
    },

    //  주소 정보 검색
    configAddrMapHand : function(page){

        $(page).find("#addr1").attr("data-i18n", "[placeholder]LOCATION:ADDR3_INPUT");
        $(page).find("#addr2").attr("data-i18n", "[placeholder]LOCATION:ADDR2_INPUT");
        $(page).find("#message").attr("data-i18n", "[placeholder]SYS:REMARK");

        myApp.i18Next.changeLang();

        var objSelctCity    = $(page).find("#city");
        var objDistrict     = $(page).find("#district");
        var objBuilding     = $(page).find("#building");
        var objBranch       = $(page).find("#branch");

        var option = {
            level       : 1,
            city        : "",
            district    : "",
            building    : "",
            branch_id   : "",
            searchtext  : "",
        };

        var service	= new withWSDL("api/address/list");
        // 주소목록 서비스 호출
        service.success = function(_jsonData){
            var jsonData    = _jsonData;
            if (typeof serviceModel.listAddr === 'undefined') {
                $.getScript("js/service/config.js", function(){
                    serviceModel.listAddr(page, jsonData, option);
                });
            }else{
                serviceModel.listAddr(page, jsonData, option);
            }
        };

        service.ajaxGetData(option);

        // 도시 선택
        objSelctCity.change(function(){
            option.level        = 2;
            option.city         = $(this).find("select option:selected").val();
            option.district     = "";
            option.building     = "";
            option.branch_id    = "";

            service.ajaxGetData(option);
        });

        // 구 선택
        objDistrict.change(function(){
            option.level        = 3;
            option.city         = objSelctCity.find("select option:selected").val();
            option.district     = $(this).find("select option:selected").val();
            option.building     = "";
            option.branch_id    = "";

            service.ajaxGetData(option);
        });

        // 건물 선택
        objBuilding.change(function(){
            option.level        = 4;
            option.city         = objSelctCity.find("select option:selected").val();
            option.district     = objDistrict.find("select option:selected").val();
            option.building     = $(this).find("select option:selected").val();
            option.branch_id    = "";

            service.ajaxGetData(option);
        });

        // 매장 선택
        objBranch.change(function(){
            option.level        = 5;
            option.city         = objSelctCity.find("select option:selected").val();
            option.district     = objDistrict.find("select option:selected").val();
            option.building     = objBuilding.find("select option:selected").val();
            option.branch_id    = $(this).find("select option:selected").val();

            service.ajaxGetData(option);
        });


        // 확인 => 메인 이동
        page.querySelector('.btnGoIndexPage').onclick =  function() {

            var city        = $(page).find("#city select option:selected").val();
            var district    = $(page).find("#district select option:selected").val();
            var building    = $(page).find("#building select option:selected").val();

            // 베트남 주소는 (집번호-작은골목길번호-큰 골목길 번호-대로번호)
            // 한국과 반대임
            var addr1		= building + ", " + $(page).find("#addr1").val() + ", " + city;
            var addr2       = $(page).find("#addr2").val();

            var branch_id   = $(page).find("#branch option:selected").val().trim();
            var areaMessage = $(page).find("#message").val();

            window.localStorage.setItem("IS_FIRST", true);
            window.localStorage.setItem("ADDR1", addr1);
            window.localStorage.setItem("ADDR2", addr2);
            window.localStorage.setItem("BRANCH_ID", branch_id);
            window.localStorage.setItem("AREA_MSG", areaMessage);

            if( !isNull(localStorage.getItem('joinMemberData') ) ){
                myApp.configControllers.exeJoinMember( { ADDR1 : addr1, ADDR2 : addr2, BRANCH_ID : branch_id } );
                return;
            }

            var tempAddr1 = page.querySelector("#addr1").value;
            var tempAddr2 = page.querySelector("#addr2").value;

            if( isNull(tempAddr1) || isNull(tempAddr2)) {
                var t_rqr_addr = myApp.i18Next.getTrasTxt("CONF", "RQR_ADDR");
                runWarningMessage(t_rqr_addr, "warning", 1500, "up");
                return false;
            }

            tel 	             = localStorage.getItem("TEL");
            var serviceToken	 = new withWSDL("api/userm/syncInfo/0");

            serviceToken.success = function(_jsonData){
                var jsonData      = _jsonData;
                if( jsonData.error == 500){
                    runWarningMessage(jsonData.message, "danger", 1500, "up");
                }else{
                    runWarningMessage(jsonData.message, "success", 1500, "up");
                }
                document.querySelector('#myNavigator').popPage();
            };

            if( !isNull( tel ) ){

                var option  = {
                    "phoneNumber"   : tel,
                    'is_check'      : true,
                    "CORP_ID"       : APP_CORP_ID,
                    "BRANCH_ID"     : branch_id,
                };
                serviceToken.ajaxPutData(option);
            }
        };

       
    },

    // 지도상 주소 정보 저장
    configAddrMapPage : function(page){

        $(page).find("#mapAddr2").attr("data-i18n", "[placeholder]LOCATION:ADDR2_INPUT");
        myApp.i18Next.changeLang();
        
        var t_map_drag  = myApp.i18Next.getTrasTxt("CONF", "DRAG_MAP");
        $(page).find(".watingMsg").hide();

        myApp.googleMapControllers.getCurrentGeoInfo( function(pos){    
             myApp.googleMapControllers.showGoogleMap("#addrMap", pos.lat, pos.lng, null );
             myApp.googleMapControllers.getCurrentAddr(pos);

            localStorage.setItem("LAT", pos.lat);
            localStorage.setItem("LNG", pos.lng);
            runWarningMessage(t_map_drag, "success", 2000, "bottom");
        });

        // 모바일 자판 엔터(돋보기 검색아이콘 클릭)
        $(page).off('keydown');
        $(page).on('keydown', function (event) {
            if (event.keyCode === 13) {
                searchAddrByMap();
                document.activeElement.blur();
            }
        });

        
        // 현재 위치 클릭
        page.querySelector('.btnNowLocation').onclick =  function(){

            // 검색어 입력 비우기
            page.querySelector(".searchAddr").value = "";

            if( ons.platform.isWebView()) {

                deviceExGeoLocation(function(){    
                    myApp.googleMapControllers.getCurrentGeoInfo( function(pos){
                        myApp.googleMapControllers.showGoogleMap("#addrMap", pos.lat, pos.lng, null );
                        myApp.googleMapControllers.getCurrentAddr(pos);
                        $(page).find(".watingMsg").hide();

                        localStorage.setItem("LAT", pos.lat);
                        localStorage.setItem("LNG", pos.lng);
                        runWarningMessage(t_map_drag, "success", 2000, "bottom");
                    });
                });
            }else{

                
            }
        };

        // 주소로 검색하기
        page.querySelector('.btnSearchAddr').onclick =  searchAddrByMap;

        function searchAddrByMap () {
            
            let addr = page.querySelector(".searchAddr").value;
            let geocoder = new google.maps.Geocoder();

            geocoder.geocode({'address':addr}, function(results, status){
                $(page).find(".watingMsg").hide();

                var t_err_addr  = myApp.i18Next.getTrasTxt("LOCATION", "ERR_NO_ADDR");
                var t_err_noSvc = myApp.i18Next.getTrasTxt("LOCATION", "SET_GSP_ON");
                var t_err_noFind= myApp.i18Next.getTrasTxt("LOCATION", "ERR_NO_SERVER");

                if(results!=""){

                    var location=results[0].geometry.location;

                    lat = location.lat();
                    lng = location.lng();

                    var latlng = new google.maps.LatLng(lat , lng);
                    var myOptions = {
                        zoom: 12,
                        center: latlng,
                        mapTypeControl: true,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    };

                    myApp.googleMapControllers.showGoogleMap("#addrMap", lat, lng);

                    geocoder.geocode({
                        'latLng': latlng
                    }, function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            if (results[1]) {
                                //console.log(results);
                                // alert(results[3].formatted_address);
                                var addr1 = results[0].formatted_address;
                                var place_id = results[0].place_id;
                                //console.log(place_id);
                                
                                var request = {
                                    placeId: place_id,
                                    fields: ['name', 'formatted_address', 'place_id', 'geometry']
                                };
                                
                                var service = new google.maps.places.PlacesService(map);
                                var placeName = "";
                                var placeAddr = "";
                                service.getDetails(request, function(place, status1) {
                                    if (status1 === google.maps.places.PlacesServiceStatus.OK) {
                                        
                                        placeName = place.name;
                                        placeAddr = place.formatted_address;
                                        //console.log(place.name, place.formatted_address);
                                        // google.maps.event.addListener(marker, 'click', function() {
                                        //     infowindow.setContent('<div><strong>' + place.name + '</strong><br>' +
                                        //         'Place ID: ' + place.place_id + '<br>' +
                                        //         place.formatted_address + '</div>');
                                        //     infowindow.open(map, this);
                                        // });

                                        //alert(placeName + " " + placeAddr);
                                        // document.querySelector(".addr1").value = placeName + " " + placeAddr;
                                        // document.querySelector(".addr1 input").value = placeName + " " + placeAddr;
                                        document.querySelector(".addr1").value = placeAddr;
                                        document.querySelector(".addr1 input").value = placeAddr;

                                        
                                        localStorage.setItem("LAT", latlng.lat());
                                        localStorage.setItem("LNG", latlng.lng());
                                    }
                                });

                            } else {
                                runWarningMessage(t_err_addr, "warning", 1500, "up");
                            }
                        } else {
                            runWarningMessage(t_err_noSvc + status, "warning", 1500, "up");
                        }
                    });

                }else {
                    runWarningMessage(t_err_noFind, "warning", 1500, "up");

                }
            });
        }

        // 확인 => 메인 이동
        page.querySelector('.btnGoIndexPage').onclick =  function() {

            var addr1 = page.querySelector("#mapAddr1").value;
            var addr2 = page.querySelector("#mapAddr2").value;
            var t_rqr_addr  = myApp.i18Next.getTrasTxt("CONF", "RQR_ADDR");

            if( addr1.length == 0 || addr2.length == 0){
                runWarningMessage(t_rqr_addr, "warning", 1500, "up");
                return false;
            }

            var aeraAjx	= new withWSDL("api/areaconf/list");
            aeraAjx.success = function(_jsonData){
                var jsonData    = _jsonData;
                
                var data        = jsonData.list;
                // now_area_id = localStorage.getItem("AREA_ID");
                // new_area_id = data.AREA_ID;

                // new_area_nm = localStorage.getItem("AREA_NM");
                
                // new_area_nm = getLang(data.AREA_NM, data.AREA_NM_VN, data.AREA_NM_EN);
                // new_area_nm = isNull(new_area_nm) ? "" : new_area_nm;
                
                // localStorage.setItem("AREA_ID", new_area_id);
                // localStorage.setItem("AREA_NM", new_area_nm);
                localStorage.setItem("IS_FIRST", true);
                localStorage.setItem("ADDR1", addr1);
                localStorage.setItem("ADDR2", addr2);
                localStorage.setItem("AREA_MSG", null);
                
                // 회원가입시 사용
                if( !isNull( localStorage.getItem('joinMemberData') ) ){
                    myApp.configControllers.exeJoinMember( { ADDR1 : addr1, ADDR2 : addr2, BRANCH_ID : null, AREA_ID : new_area_id} );
                    return;
                }

                if( isNull(addr1) || isNull(addr2)) {
                    runWarningMessage(t_rqr_addr, "warning", 1500, "up");
                    return false;
                }

                // localStorage.setItem("MAIN_RELOAD", "Y");
                document.querySelector('#myNavigator').popPage();
                
            };
            aeraAjx.ajaxGetData(
                {
                    lat     : localStorage.getItem("LAT"),
                    lng     : localStorage.getItem("LNG")
                }
            );
        };
    },

    // 최근주문주소
    recentAddrPage : function(page){
        myApp.i18Next.changeLang();
        var tel     = localStorage.getItem("TEL");
        
        if( !isNull(tel)){
            this.recentAddrList(page);
        }else{
            var noneMessage = "<div class='emptyOrNoData card'><i><img src='img/svg/caution.svg'></i><span data-i18n='COMM:noMoreData'> 더이상 등록된 주문주소가 없습니다. <span></div>";
            $(page).find(".after-list").hide();
            $(page).find(".recentAddrList").append(noneMessage);
        }
    },

	// 환경설정 페이지
    configPage : function(page){

        let initLang    = getLangNow();
        let version     = "";
        myApp.i18Next.changeLang();
        // 앱업데이트 다이어로그
        var verDiag                 = document.querySelector("#version-dialog");
        var btndiagVersionCancel    = document.querySelector(".diagVersionCancel");
        var btndiagVersionOk        = document.querySelector(".diagVersionOk");

        page.onHide = function(){

            // 현재 변경된 언어와 기존 언어와 비교
            // 변경이 되면 리로드됨
            var changeLang  = getLangNow();
            if (initLang != changeLang){
                location.reload();   
            }
        }

        btndiagVersionCancel.onclick = function(){
            verDiag.hide({ animation : "none"});
        }

        btndiagVersionOk.onclick = function(){
            window.open(APP_SHARE_URL, "_blank");
            verDiag.hide({ animation : "none"});
        }

        var serviceVersion	= new withWSDL("api/version/list");

        serviceVersion.success = function(_jsonData){
            var data    = _jsonData.list;
            var ver_msg = myApp.i18Next.getTrasTxt("CONF", "LAST_VERSION");

            if ( !isNull(data) ){
                var lastVer = data.version;
                var isforce = data.is_force;

                if( lastVer != version){
                    // 업데이트 강제여부 확인 => 취소버튼 숨기기
                    if( isforce == "Y"){
                        $(btndiagVersionCancel).hide();
                    }

                    $(page).find(".version").html( version + " (" + ver_msg + " " + lastVer + ")" );
                    verDiag.show();
                }else{
                    $(page).find(".version").html( version + " (" + ver_msg + ")" );
                }
            }else{
                // console.log(myApp.i18Next.getTrasTxt("CONF", "VERSION_ERR"));
            }
        };


        document.addEventListener("deviceready", onDeviceReadyConfig, false);

        function onDeviceReadyConfig() {
            // 빌드버전정보
            version = AppVersion.build;
            
            if( isNull(version) ){
                // 버전확인불가시
                version     = myApp.i18Next.getTrasTxt("CONF", "NO_VERSION");
                page.querySelector(".version").innerHTML = version;
                return;
            }else{

                LOGIN_CLB = () => { 
                    serviceVersion.ajaxGetData({ platform: "iOS" });
                } ;
                if( !showChkTel_Diag() ){
                    return false;
                }else{
                    LOGIN_CLB();
                }
            }
        }

        
        // 언어 설정
        Array.prototype.forEach.call(page.querySelectorAll("ons-radio[name=lang]"), function(ele) {

            if( ele.value == getLangNow() ){
                ele.checked = true;
            }

            ele.onclick = function(){
                window.localStorage.setItem("lang", this.value);

                var param = {
                    "MODE"  : "LANG",
                    "LANG"  : this.value,
                    "TEL"   : localStorage.getItem("TEL"),
                };

                // 전송완료되면 버튼 비활성화
                var service = new withWSDL("api/userm/list/0");
                service.success = function(_jsonData) {
                    var t_set_comple= myApp.i18Next.getTrasTxt("SYS", "SET_COMPLETE") ;
                    runWarningMessage(t_set_comple, "success", 500, "up");
                };

                service.ajaxPutData(param);        
            }
        });

        // 클릭(체인지) 알림 설정 저장
        Array.prototype.forEach.call(page.querySelectorAll("ons-switch"), function(ele) {

            alram   = window.localStorage.getItem("alram");
            alram   = isNull(alram) ? "Y" : alram;

            if( alram == "Y"){
                ele.checked = true;
            }else{
                ele.checked = false;
            }

			ele.onchange = function(event){

				var data = {
					// CORP_ID     : localStorage.getItem("CORP_ID"),
					TEL			: localStorage.getItem("TEL"),
					USE_FCM_YN	: ele.checked ? "Y" : "N" ,
					MODE		: "FCM"
				};

				var service	= new withWSDL("api/userm/list/" + data.TEL);

				service.success = function(_jsonData){

					var jsonData    = _jsonData;
					//console.log(jsonData);
                    var alrmResult	= data.USE_FCM_YN;
                    var t_set_comple= myApp.i18Next.getTrasTxt("SYS", "SET_COMPLETE") ;

                    localStorage.setItem("alram", alrmResult);
                    runWarningMessage(t_set_comple, "success", 1000, "up");
				};

				service.ajaxPutData(data);

			};
        });
    },

	// 최초 : 동의 페이지
    agreePage : function(page){


        var diagLink    = document.querySelector('#link-dialog');

        // 외부링크(새창열기) : 동적바인딩 하므로 jQuery on 이벤트로 설정
        $(document).on("click", ".exLink", function(event){
            var target = event.target;
            var url = $(target).data("url");

            $(".ext-link").text("(" + url + ")");
            diagLink.show();
            $(".button.diagLInkOk").attr("data-url", url);
        });

        // 링크연결 확인
        $(".button.diagLInkOk").click(function(){
            url = $(this).attr("data-url");
            window.open(url, "_blank");
            diagLink.hide();
        })

        // 링크연결 취소
        $(".button.diagLinkCancel").click(function(){
            diagLink.hide();
        })

        document.addEventListener("deviceready", onDeviceReadyAgreePage, false);

        function onDeviceReadyAgreePage() {
            //localStorage.clear();
            deviceGetFnPhoneNumber();
        }

        let switchAll   = page.querySelector("#switchAgreeAll");
        let switchLoc   = page.querySelector("#switchAgreeLocation");
        let switchFCM   = page.querySelector("#switchAgreeFCM");
        let switchPriv   = page.querySelector("#switchAgreeTel");

        let language    = "ko-KR";
        let modelView   = page.querySelector("#modalAgreeView");
        let modelView2  = page.querySelector("#modalAgreeView2");
        let btnNext     = page.querySelector('.btnNext');

        // 전체 동의
        switchAll.addEventListener("change", function(event){
            if(event.value){
                switchLoc.checked = true;
                switchFCM.checked = true;
                switchPriv.checked = true;
                modelView.show();
                modelView2.show();
            }else{
                switchLoc.checked = false;
                switchFCM.checked = false;
                switchPriv.checked = false;
                modelView.hide();
                modelView2.hide();
            }
            checkBtn();
        });

        switchLoc.addEventListener("change", function(){
            checkBtn();
            if( switchLoc.checked){
                modelView.show();
            }
        });
        switchFCM.addEventListener("change", checkBtn);
        switchPriv.addEventListener("change", function(){
            checkBtn();
            if(switchPriv.checked){
                modelView2.show();
            }
        });

        Array.prototype.forEach.call(page.querySelectorAll('ons-radio'), function(element) {
            element.onclick = function(){
                checkBtn();

                if( element.value == window.localStorage.getItem("lang") ){
                    element.checked = true;
                    // console.log(this.value);
                }

                window.localStorage.setItem("lang", this.value);
                myApp.i18Next.changeLang(this.value);
                $('.translation').i18n();
            }
        });

        btnNext.addEventListener("touchstart" ,function() {
            myApp.navigator.pushPage('html/config/config_addr_tabbar.html');
        });

        // btnNext.addEventListener("touchstart" ,function() {
        //     myApp.navigator.pushPage('html/config/config_addr_tabbar.html');
        // });

        page.querySelector('.btnShowViewPop').addEventListener("touchstart" ,function() {
            modelView.show();
        });

        page.querySelector('.btnClosePop').addEventListener("touchstart" ,function() {
            modelView.hide();
        });

        page.querySelector('.btnShowViewPop2').addEventListener("touchstart" ,function() {
            modelView2.show();
        });

        page.querySelector('.btnClosePop2').addEventListener("touchstart" ,function() {
            modelView2.hide();
        });


        function checkBtn(){

            //console.log( $(e) );

            radioChk = false, switcChk = false; switcChk2 = false;

            Array.prototype.forEach.call(page.querySelectorAll('ons-radio'), function(element) {
                if( element.checked ){
                    language = element.value;
                    radioChk = true;
                }
            });

            if(switchLoc.checked){
                switcChk = true;
            }

            if( switchPriv.checked){
                switcChk2 = true;
            }

            if( radioChk && switcChk && switcChk2){
                btnNext.removeAttribute("disabled");
            }else{
                btnNext.setAttribute("disabled");
            }

            if( switchLoc.checked == false || switchFCM.checked == false || switchPriv.checked == false){
                switchAll.checked = false;
            }

            if( switchLoc.checked == true && switchFCM.checked == true && switchPriv.checked == true ) {
                switchAll.checked = true;
            }
        }
    },

    exeJoinMember : function(addData) {

        // 가입진행
        var service	= new withWSDL("api/userm/list");
        service.success = function(_jsonData){

            var jsonData    = _jsonData;
			if( jsonData.status == 200){
				ons.notification.toast('<i class="fa fa-success"></i>' + jsonData.message , {
                                timeout: 1000,
                                animation: 'fall',
                                'class' : 'success',
                                callback : function(){
                                    localStorage.setItem("IS_AFTER_LOGIN_PAGE", "Y");
                                    setTimeout(function(){
                                        location.href =	"index.html";
                                    }, 1000);
                                }
				});

				localStorage.setItem('joinMemberData', null);

			}else{
                runWarningMessage(jsonData.message, "warning", 1500, "up");
			}
        };

        var retrievedObject = JSON.parse( localStorage.getItem('joinMemberData'));

        if ( !isNull( retrievedObject ) ){
            // 주소 / 지점아이디 추가
            retrievedObject.addr1		= localStorage.getItem("ADDR1");
            retrievedObject.addr2		= localStorage.getItem("ADDR2");
            retrievedObject.branch_id	= addData.BRANCH_ID;
            retrievedObject.area_id     = addData.AREA_ID;
            service.ajaxPostData( retrievedObject) ;
        }

    },

    recentAddrList : function(page){

        var option = {
            corp_id       : localStorage.getItem("CORP_ID"),
            user_id       : localStorage.getItem("USER_ID"),
            tel           : localStorage.getItem("TEL"),
            limit         : 10
        };

        var $item;
        var serviceRemove	= new withWSDL("api/order/removeAddr");
        var service	= new withWSDL("api/order/list");
        // 주소목록 서비스 호출
        service.success = function(_jsonData){
            var jsonData    = _jsonData;
            var list        = jsonData.list;
            $listBase       = $(page).find("#list-node");
            noneMessage = "<div class='emptyOrNoData card'><i><img src='img/svg/caution.svg'></i><span data-i18n='COMM:noMoreData'> 더이상 등록된 주문주소가 없습니다. <span></div>";
            
            if( list.length > 0 ){

                var t_addr1  = myApp.i18Next.getTrasTxt("CONF", "addr1");
                
                $(list).each(function(){

                    reg_dt  = moment(this.CREATED_AT).format("YYYY-MM-DD HH:mm");
                    $item   = $("<ons-list-item class='orderHistoryAddr'>" + 
                                "   <div class='left'>" +
                                "       <i class='fa fa-calendar-check-o'></i> <span class='tit green'>" + reg_dt + "</span>" +
                                "       <div class='addr1 addrText'><i class='fa fa-angle-right'></i> " + this.ADDR1 + "</div>" +
                                "       <div class='addr2 addrText'><i class='fa fa-angle-right'></i> " + this.ADDR2 + "</div>" +
                                "   </div>" +
                                "   <div class='right'>" +
                                "       <ons-button class='btnCheckOrderAddr'><i class='fa fa-check'></i></ons-button>" +
                                "   </div>" +
                                "</ons-list-item>");

                    $listBase.append($item);
                });

                if( list.length < option.limit){
                    $(page).find(".after-list").hide();
                    $(page).find(".recentAddrList").append(noneMessage);
                }

            }else{
                $(page).find(".after-list").hide();
				if( $(page).find(".recentAddrList .emptyOrNoData").length == 0 ){
                    $(page).find(".recentAddrList").append(noneMessage);
				}
            }

            // 선택시 배달주소 삭제
            $(page).find(".btnRemoveOrderAddr").click(function(){

                $item1   = $(this).closest(".orderHistoryAddr");
                addr1   = $item1.find(".addr1").text();
                addr2   = $item1.find(".addr2").text();

                serviceRemove.ajaxGetData({
                    'addr1' : addr1,
                    'addr2' : addr2,
                    'tel'   : localStorage.getItem("TEL")
                });
            });

            // 선택시 배달주소로 설정
            $(page).find(".orderHistoryAddr ons-button").click(function(){

                var t_set_comple= myApp.i18Next.getTrasTxt("SYS", "SET_COMPLETE") ;
                $item   = $(this).closest(".orderHistoryAddr");
                addr1   = $item.find(".addr1").text();
                addr2   = $item.find(".addr2").text();
                
                localStorage.setItem("ADDR1", addr1);
                localStorage.setItem("ADDR2", addr2);
                runWarningMessage(t_set_comple, "success", 1500, "up");
                myApp.navigator.popPage( { animation : "none" } );
            });
        };

        service.ajaxGetData(option);
        serviceRemove.success = function(_jsonData){
            var jsonData    = _jsonData;
            var data        = jsonData;

            if(data.status == "success"){
                runWarningMessage(data.message, "success", 1500, "up");
                $item.remove();

            }else{
                runWarningMessage(data.message, "danger", 1500, "up");
            }
        }

        //  최근주문목록 이벤트 등록
        page.onInfiniteScroll = function(done) {

            // if ( $(page).find(".emptyOrNoData.card").length == 0  ) {
                setTimeout(function() {
                   // $(page).find(".after-list").show();
                    option.start += 10;
                    option.clear = "0";
                    // option.orderby = "tb_product.VIEW_ORDER";
                    // 이벤트 목록 호출
                    service.ajaxGetData(option);
                    done();
                }, 200)
            // }
        };
    }
}
