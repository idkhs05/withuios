myApp.googleMapControllers  = {

    map             : "",
    geocoder        : "", 
    geocodemarker   : [],
    marker          : "",
    GreenIcon       : "",

    infowindow : function(){
        return new google.maps.InfoWindow();
    },

    googleMapPage : function(page){

        // console.log("this page is GoogleMap");
        var modal           = document.querySelector('#modalMap');
        
        this.GreenIcon = new google.maps.MarkerImage(
            "http://labs.google.com/ridefinder/images/mm_20_green.png",
            new google.maps.Size(12, 20),
            new google.maps.Point(0, 0),
            new google.maps.Point(6, 20)
        );

        // 크롬일경우 
        // https://onsen.io/v1/reference/ons.platform.html 참고하시오
        if( !ons.platform.isChrome() ){
            ons.notification.toast({message: "브라우저이므로 좌표를 불러올 수 없습니다.", timeout: 1000});
            this.showGoogleMap("#map_canvas", 37.250943, 127.028344);
        }

        //page.querySelector('#codeAddress').onclick = function(ele){
        //    this.codeAddress(ele);;
        //} 


    },

    getCurrentAddr : function(pos){

        myApp.googleMapControllers.geocoder = new google.maps.Geocoder();

        myApp.googleMapControllers.geocoder.geocode({
            'latLng': pos
        }, function (results, status) {

            var t_err_addr  = myApp.i18Next.getTrasTxt("LOCATION", "ERR_NO_ADDR");
            var t_gps_on    = myApp.i18Next.getTrasTxt("LOCATION", "SET_GSP_ON");

            if (status == google.maps.GeocoderStatus.OK) {
                if (results[1]) {
                    //console.log(results);
                    // alert(results[3].formatted_address);
                    var addr1 = results[0].formatted_address;
                    var place_id = results[0].place_id;
                    //console.log(place_id);
                    
                    var request = {
                        placeId: place_id,
                        fields: ['name', 'formatted_address', 'place_id', 'geometry']
                    };
                    
                    var service = new google.maps.places.PlacesService(myApp.googleMapControllers.map);
                    var placeName = "";
                    var placeAddr = "";
                    service.getDetails(request, function(place, status1) {
                        if (status1 === google.maps.places.PlacesServiceStatus.OK) {
                            
                            placeName = place.name;
                            placeAddr = place.formatted_address;
                            //console.log(place.name, place.formatted_address);
                            // google.maps.event.addListener(marker, 'click', function() {
                            //     infowindow.setContent('<div><strong>' + place.name + '</strong><br>' +
                            //         'Place ID: ' + place.place_id + '<br>' +
                            //         place.formatted_address + '</div>');
                            //     infowindow.open(map, this);
                            // });

                            //alert(placeName + " " + placeAddr);
                            document.querySelector(".addr1").value = placeName + " " + placeAddr;
                            document.querySelector(".addr1 input").value = placeName + " " + placeAddr;
                        }
                    });
                    
                } else {
                    runWarningMessage( t_err_addr , "warning", 2000, "up");
                }
            } else {
                runWarningMessage(t_gps_on , "warning", 2000, "up");
            }
        });

    },

    getCurrentGeoInfo : function(callback){
        // Try HTML5 geolocation.

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
  
                // infoWindow.setPosition(pos);
                // infoWindow.setContent('Location found.');
                // infoWindow.open(map);
                // map.setCenter(pos);

                if(typeof callback === 'function') {
                    callback(pos);
                }
            }, function() {
                // handleLocationError(true, infoWindow, map.getCenter());
            });
        } else {

            // console.log("geolocation else");
            // Browser doesn't support Geolocation
            // handleLocationError(false, infoWindow, map.getCenter());
        }
    },

    showGoogleMap : function(mapId, _lat, _lng, option ){

        var longitude   = _lng;
        var latitude    = _lat;
        var latLong     = new google.maps.LatLng(latitude, longitude);

        localStorage.setItem("LAT", latitude);
        localStorage.setItem("LNG", longitude);

        var isDraggable = true;
        var isShowCommnet = true;

        if( !isNull(option) ){
            isDraggable     = option.isDraggable;
            isShowCommnet   = option.isShowCommnet;
        }

        var mapOptions = {
            center: latLong,
            zoom: 13,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        myApp.googleMapControllers.map        = new google.maps.Map(document.querySelector(mapId), mapOptions);
        myApp.googleMapControllers.geocoder   = new google.maps.Geocoder();

        var infoWin = this.infowindow();
        infoWin.setPosition(latLong);

        infoWin.setContent('Result Location');
        if(isDraggable){
            // infoWin.setContent('현재 위치를 드래그해서 주소를 정확히 입력해주세요');
        }
        infoWin.open(myApp.googleMapControllers.map);

        myApp.googleMapControllers.marker = new google.maps.Marker({
            position    : latLong,
            map         : myApp.googleMapControllers.map,
            draggable   : isDraggable,
        });

        if( isShowCommnet ) {
            google.maps.event.addListener(myApp.googleMapControllers.marker, 'dragend', function(evt){
                // document.getElementById('current').innerHTML = '<p>Marker dropped: Current Lat: ' + evt.latLng.lat().toFixed(3) + ' Current Lng: ' + evt.latLng.lng().toFixed(3) + '</p>';
                //ons.notification.toast({ message: '좌표설정 완료 : 현재 위도: ' + evt.latLng.lat().toFixed(3) + ' 현재 경도: ' + evt.latLng.lng().toFixed(3) , timeout: 1000 });
                myApp.googleMapControllers.getCurrentAddr(evt.latLng);
            });

            google.maps.event.addListener(myApp.googleMapControllers.marker, 'dragstart', function(evt){
                ons.notification.toast({ message: 'Searching...', timeout: 500 });
                // document.getElementById('current').innerHTML = '<p>Currently dragging marker...</p>';
            });
        }

        setTimeout(function() {
            infoWin.close();
        }, 2000); // random delay to make sure you see the fade
    },

    Setmarker : function (latLng) {
        if (marker.length > 0){
            marker[0].setMap(null); 
        }
      
        // marker.length는 marker라는 배열의 원소의 개수입니다.
        // 만약 이 개수가 0이 아니라면 marker를 map에 표시되지 않게 합니다.
        // 이는 다른 지점을 클릭할 때 기존의 마커를 제거하기 위함입니다.
      
        marker = [];
        marker.length = 0;
      
        // marker를 빈 배열로 만들고, marker 배열의 개수를 0개로 만들어 marker 배열을 초기화합니다
        
        marker.push(new google.maps.Marker({
            position: latLng,
            map: this.map
      
        } ));
      
        // marker 배열에 새 marker object를 push 함수로 추가합니다.
      
    }, 

     /* 
        클릭한 지점에 마커를 표시하는 함수입니다.
         그런데 이 함수를 잘 봐야 하는 것이 바로 marker를 생성하지 않고 marker라는 배열 안에 Marker 
         obejct  원소를 계속 추가하고 있습니다. 이는 매번 클릭할 때마다 새로운 마커를 생성하기 위함입니다. 
     */
      
    //입력 받은 주소를 지오코딩 요청하고 결과를 마커로 지도에 표시합니다.  
    codeAddress : function(event) {
      
        if (myApp.googleMapControllers.geocodemarker.length > 0) {
             for (var i=0;i<myApp.googleMapControllers.geocodemarker.length ;i++ ) {
                myApp.googleMapControllers.geocodemarker[i].setMap(null);
             }
             myApp.googleMapControllers.geocodemarker =[];
             myApp.googleMapControllers.geocodemarker.length = 0;
        }
 
        //이 부분도 위와 같이 주소를 입력할 때 표시되는 marker가 매번 새로 나타나게 하기 위함.
 
        var address = document.getElementById("addr1").value;
        //아래의 주소 입력창에서 받은 정보를 address 변수에 저장.
 
        //지오코딩하는 부분입니다.
        myApp.googleMapControllers.geocoder.geocode( {'address': address}, function(results, status) {
 
            //Geocoding이 성공적이라면,
            if (status == google.maps.GeocoderStatus.OK)  {
    
                runWarningMessage( results.length + "개의 결과를 찾았습니다." , "success", 1000, "up");

                //결과의 개수를 표시하는 창을 띄웁니다. alert 함수는 알림창 함수입니다.
                for(var i=0;i<results.length;i++){
    
                    myApp.googleMapControllers.map.setCenter(results[i].geometry.location);
                    myApp.googleMapControllers.geocodemarker.push(new google.maps.Marker(    
                            {
                                center: results[i].geometry.location,
                                position: results[i].geometry.location,
                                icon: myApp.googleMapControllers.GreenIcon,
                                map: myApp.googleMapControllers.map
                            }
                        )
                    );
                } 
                //결과가 여러 개일 수 있기 때문에 모든 결과를 지도에 marker에 표시.
            } else { 
                runWarningMessage( "Geocode was not successful for the following reason: " + status , "warning", 1500, "up");
            }
        });
    }
}