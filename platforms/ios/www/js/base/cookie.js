
var delay = (function(){
    var timer = 0;
    return function(callback, ms, that){
        clearTimeout (timer);
        timer = setTimeout(callback.bind(that), ms);
    };
})();


function getCookie( name ){ 
    var nameOfCookie = name + "="; 
    var x = 0; 
    while ( x != document.cookie.length ) { 
        var y = (x+nameOfCookie.length); 
        if ( document.cookie.substring( x, y ) == nameOfCookie ) { 
            if ( (endOfCookie=document.cookie.indexOf( ";", y )) == -1 ) 
                endOfCookie = document.cookie.length; 
            return unescape( document.cookie.substring( y, endOfCookie ) ); 
        } 
        x = document.cookie.indexOf( " ", x ) + 1; 
        if ( x == 0 ) 
            break; 
    } 
    return ""; 
}

// 기존 쿠키데이터에 검색 항목이 없는 경우만 
// 쿠키에 입력시킴
function setCookie( name, value, expiredays, itemPrefix ) { 

    var cookies     = document.cookie.split("; ");
    var searchText  = unescape( value );
    var hasText     = false;
    
    for(var i = 0 ; i < cookies.length ; i++){
        if( cookies[i].split("=")[0].indexOf(itemPrefix) >= 0 ){

            cookieText  = unescape(cookies[i].split("=")[1]);        
            if( searchText == cookieText){
                hasText     = true;
                break;
            }   
        }
    } 

    if( !hasText ){
        var todayDate = new Date(); 
        todayDate.setDate( todayDate.getDate() + expiredays ); 
        document.cookie = name + "=" + escape( value ) + "; path=/; expires=" + todayDate.toGMTString() + ";"     
    }
}

function deleteCookie( cookieName ){
    var expireDate = new Date();
  
    //어제 날짜를 쿠키 소멸 날짜로 설정한다.
    expireDate.setDate( expireDate.getDate() - 1 );
    document.cookie = cookieName + "= " + "; expires=" + expireDate.toGMTString() + "; path=/";
}

// 페이지내의 히스토리 내역 전부 (쿠키)삭제
function removeAllSearch(page){
    Array.prototype.forEach.call(page.querySelectorAll('.btnRemoveCookie'), function(element) {
        tname = $(element).attr("data-cname");
        deleteCookie(tname);
        $(element).parents("ons-list-item").remove();
    });
}

// 최근에 검색된 쿠키데이터가 제일 처음에 조회되어야함으로
// 역순으로 조회시킴
function showListCookie(page, itemPrefix, callback){

    var str = "";

    if(document.cookie == ""){
        str = "입력된 쿠키가 없습니다.";
    }else{
        //  ; 을 이용해서 여러개 쿠키 읽어오기
        var cookies = document.cookie.split("; ");
        //var count = 10;
        for(var i = (cookies.length-1) ; i > 0 ; i--){
            if( cookies[i].split("=")[0].indexOf(itemPrefix) >= 0 ){
                
                var cname =  cookies[i].split("=")[0] ;
                ul = page.querySelector("ons-list.searchItemCookie");
                $(ul).append("<ons-list-item>" + 
                                "  <label class='left historyValue'>" + unescape(cookies[i].split("=")[1]) + "</label>" + 
                                "  <ons-button class='right btnRemoveCookie' modifier='outline' data-cname='" + cname + "'>" + 
                                "     <ons-icon icon='fa-remove' fixed-width='false'></ons-icon>" + 
                                "  </ons-button>" + 
                                "</ons-list-item>");
                
            }
        } 

        // 히스토리 목록 선택 및 검색
        Array.prototype.forEach.call(page.querySelectorAll('.historyValue'), function(element) {
            element.onclick = function() {
                tname = $(this).text();
                page.querySelector(".searchItem").value = tname;
                // console.log(callback);
                searchCookie(page, tname, itemPrefix, callback );
            };
        });

        // 히스토리 내역 개별 (쿠키)삭제
        Array.prototype.forEach.call(page.querySelectorAll('.btnRemoveCookie'), function(element) {
            element.onclick = function() {
                tname = $(this).attr("data-cname");
                deleteCookie(tname);
                $(this).parents("ons-list-item").remove();
            };
        });
    }
}

function searchCookie(page, searchItemValue, itemPrefix, callback) {

    var tempVal = searchItemValue;
    var length =  tempVal.length ; 
    
    if(length > 10 ){
        ons.notification.toast({message: "10자 이상은 입력할 수 없습니다", timeout: 500});
        page.querySelector(".searchItem").value = tempVal.substr(0, 9);
        return;
    }else{

    }
    
//    delay(function(){
        //ons.notification.toast({message: "검색실행", timeout: 1000});
        var name  =  itemPrefix + moment().format();
        var value =  page.querySelector(".searchItem").value ;

        // console.log(value);
        // 1자 이상 입력해야 검색됨 (change 이벤트로 인해 넣어야함)
        if( value.length >= 1 ){
            setCookie( name, value, 3650, itemPrefix );
            if(typeof callback === 'function') {
                // console.log('function');
                callback(value);
            }
        }
//    }, 1000, this);
};