myApp.controllers = {

    // 전체 검색조회 페이지(공지/이벤트, 전화번호, 상품정보 검색)
    favoritePage : function(page){

        myApp.i18Next.changeLang();
        var diagRmAll       = page.querySelector("#favoriteRemoveAll-dialog");
        var svcRmFavorite   = new withWSDL("api/favorite/list/0");
       
        // 즐겨찾기 전체 삭제 다이어로그 오픈
        $(page).find(".btnRemoveAll").click(function(){
            diagRmAll.show( { "animation": "none" } );
        });

        // 즐겨찾기 전체 삭제 다이어로그 닫기(취소)
        $(page).find(".btnDialogCancelAll").click(function(){
            diagRmAll.hide( { "animation": "none" } );
        });

         // 즐겨찾기 전체 삭제 실행
         $(page).find(".btnDialogRemoveAllFavoriteOk").click(function(){
            var option      = {
                all : "all",
                tel : localStorage.getItem("TEL"),
            };
            svcRmFavorite.ajaxDeleteData(option);
        });

        svcRmFavorite.success = function(_jsonData){

            var jsonData    = _jsonData;
            var data        = jsonData;
            
            if(data.status == 200 ){
                runWarningMessage(data.message, "success", 1500, "up");
                diagRmAll.hide({ "animation": "none" });
                callFavoriteList();
            }else{
                runWarningMessage(data.message, "danger", 1500, "up");
            }
        };

        var sOption = {
            "isShowCorp": true,
            "user_id"   : localStorage.getItem("ID"),
            "tel"       : localStorage.getItem("TEL"),
            'file_div'  : 'PRODUCT',
            'start'     : 0,
            'clear'     : 1,
            "area_id"   : localStorage.getItem("AREA_ID"),
        };

        var service = new withWSDL('api/favorite/list');

        // 즐겨찾기 목록 바인딩
        service.success = function(_jsonData) {
            var jsonData = _jsonData;
            if (typeof serviceModel.listFavorite === 'undefined') {
                $.getScript("js/service/favorite.js", function() {
                    serviceModel.listFavorite(page, jsonData, service.DOMAIN, sOption, callFavoriteList);
                });
            } else {
                serviceModel.listFavorite(page, jsonData, service.DOMAIN, sOption, callFavoriteList);
            }
        };
       
        // 장바구니 목록 호출 함수
        function callFavoriteList() {

            sOption = {
                "user_id"   : localStorage.getItem("ID"),
                "tel"       : localStorage.getItem("TEL"),
                'start'     : 0,
                'clear'     : 1,
                "area_id"   : localStorage.getItem("AREA_ID"),
            };

            // 즐겨찾기 목록 조회 서비스 호출
            service.ajaxGetData(sOption);
        }

        // 즐겨찾기 실시간 추가되므로 조회시 추가된 내역이 조회되어야하므로, onShow 이벤트로 수시로 reload 시켜야함
        page.onShow = function() {
            
            myApp.commonController.common();

            LOGIN_CLB = () => { 
                callFavoriteList();
            } ;
            if( !showChkTel_Diag() ){
                return false;
            }else{
                LOGIN_CLB();
            }
        }

        var option = {
            start       : 0,
            orderby     : "tb_product.VIEW_ORDER",
            order       : "ASC",
            //corp_id     : pOption.CORP_ID,
            corp_cls    : null,
            is_menu     : 'Y',
            file_div    : "PRODUCT",
        };

        // 장바구니 클릭
        page.querySelector(".myCart").onclick = function() {

            myApp.navigator.popPage( {
                animation : "none",
                callback : function(){
                    document.getElementById('appTabbar').setActiveTab(3);
                }
            });
        };

        page.onInfiniteScroll = function(done) {
            setTimeout(function() {
                // $(page).find(".after-list").show();
                sOption.start += 15;
                sOption.clear = "0";
                // 이벤트 목록 호출
                service.ajaxGetData(sOption);
                done();
            }, 200)
        };

        // 상품상세조회 팝업
        myApp.controllers.productPopup(page, option);

    },

    // 전체 검색조회 페이지(공지/이벤트, 전화번호, 상품정보 검색)
    searchAllPage : function(page){

        myApp.i18Next.changeLang();

        document.querySelector('ons-back-button').onClick = function() {
            document.querySelector('ons-navigator').popPage({ animation: "none"});
        };

        // 전체검색에서 넘어온 검색어(파라미터)
        var pSearchText     = page.data.search;

        myApp.commonController.common();
        var actionSheet = {};

        // 상단 카테고리 메뉴 모달 클릭 off
        $(page).off("click", ".btnCateMenu.enalble");

        var option = {
            start: 0,
            // cate_nth : nth_cate,
            orderby: "tb_product.VIEW_ORDER",
            order: "ASC",
            isprove: "Y",
            corp_id: "",
            // corp_cls: data.cateCode,
            // corp_cls : corp_cls,
            is_menu: 'Y',
            isadult: 'N',
            adult_yn : 'N',
            clear: 1,
            file_div: "PRODUCT",
            isShowCorp : true,
            area_id  : localStorage.getItem("AREA_ID"),
            sns_type : localStorage.getItem("SNS_TYPE"),
            age_range: localStorage.getItem("AGE_RANGE"),
        };

       
        var service = new withWSDL("api/product/list");

        // 상품목록 서비스 바인딩
        service.success = function(_jsonData) {

            var jsonData    = _jsonData;
            
            if (typeof serviceModel.listProduct === 'undefined') {
                $.getScript("js/service/product.js", function() {
                    serviceModel.listProduct(page, jsonData, service.DOMAIN, option);
                });
            } else {
                serviceModel.listProduct(page, jsonData, service.DOMAIN, option);
            }
        };

        // 상품목록 서비스 호출
        $(page).find(".imageList ons-list-item").remove();
        $(page).find(".imageList .emptyOrNoData").remove();

        page.querySelector(".btnSearch").click = function(){
            service.ajaxGetData(option);
        }

        // 전체검색에서 넘어온 검색어가 있을 경우 자동으로 할당 후 검색 함
        if( pSearchText.length > 0 ){
            $(page).find("#searchItem input[type='search']").val(pSearchText);
            searchProduct();
        }

        //  상품 카테고리 이벤트 등록
        page.onInfiniteScroll = function(done) {
            setTimeout(function() {
                // $(page).find(".after-list").show();
                option.start += 15;
                option.clear = "0";
                option.orderby = "tb_product.VIEW_ORDER";
                // 이벤트 목록 호출
                service.ajaxGetData(option);
                done();
            }, 200);
        
        };
        

        // 모바일 상품명 검색
        $(page).off("click", ".search.btnSearch");
        $(page).on("click", ".search.btnSearch", searchProduct);

        // 모바일 자판 엔터(돋보기 검색아이콘 클릭)
        $(page).off('keydown');
        $(page).on('keydown', function (event) {
            if (event.keyCode === 13) {
                searchProduct();
                document.activeElement.blur();
            }
        });

        // 검색 오브젝트 생성
        actionSheet.showFromObject = function() {

            var t_filer   = myApp.i18Next.getTrasTxt("PRD", "VIEW_FILT");
            var t_brand   = myApp.i18Next.getTrasTxt("PRD", "S_BRAND");
            var t_prod    = myApp.i18Next.getTrasTxt("PRD", "S_PROD");
            var t_sale    = myApp.i18Next.getTrasTxt("PRD", "S_SALE");
            var t_new     = myApp.i18Next.getTrasTxt("PRD", "S_NEW");
            var t_star    = myApp.i18Next.getTrasTxt("PRD", "S_STAR");
            var t_basic   = myApp.i18Next.getTrasTxt("PRD", "S_BS");

            ons.openActionSheet({
                title: t_filer,
                icon: "fa-close",
                cancelable: true,
                buttons: [{
                        label: t_brand,
                        icon: "md-square-o",
                        modifier: 'material'
                    },
                    {
                        label: t_prod,
                        icon: "md-square-o",
                        modifier: 'material'
                    },
                    {
                        label: t_sale,
                        icon: "md-square-o",
                        modifier: 'material'
                    },
                    {
                        label: t_new,
                        icon: "md-square-o",
                        modifier: 'material'
                    },
                    {
                        label: t_star,
                        icon: "md-square-o",
                        modifier: 'material'
                    },
                    {
                        label: t_basic,
                        icon: "md-square-o",
                        modifier: 'material'
                    },
                    {
                        label: 'Cancel',
                        icon: 'md-close'
                    }
                ]
            }).then(function(index) {

                option.start = 0;
                option.clear = 1;
                option.orderby = "tb_product.VIEW_ORDER";
                option.order = "DESC";
                option.file_div = "PRODUCT";

                switch (index) {
                    case 0:
                        option.orderby = "tb_product_class.PROD_CLS_NAME";
                        break;
                    case 1:
                        if( getLangNow() == "ko-KR"){
                            option.orderby = "tb_product.PRODUCT_NM";
                        }else if( getLangNow() == "vn-VN" ){
                            option.orderby = "tb_product.PRODUCT_NM_VN";
                        }else{
                            option.orderby = "tb_product.PRODUCT_NM_EN";
                        }
                        break;
                        
                    case 2:
                        option.orderby = "tb_product.CREATED_AT";
                        break;
                    case 3:
                        option.orderby = "tb_product.QTY_SUM";
                        break;
                    case 4:
                        option.orderby = "FN_STAR_SUM(tb_product.PRODUCT_CD)";
                        break;
                    case 5:
                        option.orderby = "tb_product.VIEW_ORDER";
                        break;
                }


                if (index != 6) {
                    service.ajaxGetData(option);
                }

            });
        };

        // 조회 필터 팝업
        page.querySelector("#searchFilter").onclick = actionSheet.showFromObject;

        // 장바구니 클릭
        page.querySelector(".myCart").onclick = function() {
            myApp.navigator.popPage(
              {
                  animation : "none",
                  callback : function(){
                      document.getElementById('appTabbar').setActiveTab(3);
              }
          });
        };

        // 상품상세조회 팝업
        this.productPopup(page, option);

        function searchProduct(){
            var productNm = $(page).find("#searchItem input[type='search']").val();

            option.start    = 0;
            option.clear    = "1";
            option.orderby  = "tb_product.VIEW_ORDER";
            option.productNm = productNm;
            // 이벤트 목록 호출
            service.ajaxGetData(option);

        }
    },

    // 카테고리 탭바
    categoryIndexPage: function(page, corpId) {

        page.onShow = function() {

            if( $(page).find("#cateTabbar").length == 0){
                var temp_msg    = myApp.i18Next.getTrasTxt("LOCATION", "NO_COMP_MSG");
                var noneMessage =   "<div class='emptyOrNoData card mainEmpty' style='margin-top:50px;'>" + 
                                    "   <i><img src='img/svg/caution.svg'></i>" + 
                                    "   <span data-i18n='LOCATION:NO_COMP_MSG'>" + temp_msg + "</span>" + 
                                    "</div>";
            
                $(page).find(".page__content").html(noneMessage);
            }
        };
        
    },

    // 상품 카테고리 페이지
    categoryPage: function(page) {
        
        page.onShow = function() {

            done = false;
            chkTelNum();

            var corpId      = $("#cateTabbar").find("ons-tab.active").data("corpid");
            var nth_cate    = 0;
            
            if( isNull(corpId) ){
                corpId = localStorage.getItem("CORP_ID_CATE");
            }else{
                localStorage.setItem("CORP_ID_CATE", corpId);
            }
            
            var nthTab      = $("#cateTabbar").find("ons-tab.active").data("nth");
            var corpName    = $("#cateTabbar").find("ons-tab.active").attr("label");

            localStorage.setItem("CATE_NAME", corpName);
            document.getElementById('cateTabbar').setActiveTab(nthTab);
            
            // 메뉴 이동시 탭바 맞추기
            var _width = 0;
            $('#cateTabbar ons-tab').each(function (i){
                // console.log("i", i, index, $(this).width(), _width-32);
                if( i == nthTab){
                    $('#cateTabbar .tabbar').scrollLeft( _width );
                    return false;
                }
                _width +=   $(this).width()+32 ;
            });

            
            localStorage.setItem("CATE_NTH", nthTab);
            $("#cateTabbar").find("ons-tab[data-corpid='" + corpId + "']").trigger("click");

            Array.prototype.forEach.call(page.querySelectorAll('.CateMenu'), function(element) {
                element.setAttribute("data-corp_id", corpId);
            });

            var option = {
                start       : 0,
                orderby     : "tb_company.VIEW_ORDER",
                order       : "ASC",
                corp_id     : corpId,
                is_menu     : 'Y',
                adult_yn    : 'N',
                file_div    : "PRODUCT_CLASS",
                root_class  : ['corpMenu', 'popBody'],
                child_clsss : ['swiper-slide', 'categoryMenu'],
                // sns_type    : localStorage.getItem("SNS_TYPE"),
                // age_range   : localStorage.getItem("AGE_RANGE"),
                sns_type : localStorage.getItem("SNS_TYPE"),
                age_range: localStorage.getItem("AGE_RANGE"),

            };
           
            var service1 = new withWSDL("api/productclass/list");
            // 상품군 목록 데이터 바인딩
            service1.success = function(_jsonData) {
                var jsonData = _jsonData;
                if (typeof serviceModel.listCategory === 'undefined') {
                    $.getScript("js/service/product.js", function() {
                        serviceModel.listCategory(page, jsonData, service1.DOMAIN, null, nth_cate);
                    });
                } else {
                    serviceModel.listCategory(page, jsonData, service1.DOMAIN, null, nth_cate);
                }

                // 페이지 로딩 감추기
                $(page).find(".pageLoading").hide();
            };

            option.orderby = "tb_product_class.VIEW_ORDER";
            // 상품군 목록 호출

            if( $(page).find(".categoryBody .card").length == 0 ){
                service1.ajaxGetData(option);
            }
        }

    },

    // 상품 목록 페이지
    productListPage: function(page, nth_cate) {

        document.querySelector('ons-back-button').onClick = function() {
            document.querySelector('ons-navigator').popPage({ animation: "none"});
        };

        page.querySelector("ons-toolbar .center span").innerHTML = localStorage.getItem("CATE_NAME");
        myApp.commonController.common();
        myApp.i18Next.changeLang();


        var data = page.pushedOptions.data;
        var clsdata  = data.clsdata;
        var corpData = data.corpData;
        var holyData = data.holyData;

        var cateName = data.cateName;
        var corpLogo = corpData.IMG_URL_MD;
        var service = new withWSDL("api/product/list");

        Array.prototype.forEach.call(document.querySelectorAll('img.corpLogo'), function(ele) {
            $(ele).show();
            ele.setAttribute("src", service.DOMAIN + "//" + corpLogo);
        });
        
        page.querySelector("h4.cateName").innerHTML = cateName;
        
        var actionSheet = {};
        var menuViewPopup = page.querySelector("#menuViewPopup");

        // 상단 카테고리 메뉴 모달 클릭 off
        $(page).off("click", ".btnCateMenu.enalble");

        var corp_cls = localStorage.getItem("CORP_CLS");
        corp_cls = isNull(corp_cls) ?  data.cateCode : corp_cls;


        // 상품군의 슬라이드 순서 초기화 작업 : 19/11/18
        nth_cate = ( isNull(nth_cate) && nth_cate != 0 ) ?  data.nthCate : nth_cate;
        var option = {
            start: 0,
            cate_nth : nth_cate,
            orderby: "tb_product.VIEW_ORDER",
            order: "DESC",
            isprove: "Y",
            corp_id: data.corpId,
            // corp_cls: data.cateCode,
            corp_cls : corp_cls,
            is_menu: 'Y',
            isadult: 'N',
            adult_yn : 'N',
            clear: 1,
            file_div: "PRODUCT",
            root_class: ['cateMenu', 'popBody'],
            child_clsss: ['swiper-slide', 'categoryMenu'],
            sns_type    : localStorage.getItem("SNS_TYPE"),
            age_range   : localStorage.getItem("AGE_RANGE"),
            tel         : localStorage.getItem("TEL"),
            lang        : localStorage.getItem("lang")
            
        };

        // 검색대 전환
        $(page).find(".searchProduct").click(function(){
            $(page).find(".showTitleProduct").hide( { "animation": "none" } );
            $(page).find(".showSearchProduct").show( { "animation": "none" } );
            $(page).find("#searchItem input[type='search']").focus();
        });

        // 상품목록 서비스 바인딩
        service.success = function(_jsonData) {

            var jsonData    = _jsonData;
            
            if (typeof serviceModel.listProduct === 'undefined') {
                $.getScript("js/service/product.js", function() {
                    serviceModel.listProduct(page, jsonData, service.DOMAIN, option);
                });
            } else {
                serviceModel.listProduct(page, jsonData, service.DOMAIN, option);
            }
        };


        // 상품목록 서비스 호출
        $(page).find(".imageList ons-list-item").remove();
        $(page).find(".imageList .emptyOrNoData").remove();
        service.ajaxGetData(option);

        
        var service1 = new withWSDL("api/productclass/list");
        // // 상품 카테고리 서비스 바인딩
        
        if (typeof serviceModel.setCategoryInProduct === 'undefined') {
            $.getScript("js/service/product.js", function() {
                serviceModel.setCategoryInProduct(page, clsdata, corpData, holyData, service1.DOMAIN, option, nth_cate);
                serviceModel.setCategoryInProduct(page, clsdata, corpData, holyData, service1.DOMAIN, null, nth_cate);
            });
        } else {
            serviceModel.setCategoryInProduct(page, clsdata, corpData, holyData, service1.DOMAIN, option, nth_cate);
            serviceModel.setCategoryInProduct(page, clsdata, corpData, holyData, service1.DOMAIN, null, nth_cate);
        }

        //  상품 카테고리 이벤트 등록
        page.onInfiniteScroll = function(done) {

            // if ( $(page).find(".emptyOrNoData.card").length == 0  ) {
                setTimeout(function() {
                   // $(page).find(".after-list").show();
                    option.start += 15;
                    option.clear = "0";
                    option.orderby = "tb_product.VIEW_ORDER";
                    // 이벤트 목록 호출
                    service.ajaxGetData(option);
                    done();
                }, 200);
            // }
        };
        

        // 모바일 상품명 검색
        $(page).off("click", ".search.btnSearch");
        $(page).on("click", ".search.btnSearch", searchProduct);

        // 모바일 자판 엔터(돋보기 검색아이콘 클릭)
        $(page).off('keydown');
        $(page).on('keydown', function (event) {
            if (event.keyCode === 13) {
                searchProduct();
                document.activeElement.blur();
            }
        });

        // 클릭 : 메뉴팝업
        page.querySelector(".btnCollapseMenu").onclick = function() {
            
            var corpName    = getLang( corpData.CORP_NM, corpData.CORP_NM_VN, corpData.CORP_EN);
            $(menuViewPopup).find(".corpName").html( corpName );
            menuViewPopup.show({ "animation": "none" });
        };

        // 상품 클래스(군) 1차 메뉴 전체 팝업 닫기
        Array.prototype.forEach.call(page.querySelectorAll(".btnCloseCollapseMenu"), function(element) {
            element.onclick = function() {
                menuViewPopup.hide({ "animation": "none" });
            };
        });

        // 검색 오브젝트 생성
        actionSheet.showFromObject = function() {
            
            var t_filer   = myApp.i18Next.getTrasTxt("PRD", "VIEW_FILT");
            var t_brand   = myApp.i18Next.getTrasTxt("PRD", "S_BRAND");
            var t_prod    = myApp.i18Next.getTrasTxt("PRD", "S_PROD");
            var t_sale    = myApp.i18Next.getTrasTxt("PRD", "S_SALE");
            var t_new     = myApp.i18Next.getTrasTxt("PRD", "S_NEW");
            var t_star    = myApp.i18Next.getTrasTxt("PRD", "S_STAR");
            var t_basic   = myApp.i18Next.getTrasTxt("PRD", "S_BS");

            ons.openActionSheet({
                title: t_filer,
                icon: "fa-close",
                cancelable: true,
                buttons: [{
                        label: t_brand,
                        icon: "md-square-o",
                        modifier: 'material'
                    },
                    {
                        label: t_prod,
                        icon: "md-square-o",
                        modifier: 'material'
                    },
                    {
                        label: t_sale,
                        icon: "md-square-o",
                        modifier: 'material'
                    },
                    {
                        label: t_new,
                        icon: "md-square-o",
                        modifier: 'material'
                    },
                    {
                        label: t_star,
                        icon: "md-square-o",
                        modifier: 'material'
                    },
                    {
                        label: t_basic,
                        icon: "md-square-o",
                        modifier: 'material'
                    },
                    {
                        label: 'Cancel',
                        icon: 'md-close'
                    }
                ]
            }).then(function(index) {

                option.start    = 0;
                option.clear    = 1;
                option.orderby  = "tb_product.VIEW_ORDER";
                option.order    = "DESC";
                option.file_div = "PRODUCT";

                switch (index) {
                    case 0:
                        option.orderby = "tb_product_class.PROD_CLS_NAME";
                        break;
                    case 1:
                        if( getLangNow() == "ko-KR"){
                            option.orderby = "tb_product.PRODUCT_NM";
                        }else if( getLangNow() == "vn-VN" ){
                            option.orderby = "tb_product.PRODUCT_NM_VN";
                        }else{
                            option.orderby = "tb_product.PRODUCT_NM_EN";
                        }
                        break;
                    case 2:
                        option.orderby = "tb_product.CREATED_AT";
                        break;
                    case 3:
                        option.orderby = "tb_product.QTY_SUM";
                        break;
                    case 4:
                        option.orderby = "FN_STAR_SUM(tb_product.PRODUCT_CD)";
                        break;
                    case 5:
                        option.orderby = "tb_product.VIEW_ORDER";
                        break;
                }

                if (index != 6) {
                    service.ajaxGetData(option);
                }

            });
        };

        // 조회 필터 팝업
        page.querySelector("#searchFilter").onclick = actionSheet.showFromObject;

        // 장바구니 클릭
        page.querySelector(".myCart").onclick = function() {
            myApp.navigator.popPage(
              {
                  animation : "none",
                  callback : function(){
                      document.getElementById('appTabbar').setActiveTab(3);
              }
          });
        };

        // 위드유 카테고리 바로가기
        // page.querySelector(".goWithUCategory").onclick = function() {
        //     localStorage.setItem("CATE_NTH", 0);
        //     document.getElementById('appTabbar').setActiveTab(1);
        //     myApp.navigator.popPage( { animation : "none" } );
        //     document.querySelector('#cateTabbar').setActiveTab(0);
        // };

        // 상단 카테고리 메뉴 클릭 이벤트
        $(page).on("click", ".btnCateMenu.enalble", function(ele) {

            localStorage.setItem("CORP_CLS", $(this).attr("data-cate_code"));
            var cateName    = $(this).find(".cateName").text();
            var nth         = $(this).data("nth");

            myApp.controllers.productListPage(page, nth);
            $(page).find("h4.cateName").html(cateName);
            menuViewPopup.hide({ "animation" : "none" });
        });

        // 상품상세조회 팝업
        this.productPopup(page, option);

        function searchProduct(){
            var productNm = $(page).find("#searchItem input[type='search']").val();

            option.start    = 0;
            option.clear    = "1";
            option.orderby  = "tb_product.VIEW_ORDER";
            option.productNm = productNm;
            // 이벤트 목록 호출
            service.ajaxGetData(option);

        }        
    },

    // 상품상세조회 팝업
    productPopup: function(page, option, pOption) {

        var joProduct;
        var modal   = document.querySelector('#detailViewPopup');

        option.file_div = "PRODUCT";

        // 상세보기 클릭 (팝업)
        // 화면이 재호출되기 때문에 클릭 이벤트 중복 호출을 차단
        $(page).find(".imageList").off("click", ".listItem .productImg, .listItem .productInfo", null);
        $(page).find(".imageList").on("click", ".listItem .productImg, .listItem .productInfo", function(e) {
            
            // 영업시간종료 및 휴일여부
            var isHoleyYn   = $(this).closest("ons-list-item").hasClass("off");
            var qty         = $(this).parents('.listItem').find('.buttonArea ons-input input').val();
            
            joProduct = {
                'CORP_ID'   : option.corp_id,
                'P_CLS_IDX' : $(this).data("product_cate"),
                'PRODUCT_CD': $(this).data("product_cd"),
                'USER_ID'   : localStorage.getItem("USER_ID"),
                'P_CORP_ID' : $(this).parents(".listItem").data("corp_cd"),
                'PRICE'     : $(this).parents('.listItem').find('.productInfo .pd_price').data("price"),
                'QTY'       : (qty = isNull(qty) ? 0 : qty), 
                'STOCK_QTY' : $(this).parents('.listItem').find('.stock').data("stock"),
                'ST_NONE_YN': $(this).parents('.listItem').hasClass("st_none"),
                'IS_HOLY'   : isHoleyYn,
            };

            var service = new withWSDL("api/product/list/" + joProduct.PRODUCT_CD);

            // 상품 상세조회 바인딩 호출
            service.success = function(_jsonData) {
                var jsonData = _jsonData;

                if (typeof serviceModel.viewProduct === 'undefined') {
                    $.getScript("js/service/product.js", function() {
                        serviceModel.viewProduct(page, jsonData, service.DOMAIN, option, joProduct);
                    });
                } else {
                    serviceModel.viewProduct(page, jsonData, service.DOMAIN, option, joProduct);
                }   

                // 상품정보 : 카카오링크
                $(modal).find(".kakao_share").click(function(){
                    // console.log(jsonData.list[0]);
                    myApp.commonController.kakaoLinkProduct(jsonData.list[0]);
                });
            };

            option.PRODUCT_CD   = joProduct.PRODUCT_CD;
            option.P_CLS_IDX    = joProduct.P_CLS_IDX;
            option.P_CORP_ID    = joProduct.P_CORP_ID;
            option.tel          = localStorage.getItem("TEL");
            //option.user_id		= joProduct.USER_ID;

            service.ajaxGetData(option);

            // 별점/상품후기 목록 클릭 (팝업)
            var service1 = new withWSDL("api/comment/list");
            service1.success = function(_jsonData) {
                $(page).find(".popHeightScroll").scrollTop(0);
                
                var jsonData = _jsonData;
                if (typeof serviceModel.listComment === 'undefined') {
                    $.getScript("js/service/product.js", function() {
                        serviceModel.listComment(page, jsonData, option);
                    });
                } else {
                    serviceModel.listComment(page, jsonData, option);
                }
            };

            var data = {
                "CORP_ID"   : joProduct.CORP_ID,
                "PRODUCT_CD": joProduct.PRODUCT_CD,
            };

            service1.ajaxGetData(option);

            modal.show(
                {
                    "animation" : "none", 
                    "callback"  : function(){
                        $('.yt_player_iframe').each(function(){
                            this.contentWindow.postMessage('{"event":"command","func":"stopVideo","args":""}', '*');
                        });
                    }

                }
            );
        });

        $(modal).off("click", ".btnSetpointer", null);
        $(modal).on("click", ".btnSetpointer", function() {
            $(modal).find(".comment_form").toggle();
        });

       
        modal.querySelector(".btnClosePopup").onclick = function() {
            option.PRODUCT_CD   = null;
            option.P_CLS_IDX    = null;
            $(modal).find(".brandNm").html("");
            $(modal).find(".productNm").html("");
            modal.hide({ "animation": "none" });

            $(modal).find(".textarea_box").val("");
            $("#detailViewPopup .swiper-slide").remove();
            $("#detailViewPopup .swiper-pagination").remove();
            $("#detailViewPopup .swiper-notification").remove();
        };

        // 상품상세 닫기
        // Array.prototype.forEach.call(page.querySelectorAll('.btnClosePopup'), function(element) {
        //     element.onclick = function() {
        //         option.PRODUCT_CD   = null;
        //         option.P_CLS_IDX    = null;
        //         // 입력란 초기화
        //         // $(page).find(".comment_form").css("height", "0");
        //         $(page).find(".brandNm").html("");
        //         $(page).find(".productNm").html("");
        //         $(this).parents("ons-modal").hide( { "animation": "none" } );

        //         $("#detailViewPopup .swiper-slide").remove();
        //         $("#detailViewPopup .swiper-pagination").remove();
        //         $("#detailViewPopup .swiper-notification").remove();
        //     };
        // });

        // 별점 선택
        $(modal).find('.starRev ons-icon').click(function() {
            $(this).parent().children('ons-icon').removeClass('on');
            $(this).addClass('on').prevAll('ons-icon').addClass('on');
            return false;
        });


        // 클릭 : 별점 추가
        $(modal).find(".btnAddComment").off("click");
        $(modal).find(".btnAddComment").click(function() {

            // 별점 등록
            var service2 = new withWSDL("api/comment/list");
            service2.success = function(_jsonData) {

                var jsonData = _jsonData;
                if (jsonData.status == 200) {
                    runWarningMessage(jsonData.message, "success", 1000, "up");

                    $(modal).find(".textarea_box").val("");
                    $(modal).find(".starR.on").removeClass("on");
                    $(modal).find(".btnClosePopup").trigger("click");
                } else {
                    runWarningMessage(jsonData.message, "danger", 1000, "up");
                }
            };

            var data = {
                "CORP_ID": localStorage.getItem("CORP_ID"),
                "USER_ID": localStorage.getItem("ID"),
                "PRODUCT_CD": option.PRODUCT_CD,
                "ORDER_ID": $(this).data("orderid"),
                "PRODUCT_COMMENT": $(modal).find(".textarea_box").val(),
                "PRODUCT_STARTS": $(modal).find(".starR.on").length,
            };

            // 댓글이 있을 경우 중복작성을 막도록 체크함
            var enableComment = true;
            $(modal).find("#list-comment .listItem").each(function() {
                if (data.USER_ID == $(this).data("userid")) {
                    enableComment = false;
                    return;
                }
            });

            if (enableComment && !isNull(data.PRODUCT_CD) ) {
                service2.ajaxPostData(data);
                
            } else {
                var msg = myApp.i18Next.getTrasTxt("BSK", "ALDY_WRITE");
                runWarningMessage(msg, "warning", 1000, "up");
            }
        });
    },

    // 장바구니 페이지
    basketPage: function(page) {

        page.hide = function() {
            $(page).find(".listItem .productImg").click = null;
        };

        var objCoinQty  = $(page).find("#useCoin");
        var myCoin      = WITHU_PAY;
        var objMyCoin   = $(page).find(".myCoin");

        var diagRmAll   = document.querySelector("#basketRemoveAll-dialog");
        var svcRmBasket = new withWSDL("api/basket/list/0");
        
        // 장바구니 전체 삭제 다이어로그 오픈
        $(page).find(".btnRemoveAll").click(function(){
            diagRmAll.show( { "animation": "none" } );
        });

        // 장바구니 전체 삭제 다이어로그 닫기(취소)
        $(diagRmAll).find(".btnDialogCancelAll").click(function(){
            diagRmAll.hide( { "animation": "none" } );
        });

        // 장바구니 전체 삭제 실행
        $(diagRmAll).find(".btnDialogRemoveAllBasketOk").click(function(){
            
            var option      = {
                all : "all",
                tel : localStorage.getItem("TEL"),
            };
            svcRmBasket.ajaxDeleteData(option);
        });

        svcRmBasket.success = function(_jsonData){

            var jsonData    = _jsonData;
            var data        = jsonData;
            
            if(data.status == 200 ){
                runWarningMessage(data.message, "success", 1500, "up");
                diagRmAll.hide();
                callBasektList();
            }else{
                runWarningMessage(data.message, "danger", 1500, "up");
            }
        };
        
        // 장바구니 목록 호출 함수
        function callBasektList() {

            var sOption = {
                //"corp_id"       : localStorage.getItem("CORP_ID"),
                "user_id"   : localStorage.getItem("ID"),
                "tel"       : localStorage.getItem("TEL"),
                'file_div'  : 'PRODUCT',
                'start'     : 0,
                'CLEAR'     : 1,
                "isShowCorp": true,
                "area_id"   : localStorage.getItem("AREA_ID"),
            };

            var service = new withWSDL('api/basket/list');

            // 장바구니 목록 바인딩
            service.success = function(_jsonData) {
                var jsonData = _jsonData;
                if (typeof serviceModel.listBasket === 'undefined') {
                    $.getScript("js/service/basket.js", function() {
                        serviceModel.listBasket(page, jsonData, service.DOMAIN, sOption, callBasektList);
                    });
                } else {
                    serviceModel.listBasket(page, jsonData, service.DOMAIN, sOption, callBasektList);
                }
            };

            // 장바구니 목록 조회 서비스 호출
            service.ajaxGetData(sOption);
        }

        // 장바구니로 실시간 추가되므로 조회시 추가된 내역이 조회되어야하므로, onShow 이벤트로 수시로 reload 시켜야함
        page.onShow = function() {

            // 장바구니에서 회사이미지 숨기기
            Array.prototype.forEach.call(document.querySelectorAll('img.corpLogo'), function(ele) {
                $(ele).hide();
            });
            
            conf_sys_onMessage(function(){

                if( $.isNumeric(ADDED_DISTANCE) && $.isNumeric(ADDED_FEE) ){
                    var added_dist  = Number(ADDED_DISTANCE).format();
                    var added_fee   = Number(ADDED_FEE).currency();
                    
                    $(page).find(".addedDist").html( added_dist + "m");
                    $(page).find(".addedFee").html( added_fee );
                }

                if( $.isNumeric(FREE_DELV_AMT) ){
                    $(page).find(".freetip.green").html( Number(FREE_DELV_AMT).currency() ) ;
                    $(page).find(".wpayRate.green").html( WITHU_PAY_RATE + " %" ) ;   
                }

                // 위드유포인트 취급안함
                if( IS_PAY_POINT != "Y") {
                    page.querySelector("#useCoin input").readOnly = true;
                    $(page).find(".msgWarning").show();
                }else{
                    page.querySelector("#useCoin input").readOnly = false;
                    $(page).find(".msgWarning").hide();
                }
                
            });

            LOGIN_CLB = () => { callBasektList() } ;

            if( !showChkTel_Diag() ){
                return false;
            }else{
                LOGIN_CLB   = false;
            }

            // 적립금 사용하기 클릭 활성화
            $(page).find(".useCoin").click(function(){
                objCoinQty.find("input").focus();
            });

            // 적립금 사용하기 클릭 활성화
            objCoinQty.focusin(function(){
                var mc = $(page).find(".myCoin").text();
                $(".myCoin").html(mc);
            });

            $(page).find("#useCoin").off("keyup");

            // 장바구니 아이콘 뱃지수 최신갱신
            myApp.commonController.common();
            
            objCoinQty  = $(page).find("#useCoin");
            myCoin      = WITHU_PAY;
            objMyCoin   = $(page).find(".myCoin");

            var pOption = {
                "USE_YN": 1,
                "CLEAR": 0,
                "orderBy": "PRD.DISPLAY_NO"
            };

            var option = {
                start       : 0,
                orderby     : "tb_product.VIEW_ORDER",
                order       : "ASC",
                corp_id     : pOption.CORP_ID,
                corp_cls    : null,
                is_menu     : 'Y',
                file_div    : "PRODUCT",
            };

            // 현재 코인보유수량 체크
            if (!isNull(myCoin)) {
                objMyCoin.text(parseInt(myCoin).format());
            } else {
                objMyCoin.text(0);
            }

            callBasektList();

            var objSumAmt   = $(page).find(".sumAll");
            var objTotalAmt = $(page).find(".total");
            objCoinQty.val(0);
            var myCoins     = WITHU_PAY;
            var totalAmt    = 0;

            tempCoin = parseInt(objCoinQty.val()); // 사용코인

            if( isNaN(tempCoin)){
                tempCoin = 0;
            }

            // 코인사용을 직접 입력
            var firstInput = 0;

            // 보유코인 직접 입력
            $(page).find("#useCoin").on("keyup", function(e) {

                // 위드유페이가 있을 경우 코인사용할 수 없도록 해야함
                var listPayCount = $(page).find("#list-node ons-list-item[data-ispay='Y']").length;
                
                if (listPayCount > 0) {
                    if ($(document).find("ons-toast.warning").length == 0) {

                        // "위드유페이가 포함된 상품은 보유페이로 사용할 수 없습니다"
                        var err_pay_no  = myApp.i18Next.changeLang("BSK", "POINT_NO_PURCH");
                        runWarningMessage(err_pay_no, "warning", 3000, "up");
                    }
                    $(this).val(0);
                    return false;
                }

                // 지수값입력 입력도 미연 방지
                $(this).val( parseInt($(this).val().replace(/[^0-9]/g, "") ));

                if( firstInput == 0){
                    objSumAmt   = $(page).find(".sumAll");
                    objTotalAmt = $(page).find(".total");

                    objMyCoin   = $(page).find(".myCoin");
                    myCoins     = parseInt(removeCommas(objMyCoin.html().replace(/[^0-9]/g, "")));
                    totalAmt    = parseInt(objTotalAmt.html().replace(/[^0-9]/g, ""));
                    
                    firstInput++;
                }else {
                    firstInput = 1;
                }

                var listCount = $(page).find("#list-node ons-list-item").length;
                if (listCount == 0) {
                    if ($(document).find("ons-toast.warning").length == 0) {

                        // 등록된 상품이 없습니다
                        var t_no_item   = myApp.i18Next.getTrasTxt("BSK", "NO_ITEM");
                        runWarningMessage(t_no_item, "warning", 1500, "up");
                    }
                    return false;
                }

                useCoin = parseInt($(this).val()); // 사용코인(inputbox)

                if (isNaN(useCoin)) {
                    useCoin = 0;
                }

                if (useCoin < 0) {
                    useCoin = 0;
                }

                if (useCoin > myCoins) {
                    var err_msg     = myApp.i18Next.getTrasTxt("BSK", "COIN_EXCEED");
                    runWarningMessage(err_msg, "warning", 3000, "up");
                    $(this).val(0);
                    useCoin = 0;
                }

                if (useCoin > totalAmt) {
                    var err_msg     = myApp.i18Next.getTrasTxt("BSK", "CAL_EXCEED");
                    runWarningMessage(err_msg, "warning", 2000, "up");
                    $(this).val(0);
                    useCoin = 0;
                }

                if ( useCoin > ( totalAmt * (WITHU_PAY_RATE/100) )) {
                    var err_msg     = myApp.i18Next.getTrasTxt("BSK", "WPAY_RATE");
                    err_msg += WITHU_PAY_RATE + " %";
                    runWarningMessage(err_msg, "warning", 2000, "up");
                    $(this).val(0);
                    useCoin = 0;
                }

                objTotalAmt.html(totalAmt - useCoin);

            });

            // 클릭 : 상품주문하기
            $(page).find(".btnOrder").click(function() {

                var arrProduct = new Array();

                var nHoly   = $(page).find("ons-list-item.off").length;
                var ntmOut  = $(page).find("ons-list-item.sale_off").length;
                var next_dt = $(page).find("input[name='NEXT_DT']").val(); 
                var conf_dt = $(page).find("input[name='CONFIG_START']").val();

                var sumAll = parseInt(getfilterNumber($(page).find(".sumAll").html()));
                var sumEtc = parseInt(getfilterNumber($(page).find(".sumEtc").html()));
                var wpay   = parseInt($(page).find("#useCoin").val().replace(/[^0-9]/g, ""));

                // TODO 기본배송비 추가
                // 상품 합계가 무료배송금액일 경우 배송비 무료
                var baseDelvFee = BASE_DELV_FEE;
                if( (sumAll+sumEtc) >= FREE_DELV_AMT ){
                    baseDelvFee = 0;
                }
               
                goOrder();
                function goOrder(){

                    $(page).find("#list-node ons-list-item").each(function(i) {

                        var obj = new Object();
                        obj.CORP_ID     = localStorage.getItem("CORP_ID");
                        obj.USER_ID     = localStorage.getItem("USER_ID");
                        obj.BRANCH_ID   = localStorage.getItem("BRANCH_ID");
                        obj.PRODUCT_CD  = $(this).find(".productInfo").data("product_cd");
                        obj.SALE_CORP_ID= $(this).data("p_corp_id");
                        obj.DIV         = null;
                        obj.BARCODE     = null;
                        obj.PRODUCT_NM  = $(this).find(".productName").text();
                        obj.DISPLAY_NO  = null;
                        obj.QTY         = $(this).find(".qty").val();
                        obj.UNIT        = $(this).find('.productInfo .pd_price').data("price");
                        obj.IS_OUT_YN   = "Y";
                        obj.RMK         = null;
                        // total += parseFloat(obj.UNIT);
                        sumEtc += 0;
    
                        arrProduct.push(obj);
                    });
    
                    var objProduct = {
    
                        SNS_TYPE        : localStorage.getItem("SNS_TYPE"),
                        
                        CORP_ID         : localStorage.getItem("CORP_ID"),
                        USER_ID         : localStorage.getItem("USER_ID"),
                        BRANCH_ID       : localStorage.getItem("BRANCH_ID"),
                        DELV_CORP_ID    : null,
                        ORDER_STATE     : "ORD_NEW",
                        ORDER_DT        : null,
                        DLV_WISH_TIME   : null, // page.find("#select-productQty option:selected").val().trim()
    
                        ORDER_TEL       : localStorage.getItem("TEL"),
                        ADDR1           : localStorage.getItem("ADDR1"),
                        ADDR2           : localStorage.getItem("ADDR2"),
                        
                        MEMO            : null, //page.querySelector("#req_text").value
                        PAY_OPTION      : null, ///page.find("ons-checkbox[name='payway']:checked")
    
                        PAY_OPTION_COIN : wpay,
                        DELV_USER_ID    : null,
                        RECEPTION_DT    : null,
                        DLV_READY_TM    : null,
                        DLV_DOING_TM    : null,
                        DLV_END_TM      : null,

                        // TODO 기본배송비 추가
                        BASE_DELV_FEE   : baseDelvFee,

                        // TODO 추가배송비 추가
                        ADDED_DELV_FEE  : 0,
                        ADDED_DELV_DIST : 0,
                        
                        TOTAL_AMT       : (sumAll + sumEtc + baseDelvFee),
                        LAT             : localStorage.getItem("LAT"),
                        LNG             : localStorage.getItem("LNG"),
                        AREA_ID         : localStorage.getItem("AREA_ID"),

                        ORDER_DT        : null,
                        ORDER_TM        : null,
                    };

                    // 중복상품 체크
                    if( !myApp.commonController.chkCartProduct(arrProduct) ){
                        var msg	= myApp.i18Next.getTrasTxt("BSK", "CART_CHK_2");
                        runWarningMessage(msg, "warning", 2000, "up");
                        return false;
                    }
    
                    myApp.navigator.pushPage('html/order_detail.html', { 

                        data : { 
                                    TB_ORDER_INFO   : objProduct, 
                                    TB_ORDER_DETAIL : arrProduct, 
                                    OPTION : { 
                                        "NEXT_DT" : next_dt, 
                                        "CONF_DT" : conf_dt
                                    } 
                            }, 
                            animation : "none" 
                        }
                    );
                }
            });
            // 상품상세조회 팝업
            myApp.controllers.productPopup(page, option);
        };

        page.onHide = function() {
            $(page).find(".btnOrder").off("click");
            $(page).find("#useCoin").on("keyup");
        };
    }, 

    addFavorite: function (corpId, productCd, element){

        var data = {
            ID					: localStorage.getItem("ID"),
            TEL					: localStorage.getItem("TEL"),
            PRODUCT_CORP_ID     : corpId,
            REF_PRODUCT_CD      : productCd,
        };

		// 장바구니에 상품 추가/삭제 : UPDATE
		var service	= new withWSDL("api/favorite/list/0");

		// 장바구니 서비스 바인딩
		service.success = function(_jsonData){
            var jsonData    = _jsonData;
            $objHeart   = $("#detailViewPopup span.heart");

            if( !isNull(element) && $(element).length > 0 ){
                $objHeart = $(element);
            }
            
            if( jsonData.status == 'success'){
                
                var t_added     = myApp.i18Next.getTrasTxt("FAV", "ADDED");
                var t_deleted   = myApp.i18Next.getTrasTxt("FAV", "DELETED");

                if( jsonData.result == "deleted" ){
                    // "즐겨찾기 삭제"
                    $objHeart.html("<i class='fa fa-heart-o'></i>");
                    runWarningMessage(t_deleted, "success", 500, "up");
                }else{
                    // "즐겨찾기 추가"
                    $objHeart.html("<i class='fa fa-heart'></i>");
                    runWarningMessage(t_added, "success", 500, "up");
                }
            }else{
                runWarningMessage(jsonData.message, "danger", 500, "up");


            }
        };

        if( isNull( data.TEL ) ){
            // "전화번호를 읽어올수 없습니다. 관리자에게 문의하세요"
            var t_error     = myApp.i18Next.changeLang("SYS", "ERR_TEL_AUTH");
            runWarningMessage(t_error, "danger", 2000, "up");
        }else{
            service.ajaxPutData(data);
        }
    }
};
