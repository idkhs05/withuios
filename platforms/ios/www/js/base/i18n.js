
myApp.i18Next = {

    data : {
        "vn-VN": {
            "SYS" : {
                "NO_CONN"       : "Không kết nối với mạng",
                "DELAY_CONN"    : "Kết nối bị trì hoãn và dữ liệu không thể được tải",
                "CHK_CONN"      : "Vui lòng kiểm tra kết nối internet của bạn và thử lại",
                "UNSAFE_CONN"   : "Kết nối internet tạm thời không ổn định",
                "KAKAO_INQUIRY" : "Tư vấn qua",
                "KAKAO_SHARE"   : "chia sẻ",
                "APP_EXIT_MSG"  : "Nhấn một lần nữa để thoát",
                "APP_EXIT"      : "Thoát khỏi ứng dụng",
                "AGREE_PRIVATE" : "Đồng ý thu thập thông tin cá nhân",
                "AGREE"         : "Đồng ý",
                "COPY"          : "Sao chép",
                "SET_COMPLETE"  : "Hoàn tất thay đổi cài đặt",
                "GO_LOGIN"      : "Đi đến màn hình đăng nhập",
                "ERR_TEL_AUTH"  : "Nhập số điện thoại của bạn và sử dụng sau khi xác thực",
                "NO_AGREE_MSG"  : "Dịch vụ do không thỏa thuận có thể được sử dụng bình thường",
                "NO_AREA"       : "No Area",
                "REMARK"        : "nhận xét",
                "RQR_INPUT"     : "Yêu cầu đầu vào",
                "CALL"          : "Gọi điện thoại",
                "PULL_RESET_MSG": "Kéo để làm mới",
            },

            "FAV" : {
                "ADDED"         : "Đã thêm vào mục yêu thích",
                "DELETED"       : "Đã xóa để yêu thích",
            },

            "DIAG" : {
                "P_NUM"         : "Số điện thoại để đặt hàng",
                "I_ONCE"        : "Chỉ nhập một lần",
                "I_NUMBER"      : "Vui lòng chỉ nhập số",
                "I_VAL_10_20"   : "10 chữ số trở lên, 12 chữ số trở xuống",

                "DEL_SHOP_CART" : "Bạn có chắc chắn muốn xóa sản phẩm đã chọn khỏi giỏ hàng của mình không?",
                "DEL_SHOP_CART_ALL" : "Bạn có chắc chắn muốn xóa tất cả các sản phẩm trong giỏ hàng của bạn không?",

                "DEL_FAV_CART"  : "Bạn có chắc chắn muốn xóa sản phẩm đã chọn khỏi Favorites không?",
                "DEL_FAV_CART_ALL" : "Bạn có chắc chắn muốn xóa tất cả các sản phẩm đã chọn khỏi Favorites không?",

                "DEL_ORDER"     : "Bạn có chắc chắn muốn xóa đơn hàng này không?",
                "COPY_CART"     : "Bạn có muốn sao chép đơn hàng này vào giỏ hàng của bạn không?",
                "CNCL_ORDER"    : "Bạn có chắc chắn muốn hủy đơn hàng này không?",

                "CONFIRM_AGAIN" : "Vui lòng xác nhận lại.",    
                "CHANGE_NUMBER" : "Bạn có muốn thay đổi số điện thoại không?",          
                "MSG_LIST"      : "Danh sách tin nhắn",
                "APP_UPDATED_LAST" : "Ứng dụng này đã được cập nhật lên phiên bản mới nhất.", 
                "APP_MOVE_PAGE" : "Bạn có muốn cập nhật lên phiên bản mới nhất không?", 
                "REQ_LINK"      : "Bạn có muốn truy cập liên kết này không?",
                "NO"            : "Không",
                "YES_ADULT"     : "Đúng, là người lớn",

                "IS_TRUST_LINK" : "Đây có phải là một địa chỉ liên kết đáng tin cậykhông ?", // "신뢰할 수 있는 링크 인지", 
                "CHECK_AGIN"    : "Hãy kiểm tra lại.", // 한번 더 확인해 주세요",  

                "AGREE_DEVICE"  : "Đồng ý thu thập thông tin thiết bị cá nhân", //"사용자 개인기기정보 수집 동의"
                "PURPOSE_CLLT"  : "Mục đích thu thập", //수집 목적",                  
                "GO_POLICY"     : "Chuyển sang chính sách bảo mật", // "개인정보취급방침 바로가기",     
                "AGREE_DEVICE1" : "Xác thực người dùng chỉ được xác thực một lần khi khởi động.", // 사용자관련 인증은 시작시 한번만 인증됩니다.",  

                "POLICY_1_MSG"  : "Chúng tôi thu thập thông tin cá nhân của người dùng (số điện thoại, thông tin thiết bị, thông tin GPS) để cung cấp dịch vụ vận chuyển suôn sẻ.", // 원활한 배달서비스를 위해 사용자의 개인정보(전화번호,기기정보, GPS정보)를 수집합니다"
                "POLICY_2_MSG"  : "Thông tin cá nhân chỉ được sử dụng cho mục đích dịch vụ và không được sử dụng cho bất kỳ mục đích nào khác", // 개인정보는 오직 본앱의 서비스만을 위해 사용되며, 그 외 목적에는 사용되지 않음을 알립니다"

                "AGE_19"        : "(Bạn trên 19 tuổi đúng không?)", // 만 19세 이상
                "IS_ADULT"      : "Bạn là người lớn đúng không?", // 성인입니까?
                "ADULT_POLICY"  : "'WithU' không bán thuốc lá và rượu cho trẻ vị thành niên. Chỉ dành cho người lớn trên 19 tuổi",
                "ADULT_MSG"     : "Đối với trẻ vị thành niên, thuốc lá / rượu sẽ bị hủy tại chỗ khi trẻ vị thành niên nhận hàng", // 미성년자의 경우 배달수령시 담배/주류는 현장에서 취소합니다
            },

            "COMM" : {
                "SEARCH_PRODUCT": "Tìm kiếm sản phẩm",
                "SEARCH_ADDR"   : "Search Addr.",
                "close"         : "Gần",
                "ok"            : "Xác nhận",
                "noMoreData"    : "Không có thêm dữ liệu",
                "noData"        : "Không tìm thấy dữ liệu nào",
                "cancel"        : "Hủy bỏ",
                "changePassword": "Đổi mật khẩu",
                "save"          : "Tiết kiệm",
                "questChangePassword" :  "Bạn có thay đổi mật khẩu của bạn không?",
                "MOD"           : "Sửa đổi",
                "DEL"           : "Xóa",
                "CLS"           : "Đóng",
                "NONE"          : "Không",
                "RD_SCV"        : "Chuẩn bị dịch vụ",
                "EXT_LINK"      : "Thoát kết nối",
                "WARN_KR_PHONE" : "Không thể đặt hàng với số điện thoại của Hàn Quốc. Vui lòng thay đổi thành một số điện thoại địa phương nơi bạn có thể đạt được",
                "MOD_COMPLTE"   : "Thay đổi này đã hoàn tất",
            },

            "COIN" : {
                "BONUS"         : "Tiền tích lũy",
                "BONUS_QTY"     : "Tiền tích lũy còn lại",
                "BONUS_HISTORY" : "Tiền gửi tích lũy và lịch sử rút tiền",
                "IN"            : "Tiền gửi",
                "OUT"           : "Tiền rút",
                "ALL"           : "Tất cả",
            },

            "LOCATION" : {
                "REG_ADDR"      : "Đăng ký địa chỉ của bạn" ,
                "MY_LOC"        : "Ví trí hiện tại",

                "MY_AREA"       : "Cài đặt khu vực giao hàng",
                "MY_AREA_MSG"   : "Vui lòng đặt khu vực giao hàng",
                "NO_COMP_MSG"   : "Không có cửa hàng (cửa hiệu) nào trong khu vực hiện tại",

                "CONF_ADDR"     : "Đặt địa chỉ giao hàng",
                "CONF_MAP"      : "Tìm kiếm theo bản đồ",
                "CONF_BUILD"    : "Tìm kiếm theo tòa nhà",
                "SEARCH_ADDR"   : "Tìm kiếm địa chỉ",
                "NOW_POSTION"   : "Vị trí hiện tại",
                "NOW_LOADING"   : "Thông tin vị trí đang được nhận",
                "ADDR2_INPUT"   : "Vui lòng nhập chính xác địa chỉ đường phố", 
                "ADDR3_INPUT"   : "Tự động nhập khi chọn tòa nhà", 

                "ERR_NO_ADDR"   : "Không thể tải thông tin địa chỉ",
                "ERR_NO_SERVER" : "Dịch vụ Google không tải được",
                "ERR_LNG_LONG"  : "Địa chỉ không được nhập và vĩ độ và kinh độ không được tìm thấy : ",

                "SET_GSP_ON"    : "Vui lòng cho phép cài đặt GPS điện thoại của bạn",

                "CITY"          : "City",
                "CITY_MSG"      : "Select a city",
                "LOCAL"         : "Region",
                "LOCAL_MSG"     : "Select a region",
                "BUILDING"      : "Building",
                "BUILDING_MSG"  : "Select a building",
                "STORE"         : "Branch store",
                "STORE_MSG"     : "Select a store",
                "REMARK"        : "Ghi chú",

                "SEL_CNTNT"     : "Lựa chọn khu vực",
                "SEL_NATION"    : "Lựa chọn quốc gia",
                "SEL_CITY"      : "Lựa chọn thành phố",

                "SEARCH_RMK1"   : "1. Tìm kiếm địa chỉ trong hộp tìm kiếm hàng đầu và nhập địa chỉ chi tiết",
                "SEARCH_RMK2"   : "2. Khi cài đặt phân phối đến vị trí hiện tại, hãy nhấp vào nút 'Vị trí hiện tại' ở trên cùng và nhập địa chỉ chi tiết",

            },

            "TOP" : {
                "SEARCH_ALL"    : "Tìm kiếm sản phẩm",
                "BS_ORDER"      : "Mua hàng",
                "GB_ORDER"      : "Mua chung",
            },

            "PRD" : {
                "CLASSIFY"      : "Phân loại sản phẩm",
                "NO_CONTAIN"    : "Tổng số không bao gồm",
                "VIEW_FILT"     : "Xem bộ lọc",
                "S_BRAND"       : "Nhãn hiệu",
                "S_PROD"        : "Tên sản phẩm",
                "S_SALE"        : "Tỷ lệ bán hàng",
                "S_NEW"         : "Sản phẩm mới nhất",
                "S_STAR"        : "Phổ biến",
                "S_BS"          : "Căn bản",
                "AMT"           : "Giá", 
                "ORD_TIMES"     : "Thời gian đặt hàng",
                "REMARK"        : "Chi tiết sản phẩm",

                "STAR"          : "Đánh giá sao", 
                "STAR_MSG"      : "Vui lòng đánh giá sao", 
                "COMMET_INP"    : "Vui lòng nhập một dòng bình luận",
                "LINEUP"        : "Tập hợp",
                "COOK_TIME"     : "Thời gian nấu ăn",

                "OPEN_TIME"     : "Open Time",
                "HOLY_DAY"      : "Day off",
                "EVERY_OPEN"    : "Open everyday",
                "LIMITED_STOCK" : "Không còn số lượng sản phẩm",
            }, 

            "LEFTMENU" : {
                "MY_ADDR"       : "Địa chỉ",
                "RCT_ADDR"      : "Địa chỉ đặt hàng gần đây",
                "FOVORITE"      : "Yêu thích",
                "FCM_MSG"       : "Nhận thông tin",
                "EVT_NOTICE"    : "Thông báo và sự kiện",
                "EVENT"         : "Sự kiện",
                "NOTICE"        : "Thông báo",
                "MY_CONFIG"     : "Cài đặt",
                "TEL_LIST"      : "Số điện thoại",
                "KOR_INFO"      : "Tin tức hội người Hàn",
                "NOW_AIR"       : "Bụi mịn cập nhật thực tế",
                'REQUEST'       : 'Liên hệ chúng tôi',
                'OTHER_REQUEST' : "Các quan hệ đối tác khác",
                "POLICY"        : "Điều khoản dịch vụ",
                'CONTACK_INQURY': "Việc hỏi",

            },

            "TABMENU" : {
                "HOME"          : "Home",
                "KIND"          : "Products",
                "GB"            : "GroupBuying",
                "BASKET"        : "S.Cart",
                "ORDER_LIST"    : "MyOrder",
                "PORFILE"       : "Profile",
            },

            "CONF" : {
                "agreeAll"      : "Tất cả đều đồng ý",
                "addr1"         : "Địa chỉ nhà",
                "addr2"         : "Địa chỉ chi tiết",
                "PUSH_SET"      : "Cài đặt thông báo",
                "PUSH_AGREE"    : "Đồng ý thông báo",
                "NO_VERSION"    : "Không thể được xác nhận",
                "LAST_VERSION"  : "Latest version",
                "VERSION_ERR"   : "Can't Not Check Version",
                "DRAG_MAP"      : "Bạn có thể chỉ định địa chỉ bằng cách kéo biểu tượng màu đỏ trên bản đồ.",
                "RQR_ADDR"      : "Vui lòng nhập địa chỉ để được giao",
            },

            "POP": {
                "todayClose"    : "Hôm nay đóng",
            },

            "BSK" : {
                "SUM"           : "Tổng",
                "ALL_SUM"       : "Tổng cộng",
                "ALL_DEL"       : "Xóa hết",
                "COM_SUM"       : "Tổng sản phẩm",
                "SVC_SUM"       : "Tổng sản phẩm dịch vụ",
                "MY_COIN"       : "Tiền tích lũy còn lại",
                "USE_COIN"      : "Sử dụng tiền tích lũy",
                "STT_ORD"       : "Mua ngay",
                "CAL_AMT"       : "Thanh toán",
                "MORE_HIGH"     : "Mua trên",
                "CANT_WRITE"    : "Bạn không thể viết vì bạn không có lịch sử mua hàng",
                "ALDY_WRITE"    : "Bạn đã viết rồi",
                "CAL_EXCEED"    : "Số tiền thanh toán đã vượt quá",
                "COIN_EXCEED"   : "Tiền tích lũy đã vượt quá",
                "MIN_ORD_QTY"   : "Số lượng đặt hàng tối thiểu",

                "WARN_SOLD_OUT" : "Sản phẩm hết hàng, vui lòng đặt hàng sau khi xóa",               // "품절상품이 있으니 삭제 후 주문해주세요"
                "WARN_CLOSED"   : "Nếu đặt hàng sản phẩm ngoài giờ kinh doanh thì chỉ có thể xóa sản phẩm hoặc hẹn đặt giờ giao hàng", //  "휴무업체 제품이 있으니 삭제 후 주문해주세요", 
                "WARN_TIMEOUT"  : "Nếu bạn đặt hàng ngoài giờ bán hàng, bạn có thể xóa sản phẩm hoặc đặt thời gian giao hàng",                //"판매시간종료 제품이 있으니 삭제 후 주문해주세요", 

                "NO_ITEM"       : "Không có sản phẩm nào được thêm vào",
                "POINT_NO_PURCH": "Điểm dự trữ không thể được mua làm điểm dự trữ",
                "BS_DELV_AMT"   : "Chuyển",
                "REMARK"        : "Nếu khoảng cách dài, phí giao hàng bổ sung có thể được thêm vào.",
                "FREE"          : "miễn phí : ",
                "ST_LIMIT_MSG"  : "Không đủ số lượng có sẵn để đặt hàng",
                "WPAY_RATE"     : "Phần trăm điểm khả dụng : ",
                "NO_USE_POINT"  : "Cửa hàng này không xử lý điểm",
                "NO_TAX"        : "Cửa hàng này không xử lý hóa đơn thuế",
                "CART_EMPTY"    : "Bạn có thể thêm sau khi hết giỏ hàng",
                "CART_CHK_2"    : "Bạn không thể đặt hàng nhiều hơn hai cửa hàng trong giỏ hàng của mình. Giao hàng tập thể là không thể",
            },

            "GB" : {
                "APP_AMT"       : "Số lượng ứng dụng",
                "TAR_AMT"       : "Số lượng mục tiêu",
                "MY_APPLY"      : "Số đơn hàng của tôi",
                "REMAIN_DT"     : "Thời gian còn lại để đăng ký",
                "ABLE_DT"       : "Thời gian có thể đăng ký",
                "DELV_DT"       : "Thời gian có thể giao hàng",
                "GOAL_AMT"      : "Số tiền mục tiêu",
                "GOAL_RATE"     : "Tỷ lệ đạt được",
                "ORGIN"         : "Xuất sứ",
                "CUSTOM"        : "Đơn vị cung cấp",
                "COLORS"        : "Màu sắc",
                "SIZES"         : "Kích cỡ",
                "FOOD_RMK"      : "Chi tiết thực phẩm",
                "PROD_RMK"      : "Chi tiết sản phẩm",
                "DETAIL_VW"     : "Xem chi tiết",
                "RMK_SM"        : "Đăng ký ' Thanh toán ngay chưa được thực hiện",
                "RMK_MD"        : "Vì sản phẩm có số tiền mục tiêu và hạn đăng ký nên 1 ngày sau khi số tiền đạt được mục tiêu thì sản phẩm sẽ được giao theo thứ tự ngày nhập thông tin của bạn",
                "REQ_MEM"       : "Người",
                "MEM"           : "Hiện tại",
                "D_DAY_AFT"     : " Sau ngày D-Day",
                "D_DAY_BFR"     : " Trước ngày D-Day",
                "D_DAY_RMN"     : " Số ngày còn lại",
                "DEADLINE"      : "Hạn chót",
                "INPUT_TIME_CAL": "Vui lòng chọn và nhập đúng múi giờ ở bên phải của lịch",
                "ARR_TIME"      : "Thời gian đến",
                "TIME_RMK"      : "Thời gian không hoạt động là thời gian ứng dụng giao hàng đặt phòng được hoàn thành",
            },

            "SEARCH" : {
                "ALL_P_SEARCH"  : "Tìm kiếm tất cả sản phẩm",
            }, 

            "AUTH": {
                "ID"            : "ID",
                "PW"            : "Mật khẩu",
                "PW1"           : "Xác nhận mật khẩu",
                "RESET_PW"      : "Đặt lại mật khẩu",
                "LOGIN"         : "Đăng nhập",
                "LOGOUT"        : "Đăng xuất",
                "FIND_ID"       : "Tìm ID", 
                "FIND_PASS"     : "Quên mật khẩu", 
                "questLogout"   : "Bạn có chắc chắn muốn đăng xuất không?",
                "JOIN"          : "Đăng ký", 
                "JOIN_INFO"     : "Thông tin thành viên",
                "questOffService" : "Bạn có muốn ngắt kết nối khỏi dịch vụ đăng nhập SNS không?",
                "SENDEMAIL"     : "Gửi email",
                "FIND_BY_EMAIL" : "Tìm kiếm qua email",
                "I_EMAIL"       : "Vui lòng nhập email bạn đã nhập khi đăng ký",
                "TXT_1"         : "Nhập email của bạn và nhấn nút 'Gửi email'",
                "TXT_2"         : "Khi bạn truy cập vào tài khoản email bạn đã nhập, bạn sẽ được thông báo về ID của mình.",
                "TXT_3"         : "Nhập email, ID và số điện thoại của bạn và nhấn nút Gửi Email",
                "TXT_4"         : "Chúng tôi đã gửi cho bạn mật khẩu mới vào tài khoản email bạn đã nhập.",

                "ID_CHK"        : "Kiểm tra ID",
                "EMAIL_CHK"     : "Kiểm tra email",
                "PLY_AGREE"     : "Điều khoản sử dụng và sự đồng ý",
                "VW_CNTT"       : "Xem nội dung",
                "AGREE_USE_RQR" : "Chấp nhận các điều khoản sử dụng (bắt buộc)",
                "AGREE_PRI_RQR" : "Đồng ý thu thập và sử dụng thông tin cá nhân (bắt buộc)",
                "AGREE_ALRAM"   : "WITHU Đồng ý thông báo(tùy chọn)",

                "MOD_TEL"       : "Vui lòng chỉnh sửa số điện thoại của bạn",
                "INP_TEL"       : "Vui lòng điền số điện thoại của bạn",

                "AVAL_ID"       : "ID có sẵn",         
                "ALRDY_ID"      : "ID đã được sử dụng",
                "EMAIL_USE_MSG" : "Địa chỉ email của bạn sẽ được sử dụng nếu bạn mất tài khoản", 
                "EMAIL_AVABLE"  : "Địa chỉ email có sẵn",
                "ALRDY_EMAIL"   : "Địa chỉ email này đã được sử dụng",

                
                "VAL_ID"        : "Vui lòng nhập ít nhất 2 ký tự cho 'ID'",     //"아이디는 최소 2자 이상 입력"
                "VAL_ID_CHK"    : "Kiểm tra trùng lặp 'ID'",                    // "아이디 중복 체크"
                "VAL_EMAIL"     : "Vui lòng nhập vào định dạng 'email'",        //"이메일형식으로 입력"
                "VAL_EMAIL_CHK" : "Kiểm tra trùng lặp 'email'",                 //"이메일 중복 체크"
                "VAL_PW"        : "Vui lòng nhập mật khẩu của bạn bao gồm số và tiếng Anh",// "비밀번호는 숫자, 영문을 포함"
                "VAL_PW6"       : "Vui lòng nhập ít nhất 6 chữ số cho 'mật khẩu'",// "비밀번호는 6자이상 입력"
                "VAL_PW_CHK"    : "Vui lòng nhập mật khẩu và mật khẩu xác nhận để khớp.", //"비밀번호와 비밀번호 확인을 동일하게 입력"
                "VAL_NICK"      : "Vui lòng nhập ít nhất 2 ký tự",              //"닉네임은 2자 이상 입력"
                "CHK_AGAIN"     : "Vui lòng kiểm tra lại ID và mật khẩu của bạn và đăng nhập lại.",

                "PW_CHANGED"    : "Mật khẩu của bạn đã được thay đổi",
                "NICK_CHANGED"  : "Biệt danh của bạn đã thay đổi",
                "INPUT_REF"     : "Vui lòng nhập ID người giới thiệu",
            },

            "PROFILE" : {
                "COIN_HIS"      : "Xác nhận lý lịch",
                "EMAIL"         : "EMAIL",
                "NICK"          : "Biệt danh",
                "REF_ID"        : "Tên tài khoản đề cử",
                "CANNT_MODE"    : "Không thể sửa đổi",

                "SAVE"          : "Lưu",
                "TEL"           : "Số điện thoại",
                "SVC_OUT"       : "Kết thúc dịch vụ",
                "PW_VALI_1"     : "Phải bao gồm số và tiếng Anh",
                "PW_VALI_2"     : "Phải nhập ít nhất 6 chữ số",
                "PW_VALI_3"     : "Nhập cùng một mật khẩu và xác nhận mật khẩu",
            },

            "ORD" : {
                "AMT"           : "Tổng tiền mua hàng",
                "REG_DT"        : "Ngày đặt hàng",
                "CASH"          : "Tiền mặt",
                "CARD"          : "Thẻ",
                "DLV_AMT"       : "Chuyển",
                "ADD_DELV_AMT"  : "Chi phí giao hàng bổ sung",
                "CAL_AMT"       : "Số tiền thanh toán",
                "DELV_NM"       : "Vận chuyển",
                "DPT_DT"        : "Thời gian dự đoán đến nơi",
                "DELV_ADDR"     : "Địa chỉ nhận hàng",
                "DETAIL"        : "Chi tiết đặt hàng",
                "LIST"          : "Danh sách đặt hàng",
                "ABOUT_TIME"    : "Mất khoảng",
                "MINUTS"        : "Phút",
                "READY"         : "Sẵn sàng",
                "ING"           : "Đang vận chuyển",
                "CNCL_OK"       : "Đã hủy",
                "CNCL"          : "Hủy đơn hàng",
                "COPY"          : "Sao chép sang giỏ hàng",
                "COMPLETE"      : "Đã giao hàng",
                "EXT_DELVING"   : "Người vận chuyển bên ngoài",
                "COMP_TIME"     : "Thời gian hoàn thành",
                "READY_PRD"     : "Sắp có",
                "ENDED_PRD"     : "Đã kết thúc",
                "SOLD_PRD"      : "Đã hết hàng",

                "COPYED_CART"   : "Đơn hàng của bạn đã được sao chép vào giỏ hàng của bạn",
                "COPYED_FAIL"   : "Sao chép đơn hàng thất bại",
                "AFTER_PROC"    : "Tiếp tân sau ",

            },

            "ORDD" : {
                "ORD_INFO"      : "Thông tin giao hàng (Sửa)",
                "DO_ORDER"      : "Đặt hàng",
                "RCT_ADDR"      : "Địa chỉ giao hàng gần đây nhất",
                "ORD_INPUT"     : "Nhập nội dung",
                "ORD_NOW"       : "Giao hàng ngay",
                "ORD_RESRV"     : "Hẹn đặt thời gian giao giao hàng",
                "ORD_MEMO_MSG"  : "Nội dung yêu cầu (thuốc lá và sim điện thoại không bao gồm trong tiền tích lũy)",
                "ORD_GIFT"      : "Chọn quà tặng",
                "ORD_GIFT_MSG"  : "Nếu bạn không chọn quà tặng, bạn sẽ tích lũy được 2%",
                "BILL_WAY"      : "Phương thức thanh toán",
                "TAX"           : "Hóa đơn thuế",
                "ORD_TAX"       : "Yêu cầu cấp hóa đơn đỏ",
                "TAX_NUM"       : "Mã số thuế",
                "TAX_ADDR"      : "Địa chỉ thuế",
                "CORP_NM"       : "Tên công ty",
                "CORP_EMAIL"    : "Email công ty",
                "COMPLEATE"     : "Xác nhận đặt hàng ",
                "TAX_WARNING"   : "Nếu bạn yêu cầu hóa đơn thuế cho thực phẩm (sản phẩm) không phải là sản phẩm của 'WITHU MART', bạn phải liên hệ với số điện thoại ở đầu menu của công ty hoặc 'nói chuyện riêng', vì đó là thực phẩm (sản phẩm) không có thuế.",

                "CONFIRM"       : "Xác nhận đặt hàng ",
                "REQ_CNFRM"     : "Bạn muốn đặt hàng không?",
                "MM_REQ"        : "Ghi chú và yêu cầu",
            },

            "INQUERY": {
                "INQ_TYPE"      : "Loại yêu cầu",  // Type of inquiry
                "INQ_NM"        : "Tên",             // Inquirer
                'INP_BIZ_TYPE'  : "Vui lòng chọn một ngành",
                'BIZ_TYPE'      : "Loại hình kinh doanh",
                "CIRCULATION"   : "Kinh doanh lưu thông",
                "SALE"          : "giảm giá",
                "ETC"           : "Etc.",
                "INQ_TEL"       : "TEL No.",
                "ADDR1"         : "Địa chỉ",
                "ADDR2"         : "Địa chỉ chi tiết",
                "INQ_CNTN"      : "Nội dung yêu cầu",
                "AGREE"         : "Đồng ý thu thập thông tin cá nhân",
                "AGREE_REMARK"  : "Chúng tôi giữ thông tin cá nhân ở trên để giải đáp thắc mắc.", 
                "REQUEST_SAVE"  : "Save",
                "MSG_AGREE"     : "Vui lòng kiểm tra sự đồng ý để thu thập thông tin cá nhân",
                "MSG_MIN_10"    : "Vui lòng gửi ít nhất 10 ký tự cho yêu cầu của bạn",

            }
            

        },
        "ko-KR": {
            "SYS" : {
                "NO_CONN"       : "현재 네트워크에 연결되어 있지 않습니다",
                "DELAY_CONN"    : "연결이 지연되어 데이터를 불러오지 못했습니다",
                "CHK_CONN"      : "인터넷연결상태를 확인 후 재실행 해주세요",
                "UNSAFE_CONN"   : "연결이 일시적으로 불안정합니다",
                "KAKAO_INQUIRY" : "실시간 상담",
                "KAKAO_SHARE"   : "카카오톡 공유",
                "APP_EXIT_MSG"  : "종료 하시려면 한번 더 누르세요",
                "APP_EXIT"      : "앱 종료",
                "AGREE_PRIVATE" : "개인정보 수집 동의",
                "AGREE"         : "동의",
                "COPY"          : "복사 되었습니다",
                "SET_COMPLETE"  : "설정 변경 완료",
                "GO_LOGIN"      : "로그인 화면으로 이동합니다",
                "ERR_TEL_AUTH"  : "전화번호를 입력하고 인증 후 이용할 수 있습니다",
                "NO_AGREE_MSG"  : "미동의로 인한 서비스를 정상적으로 사용할 수 없습니다",
                "NO_AREA"       : "지역없음",
                "REMARK"        : "비고",
                "RQR_INPUT"     : "필수입력",
                "CALL"          : "전화하기",
                "PULL_RESET_MSG": "당겨서 새로고침",
            },

            "FAV" : {
                "ADDED"         : "즐겨찾기에 추가되었습니다",
                "DELETED"       : "즐겨찾기에서 삭제되었습니다",
            },
            
            "DIAG" : {
                "P_NUM"         : "주문받을 연락처",
                "I_ONCE"        : "최초 한번만 입력하시면 됩니다",
                "I_NUMBER"      : "숫자만 입력하세요",
                "I_VAL_10_20"   : "10자리이상, 12자리 미만으로 입력하세요",

                "DEL_SHOP_CART" : "선택한 제품을 장바구니에서 삭제 하시겠습니까?",
                "DEL_SHOP_CART_ALL" : "장바구니에 담긴 상품 모두 삭제 하시겠습니까?",
                "DEL_ORDER"     : "해당주문내역을 삭제 하시겠습니까?",
                "COPY_CART"     : "해당주문내역을 장바구니로 복사하시겠습니까?",

                "DEL_FAV_CART" : "선택한 제품을 즐겨찾기에서 삭제하시겠습니까? ",
                "DEL_FAV_CART_ALL" : "즐겨찾기에 담긴 상품 모두 삭제하시겠습니까?",

                "CNCL_ORDER"    : "해당주문을 취소하시겠습니까?",

                "CONFIRM_AGAIN" : "신중히 확인 후 변경바랍니다",       // Vui lòng thay đổi nó sau khi kiểm tra cẩn thận.
                "CHANGE_NUMBER" : "로 전화번호를 변경하시겠습니까?",   // Bạn có muốn thay đổi số điện thoại không?
                "MSG_LIST"      : "메시지 목록",
                "APP_UPDATED_LAST" : "앱이 최신 버전으로 업데이트되었습니다.", 
                "APP_MOVE_PAGE" : "업데이트로 이동하시겠습니까?", 
                "REQ_LINK"      : "이 링크로 연결할까요?",
                "NO"            : "아니요",
                "YES_ADULT"     : "네 성인 입니다",
                "IS_TRUST_LINK" : "신뢰할 수 있는 링크 인지",   //Đây có phải là một địa chỉ liên kết đáng tin cậy?
                "CHECK_AGIN"    : "한번 더 확인해 주세요",      //Hãy kiểm tra.

                "AGREE_DEVICE"  : "사용자 개인기기정보 수집 동의", // Đồng ý thu thập thông tin thiết bị cá nhân
                "PURPOSE_CLLT"  : "수집 목적",                  //  Mục đích thu thập
                "GO_POLICY"     : "개인정보취급방침 바로가기",     //  Chuyển đến Chính sách bảo mật
                "AGREE_DEVICE1" : "사용자 인증은 시작시 한번만 인증됩니다.",  //  Xác thực người dùng chỉ cần được xác thực một lần khi khởi động.

                "POLICY_1_MSG"  : "원활한 배달서비스를 위해 사용자의 개인정보(전화번호,기기정보, GPS정보)를 수집합니다", // Chúng tôi thu thập thông tin cá nhân của người dùng (số điện thoại, thông tin thiết bị, thông tin GPS) để cung cấp dịch vụ chuyển phát suôn sẻ.
                "POLICY_2_MSG"  : "개인정보는 오직 본앱의 서비스만을 위해 사용되며, 그 외 목적에는 사용되지 않음을 알립니다", // Thông tin cá nhân chỉ được sử dụng cho mục đích dịch vụ và không được sử dụng cho bất kỳ mục đích nào khác.

                "AGE_19"        : "만 19세 이상", // (trên 19 tuổi?)
                "IS_ADULT"      : "성인입니까?", // Bạn là người lớn?
                "ADULT_POLICY"  : "'WITHU'는 미성년자에게 담배 및 주류 판매를 하지 않습니다. 19 세 이상의 성인 만 구매 가능합니다",
                "ADULT_MSG"     : "미성년자의 경우 배달수령시 담배/주류는 현장에서 취소합니다", // Đối với trẻ vị thành niên, thuốc lá / rượu sẽ bị hủy trên trang web khi nhận được
                "AREA_DO_SELECT": "배달받을 지역을 선택해주세요",

            },
            "COMM" : {
                "SEARCH_PRODUCT": "찾으시는 상품을 검색하세요",
                "SEARCH_ADDR"   : "주소검색",
                "CLS"           : "닫기",
                "close"         : "닫기",
                "ok"            : "확인",
                "noMoreData"    : "더이상 조회할 데이터가 존재하지 않습니다",
                "noData"        : "데이터가 존재하지 않습니다",
                "cancel"        : "취소",
                "changePassword": "비밀번호 변경",
                "save"          : "저장",
                "questChangePassword" :  "비밀번호를 변경하시겠습니까?",
                "DEL"           : "삭제",
                "MOD"           : "수정",
                "NONE"          : "없음",
                "RD_SCV"        : "서비스 준비중",
                "EXT_LINK"      : "외부링크 연결 포함",
                "WARN_KR_PHONE" : "한국 전화 번호로는 주문할 수 없습니다. 연락 가능한 현지 전화 번호로 변경하십시오",
                "MOD_COMPLTE"   : "수정 되었습니다",
            },

            "COIN" : {
                "BONUS"         : "적립금",
                "BONUS_QTY"     : "적립금 수량",
                "BONUS_HISTORY" : "적립금 입출금 내역",
                "IN"            : "입금",
                "OUT"           : "출금",
                "ALL"           : "전체",
            },

            "LOCATION" : {
                "REG_ADDR"      : "주소를 등록하세요" ,
                "MY_LOC"        : "현위치",
                "MY_AREA"       : "배달구역 설정",
                "MY_AREA_MSG"   : "배달구역 설정을 해주세요",
                "NO_COMP_MSG"   : "현재 설정하신 지역에는 점포(가게)가 없습니다",
                "CONF_ADDR"     : "배달주소 설정",
                "CONF_MAP"      : "지도검색",
                "CONF_BUILD"    : "건물검색",
                "SEARCH_ADDR"   : "주소를 검색하세요",
                "NOW_POSTION"   : "현재위치로 설정",
                "NOW_LOADING"   : "위치정보를 수신중입니다", 
                "ADDR2_INPUT"   : "나머지 상세주소를 정확히 입력해주세요",
                "ADDR3_INPUT"   : "주소는 건물을 선택시 자동입력됩니다",

                "ERR_NO_ADDR"   : "주소정보를 불러오지 못했습니다",
                "ERR_NO_SERVER" : "구글서비스를 불러오지 못했습니다",
                "ERR_LNG_LONG"  : "주소 미입력 및 위도와 경도를 찾을 수 없습니다 : ",

                "SET_GSP_ON"    : "핸드폰의 GPS 사용설정을 허용해주세요",

                "CITY"          : "도시",
                "CITY_MSG"      : "도시를 선택하세요",
                "LOCAL"         : "지역",
                "LOCAL_MSG"     : "지역을 선택하세요",
                "BUILDING"      : "건물선택",
                "BUILDING_MSG"  : "건물을 선택하세요",
                "STORE"         : "매장",
                "STORE_MSG"     : "매장을 선택하세요",
                "STORE"         : "매장",
                "REMARK"        : "특이사항",

                "AREA_SETTING"  : "지역 설정",
                "SEL_CNTNT"     : "지역 선택",
                "SEL_NATION"    : "국가 선택",
                "SEL_CITY"      : "도시 선택",

                "SEARCH_RMK1"   : "1. 상단 검색창에서 주소를 검색 후 상세주소를 입력해주세요",
                "SEARCH_RMK2"   : "2. 현재위치일 경우 상단 있는 '현재위치로 설정' 버튼을 클릭 후 상세주소를 입력해주세요",

            },

            "TOP" : {
                "SEARCH_ALL"    : "찾으시는 상품을 검색하세요",
                "BS_ORDER"      : "일반주문",
                "GB_ORDER"      : "공구주문",
            },

            
            "PRD" : {
                "CLASSIFY"      : "상품분류",
                "NO_CONTAIN"    : "최소주문액미포함",
                "VIEW_FILT"     : "조회 필터",
                "S_BRAND"       : "브랜드순",
                "S_PROD"        : "상품명순",
                "S_SALE"        : "판매순",
                "S_NEW"         : "신상등록순",
                "S_STAR"        : "별점순",
                "S_BS"          : "기본",
                "AMT"           : "가격", 
                "ORD_TIMES"     : "주문시간",
                "REMARK"        : "상품설명",

                "STAR"          : "별점평가", 
                "STAR_MSG"      : "별점을 선택해주세요", 
                "COMMET_INP"    : "한줄평을 입력해주세요",
                "LINEUP"        : "정렬",

                "COOK_TIME"     : "음식조리소요시간",
                "OPEN_TIME"     : "영업시간 안내",
                "HOLY_DAY"      : "휴일",
                "EVERY_OPEN"    : "무휴",
                "LIMITED_STOCK" : "주문가능한 수량이 부족합니다",
            }, 

            "LEFTMENU" : {
                "MY_ADDR"       : "나의주소",
                "RCT_ADDR"      : "최근주문주소",
                "FOVORITE"      : "즐겨찾기",
                "FCM_MSG"       : "수신메시지",
                "EVT_NOTICE"    : "이벤트/공지사항",
                "EVENT"         : "이벤트",
                "NOTICE"        : "공지사항",
                "MY_CONFIG"     : "환경설정",
                "TEL_LIST"      : "전화번호부",
                "KOR_INFO"      : "한인소식",
                "NOW_AIR"       : "실시간 미세먼지",
                "POLICY"        : "개인정보처리방침",
                'REQUEST'       : "입점문의",
                'OTHER_REQUEST' : "기타 제휴문의",
                'CONTACK_INQURY': "기타문의",

            },

            "TABMENU" : {
                "HOME"          : "HOME",
                "KIND"          : "상품분류",
                "GB"            : "공동구매",
                "BASKET"        : "장바구니",
                "ORDER_LIST"    : "주문내역",
                "PORFILE"       : "나의정보",
            },

            "CONF" : {
                "agreeAll"      : "전체 동의",
                "addr1"         : "주소",
                "addr2"         : "상세주소",
                "PUSH_SET"      : "알림설정",
                "PUSH_AGREE"    : "알림동의",
                "NO_VERSION"    : "버전 확인 불가",
                "LAST_VERSION"  : "최신버전",
                "VERSION_ERR"   : "버전 체크 오류 혹은 데이터가 없습니다",
                "DRAG_MAP"      : "지도안 빨간색 아이콘을 드래그하여 주소를 지정할 수 있습니다",
                "RQR_ADDR"      : "배달 받을 주소를 필수로 입력하세요",

            },
            "POP": {
                "todayClose"    : "오늘하루 그만보기",
            },
            
            "BSK" : {
                "SUM"           : "합계",
                "ALL_SUM"       : "전체합계",
                "ALL_DEL"       : "전체삭제",
                "COM_SUM"       : "일반상품합계",
                "SVC_SUM"       : "서비스상품합계",
                "MY_COIN"       : "보유중인 적립금",
                "USE_COIN"      : "적립금 사용하기",
                "STT_ORD"       : "주문시작하기",
                "CAL_AMT"       : "지불금액",
                "MORE_HIGH"     : "만동 이상 구매",
                "CANT_WRITE"    : "구매이력이 없어 입력할 수 없습니다",
                "ALDY_WRITE"    : "이미 작성하셨습니다",
                "COIN_EXCEED"   : "보유수량을 초과했습니다",
                "CAL_EXCEED"    : "계산금액을 초과했습니다",
                "MIN_ORD_QTY"   : "본 상품의 최소구매 수량",

                "WARN_SOLD_OUT" : "품절상품이 있으니 삭제 후 주문해주세요", // Có những sản phẩm hết hàng, vui lòng đặt hàng sau khi xóa.
                "WARN_CLOSED"   : "휴무업체 제품이 있으니 삭제 또는 예약 배송만 가능합니다",   // Có một sản phẩm ngày lễ, vì vậy xin vui lòng đặt hàng sau khi xóa.
                "WARN_TIMEOUT"  : "판매시간종료 제품은 삭제 또는 예약 배송만 가능합니다",   // Có một sản phẩm vào cuối bán hàng, vì vậy vui lòng đặt hàng sau khi xóa

                "NO_ITEM"       : "추가한 상품이 없습니다",
                "POINT_NO_PURCH": "적립금포인트는 적립금으로 구매할수 없습니다",
                "BS_DELV_AMT"   : "기본배달비",
                "REMARK"        : "거리에 따라 추가배달비용이 발생할수 있습니다",
                "FREE"          : "무료배달 : ",
                "ST_LIMIT_MSG"  : "주문가능한 수량이 부족합니다" ,
                "WPAY_RATE"     : "적립금 이용 가능 비율 : ",
                "NO_USE_POINT"  : "해당 상점은 적립금을 취급하지 않습니다",
                "NO_TAX"        : "이 상점은 세금계산서를 취급하지 않습니다",
                "CART_EMPTY"    : "장바구니를 비운 뒤 추가 가능합니다",
                "CART_CHK_2"    : "장바구니에 두 점포 이상의 상품은 주문 할수 없습니다. 취합배송 할수 없습니다",
            },

            "GB" : {
                "APP_AMT"       : "신청금액",
                "TAR_AMT"       : "목표금액",
                "MY_APPLY"      : "나의신청 건수",
                "REMAIN_DT"     : "남은신청기간",
                "ABLE_DT"       : "신청가능기간",
                "DELV_DT"       : "배송가능기간",
                "GOAL_AMT"      : "목표금액",
                "GOAL_RATE"     : "달성률",
                "ORGIN"         : "원산지",
                "CUSTOM"        : "공급처",
                "COLORS"        : "색상",
                "SIZES"         : "사이즈",
                "FOOD_RMK"      : "음식비고",
                "PROD_RMK"      : "상품설명",
                "DETAIL_VW"     : "자세히보기",
                "RMK_SM"        : "'신청하기' 즉시 결제가 진행되지 않습니다",
                "RMK_MD"        : "상품의 목표금액과 마감일이 있어 해당 상품의 목표금액을 달성했을때만 마감일 다음날 입력하신 일자에 따라 순차적으로 배송이 됩니다.",
                "REQ_MEM"       : "명 신청",
                "MEM"           : "현재",
                "D_DAY_AFT"     : " 일 지남",
                "D_DAY_BFR"     : " 일 후 시작",
                "D_DAY_RMN"     : " 일 남음",
                "DEADLINE"      : "마감임박",
                "INPUT_TIME_CAL": "달력 내 우측 시간대를 정확히 선택하여 입력해주세요",
                "ARR_TIME"      : "도착시간",
                "TIME_RMK"      : "시간대별로 정해진 배송건수가 설정되어 있으므로 비활성된 시간대에는 예약배송신청이 완료된 시간대입니다.",


            },
            "ORD" : {
                "AMT"           : "주문합계",
                "REG_DT"        : "주문일시",
                "CASH"          : "현금",
                "CARD"          : "CARD",
                "DLV_AMT"       : "기본배달비",
                "ADD_DELV_AMT"  : "추가배달비",
                "CAL_AMT"       : "지불금액",
                "DELV_NM"       : "배달자",
                "DPT_DT"        : "예상도착",
                "DELV_ADDR"     : "배달주소",
                "DETAIL"        : "주문상세정보",
                "LIST"          : "주문상품목록",
                "ABOUT_TIME"    : "소요시간 약",
                "MINUTS"        : "분",
                "READY"         : "상품(음식)준비중",
                "ING"           : "배달중",
                "CNCL_OK"       : "취소완료",
                "CNCL"          : "주문취소",
                "COPY"          : "장바구니로 복사",
                "COMPLETE"      : "완료",
                "EXT_DELVING"   : "외부배송업체 배송중",
                "COMP_TIME"     : "완료시각",
                "READY_PRD"     : "준비중인 상품입니다",
                "ENDED_PRD"     : "종료된 상품입니다",
                "SOLD_PRD"      : "품절된 상품입니다",

                "COPYED_CART"   : "주문내역이 장바구니로 복사되었습니다",
                "COPYED_FAIL"   : "장바구니 복사 실패",
                "AFTER_PROC"    : " 이후 접수 예정",
            },

            "ORDD" : {
                "ORD_INFO"      : "배송정보(수정가능)",
                "DO_ORDER"      : "주문하기",
                "RCT_ADDR"      : "최근주문주소",
                "ORD_INPUT"     : "입력사항",
                "ORD_NOW"       : "즉시배송",
                "ORD_RESRV"     : "예약배송",
                "ORD_MEMO_MSG"  : "요청사항(담배, 전화카드는 적립금에 포함되지 않습니다)",
                "ORD_GIFT"      : "증정품 선택",
                "ORD_GIFT_MSG"  : "증정품 미선택시 적립금 2% 적립",
                "BILL_WAY"      : "결제방식",
                "TAX"           : "세금계산서",
                "ORD_TAX"       : "세금계산서 신청",
                "TAX_NUM"       : "세금번호",
                "TAX_ADDR"      : "세금주소",
                "CORP_NM"       : "회사명",
                "CORP_EMAIL"    : "회사이메일",
                "COMPLEATE"     : "최종주문하기",
                "TAX_WARNING"   : "위드유마트 상품이 아닌 음식(상품)의 세금계산서 요청시 해당업체의 메뉴 상단 전화번호 혹은 카톡으로 따로 연락을 하셔야 됩니다.세금 미포함 음식(상품)이기 때문입니다",
                "CONFIRM"       : "최종주문확인",
                "REQ_CNFRM"     : "주문완료하시겠습니까?",
                "MM_REQ"        : "메모 및 요청사항을 입력하세요",
            },

            "SEARCH" : {
                "ALL_P_SEARCH"  : "전체 상품 검색",
            }, 
            

            "AUTH": {
                "ID"            : "아이디",
                "PW"            : "비밀번호",
                "PW1"           : "비밀번호 확인",
                "RESET_PW"      : "비밀번호 재설정",
                "LOGIN"         : "Login",
                "LOGOUT"        : "Logout",
                "JOIN"          : "회원가입",
                "JOIN_INFO"     : "회원가입정보",
                "questLogout"   : "로그아웃 하시겠습니까?",
                "questOffService" : "서비스와 연결을 끊겠습니까?",
                "SENDEMAIL"     : "이메일 전송",
                "FIND_PASS"     : "비밀번호 찾기",
                "FIND_ID"       : "아이디 찾기",
                "FIND_BY_EMAIL" : "이메일로 찾기",
                "I_EMAIL"       : "회원가입시 입력하신 이메일을 입력하세요",
                "TXT_1"         : "이메일 주소를 입력한 후 이메일 전송 버튼을 클릭하세요",
                "TXT_2"         : "입력하신 이메일 계정에 접속하면 아이디를 알려드립니다",
                "TXT_3"         : "이메일, 아이디, 전화번호를 입력한 후 이메일 전송 버튼을 클릭하세요",
                "TXT_4"         : "입력하신 이메일 계정으로 새 비밀번호를 보내드렸습니다",

                "ID_CHK"        : "아이디 중복 체크",
                "EMAIL_CHK"     : "이메일 중복 체크",
                "PLY_AGREE"     : "이용약관 및 이용 동의",
                "VW_CNTT"       : "내용보기",
                "AGREE_USE_RQR" : "이용약관 동의(필수)",
                "AGREE_PRI_RQR" : "개인정보 수집 및 이용동의(필수)",
                "AGREE_ALRAM"   : "WITHU 혜택 알림 동의(선택)",

                "MOD_TEL"       : "전화번호를 수정하세요",
                "INP_TEL"       : "전화번호를 입력하세요",

                "AVAL_ID"       : "사용가능한 아이디 입니다",       
                "ALRDY_ID"      : "이미 사용중인 아이디 입니다",    
                "EMAIL_USE_MSG" : "이메일 주소는 계정 분실시 사용됩니다", 
                "EMAIL_AVABLE"  : "사용 가능한 이메일 주소입니다",   
                "ALRDY_EMAIL"   : "이미 사용중인 이메일 주소입니다", 

                "VAL_ID"        : "아이디는 최소 2자 이상 입력",
                "VAL_ID_CHK"    : "아이디 중복 체크",
                "VAL_EMAIL"     : "이메일형식으로 입력",
                "VAL_EMAIL_CHK" : "이메일 중복 체크",
                "VAL_PW"        : "비밀번호는 숫자, 영문을 포함",
                "VAL_PW6"       : "비밀번호는 최소 6자 이상 입력",
                "VAL_PW_CHK"    : "비밀번호와 비밀번호 확인을 동일하게 입력",
                "VAL_NICK"      : "닉네임은 2자 이상 입력",

                "CHK_AGAIN"     : "아이디와 비밀번호를 다시 한번 확인 후 다시 로그인해주세요",
                "PW_CHANGED"    : "비밀번호가 변경되었습니다",
                "NICK_CHANGED"  : "닉네임이 변경되었습니다",

                "INPUT_REF"     : "추천인 아이디를 입력하세요",
                
            },
            "PROFILE" : {
                "COIN_HIS"      : "이력조회",
                "EMAIL"         : "이메일",
                "NICK"          : "닉네임",
                "REF_ID"        : "추천인 아이디",
                "CANNT_MODE"    : "저장 후 수정불가능",
                "SAVE"          : "저장",
                "TEL"           : "전화번호",
                "SVC_OUT"       : "서비스 연결끊기",
                "PW_VALI_1"     : "비밀번호는 숫자, 영문을 포함해야 합니다",
                "PW_VALI_2"     : "비밀번호는 최소 6자리 이상입니다",
                "PW_VALI_3"     : "비밀번호와 비밀번호 확인을 동일하게 입력하세요",
            },

            "INQUERY": {
                "INQ_TYPE"      : "문의 종류",  // Type of inquiry
                "INQ_NM"        : "문의자",     // Inquirer
                'INP_BIZ_TYPE'  : "업종을 선택하세요",
                'BIZ_TYPE'      : "업종",
                "CIRCULATION"   : "유통",
                "SALE"          : "판매",
                "ETC"           : "기타",
                "INQ_TEL"       : "연락처",
                "ADDR1"         : "주소",
                "ADDR2"         : "상세주소",
                "INQ_CNTN"      : "문의내용",
                "AGREE"         : "개인정보 수집 동의",
                "AGREE_REMARK"  : "원활한 문의를 위해 위의 개인 정보를 보관하고 있습니다.", 
                "REQUEST_SAVE"  : "문의하기",
                "MSG_AGREE"     : "개인정보 수집 동의를 체크해주세요", 
                "MSG_MIN_10"    : "문의내용은 최소 10자 이상 보내주세요",
            }

        },
        "en-EN": {
            "SYS" : {
                "NO_CONN"       : "Not connected to network",
                "DELAY_CONN"    : "Data could not be loaded because the connection was unstable",
                "CHK_CONN"      : "Please check your internet connection and try again",
                "UNSAFE_CONN"   : "Communication status temporarily unstable",
                "KAKAO_INQUIRY" : "Inquiry",
                "KAKAO_SHARE"   : "Share",
                
                "APP_EXIT_MSG"  : "Click once more to exit",
                "APP_EXIT"      : "Exit APP",
                "AGREE_PRIVATE" : "Consent to collection of personal information",
                "AGREE"         : "Agree",
                "COPY"          : "Copied",
                "SET_COMPLETE"  : "Setup complete",
                "GO_LOGIN"      : "Go to the login screen",
                "ERR_TEL_AUTH"  : "Enter your phone number and use it after authentication",
                "NO_AGREE_MSG"  : "The service due to non-agreement cannot be used normally",
                "NO_AREA"       : "No Area",
                "REMARK"        : "remark",
                "RQR_INPUT"     : "Required input",
                "CALL"          : "Call",
                "PULL_RESET_MSG": "Pull to refresh",
            },

            "FAV" : {
                "ADDED"         : "Added to Favorites",
                "DELETED"       : "Deleted to favorites",
            },

            "DIAG" : {
                "P_NUM"         : "Phone number to order",
                "I_ONCE"        : "You only need to enter it once at first",
                "I_NUMBER"      : "Please enter only numbers",
                "I_VAL_10_20"   : "Enter more than 10 digits and less than 12 digits",

                "DEL_SHOP_CART" : "Are you sure you want to delete the selected product from your shopping cart?",
                "DEL_SHOP_CART_ALL" : "Are you sure you want to delete all products in your shopping cart?",
                "DEL_ORDER"     : "Are you sure you want to delete this order history?",
                "COPY_CART"     : "Would you like to copy this order to your shopping cart?",

                "DEL_FAV_CART"  : "Are you sure you want to remove the selected product from Favorites? ",
                "DEL_FAV_CART_ALL" : "Are you sure you want to delete all of your favorite items?",

                "CNCL_ORDER"    : "Are you sure you want to cancel the order?",

                "CONFIRM_AGAIN" : "Please change it after checking carefully.",       // Vui lòng thay đổi nó sau khi kiểm tra cẩn thận.
                "CHANGE_NUMBER" : "Would you like to change your phone number in this app?",   // Bạn có muốn thay đổi số điện thoại không?
                "MSG_LIST"      : "Messages list",
                "APP_UPDATED_LAST" : "The app has been updated to the latest version.", 
                "APP_MOVE_PAGE" : "Would you like to go to update?", 
                "REQ_LINK"      : "Would you like to access this link?",
                "NO"            : "No",
                "YES_ADULT"     : "Yes I am an adult",
                "IS_TRUST_LINK" : "Trusted link recognition",   //Đây có phải là một địa chỉ liên kết đáng tin cậy?
                "CHECK_AGIN"    : "Please check again",      //Hãy kiểm tra.

                "AGREE_DEVICE"  : "User's consent to collect personal device information", // Đồng ý thu thập thông tin thiết bị cá nhân
                "PURPOSE_CLLT"  : "Purpose of collection",                  //  Mục đích thu thập
                "GO_POLICY"     : "Go to Privacy Policy",     //  Chuyển đến Chính sách bảo mật
                "AGREE_DEVICE1" : "User authentication is only authenticated once at startup",  //  Xác thực người dùng chỉ cần được xác thực một lần khi khởi động.

                "POLICY_1_MSG"  : "We collect user's personal information (phone number, device information, GPS information) for stable delivery service", // Chúng tôi thu thập thông tin cá nhân của người dùng (số điện thoại, thông tin thiết bị, thông tin GPS) để cung cấp dịch vụ chuyển phát suôn sẻ.
                "POLICY_2_MSG"  : "Please note that personal information is only used for the services of this app and not for other purposes.", // Thông tin cá nhân chỉ được sử dụng cho mục đích dịch vụ và không được sử dụng cho bất kỳ mục đích nào khác.

                "AGE_19"        : "19 years old or older", // (trên 19 tuổi?)
                "IS_ADULT"      : "Are you an adult?", // Bạn là người lớn?
                "ADULT_POLICY"  : "'WITHU' does not sell tobacco and alcohol to minors. Available only for adults over 19",
                "ADULT_MSG"     : "For minors, cigarettes/alcohol will be canceled on site upon receipt of delivery", // Đối với trẻ vị thành niên, thuốc lá / rượu sẽ bị hủy trên trang web khi nhận được

            },
            "COMM" : {
                "SEARCH_PRODUCT": "Search for the product Here",
                "SEARCH_ADDR"   : "Search Addr.",
                "CLS"           : "Close",
                "close"         : "Close",
                "ok"            : "OK",
                "noMoreData"    : "There is no more data to search",
                "noData"        : "No data found",
                "cancel"        : "Cancel",
                "changePassword": "Change Password",
                "save"          : "Save",
                "questChangePassword" :  "Would you like to change your password?",
                "DEL"           : "Delete",
                "MOD"           : "Modified",
                "NONE"          : "none",
                "RD_SCV"        : "Preparing service",
                "EXT_LINK"      : "External link connection included",
                "WARN_KR_PHONE" : "It is not possible to order with a Korean phone number. Please change to a local phone number where you can be reached",
                "MOD_COMPLTE"   : "This change is complete",
            },

            "COIN" : {
                "BONUS"         : "Reserves",
                "BONUS_QTY"     : "Reserve amount",
                "BONUS_HISTORY" : "Deposit and withdrawal",
                "IN"            : "Deposit",
                "OUT"           : "Withdraw",
                "ALL"           : "ALL",
            },

            "LOCATION" : {
                "REG_ADDR"      : "Register your address" ,

                "MY_AREA"       : "Delivery zone setting",
                "MY_AREA_MSG"   : "Please set the delivery area",
                "NO_COMP_MSG"   : "There are no stores (shops) in the currently set area",

                "MY_LOC"        : "Current location",
                "CONF_ADDR"     : "Set delivery address",
                "CONF_MAP"      : "Search by map",
                "CONF_BUILD"    : "Search by building",
                "SEARCH_ADDR"   : "Search for an address",
                "NOW_POSTION"   : "Set to my location",
                "NOW_LOADING"   : "Location is being received", 
                "ADDR2_INPUT"   : "Please enter the detailed address",
                "ADDR3_INPUT"   : "Automatically entered when selecting a building",

                "ERR_NO_ADDR"   : "Failed to load address information",
                "ERR_NO_SERVER" : "Google service failed to load",
                "ERR_LNG_LONG"  : "Address not entered and latitude and longitude not found : ",

                "SET_GSP_ON"    : "Please allow your phone's GPS settings",

                "CITY"          : "City",
                "CITY_MSG"      : "Select a city",
                "LOCAL"         : "Region",
                "LOCAL_MSG"     : "Select a region",
                "BUILDING"      : "Building",
                "BUILDING_MSG"  : "Select a building",
                "STORE"         : "Branch store",
                "STORE_MSG"     : "Select a store",
                "REMARK"        : "Remark",

                "SEL_CNTNT"     : "Region selection",
                "SEL_NATION"    : "Country selection",
                "SEL_CITY"      : "City selection",

                "SEARCH_RMK1"   : "1. Search the address in the top search box and enter the detailed address",
                "SEARCH_RMK2"   : "2. When setting delivery to the current location, click the 'Set to my Location' button at the top and enter the detailed address",
            },

            "TOP" : {
                "SEARCH_ALL"    : "Search for the product Here",
                "BS_ORDER"      : "Common Order",
                "GB_ORDER"      : "Group buying Order",
            },

            "PRD" : {
                "CLASSIFY"      : "Classification",
                "NO_CONTAIN"    : "Total not applicable",
                "VIEW_FILT"     : "Lookup filter",
                "S_BRAND"       : "Brand Name Sort",
                "S_PROD"        : "Product Name Sort",
                "S_SALE"        : "Sales Sort",
                "S_NEW"         : "New registration Sort",
                "S_STAR"        : "Star Sort",
                "S_BS"          : "Basic sort",
                "AMT"           : "Price", 
                "ORD_TIMES"     : "Order time",
                "REMARK"        : "Product description",

                "STAR"          : "Star rating", 
                "STAR_MSG"      : "Please select a star", 
                "COMMET_INP"    : "Please enter one line comment",
                "LINEUP"        : "Sort",

                "COOK_TIME"     : "Food cooking time",
                "OPEN_TIME"     : "Open Time",
                "HOLY_DAY"      : "Day off",
                "EVERY_OPEN"    : "Open everyday",
                "LIMITED_STOCK" : "There are no more product quantities.",

            }, 

            "LEFTMENU" : {
                "MY_ADDR"       : "My address",
                "RCT_ADDR"      : "Last order address",
                "FOVORITE"      : "Favorites",
                "FCM_MSG"       : "Message received",
                "EVT_NOTICE"    : "Event/Notice",
                "EVENT"         : "Event",
                "NOTICE"        : "Notice",
                "MY_CONFIG"     : "Settings",
                "TEL_LIST"      : "Phone book",
                "KOR_INFO"      : "Korean News",
                "NOW_AIR"       : "Real-time fine dust",
                'REQUEST'       : 'Contact us',
                "POLICY"        : "Personal Information Processing Policy",
                'OTHER_REQUEST' : "Other partnership inquiries",
                'CONTACK_INQURY': "Inquiries",

            },

            "TABMENU" : {
                "HOME"          : "HOME",
                "KIND"          : "Products",
                "GB"            : "GroupBuying",
                "BASKET"        : "S.Cart",
                "ORDER_LIST"    : "MyOrder",
                "PORFILE"       : "Profile",
            },

            "CONF" : {
                "agreeAll"      : "Full Agree",
                "addr1"         : "Address",
                "addr2"         : "Detailed Address",
                "PUSH_SET"      : "Notification Settings",
                "PUSH_AGREE"    : "Notification consent",
                "NO_VERSION"    : "Version unknown",
                "LAST_VERSION"  : "The last version",
                "VERSION_ERR"   : "Version check error or no data",
                "DRAG_MAP"      : "You can specify the address by dragging the red icon on the map.",
                "RQR_ADDR"      : "Please enter the address to be delivered",

            },
            "POP": {
                "todayClose"    : "Close for today",
            },
            
            "BSK" : {
                "SUM"           : "Sum",
                "ALL_SUM"       : "Total Sum",
                "ALL_DEL"       : "Delete all",
                "COM_SUM"       : "Common product total",
                "SVC_SUM"       : "Service product total",
                "MY_COIN"       : "Reserves",
                "USE_COIN"      : "Using reserves",
                "CAL_AMT"       : "Payment amount",
                "MORE_HIGH"     : "dongs Purchase over",
                "STT_ORD"       : "Start ordering",
                "CANT_WRITE"    : "There is no purchase history, so you can't enter",
                "ALDY_WRITE"    : "You have already written",
                "COIN_EXCEED"   : "The amount exceeded",
                "CAL_EXCEED"    : "The amount has been exceeded",
                "MIN_ORD_QTY"   : "Minimum purchase quantity",
                "WARN_SOLD_OUT" : "There are out-of-stock products, please order after deletion.", // Có những sản phẩm hết hàng, vui lòng đặt hàng sau khi xóa.
                "WARN_CLOSED"   : "Holiday maker's products can only be deleted or pre-delivered",   // Có một sản phẩm ngày lễ, vì vậy xin vui lòng đặt hàng sau khi xóa.
                "WARN_TIMEOUT"  : "For orders whose products have expired, they can only be deleted or pre-delivered.",   // Có một sản phẩm vào cuối bán hàng, vì vậy vui lòng đặt hàng sau khi xóa

                "NO_ITEM"       : "There are no products added",
                "POINT_NO_PURCH": "Reserve points cannot be purchased as reserve points",
                "BS_DELV_AMT"   : "Delivery cost",
                "REMARK"        : "If the distance is long, additional delivery fee may be added",
                "FREE"          : "Free : ",
                "ST_LIMIT_MSG"  : "Not enough quantity available for order" ,
                "WPAY_RATE"     : "Available points percentage : ",
                "NO_USE_POINT"  : "This store does not handle points",
                "NO_TAX"        : "This store does not handle tax invoices",
                "CART_EMPTY"    : "You can add after emptying your cart",
                "CART_CHK_2"    : "You cannot order more than two stores in your shopping cart. Collective delivery is not possible",
                
            },

            "GB" : {
                "APP_AMT"       : "Application price",
                "TAR_AMT"       : "Target price",
                "MY_APPLY"      : "Number of orders",
                "REMAIN_DT"     : "Order remaining",
                "ABLE_DT"       : "Available period",
                "DELV_DT"       : "Delivery period",
                "GOAL_AMT"      : "Target price",
                "GOAL_RATE"     : "Achievement rate",
                "ORGIN"         : "Origin",
                "CUSTOM"        : "Supplier",
                "COLORS"        : "Color",
                "SIZES"         : "Size",
                "FOOD_RMK"      : "Food remarks",
                "PROD_RMK"      : "Product description",
                "DETAIL_VW"     : "View details",
                "RMK_SM"        : "Payment does not proceed immediately after'Apply'",
                "RMK_MD"        : "There is a target price and a deadline for the product, and only when the target price for the product is achieved will be delivered sequentially according to the date entered on the next day of the deadline.",
                "REQ_MEM"       : "Order",
                "MEM"           : "Now",
                "D_DAY_AFT"     : " day passed",
                "D_DAY_BFR"     : " Days Start after",
                "D_DAY_RMN"     : " day left",
                "DEADLINE"      : "Deadline",
                "INPUT_TIME_CAL": "Please select and enter the right time zone on the right side of the calendar",
                "ARR_TIME"      : "Arrival time",
                "TIME_RMK"      : "The inactivity time is the time when the reservation delivery application is completed"

            },
            "ORD" : {
                "AMT"           : "Order total",
                "REG_DT"        : "Order date",
                "CASH"          : "CASH",
                "CARD"          : "CARD",
                "DLV_AMT"       : "Delivery cost",
                "ADD_DELV_AMT"  : "Additional delivery costs",
                "CAL_AMT"       : "Payment price",
                "DELV_NM"       : "Delivery",
                "DPT_DT"        : "Expected arrival",
                "DELV_ADDR"     : "Address",
                "DETAIL"        : "Order details",
                "LIST"          : "Order product list",
                "ABOUT_TIME"    : "Time required",
                "MINUTS"        : "minute",
                "READY"         : "Preparing",
                "ING"           : "In delivery",
                "CNCL_OK"       : "Canceled",
                "CNCL"          : "Cancel",
                "COPY"          : "Copy to ShoppingCart",
                "COMPLETE"      : "Complete",
                "EXT_DELVING"   : "Shipping outside",
                "COMP_TIME"     : "Time completed",
                "READY_PRD"     : "Coming soon",
                "ENDED_PRD"     : "Ended product",
                "SOLD_PRD"      : "Sold Out",

                "COPYED_CART"   : "Your order has been copied to your cart",
                "COPYED_FAIL"   : "Order copy failed",
                "AFTER_PROC"    : "Reception after ",
            },

            "ORDD" : {
                "ORD_INFO"      : "Delivery information (modifiable)",
                "DO_ORDER"      : "Place an order",
                "RCT_ADDR"      : "Last order address",
                "ORD_INPUT"     : "Input part",
                "ORD_NOW"       : "Now order",
                "ORD_RESRV"     : "Advance order",
                "ORD_MEMO_MSG"  : "Requests (Cigarettes and phone cards are not included in the deposit)",
                "ORD_GIFT"      : "Select gift",
                "ORD_GIFT_MSG"  : "2% reserve if no gift is selected",
                "BILL_WAY"      : "Payment type",
                "TAX"           : "Tax bill",
                "ORD_TAX"       : "Request a tax invoice",
                "TAX_NUM"       : "Tax number",
                "TAX_ADDR"      : "Tax Address",
                "CORP_NM"       : "Company Name",
                "CORP_EMAIL"    : "Company email",
                "COMPLEATE"     : "Final order",
                "TAX_WARNING"   : "If you request a tax invoice for food (product) that is not a 'WITHU MART' product, you must contact the phone number at the top of the company's menu or 'KAKAO Talk' separately, because it is food (product) without tax.",
                "CONFIRM"       : "Final order confirmation",
                "REQ_CNFRM"     : "Would you like to complete the order?",
                "MM_REQ"        : "Notes and requests",
            },

            "SEARCH" : {
                "ALL_P_SEARCH"  : "Search All Products",
            }, 
            

            "AUTH": {
                "ID"            : "ID",
                "PW"            : "Password",
                "PW1"           : "Confirm Password",
                "RESET_PW"      : "Reset password",
                "LOGIN"         : "Login",
                "LOGOUT"        : "LogOut",
                "JOIN"          : "Sign Up",
                "JOIN_INFO"     : "Membership information",
                "questLogout"   : "Would you like to log out?",
                "questOffService" : "Do you want to disconnect from SNS login service?",
                "SENDEMAIL"     : "Send email",
                "FIND_PASS"     : "Forgot your password",
                "FIND_ID"       : "Find ID",
                "FIND_BY_EMAIL" : "Search by email",
                "I_EMAIL"       : "Please enter the email you entered when you signed up",
                "TXT_1"         : "Enter your email address and click the Send Email button",
                "TXT_2"         : "When you access the email account you entered, you will be notified of your ID.",
                "TXT_3"         : "Enter your email, ID and phone number and click the Send Email button",
                "TXT_4"         : "We sent you a new password to the email account you entered.",

                "ID_CHK"        : "Duplicate ID check",
                "EMAIL_CHK"     : "Email duplicate check",
                "PLY_AGREE"     : "Terms of use and consent",
                "VW_CNTT"       : "View content",
                "AGREE_USE_RQR" : "Accept the terms of use (required)",
                "AGREE_PRI_RQR" : "Consent to collection and use of personal information (required)",
                "AGREE_ALRAM"   : "Agree to WITHU Benefit Notification (optional)",

                "MOD_TEL"       : "Please edit your phone number",
                "INP_TEL"       : "Please enter your phone number",

                "AVAL_ID"       : "ID is available",       // ID có sẵn
                "ALRDY_ID"      : "This ID is already taken",    // ID đã được sử dụng
                "EMAIL_USE_MSG" : "E-mail will be used if your account is lost", // Địa chỉ email của bạn sẽ được sử dụng nếu bạn mất tài khoản
                "EMAIL_AVABLE"  : "E-mail address is available",   // Địa chỉ e-mail có sẵn
                "ALRDY_EMAIL"   : "This email address is already in use",    // Địa chỉ email này đã được sử dụng

                "VAL_ID"        : "Enter at least 2 characters",    // Vui lòng nhập ít nhất 2 ký tự cho 'ID'
                "VAL_ID_CHK"    : "Duplicate ID check",               // Kiểm tra trùng lặp 'ID'
                "VAL_EMAIL"     : "Enter in email format",            // Vui lòng nhập vào định dạng 'email'
                "VAL_EMAIL_CHK" : "Email duplicate check",           // Kiểm tra trùng lặp 'email'
                "VAL_PW"        : "Password includes numbers and letters",    // Vui lòng nhập mật khẩu của bạn bao gồm số và tiếng Anh
                "VAL_PW6"       : "Password must be at least 6 characters",
                "VAL_PW_CHK"    : "Enter the same password and password confirmation",   // Vui lòng nhập mật khẩu và mật khẩu xác nhận để khớp.
                "VAL_NICK"      : "Nickname must be 2 or more characters long",         // Vui lòng nhập ít nhất 2 ký tự

                "CHK_AGAIN"     : "Please check your ID and password again and log in again.",
                "PW_CHANGED"    : "Your password has been changed",
                "NICK_CHANGED"  : "Your nickname has changed",

                "INPUT_REF"     : "Please enter the referrer ID",
                
            },
            "PROFILE" : {
                "COIN_HIS"      : "View history",
                "EMAIL"         : "e-mail",
                "NICK"          : "Nickname",
                "REF_ID"        : "Referrer ID",
                "CANNT_MODE"    : "Not modified after saving",
                "SAVE"          : "Save",
                "TEL"           : "Phone number",
                "SVC_OUT"       : "Disconnect SNS Service",
                "PW_VALI_1"     : "Password must contain numbers and letters",
                "PW_VALI_2"     : "Password is at least 6 digits",
                "PW_VALI_3"     : "Enter the same password and password confirmation",
            },

            "INQUERY": {
                "INQ_TYPE"      : "Type of inquiry",  
                "INQ_NM"        : "Inquirer",         
                'INP_BIZ_TYPE'  : "Please select an industry",
                'BIZ_TYPE'      : "Type of industry",
                "CIRCULATION"   : "Circuration",
                "SALE"          : "Sale",
                "ETC"           : "Etc.",
                "INQ_TEL"       : "TEL No.",
                "ADDR1"         : "Address",
                "ADDR2"         : "Detail Address",
                "INQ_CNTN"      : "Content of inquiry",
                "AGREE"         : "Consent to collection of personal information",
                "AGREE_REMARK"  : "We keep the above personal information for smooth inquiries.", 
                "REQUEST_SAVE"  : "Save Inquire",
                "MSG_AGREE"     : "Please check the consent to collect personal information",
                "MSG_MIN_10"    : "Please send at least 10 characters for your inquiry",

            }
        },
    },

    // https://www.i18next.com/overview/configuration-options 참조
    init : function(){
        
        $.i18n.init({
            //lng: 'en-US',
            lng: localStorage.getItem("lang"),
            debug: false,
            useLocalStorage: false,
            localStorageExpirationTime: 86400000, // in ms, default 1 week
            resStore: myApp.i18Next.data
        }, function () {

            if( $('.translation').length > 0 ){
                $('.translation').i18n();

                // todo : 실제적으로 언어 선택으로 가야함
                var lang    = getLangNow();
                lang        = isNull(lang) ? "ko-KR" : lang;
        
                if( lang != "ko-KR"){
                    loadjscssfile("css/font_init.css", "css");
                }else{
                    removejscssfile("css/font_init.css", "css");
                }
            }
        });
    }, 

    changeLang : function () {
        
        $("#appTabbar").find(".homeTabbar .tabbar__label").attr("data-i18n", "TABMENU:HOME");
        $("#appTabbar").find(".cateTabbar .tabbar__label").attr("data-i18n", "TABMENU:KIND");
        $("#appTabbar").find(".grpTabbar .tabbar__label").attr("data-i18n", "TABMENU:GB");
        $("#appTabbar").find(".myBasket .tabbar__label").attr("data-i18n", "TABMENU:BASKET");
        $("#appTabbar").find(".myOrder .tabbar__label").attr("data-i18n", "TABMENU:ORDER_LIST");
        $("#appTabbar").find(".profileTab .tabbar__label").attr("data-i18n", "TABMENU:PORFILE");

        if( isNull( localStorage.getItem("USER_ID") )){
            $("#appTabbar").find(".profileTab .tabbar__label").attr("data-i18n", "AUTH:LOGIN");
        }
        
        // 주문하기 탭바
        $("#orderTabbar").find(".tabCm .tabbar__label").attr("data-i18n", "TOP:BS_ORDER");
        $("#orderTabbar").find(".tabGB .tabbar__label").attr("data-i18n", "TOP:GB_ORDER");

        // 지도로주소 검색 탭바
        $("#addrTabbar").find(".mapAddr .tabbar__label").attr("data-i18n", "LOCATION:CONF_MAP");
        $("#addrTabbar").find(".selectAddr .tabbar__label").attr("data-i18n", "LOCATION:CONF_BUILD");
        $("#searchAddrMap").attr("data-i18n", "[placeholder]LOCATION:SEARCH_ADDR");
        
        // 아이디/비번찾기 탭바
        $("#findIdPwTabbar").find(".tab_findid .tabbar__label").attr("data-i18n", "AUTH:FIND_ID");
        $("#findIdPwTabbar").find(".tab_findpw .tabbar__label").attr("data-i18n", "AUTH:RESET_PW");
        $("#findIdPage").find("#findIdEmail").attr("data-i18n", "[placeholder]AUTH:I_EMAIL");
        $("#findPwPage").find("#email").attr("data-i18n", "[placeholder]AUTH:I_EMAIL");
        
        // 이벤트 공지 탭바
        $("#noticeTabbar").find(".tab_event .tabbar__label").attr("data-i18n", "LEFTMENU:EVENT");
        $("#noticeTabbar").find(".tab_notice .tabbar__label").attr("data-i18n", "LEFTMENU:NOTICE");

        $("#searchTopInput").attr("data-i18n", "[placeholder]TOP:SEARCH_ALL");
        $("#searchItem").attr("data-i18n", "[placeholder]TOP:SEARCH_ALL");
        $("#user_id").attr("data-i18n", "[placeholder]AUTH:ID");
        $("#pass_wd").attr("data-i18n", "[placeholder]AUTH:PW");

        $("#loginPage").find("#phone_number").attr("placeholder", this.getTrasTxt("AUTH", "MOD_TEL"));

        $("#password").attr("placeholder", this.getTrasTxt("AUTH", "PW"));
        $("#passwordconfirm").attr("placeholder", this.getTrasTxt("AUTH", "PW1"));
        $("#name").attr("placeholder", this.getTrasTxt("PROFILE", "NICK"));

        // 요청사항
        $("#reqtext").attr("placeholder", this.getTrasTxt("ORDD", "MM_REQ")); //
        
        // 메인
        var myAddr  = localStorage.getItem("ADDR2");
        if( isNull(myAddr) ){
            myAddr = myApp.i18Next.getTrasTxt("LOCATION","REG_ADDR");    
        }
        $(document).find(".myloc").html(myAddr);

        // 프로필
        $("#refferor").attr("placeholder", this.getTrasTxt("PROFILE", "CANNT_MODE") );

        // todo : 실제적으로 언어 선택으로 가야함
        if( getLangNow() != "ko-KR"){
            loadjscssfile("css/font_init.css", "css");
        }else{
            removejscssfile("css/font_init.css", "css");
        }

        if( $('.translation').length > 0 ){
            $('.translation').i18n();
        }
    }, 

    // 개별로 변경
    getTrasTxt : function(item, field){
        var lang    = getLangNow();
        lang        = isNull(lang) ? "ko-KR" : lang;
        trasnText   = myApp.i18Next.data[lang][item][field];
        trasnText   = isNull(trasnText) ? "" : trasnText;
        return trasnText;
    }
}

