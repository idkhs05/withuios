myApp.mainController = {
	//////////////////////////
    // Tabbar(Init) Page Controller //
    //////////////////////////
    getCompanyList : function(page){

        // 회사별 메인
        var service	= new withWSDL("api/company/list" );
        // 회사목록 서비스 호출
        service.success = function(_jsonData){

            $(page).find(".companyListMain > *").remove();
            var jsonData    = _jsonData;
            if (typeof serviceModel.listCompany === 'undefined') {
                $.getScript("js/service/company.js", function(){
                    serviceModel.listCompany(page, jsonData, service.DOMAIN, null);
                });
            }else{
                serviceModel.listCompany(page, jsonData, service.DOMAIN, null);
            }
        };

        service.ajaxGetData(
            {
                start   : 0 ,
                orderby : "tb_company.VIEW_ORDER" ,
                order   : "ASC",
                isuse   : "Y",
                area_id : localStorage.getItem("AREA_ID"),
                lang    : localStorage.getItem("LANG"),
                
            }
        );
    },

    tabbarPage: function(page) {

        page.onShow     = function(){
            myApp.i18Next.changeLang();
        };

        // 좌측메뉴 열고 닫기 이벤트 
        page.querySelector('[component="button/menu"]').onclick = function() {
            myApp.splitter.left.toggle();
        };

        // 검색 아이콘 클릭
        Array.prototype.forEach.call(page.querySelectorAll('.btnSearch'), function(ele) {
            ele.onclick = function() {
                serchText   = page.querySelector("#btnSearchTop").value;
                myApp.navigator.pushPage('html/search_all.html', { data : { search : serchText },  animation : "none", "swipe-threshold" : 0 });
            };  
        });

        $(page).off('keydown');
        $(page).on('keydown', function (event) {
            if (event.keyCode === 13) {
                serchText   = page.querySelector("#btnSearchTop").value;
                myApp.navigator.pushPage('html/search_all.html', { data : { search : serchText },  animation : "none", "swipe-threshold" : 0 });
            }
        });

        // 주소 설정 페이지 이동
        Array.prototype.forEach.call(page.querySelectorAll('.goAddr'), function(ele) {
            ele.onclick = function() {

                localStorage.setItem("joinMemberData", null);
                localStorage.setItem("ADDR_ACTIVE_TAB", 0);
                localStorage.setItem("ADDR_POP_PAGE", 1);
                myApp.navigator.pushPage('html/config/config_addr_tabbar.html',  {  animation : "none" } );
            }; 
        });
    },

    ////////////////////////
    // Menu Page Controller //
    ////////////////////////
    menuPage: function(page) {
        
        var isLoginYn   = localStorage.getItem("IS_LOGIN_YN");
        var userName    = localStorage.getItem("USER_NM");
        var tel         = localStorage.getItem("TEL");
        tel             = isNull(tel) ? "" : tel;
        $(".myTel").html( tel );        

        isLoginYn       = isLoginYn !== "Y" ? "N" : "Y";
        nick            = localStorage.getItem("NICK_NM"); 
        p_img           = localStorage.getItem("PROFILE_IMG"); 

        if( isNull(p_img) ) {
            p_img           =  "img/user.jpg";
            $(page).find(".user-image").css("margin-top", "-10px");
        } 

        page.querySelector(".user-image").setAttribute("src", p_img);

        if( isLoginYn.indexOf("Y") >= 0) {

            userName        = isNull(nick) ?  userName : nick;

            page.querySelector(".userName").innerHTML = userName;
            page.querySelector(".userProfile").style.display = "block";
            page.querySelector(".linkLoginPage").style.display = "none";
            
        }else{

            localStorage.setItem("SNS_TYPE",     null ); 
            localStorage.setItem("PROFILE_IMG",  null ); 

            page.querySelector(".userName").innerHTML = "";
            page.querySelector(".linkLoginPage").style.display = "block";
            page.querySelector(".userProfile").style.display = "none";
        }

        // 전화번호 클릭 5번째 탭으로 이동
        Array.prototype.forEach.call(page.querySelectorAll('.userPhoneNumber'), function(element) {
            element.onclick = function() {
                document.getElementById('appTabbar').setActiveTab(5);
                myApp.splitter.left.toggle();
            };
            element.show && element.show(); // Fix ons-fab in Safari.
        });

         // 프로필 페이지 이동
         Array.prototype.forEach.call(page.querySelectorAll('.userProfile'), function(element) {
            element.onclick = function() {

                var t_goLogin   = myApp.i18Next.getTrasTxt("SYS", "GO_LOGIN");
                var isLoginYn   = localStorage.getItem("IS_LOGIN_YN");
                isLoginYn       = isLoginYn !== "Y" ? "N" : "Y";
                
                if( isLoginYn.indexOf("Y") >= 0) {
                    //page 중첩이 생기므로 아래와 같이 수정
                    document.getElementById('appTabbar').setActiveTab(5);
                    myApp.splitter.left.toggle();
                    
                }else{
                    runWarningMessage(t_goLogin, "", 1000, "");
                    document.getElementById('appTabbar').setActiveTab(5);
                }

            };
            element.show && element.show(); // Fix ons-fab in Safari.
        });

        // 로그인 페이지 이동
        Array.prototype.forEach.call(page.querySelectorAll('.linkLoginPage'), function(element) {
            element.onclick = function() {
				//page 중첩이 생기므로 아래와 같이 수정
				document.getElementById('appTabbar').setActiveTab(5);
				//myApp.navigator.setActiveTab(4);
                myApp.splitter.left.toggle();
            };
            element.show && element.show(); // Fix ons-fab in Safari.
        });

        // 회원가입 페이지 이동
        Array.prototype.forEach.call(page.querySelectorAll('.btnJoin'), function(element) {
            element.onclick = function() {
                myApp.navigator.pushPage('html/auth/join.html', { animation : "none"});
                myApp.splitter.left.toggle();
            };
            element.show && element.show(); // Fix ons-fab in Safari.
        });

        // 나의주소 페이지 이동
        Array.prototype.forEach.call(page.querySelectorAll('.goAddr'), function(element) {
            element.onclick = function() {
                myApp.navigator.pushPage('html/config/config_addr_tabbar.html', { animation : "none"} );
                //myApp.splitter.left.toggle();
            };
            element.show && element.show(); // Fix ons-fab in Safari.
        });

        // 최근주문주소 페이지 이동
        Array.prototype.forEach.call(page.querySelectorAll('.goRecent'), function(element) {
            element.onclick = function() {
                myApp.navigator.pushPage('html/config/recent_addr.html', { animation : "none"} );
                //myApp.splitter.left.toggle();
            };
            element.show && element.show(); // Fix ons-fab in Safari.
        });

        // 즐겨찾기 조회 페이지  이동
        Array.prototype.forEach.call(page.querySelectorAll('.goFavorite'), function(element) {
            element.onclick = function() {
                myApp.navigator.pushPage('html/product/favorite.html', { animation : "none"} );
                //myApp.splitter.left.toggle();
            };
            element.show && element.show(); // Fix ons-fab in Safari.
        });

        // 환경설정 페이지 이동
        Array.prototype.forEach.call(page.querySelectorAll('.goConfig'), function(element) {
            element.onclick = function() {
                myApp.navigator.pushPage('html/config/config.html', { animation : "none"} );
                //myApp.splitter.left.toggle();
            };
            element.show && element.show(); // Fix ons-fab in Safari.
        });

        // 공지사항 페이지 이동
        Array.prototype.forEach.call(page.querySelectorAll('.goNotice'), function(element) {
            element.onclick = function() {
                myApp.navigator.pushPage('html/notice/index.html', { animation : "none"} );
                //myApp.splitter.left.toggle();
            };
            element.show && element.show(); // Fix ons-fab in Safari.
        });
        
        // 전화번호부 조회 페이지  이동
        Array.prototype.forEach.call(page.querySelectorAll('.goTel'), function(element) {
            element.onclick = function() {
                myApp.navigator.pushPage('html/etc/tel.html', { animation : "none"} );
                //myApp.splitter.left.toggle();
            };
            element.show && element.show(); // Fix ons-fab in Safari.
        });

        // 수신메시지 이동
        Array.prototype.forEach.call(document.querySelectorAll('.goMessage'), function(ele) {
            ele.onclick = function() {
                myApp.navigator.pushPage('html/message/message.html', { animation : "none"} );
                //myApp.splitter.left.toggle();
            };    
        }); 

        // 회원가입 페이지 이동
        Array.prototype.forEach.call(document.querySelectorAll('.goJoin'), function(ele) {
            ele.onclick = function() {
                myApp.navigator.pushPage('html/auth/join.html', { animation : "none"} );
				//myApp.splitter.left.toggle();
            };    
        }); 

        // 한인소식 페이지 이동
        Array.prototype.forEach.call(document.querySelectorAll('.goNews'), function(ele) {
            ele.onclick = function() {
                myApp.navigator.pushPage('html/etc/news.html', { animation : "none"} );
				//myApp.splitter.left.toggle();
            };    
        }); 

        // 실시간 미세먼저 이동
        Array.prototype.forEach.call(document.querySelectorAll('.goAir'), function(ele) {
            ele.onclick = function() {
                myApp.navigator.pushPage('html/etc/airWeather.html', { animation : "none"} );
				//myApp.splitter.left.toggle();
            };    
        });

        // 입점문의
        Array.prototype.forEach.call(document.querySelectorAll('.goRequest'), function(ele) {
            ele.onclick = function() {
                myApp.navigator.pushPage('html/etc/inquiry.html', { animation : "none"} );
				//myApp.splitter.left.toggle();
            };    
        });
        // 개인정보 보호정책
        Array.prototype.forEach.call(document.querySelectorAll('.goPolicy'), function(ele) {
            ele.onclick = function() {
                myApp.navigator.pushPage('html/etc/policy.html', { animation : "none"} );
                //myApp.splitter.left.toggle();
            };
        });
    },

    homePage: function(page) {

        // 나의주소 페이지 이동
        Array.prototype.forEach.call(page.querySelectorAll('.goAddr'), function(element) {
            element.onclick = function() {
                myApp.navigator.pushPage('html/auth/user_location.html',  { animation: "none" , data: {} }); 
            };
        });

        // 최근주문내역 페이지 이동
        Array.prototype.forEach.call(page.querySelectorAll('.coinArea'), function(element) {
            element.onclick = function() {
                myApp.navigator.pushPage('html/auth/coin.html', { animation : "none"} );
            };
        });

        // 입점문의
        Array.prototype.forEach.call(document.querySelectorAll('.goRequest'), function(ele) {
            ele.onclick = function() {
                myApp.navigator.pushPage('html/etc/inquiry.html', { animation : "none"} );
				//myApp.splitter.left.toggle();
            };    
        });
        
        // document.querySelector("#modalAgreeTel").show(); // 테스트용
        // 앱업데이트 다이어로그
        var verDiag = document.querySelector("#version-dialog");
        var btndiagVersionCancel    = document.querySelector(".diagVersionCancel");
        var btndiagVersionOk        = document.querySelector(".diagVersionOk");

        chkTelNum();

        btndiagVersionCancel.onclick = function(){
            verDiag.hide({ animation : "none"});
        };

        btndiagVersionOk.onclick = function(){
            window.open(APP_SHARE_URL, "_blank");
            verDiag.hide({ animation : "none"});
        };

        var pullHook = document.getElementById('pull-hook-home');

        pullHook.addEventListener('changestate', function(event) {
            var message = '';

            switch (event.state) {
                case 'initial':
                    message = 'Pull to refresh';
                    break;
                case 'preaction':
                    message = 'Release';
                    break;
                case 'action':
                    message = "<img src='img/button/ajax-loader.gif' />";
                    break;
            }
            pullHook.innerHTML = message;
        });
        
        pullHook.onAction = function(done) {
            setTimeout(function(){
                myApp.mainController.getCompanyList(page);
                done();
            }, 1000);
        };
        
        this.getCompanyList(page);

        var modalAdPop  = document.querySelector('#adPopup');
        var modalFcm    = document.querySelector('#modalFcm');

        // fcm 메시지용 모달 show Test 용
        // modalFcm.show();
        modalAdPop.hide( { animation : "none"} );

        // fcm 메시지용 모달 확인창 닫기
        $(document).on("click", ".btnCloseFcm", function(){
            modalFcm.hide( { animation : "none"} );
        });

        // fcm 메시지용 모달 메시지목록이동
        $(document).on("click", ".btngoFcmList", function(){
            myApp.navigator.pushPage('html/message/message.html', { animation : "none"} );
            modalFcm.hide( { animation : "none"} );
        });

        // 장바구니 아이콘 뱃지수 최신갱신
        myApp.commonController.common();

        page.onShow = function() {

            if( IS_CHANGE_AREA ){
                myApp.mainController.getCompanyList(page);
                IS_CHANGE_AREA = false;
            }

            var reloadYN = localStorage.getItem("MAIN_RELOAD");
            
            if( !isNull(reloadYN) ){
                
                if(reloadYN == "Y"){
                    localStorage.setItem("MAIN_RELOAD", null);
                    location.reload();
                }
            }         

            var t_none_area = myApp.i18Next.getTrasTxt("SYS", "NO_AREA");
            var myAddr 		= localStorage.getItem("ADDR2");
            var area_nm		= localStorage.getItem("AREA_NM");
            myAddr          = isNull(myAddr) ? ""  : myAddr;
            area_nm         = isNull(area_nm) ? t_none_area : area_nm;

            if( myAddr == ""){
                myAddr = myApp.i18Next.getTrasTxt("LOCATION","REG_ADDR");
                
            }
            
            $(page).find(".myloc").html(myAddr);
            $(page).find(".area_nm").html(area_nm);
        };

        var service1	= new withWSDL("api/board/list");
        // 배너정보 바인딩
        service1.success = function(_jsonData){
            var jsonData    = _jsonData;

            if (typeof serviceModel.listBanner === 'undefined') {
                $.getScript("js/service/notice.js", function(){
                    serviceModel.listBanner(page, jsonData, service1.DOMAIN);

                    // 오늘 하루만 보기 설정 해제인 경우만 팝업 보기
                    if(getCookie("DsafeNoOpen1") !="1"){
                        serviceModel.listPopup(page, jsonData, service1.DOMAIN);
                    }else{
                        modalAdPop.hide( { animation : "none"} );
                    }
                });
            }else{
                serviceModel.listBanner(page, jsonData, service1.DOMAIN);

                // 오늘 하루만 보기 설정 해제인 경우만 팝업 보기
                if(getCookie("DsafeNoOpen1") !="1"){
                    serviceModel.listPopup(page, jsonData, service1.DOMAIN);
                }else{
                    modalAdPop.hide( { animation : "none"} );
                }
            }
        };

        // 배너정보 가져오기
		service1.ajaxGetData(
            {
                start : 0 ,
				orderby : "tb_board.IDX" ,
                order : "DESC",
                or_is_banner_yn : "Y",
                or_is_m_popup_yn : "Y",
                areaid  : localStorage.getItem("AREA_ID")
            }
        );
    
        
        document.addEventListener("deviceready", onDeviceReadyAll, false);

        function onDeviceReadyAll() {


            //alert( typeof handleOpenURL === 'function');
            
//            var sApp = startApp.set("kakaolink://");
//
//            sApp.getExtra(function(fields) { /* success */
//                alert("OK");
//                console.log(fields)
//            }, function(error) { /* fail */
//                alert(error);
//            });
            
//            sApp.check(function(values) { /* success */
//                alert(values);
//                console.log(values);
//            }, function(error) { /* fail */
//                alert(error);
//            });


            // MainInterface
            function myHandler(indent){

                alert("indent reveived");
                var data    = indent.data;

                
                if( !isNull(data) ){
                    var corpid  = getParam(data, "corpid");
                    var clsid   = getParam(data, "clsid");
                    var pid     = getParam(data, "pid");
                    var gid     = getParam(data, "gid");
                
                    REFFROR_CORP_ID = corpid;
                    REFFROR_CLS_ID  = clsid;
                    REFFROR_P_ID    = pid;
                    REFFROR_G_ID    = gid;

                }else{
                    REFFROR_CORP_ID = null;
                    REFFROR_CLS_ID  = null;
                    REFFROR_P_ID    = null;
                    REFFROR_G_ID    = null;
                }
            }


            $(page).find(".bt_banner").click(function(e){

                navigator.share({
                      text    : "WITHU-위드유",
                      url     : APP_SHARE_URL,
                      title   : "WITHU-위드유 ",
                      desc    : "WITHU 앱주문배달 집에서 장보자",
                      link    : APP_SHARE_URL,
                      imageURL: "https://lh3.googleusercontent.com/rWx91CCWXA3hLapic-_WtG55XFafoxs7g1wU-5a5lhkMvvbXv1AA7lQZfIHAQ7BbHlo=w1520-h752-rw"

                }).then(() => {
                    console.log("Data was shared successfully");
                }).catch((err) => {
                    console.error("Share failed:", err.message);
                    runWarningMessage(err.message, "danger", 1500, "up");
                });
            });;

            ons.disableDeviceBackButtonHandler();

            // pause 모드일때 유튜버 재생은 모두 정지되야함
            document.addEventListener("pause", function(){
                
                // When Change slide then  stop youtube  : 중요
                $('.yt_player_iframe').each(function(){
                    this.contentWindow.postMessage('{"event":"command","func":"stopVideo","args":""}', '*');
                });

            }, false);
            
            window.document.addEventListener('backbutton', function(e) {

                // 시스템 서비스 모달인 경우 종료
                if( $("ons-modal#sysModal").is(":visible")){
                    navigator.app.exitApp();
                    return;
                }

                // 열린 좌측메뉴 닫기
                if( myApp.splitter.left.isOpen ){
                    myApp.splitter.left.close({ animation : "none"});
                    return;
                }

                // 열려있는 모달 닫기
                if( $("ons-modal").is(":visible")){
                    $("ons-modal").hide( { animation : "none"} );
                    return;
                }

                // 열린 다이어로그 닫기
                if( $("ons-dialog").is(":visible")){
                    $("ons-dialog").hide( { animation : "none"} );
                    return;
                }

                // 열린 alert(prompt) 다이어로그 닫기
                // if( $("ons-alert-dialog").is(":visible")){
                //     $("ons-alert-dialog").hide( { animation : "none"} );
                //     return;
                // }

                if ( document.querySelector('#myNavigator').pages.length > 1) {
                    document.querySelector('#myNavigator').popPage( { animation : "none" } );
                    return ;
                }

                // 강제로 메인으로 가게 하기
                if( document.getElementById('appTabbar').getActiveTabIndex() != 0 ){
                    document.getElementById('appTabbar').setActiveTab(0);
                    return ;
                }else {
                    e.preventDefault();
                    e.stopPropagation();

                    if( $("ons-alert-dialog").length == 0){
                        // 2번 눌러서 앱종료하기
                        if(new Date().getTime() - lastTimeBackPress < timePeriodToExit){
                            navigator.app.exitApp();
                        }else{
                            var t_existApp  = myApp.i18Next.getTrasTxt("SYS", "APP_EXIT_MSG");
                            runWarningMessage(t_existApp, "none", 2000, "bottom");
                            lastTimeBackPress = new Date().getTime();
                        }
                    }
                }
            }, false);


            // 버전체크
            let appVirsion  = AppVersion.build;
            localStorage.setItem("APP_VERSION", appVirsion);

            var serviceVersion	= new withWSDL("api/version/list");
            serviceVersion.success = function(_jsonData){

                if (_jsonData.list != null){
                    var lastVer = _jsonData.list.version;
                    var isforce = _jsonData.list.is_force;

                    if( lastVer != appVirsion){
                        // 업데이트 강제여부 확인 => 취소버튼 숨기기
                        if( isforce == "Y"){
                            $(btndiagVersionCancel).hide();
                        }
                        verDiag.show();
                    }
                }else{
                    // console.log("버전 체크 오류 혹은 데이터가 없습니다");
                }
            };

            serviceVersion.ajaxGetData({ platform: "iOS" });

            cordova.plugins.firebase.messaging.requestPermission().then(function(){
                 console.log("Pussh messaing allowd");
            });

            cordova.plugins.firebase.messaging.requestPermission({forceShow: true}).then(function() {
                console.log("You'll get foreground notifications when a push message arrives");
            });
            
            // 앱이 실행중이지 않을때 fcm 수신 알림 선택후 앱이 실행되면 이동
            cordova.plugins.firebase.messaging.onBackgroundMessage(function(payload) {
                myApp.navigator.pushPage('html/message/message.html');
            });
            
            // 앱이 실행중일때 FCM 메시지가 수신된 경우 팝업으로 메시지 표시
            cordova.plugins.firebase.messaging.onMessage(function(payload) {

                 $(".fcmContentPopTel ons-list-header").html("새메시지가 도착했습니다");
                 $(".fcmContentPopTel .fcmBody").html("빈메시지 입니다");
                
                 message = payload;
                 aps     = message.aps.alert;
                 
                 title   = isNull(aps.title) ? "" : aps.title ;
                 body    = isNull(aps.body) ? "" : aps.body;
                 
                 $(".fcmContentPopTel ons-list-header").html(title);
                 $(".fcmContentPopTel .fcmBody").html(body);
                 modalFcm.show();
             });
            
            window.plugins.sim.getSimInfo(successPhoneInfoCallback, fnErrorCl);
         }

        // Set button functionality to open/close the menu.
        $(page).find('[component="button/menu"]').onclick = function() {
            document.querySelector('#mySplitter').left.toggle();
        };

    }
}
