// 기본 앱 회사아이디
const APP_URL 		= "https://withuvn.net/";
const APP_CORP_ID   = "WITH_00";
const APP_BRANCH_ID = "BRANCH_01";
const APP_SHARE_URL	= "https://apps.apple.com/app/id1479637078";

const KAKAO_SDK_KEY = "2fd2ca282d527e2813ad6c9078c65ff2";


// reffer 변수 : 회사아이디
let REFFROR_CORP_ID		= null;

// reffer 변수 : 상품분류 아이디
let REFFROR_CLS_ID		= null;

// reffer 변수 : 상품 아이디
let REFFROR_P_ID 		= null;

// reffer 변수 : 공구 아이디
let REFFROR_G_ID 		= null;

let IS_PAY_CASH 		= "Y";
let IS_PAY_CARD 		= "Y";
let IS_PAY_EPAY 		= "N";
let IS_PAY_POINT 		= "Y";
let IS_USE_TAX 			= "Y";

// 최소주문금액 (20만동) : 시스템설정에 따른 변경
let MIN_ORDER_AMT       = 200000;
let MIN_ORDER_WORD 		= parseInt(MIN_ORDER_AMT/10000);

// 기본배달금액
let BASE_DELV_FEE 		= 0;

// 기본배달거리
let BASE_DISTANCE 		= 0;

// 1KM당 추가 요금
let ADDED_FEE			= 0;

// 추가 기준 거리(m)
let ADDED_DISTANCE 		= 0;

// 무료배송금액 		
let FREE_DELV_AMT		= 0;

// 계산된 거리
let CALC_DIST 			= 0;

// 추가요금 적용
let CALC_ADDED_FEE		= 0;

var lastTimeBackPress   = 0;
var timePeriodToExit    = 2000;
var IS_SHOW_ORDER_ALARM	= true;
let IS_ADULT_AGREE		= false;	// 성인 동의 여부
let WITHU_PAY			= 0;		// 위드유페이
let APP_SYS_ON_OFF 		= true;		// 시스템 사용여부
let IS_USE_WITHU_PAY	= true;
let WITHU_PAY_RATE 		= 0;

let LOGIN_CLB 			= null;		// 로그인 후 콜백 함수

let IS_CHANGE_AREA 		= false;	// 지역변경 여부 변수

// 베트남 시간대 설정
moment.tz.setDefault('Asia/Ho_Chi_Minh');

myApp.navigator = document.querySelector('#myNavigator');
myApp.splitter  = document.querySelector('#mySplitter');

if (navigator.onLine == false){
	var message = myApp.i18Next.getTrasTxt("SYS", "NO_CONN"); 
	ons.notification.toast({message: message , timeout: 1000});
}

myApp.i18Next.init();

document.addEventListener("deviceready", onDeviceReadyAppRoot, false);

document.addEventListener('init', function(event) {
	
	var page = event.target;

	if (myApp.controllers.hasOwnProperty(page.id)) {
		myApp.controllers[page.id](page);
	}else if(myApp.authControllers.hasOwnProperty(page.id)) {
		myApp.authControllers[page.id](page);
	}else if(myApp.googleMapControllers.hasOwnProperty(page.id)){
		myApp.googleMapControllers[page.id](page);
	}else if(myApp.configControllers.hasOwnProperty(page.id)){
		myApp.configControllers[page.id](page);
	}else if(myApp.mainController.hasOwnProperty(page.id)) {
		myApp.mainController[page.id](page);
	}else if(myApp.etcController.hasOwnProperty(page.id)) {
		myApp.etcController[page.id](page);
	}else if(myApp.orderController.hasOwnProperty(page.id) ){
		myApp.orderController[page.id](page);
    }else if(myApp.grpControllers.hasOwnProperty(page.id) ){
		myApp.grpControllers[page.id](page);
	}
	// 언어셋 : ko-Kr 지원
    
});

document.addEventListener('show', function(event) {
	stopVideo();
});


// 장바구니내역을 localStorage와 동기화 => DB 장바구니와 동기화도 필요
if( window.baskets === null || window.baskets.length == 0 || typeof window.baskets === undefined){
	//localStorage.setItem("basket", null);
	window.baskets = [];
}

function onDeviceReadyAppRoot() {
    // 스플래시 이미지 종료
    isSplashShown = localStorage.getItem("IS_SPLASH_SHOW");
	if( isNull(isSplashShown) || isSplashShown != false ){
		setTimeout(function () {
			navigator.splashscreen.hide();
			isSplashShown = false;
			localStorage.setItem("IS_SPLASH_SHOW", false);
		}, 400);
	}
    
    Keyboard.shrinkView(true);

    window.addEventListener('keyboardDidHide', function () {
       // Describe your logic which will be run each time keyboard is closed.
    });

    window.addEventListener('keyboardDidShow', function () {
       document.activeElement.scrollIntoView();
    });
}
