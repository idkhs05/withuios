/***********************************************************************
 * App Controllers. These controllers will be called on page initialization. *
 ***********************************************************************/

myApp.etcController = {

    policyPage : function(page){

        myApp.i18Next.changeLang();
        var lang    = getLangNow();
        switch(lang){
            case "ko-KR" : $(".policyData").load("html/etc/policy_ko.html"); break;
            case "vn-VN" : $(".policyData").load("html/etc/policy_vn.html"); break;
            case "en-EN" : $(".policyData").load("html/etc/policy_en.html"); break;
        }
    },

	// 입점 및 제휴문의 페이지
	inQuiryPage : function(page){

		myApp.i18Next.changeLang();

		var isValid		= false;
		var objBtnReq	= page.querySelector(".btnInquiry");
		var eventList   = ["keydown", "keyup", "focusout", "focusin"];
        
        var name        = localStorage.getItem("USER_NM");
        var tel         = localStorage.getItem("TEL");
        
        name            = isNull(name)  ? "" : name;
        tel             = isNull(tel)   ? "" : tel;

        $(page).find("#name input").val(name);
        $(page).find("#name").val(name);
        $(page).find("#tel input").val(tel);
        $(page).find("#tel").val(tel);
		
		// 클릭 : 개인정보 공개 동의 버튼
		page.querySelector("#agree_privacy").onclick = function(event){

			if( $(event.target).is(":checked") ){
				if(fnValidationInput(page.id)){
					isValid = true;
				}else{
					isValid = false;
				}
			}else{
				isValid = true;
			}
		};

		function checkCheckBox(){
			if( page.querySelector("#agree_privacy").checked ){
				return true;
			}else{
                var msg	= myApp.i18Next.getTrasTxt("INQUERY", "MSG_AGREE");
                runWarningMessage(msg, "warning", 1500, "up");
				return false;
			}
		};	

		function checkElement(){
			
			Array.prototype.forEach.call(page.querySelectorAll(".check"), function(ele){

				//ele.addEventListener(event, function() { 
					if(fnValidationInput(page.id)){
						isValid = true;
					}else{
						isValid = false;
					}
				//});
			});	
		};

		// 클릭 : 지점 입점/제휴 내역 저장 
		objBtnReq.onclick = function(){

			for(event of eventList) {
				checkElement();
			}
			
			if( isValid && checkCheckBox()){
                
                $objTextArea    = $(page).find("textarea");
                var tlen        = $objTextArea.val();

                if( tlen.length < 10){
                    var msg	= myApp.i18Next.getTrasTxt("INQUERY", "MSG_MIN_10");
                    runWarningMessage(msg, "warning", 1500, "up");
                    return;
                }
                
				var data = {
                    ID		            : localStorage.getItem("ID"),
                    TEL		            : localStorage.getItem("TEL"),
                    KAKAO_ID            : $("#kakao_id input").val(),
                    REG_CORP_NAME       : null,
                    REG_CORP_USER_NAME  : $("#name input").val(),
                    REQ_CLASS		    : "INQUERY",
                    REQ_STATUS          : 'NEW',
                    RMK                 : null,
				};

                var arrObjDetail = new Array();
                
				Array.prototype.forEach.call(page.querySelectorAll(".check"), function(ele){
					
					var _val = $(ele).val();

					if( $(ele).hasClass("radio")){
						_val = $(ele).find(":checked").val() ;
					}
					
					if( $(ele).hasClass("selectBox")){
						_val = $(ele).find("option:selected").val() ;
					}	

					var obj =  {
						ITEM_NM		: $(ele).data("label"),
						ITEM_VALUE	: _val,
						ITEM_CLASS	: $(ele).data("class"),
						ITEM_LENGTH	: $(ele).data("length")
					};
					
					arrObjDetail.push(obj);

				});

				var service	= new withWSDL("api/request/list");
				service.success = function(_jsonData){
					
					jsonData = _jsonData;
					
					if( jsonData.status == "success"){
                        runWarningMessage(jsonData.message, "success", 1500, "up");
                        myApp.navigator.popPage();
						
					}else{
                        runWarningMessage(jsonData.message, "danger", 1500, "up");
					}
				};

				service.ajaxPostData({
					'REQ_INFO'      : data,
					'REQ_DETAIL'    : arrObjDetail
				});
			}
		};		
	},
	
	// 상품 문의 및 요청사항 페이지
	requestPage : function(page){
        
        $(page).find(".btnExRequest").prop("disabled", true);

		var objBtnReq	= page.querySelector(".btnExRequest");
        var eventList   = ["keydown", "keyup", "focusout"];
        var email       = localStorage.getItem("EMAIL");

        email           = isNull(email) ? "" : email;
        page.querySelector("#reqEmail").value = email;

        var objEmail    = page.querySelector("#reqEmail");
        var objText     = page.querySelector("#reqContent");


        for(event of eventList) {
            objEmail.addEventListener(event, function() { 
                if(fnValidationInput("requestPage")){
                    $(objBtnReq).prop("disabled", false)
                }else{
                    $(objBtnReq).prop("disabled", true)
                }
            });

            objText.addEventListener(event, function() { 
                if(fnValidationInput("requestPage")){
                    $(objBtnReq).prop("disabled", false)
                }else{
                    $(objBtnReq).prop("disabled", true)
                }
            });
        }
        
        // 클릭 : 지점 입점/제휴 내역 저장 
		objBtnReq.onclick = function(){
            

            var requestAt   = localStorage.getItem("REQUEST_AT");

            // 과요청 방지를 위해 하루에 1번만 등록할 수 있도록 설정
            if( !isNull(requestAt)){

                var now         = moment();
                var duration    = moment.duration(now.diff(requestAt));
                var days        = duration.asDays();

                // console.log(days);
                if(days < 1){
                    runWarningMessage("요청 및 문의는 하루한번만 등록 가능합니다", "info", 1500, "up");
                    return false;
                }
            }
            
			var data = {
                ID		            : localStorage.getItem("ID"),
                TEL		            : localStorage.getItem("TEL"),
                REG_CORP_NAME       : null,
                REG_CORP_USER_NAME  : null,
                REQ_CLASS		    : "REQUEST",
                REQ_STATUS          : 'NEW',
                RMK                 : null,
			};

			var arrObjDetail = new Array();
			Array.prototype.forEach.call(page.querySelectorAll(".check"), function(ele){
				
				var _val = $(ele).val();

				var obj =  {
					ITEM_NM		: $(ele).data("label"),
					ITEM_VALUE	: _val,
					ITEM_CLASS	: $(ele).data("class"),
					ITEM_LENGTH	: $(ele).data("length")
				};
				
				arrObjDetail.push(obj);

			});

			var service	= new withWSDL("api/request/list");
			service.success = function(_jsonData){
                
                jsonData = _jsonData;
                
                if( jsonData.status == "success"){
                    runWarningMessage(jsonData.message, "success", 1500, "up");
                    localStorage.setItem("REQUEST_AT", moment().format());
                    myApp.navigator.popPage();

                }else{
                    runWarningMessage(jsonData.message, "danger", 1500, "up");
                }
            };

            // console.log({'REQ_INFO'      : data,
            // 'REQ_DETAIL'    : arrObjDetail});
            
            service.ajaxPostData({
                'REQ_INFO'      : data,
                'REQ_DETAIL'    : arrObjDetail
            });
		}
	},
    

	// 전화번호 카테고리 조회
    telPage : function(page){

        myApp.i18Next.changeLang();
        
        var option      = {
            start           : 0 ,
            orderby         : "tb_tel_class.TEL_CLASS_IDX" ,
            order           : "ASC",
            tel_branch_nm   : "", 
            is_search       : false
        };

        var search = page.querySelector("#searchItem");
        $(search).find("input").focus();

        var service 	= new withWSDL("api/telclass/list");

        // 전화번호분류 목록정보 바인딩
        service.success = function(_jsonData){
            var jsonData    = _jsonData;
            if (typeof serviceModel.listTelClass === 'undefined') {
                $.getScript("js/service/tel.js", function(){
                    serviceModel.listTelClass(page, jsonData, option);
                });    
            }else{
                serviceModel.listTelClass(page, jsonData, option);
            }
        };

        // 전화번호분류 목록 호출
		service.ajaxGetData(option);
       
        // 스크롤다운
        page.onInfiniteScroll = function(done) {
            
            if( page.querySelector(".emptyOrNoData.card") === null ){           
                setTimeout(function() {      
                    option.start += 10;
                     // 공지사항 목록 호출
                    service.ajaxGetData(option);
                    done();
                }, 300)
            }
        };

        page.querySelector(".btnSearchCc").onclick = function(){    
            $(search).val("");
            $(search).find("input").focus();

            option.start = 0;
            option.tel_branch_nm = "";
            option.is_search     = true;    
            service.ajaxGetData(option);
        }
        
        // 검색내역 목록 조회
        showListCookie(page, "telClasssearchItem", function(){
            option.tel_branch_nm   = search.value ;
            option.is_search       = true;
            service.ajaxGetData(option);
        });

        // 검색어 입력시 검색
        search.onkeyup  = function(){
            
            searchCookie(page, $(search).find("input").val() , "telClasssearchItem", function(searchItem){
                    // console.log(searchItem);
                    // 전화번호목록군 정보 가져오기
                    option.tel_branch_nm   = searchItem;
                    if( searchItem.length == 0){
                        option.is_search       = false;    
                    }else{
                        option.is_search       = true;
                    }
                    
                    console.log(option);
                    service.ajaxGetData(option);
                } 
            );
        };

        // 전화번호목록 정보 가져오기
        service.ajaxGetData(option);
    },

	// 전화번호 목록 페이지(상세목록)
    telListPage : function(page){
        
        myApp.i18Next.changeLang();

        var idx  = page.pushedOptions.data.idx;
        var title  = page.pushedOptions.data.title;

        var option      = {
            start           : 0 ,
            orderby         : "tb_tel.TEL_IDX" ,
            order           : "ASC",
            agent_nm        : "", 
            is_search       : false,
            cls_idx         : idx,
        };
        page.querySelector("ons-toolbar .center").innerHTML = title;

        var search = page.querySelector("#searchItem");
        $(search).find("input").focus();

        var service	= new withWSDL("api/tel/list");
        // 전화목록 서비스 호출
        service.success = function(_jsonData){
            var jsonData    = _jsonData;
            
            if (typeof serviceModel.listTelList === 'undefined') {
                $.getScript("js/service/tel.js", function(){
                    serviceModel.listTelList(page, jsonData, option);
                });    
            }else{
                serviceModel.listTelList(page, jsonData, option); 
            }
        };

        // 검색어 입력시 검색
        search.onkeyup  = function(){
            
            searchCookie(page, $(search).find("input").val() , "telsearchItem", function(searchItem){
                    // console.log(searchItem);
                    // 전화번호목록군 정보 가져오기
                    option.agent_nm   = searchItem;
                    if( searchItem.length == 0){
                        option.is_search       = false;    
                    }else{
                        option.is_search       = true;
                    }
                    console.log(option);
                    service.ajaxGetData(option);
                } 
            );
        };

        // 전화번호목록 정보 가져오기
        service.ajaxGetData(option);
    },

    // 이벤트 목록 조회
    eventListPage : function(page){
        myApp.i18Next.changeLang();

        var option      = {
            start       : 0 ,
            orderby     : "tb_board.IDX" ,
            order       : "DESC",
            brd_type    : "event"
        };

        var service 	= new withWSDL("api/board/list");

        // 이벤트 목록 서비스 호출
        service.success = function(_jsonData){
            var jsonData    = _jsonData;
            if (typeof serviceModel.listEvent === 'undefined') {
                $.getScript("js/service/notice.js", function(){
                    serviceModel.listEvent(page, jsonData, service.DOMAIN);
                });    
            }else{
                serviceModel.listEvent(page, jsonData, service.DOMAIN); 
            }
        };
       
        // 이벤트 목록 호출
		service.ajaxGetData(option);
       
        // 스크롤다운
        page.onInfiniteScroll = function(done) {
            
            if( page.querySelector(".emptyOrNoData.card") === null ){           
                setTimeout(function() {      
                    option.start += 10;
                     // 이벤트 목록 호출
                    service.ajaxGetData(option);
                    done();
                }, 300)
            }
        };
    },
	
	// 공지사항 목록 페이지
    noticeListPage : function(page){

        myApp.i18Next.changeLang();

        var option      = {
            start       : 0 ,
            orderby     : "tb_board.IDX" ,
            order       : "DESC",
            brd_type    : "notice"
        };

        var service 	= new withWSDL("api/board/list");

        // 공지사항 목록정보 바인딩
        service.success = function(_jsonData){
            var jsonData    = _jsonData;
            if (typeof serviceModel.listNotice === 'undefined') {
                $.getScript("js/service/notice.js", function(){
                    serviceModel.listNotice(page, jsonData, service.DOMAIN);
                });    
            }else{
                serviceModel.listNotice(page, jsonData, service.DOMAIN);
            }
        };

        // 공지사항 목록 호출
		service.ajaxGetData(option);
       
        // 스크롤다운
        page.onInfiniteScroll = function(done) {
            
            if( page.querySelector(".emptyOrNoData.card") === null ){           
                setTimeout(function() {      
                    option.start += 10;
                     // 공지사항 목록 호출
                    service.ajaxGetData(option);
                    done();
                }, 300)
            }
        };
    },

	// 이벤트/공지 상세화면 조회
    detailEventPage : function(page){
        
        var objTitle    = page.querySelector(".hTitle");
        var pData       = page.pushedOptions.data;        
        var option      = {
            brd_type    : pData.DIV
        };
        
        // 공지 및 이벤트 상세조회 정보 바인딩
        var service 	= new withWSDL("api/board/list/" + pData.IDX);
        service.success = function(_jsonData){
            var jsonData    = _jsonData;
            
            if (typeof serviceModel.detailNotice === 'undefined') {
                $.getScript("js/service/notice.js", function(){
                    serviceModel.detailNotice(page, jsonData, service.DOMAIN);
                });    
            }else{
                serviceModel.detailNotice(page, jsonData, service.DOMAIN); 
            }
        };

        // 이벤트/공지사항 상세 호출
		service.ajaxGetData(option);
    },
	
	// 한인 페이지
    newsPage : function(page){

        myApp.i18Next.changeLang();

        var option      = {
            start       : 0 ,
            orderby     : "tb_board.IDX",
            order       : "DESC",
            brd_type    : "news_kor"
        };

        var service 	= new withWSDL("api/board/list");

        // 한인소식 목록 서비스 호출
        service.success = function(_jsonData){
            var jsonData    = _jsonData;
            if (typeof serviceModel.listNews === 'undefined') {
                $.getScript("js/service/notice.js", function(){
                    serviceModel.listNews(page, jsonData, service.DOMAIN);
                });    
            }else{
                serviceModel.listNews(page, jsonData, service.DOMAIN); 
            }
        };
       
        // 한인소식 목록 호출
		service.ajaxGetData(option);
       
        // 스크롤다운
        page.onInfiniteScroll = function(done) {
            
            if( page.querySelector(".emptyOrNoData.card") === null ){           
                setTimeout(function() {      
                    option.start += 10;
                     // 한인소식 목록 호출
                    service.ajaxGetData(option);
                    done();
                }, 300)
            }
        };
    },

    // 공지사항 목록 페이지
    noticeListPage : function(page){

        myApp.i18Next.changeLang();

        var option      = {
            start       : 0 ,
            orderby     : "tb_board.IDX" ,
            order       : "DESC",
            brd_type    : "notice"
        };

        var service 	= new withWSDL("api/board/list");

        // 공지사항 목록정보 바인딩
        service.success = function(_jsonData){
            var jsonData    = _jsonData;
            if (typeof serviceModel.listNotice === 'undefined') {
                $.getScript("js/service/notice.js", function(){
                    serviceModel.listNotice(page, jsonData, service.DOMAIN);
                });    
            }else{
                serviceModel.listNotice(page, jsonData, service.DOMAIN);
            }
        };

        // 공지사항 목록 호출
		service.ajaxGetData(option);
       
        // 스크롤다운
        page.onInfiniteScroll = function(done) {
            
            if( page.querySelector(".emptyOrNoData.card") === null ){           
                setTimeout(function() {      
                    option.start += 10;
                     // 공지사항 목록 호출
                    service.ajaxGetData(option);
                    done();
                }, 300)
            }
        };
    },

	// 메시지 조회
    messageListPage : function(page){
        
        var tel     = localStorage.getItem("TEL");
        
        if( isNull(tel)){
            var noneMessage = "<div class='emptyOrNoData card'><i><img src='img/svg/caution.svg'></i><span data-i18n='COMM:noMoreData'> 더이상 수신된 메시지가 없습니다. <span></div>";
            $(page).find(".after-list").hide();
            $(page).find(".dataList").append(noneMessage);
            return false;
        }
        
        myApp.i18Next.changeLang();

        var option      = {
            start       : 0 ,
            msg_type    : "FCM",
            orderby     : "tb_message.id",
            order       : "DESC",
            phoneNumber : localStorage.getItem("TEL"),
        };
        
        // 푸시메시지 정보 바인딩
        var service 	= new withWSDL("api/message/list");
        service.success = function(_jsonData){
            var jsonData    = _jsonData;
            
            if (typeof serviceModel.listMessage === 'undefined') {
                $.getScript("js/service/message.js", function(){
                    serviceModel.listMessage(page, jsonData, service.DOMAIN, option);
                });    
            }else{
                serviceModel.listMessage(page, jsonData, service.DOMAIN, option); 
            }
        };

        // 메시지 정보 호출
        service.ajaxGetData(option);
        
        //  상품 카테고리 이벤트 등록
        page.onInfiniteScroll = function(done) {

            if ( $(page).find(".emptyOrNoData.card").length == 0  ) {
                setTimeout(function() {
                    option.start += 15;
                    option.clear = "0";
                    option.orderby = "tb_message.id";
                    service.ajaxGetData(option);
                    done();
                }, 200)
            }
        };
    },

    airWeatherPage : function(page){
        myApp.i18Next.changeLang();
    },
};