myApp.orderController = {

    grporderlistPage : function(page){
         
        var dialog		    = page.querySelector('#reOrder-dialog');
        var dialogCancel    = page.querySelector('#reFund-dialog');

        // 클릭 : 다이어로그 재구매 확인
        $(dialog).find('.btnDialogCancelOrder').click(function(){
            dialog.hide();
        });

        // 공구주문 사용
        $(page).find(".grp").click(function(){
            $(page).find(".common").removeClass("checked");
            $(page).find(".grp").addClass("checked");
            $(page).find(".commonList").hide();
            $(page).find(".grpList").show();
        });

        var oOption      = {
            "corp_id"       : "WITH_00",
            "user_id"       : localStorage.getItem("USER_ID"),
            "tel"           : localStorage.getItem("TEL"),
            "start"         : 0,
            "clear"         : 1
        };

		page.onShow = function() {

            if( !showChkTel_Diag() ){
                return false;
            }

            // 공구주문 조회 추가 : 19-11-06
            $(page).off("click", ".grpList .after-list");

            oOption.start = 0;
            oOption.clear = 1;

            var gService	= new withWSDL("api/gorder/list");
            
			// 주문목록 바인딩 호출
			gService.success = function(_jsonData){

				var jsonData    = _jsonData;
				if (typeof serviceModel.listGrpOrder === 'undefined') {
					$.getScript("js/service/gorder.js", function(){
						serviceModel.listGrpOrder(page, jsonData, service.DOMAIN, oOption);
					});
				}else{
					serviceModel.listGrpOrder(page, jsonData, service.DOMAIN, oOption);
                }
            };

			// 주문목록 조회 서비스 호출
            gService.ajaxGetData(oOption);

            //  상품 카테고리 이벤트 등록
            page.onInfiniteScroll = function(done) {

                if( page.querySelector(".grpList .emptyOrNoData.card") === null ){
                    setTimeout(function() {
                        oOption.start    += 30;
                        oOption.clear    = "0" ;
                        // 이벤트 목록 호출
                        gService.ajaxGetData(oOption);
                        done();
                    }, 300)
                }
            };

            // 더보기 클릭
            $(page).on("click", ".grpList .after-list", function(){

                //console.log(sOption.start);
                oOption.start += 30;
                oOption.clear = '0';
                // 이벤트 목록 호출
                gService.ajaxGetData(sOption);
            });
        };

        // 클릭 : 공동구매 주문취소 버튼
        $(page).find(".grpList").on("click", ".btnUndoGrpOrder", function(){

            var orderId = $(this).data("orderid");
            //console.log(orderId);
            var objList = $(page).find(".grpList." + orderId );

            var option = {
                TEL         : localStorage.getItem("TEL"),
                GORDER_IDX   : orderId,
                ORDER_STATE : "ORD_CNCL_NEW",
            }
            if( !isNull(orderId ) ){

                var serviceCnclGrpOrder = new withWSDL("api/gorder/list/" + orderId);
                serviceCnclGrpOrder.success = function(_jsonData){
                    var jsonData    = _jsonData;

                    if( jsonData.status == "success"){
                        runWarningMessage(jsonData.message, "success", 1500, "up");
                        objList.find(".statusName").text("주문취소");
                        $(page).find(".card.bottom." + orderId + " .btnUndoGrpOrder").prop("disabled", true);
                        dialogCancel.hide();
                    }else{
                        runWarningMessage(jsonData.message, "danger", 1500, "up");
                    }
                };

                serviceCnclGrpOrder.ajaxPutData(option);
            }
        });

    },
    // 주문내역 조회 및 주문페이지 + 공구주문내역 조회 추가(19-11-06)
    ordrlistPage : function(page){

        // 상품 주문후 바로 해당 화면으로 이동되므로 show 이벤트로 최신목록 유지
        //var dialog = document.querySelector("#order-dialog");
        var dialog		    = document.querySelector('#reOrder-dialog');
        var dialogCancel	= document.querySelector('#cancelOrder-dialog');
		var dialogRemove    = document.querySelector("#removeOrder-dialog");

        // 주문취소 다이어로그 버튼
        

        var arrEmptyStock   = [];
        var arrSetStock     = [];

        // 클릭 : 다이어로그 재구매 확인
        $(dialog).find('.btnDialogCancelOrder').click(function(){
            dialog.hide();
        });
    
		page.onShow = function() {

            // chkTelNum();
            if( !showChkTel_Diag() ){
                return false;
            }
            //myApp.commonController.chkLogin();
            myApp.i18Next.changeLang();
            var sOption      = {
                //"corp_id"       : localStorage.getItem("CORP_ID"),
                "corp_id"       : "WITH_00",
                "user_id"       : localStorage.getItem("USER_ID"),
                "tel"           : localStorage.getItem("TEL"),
                "start"         : 0,
                "clear"         : 1
            };
            
            // 임시 알림
            if( IS_SHOW_ORDER_ALARM ){
                IS_SHOW_ORDER_ALARM = false;
            }

            $(page).off("click", ".commonList .after-list");

            // 검색 시작, 초기화 설정
            sOption.start = 0;
            sOption.clear = 1;

            $(page).find(".commonList .after-list").show();

			// 로그인 체크
			var service	= new withWSDL("api/order/list");
			// 주문목록 바인딩 호출
			service.success = function(_jsonData){

				var jsonData    = _jsonData;
				if (typeof serviceModel.listOrder === 'undefined') {
					$.getScript("js/service/order.js", function(){
						serviceModel.listOrder(page, jsonData, service.DOMAIN, sOption);
					});
				}else{
					serviceModel.listOrder(page, jsonData, service.DOMAIN, sOption);
                }
            };

			// 주문목록 조회 서비스 호출
            service.ajaxGetData(sOption);

            //  상품 카테고리 이벤트 등록
            page.onInfiniteScroll = function(done) {

                if( page.querySelector(".commonList .emptyOrNoData.card") === null ){
                    setTimeout(function() {
                        sOption.start    += 10;
                        sOption.clear    = "0" ;
                        // 이벤트 목록 호출
                        service.ajaxGetData(sOption);
                        done();
                    }, 300)
                }
            };

            // 더보기 클릭
            $(page).on("click", ".commonList .after-list", function(){
                sOption.start += 10;
                sOption.clear = '0';
                // 이벤트 목록 호출
                service.ajaxGetData(sOption);
            });
        }

            
        // 클릭 : 다이어로그 재구매 확인
        $(dialog).find('.btnDialogReOrder').click(function(){
            
            var serviceReOrder = new withWSDL("api/basket/list");
            serviceReOrder.success = function(_jsonData){
                var jsonData    = _jsonData;

                var t_copyed    = myApp.i18Next.getTrasTxt("ORD", "COPYED_CART");
                var t_failed    = myApp.i18Next.getTrasTxt("ORD", "COPYED_FAIL")
                if( jsonData.status == 200){
                    runWarningMessage(t_copyed, "success", 1500, "up");
                    document.getElementById('appTabbar').setActiveTab(3);
                    dialog.hide();
                }else{
                    runWarningMessage(t_failed, "danger", 1500, "up");
                }
                arrSetStock = [];
            };

            serviceReOrder.error = function(){
                arrSetStock = [];
            }
            serviceReOrder.ajaxPostData( { arrSetStock : arrSetStock });
        });

		// 클릭 : 주문내역 취소 다이어로그 닫기
        $(dialogCancel).find(".btnDialogReOrder").click(function(){
            dialogCancel.hide();
        })

		// 클릭 : 주문내역 삭제하기 ON Dialog
        $(dialogRemove).on("click", ".btnDialogRemove", function(){
             
            var orderId		= $(dialogRemove).attr("data-orderid");
            var objList		= $(page).find(".dataList." + orderId );
            
            var option      = {
                ID          : localStorage.getItem("ID"),
				ORDER_IDX   : orderId,
            };

            var serviceRemove = new withWSDL("api/order/destoryOrder");
            serviceRemove.success = function(_jsonData){

                var jsonData    = _jsonData;
                
                if( jsonData.status == "success"){
                    runWarningMessage(jsonData.message, "success", 1500, "up");
                    objList.remove();
                    $(page).find(".card.bottom." + orderId ).remove();
                    dialogRemove.hide();
                }else{
                    runWarningMessage(jsonData.message, "danger", 1500, "up");
                }
            };

            // 완료시 장바구니 정보 삭제
            // 실제 데이터 삭제는 아님 사용자모바일에서만 안보이도록 하기
            serviceRemove.ajaxGetData(option);

        });

        $(dialogRemove).on("click", ".btnDialogCancelRemove", function(){
            dialogRemove.hide();
        });

       
        // 클릭 : 재구매 버튼 => 장바구니에 병합
        $(page).find(".commonList").on("click", ".btnReOrder", function(){

            var orderId = $(this).data("orderid");

            // 초기화
            arrEmptyStock   = [];
            arrSetStock     = [];

            if( !isNull(orderId)){

                $(page).find(".dataList." + orderId + " .orderSummary li").each(function(i){

                    var stock   = $(this).data("stock");
                    var qty     = $(this).data("qty");
                    var p_cd    = $(this).data("product_cd");
                    var p_nm    = $(this).find(".itemName").text();
                    var s_corp  = $(this).data("sale_corp_id");
                    var unit    = $(this).data("unit");

                    // 사은품은 빼야함. 가격이 0 이상인 것만 재구매 되야함
                    if( unit > 0 ){
                        var obj = {
                            'ID'             : localStorage.getItem("ID"),
                            'TEL'            : localStorage.getItem("TEL"),
                            'REF_PRODUCT_CD' : p_cd,
                            'QTY'            : stock,
                            'PRODUCT_CORP_ID': s_corp,
                        }

                        if( parseInt(stock) <= 0 ){
            
                            if( $(this).find("ons-icon").length == 0 ){
                                $(this).prepend("<ons-icon icon='fa-info'></icon>");
                            }
                            $(this).addClass('warning');
                            arrEmptyStock.push(obj);

                        }else{
                            //재고가 있는 상품은 기존주문한 수량 할당
                            obj.QTY = qty;
                            arrSetStock.push(obj);
                        }
                    }
                });

                if( arrEmptyStock.length > 0 ){
                    if( $(document).find(".fa-warning").length == 0 ){
                        runWarningMessage("재고가 없는 상품을 확인하세요", "warning", 1500, "up");
                    }
                }

                if (dialog) { dialog.show(  { animation : "none" } ); }
            }
        });

        $(dialogCancel).find(".btnDialogCancelClose").click(function(){
            dialogCancel.hide( { animation : "none" } )
        })

        $(dialogCancel).find(".btnDialogOkCencel").click(function(){

            
            var orderId =  $(this).data("orderid");
            var objList = $(page).find(".dataList." + orderId );

            var option = {
                TEL         : localStorage.getItem("TEL"),
                ORDER_IDX   : orderId,
                ORDER_STATE : "ORD_CNCL_NEW",
            }
            if( !isNull(orderId ) ){

                var serviceReOrder = new withWSDL("api/order/list/" + orderId);
                serviceReOrder.success = function(_jsonData){
                    var jsonData    = _jsonData;

                    if( jsonData.status == "success"){
                        runWarningMessage(jsonData.message, "success", 1500, "up");
                        objList.find(".statusName").text("주문취소");
                        $(page).find(".card.bottom." + orderId + " .btnUndoOrder").prop("disabled", true);
                        $(page).find(".card.bottom." + orderId + " .btnRefund").prop("disabled", true);
                        
                    }else{
                        runWarningMessage(jsonData.message, "warning", 1500, "up");
                    }

                    dialogCancel.hide();
                };
                serviceReOrder.ajaxPutData(option);
            }
        });

        // 클릭 : 주문취소 버튼
        $(page).find(".commonList").on("click", ".btnUndoOrder", function(){

            $(dialogCancel).find(".btnDialogOkCencel").attr("data-orderid", $(this).data("orderid"));
            dialogCancel.show( { animation : "none" } );
        });

        // 개별로 담기
        $(page).find(".commonList").on("click", ".btnReOrderItem", function(){

            var stockQty    = $(this).data("stock");

            if( isNull(stockQty) || stockQty <= 0){
                runWarningMessage("상품재고가 없습니다", "warning", 1500, "up");
                return false;
            }

            var obj = {
                p_corp_id   : $(this).data("sale_corp_id"),
                productCd   : $(this).data("product_cd"),
                qty         : $(this).data("qty"),
                isEnable    : true
            }

            myApp.basket.setProductBasket(page, obj);
        });
    },

	orderDetailPage : function(page){

        myApp.i18Next.changeLang();
        
        // 주문확인 모달 창
        var orderConfirmPopup= page.querySelector("#orderConfirmPopup");
        var data             = page.pushedOptions.data;
        var userid           = localStorage.getItem("ID");

        var dialogTel        = page.querySelector("#phonenumber-dialog");
        var btnDiagTelCancel = page.querySelector(".diagTelCancel");
        var btnDiagTelOk     = page.querySelector(".diagTelOk");
        var btnPhoneNumber   = page.querySelector("#btnCheckTelNumber");
        var t_iTel           = myApp.i18Next.getTrasTxt("AUTH", "INP_TEL");
        var t_mTel           = myApp.i18Next.getTrasTxt("DIAG", "I_VAL_10_20");
        var t_warn_korean    = myApp.i18Next.getTrasTxt("COMM", "WARN_KR_PHONE");
                
         // 전화면으로 부터 구매정보와 구매상품목록정보를 가져온다
         var productData		= null;
         var prodcutDetail	= null;
         var obj			    = null;     // 증정품
         var taxData         = null;
         var timeCheck       = false;

        // 클릭 : 전화번호 변경 다이어로그
        btnPhoneNumber.onclick = function(){
            var tel     = page.querySelector("#phone_number").value;
            
            $(page).find(".changeTelNum").text(tel);

            if ( isNull(tel) || tel == "" ) {
                runWarningMessage(t_iTel, "warning", 1500, "up");
            } else if( tel.length < 10 || tel.length >= 12){
                runWarningMessage(t_mTel, "warning", 1500, "up");
            } else{
                dialogTel.show( { "animation": "none" } );
            }
        };

        // 전화번호 변경 확인 
        btnDiagTelOk.onclick = function(){
            var tel     = page.querySelector("#phone_number").value;
            var id      = localStorage.getItem("ID");
            
            var servicePhoneNum = new withWSDL("api/userm/phonenumber/" + id );
            servicePhoneNum.success = function(_jsonData) {

                var jsonData = _jsonData;
                if (jsonData.status == 200) {
                    localStorage.setItem("TEL", tel);
                    runWarningMessage(jsonData.message, "success", 2000, "up");
                    $(".myTel").text(tel);
                    $(page).find(".hide").hide("slow");
                    
                    // 주문 가능한 전화번호인지 체크
                    if( isAbleNumber ( tel ) ){
                        $(page).find(".btnOrderComplete").prop("disabled", true);   
                        
                        runWarningMessage(t_warn_korean, "warning", 2000, "up");
                    }else{
                        $(page).find(".btnOrderComplete").prop("disabled", false);   
                    }
                    
                } else {
                    runWarningMessage(jsonData.message, "danger", 2000, "up");
                }
            };

            var data = {
                "id"            : id,
                "phone_number"  : tel,
            };

            if ( isNull(tel) || tel == "" ) {
                runWarningMessage(t_iTel, "warning", 1500, "up");

            } else if( tel.length < 10 || tel.length >= 12){
                runWarningMessage(t_mTel, "warning", 1500, "up");
            }else {
                servicePhoneNum.ajaxPutData(data);
                dialogTel.hide();
            }
        };

        // 전화번호 변경 다이어로그 닫기
        btnDiagTelCancel.onclick = function(){
            dialogTel.hide();
        };

        // 로그인 여부에 따라 사은품 필수 처리 (적립금 포함)
        if( isNull( userid ) ){
            $(page).find("#giftItem").attr("required");
            $(page).find(".giftMessage").hide();
        }else{
            $(page).find("#giftItem").removeAttr("required");
            $(page).find(".giftMessage").show();
        }

        // 구매버튼 클릭시 첫 팝업여부 한번만 실행되도록 체크 하기위한 카운트
        var nPopFirst       = 0;

        // 주소검색
        $(page).find("#goAddr").click(function(){
            localStorage.setItem("ADDR_ACTIVE_TAB", 0);
            localStorage.setItem("ADDR_POP_PAGE", 1);
            myApp.navigator.pushPage('html/config/config_addr_tabbar.html');
        });

        
        $(page).find(".goRecent").click(function(){
            myApp.navigator.pushPage('html/config/recent_addr.html', { animation : "none"} );
        });
        
        // 사은품 정보 가져오기(합계금액 대비)
        var gOption = null;
        
        page.onShow = function() {

            // 회사별 현금 취급여부
            if( IS_PAY_CASH != "Y") {
                page.querySelector("#radio-cash").disabled = true;
                $(page).find(".payCash").hide();
            }else{
                page.querySelector("#radio-cash").disabled = false;
                $(page).find(".payCash").show();
            }

            // 회사별 카드 취급여부
            if( IS_PAY_CARD != "Y") {
                page.querySelector("#radio-card").disabled = true;
                $(page).find(".payCard").hide();
                
            }else{
                page.querySelector("#radio-card").disabled = false;
                $(page).find(".payCard").show();
            }

            // 회사별 e-pay 취급여부
            if( IS_PAY_EPAY != "Y") {
                page.querySelector("#radio-epay").disabled = false;
                $(page).find(".payEpay").hide();
                
            }else{
                page.querySelector("#radio-epay").disabled = false;
                $(page).find(".payEpay").show();
            }

            // 회사별 계산서 취급 여부
            if( IS_USE_TAX != "Y") {
                page.querySelector("#checkbox-tax").disabled= true;
                $(page).find(".useTax").hide();
                $(page).find(".noTaxMessage").show();
                
            }else{
                page.querySelector("#checkbox-tax").disabled= false;
                $(page).find(".useTax").show();
                $(page).find(".noTaxMessage").hide();
            }

            // 주문 가능한 전화번호인지 체크
            if( isAbleNumber ( page.querySelector("#phone_number").value ) ){
                $(page).find(".btnOrderComplete").prop("disabled", true);   
                runWarningMessage(t_warn_korean, "warning", 2000, "up");
            }else{
                $(page).find(".btnOrderComplete").prop("disabled", false);   
            }

            var t_addr1         = myApp.i18Next.getTrasTxt("CONF", "addr1");
            var t_addr2         = myApp.i18Next.getTrasTxt("CONF", "addr2");

            $(page).find("#dlvaddr").attr("placeholder", t_addr1);
            $(page).find("#dlvaddrDetail").attr("placeholder", t_addr2);
            
            timeCheck       = false;
            // 팝업 카운트 초기화
            productData		= {};
            prodcutDetail	= null;
            obj			    = new Object();
            
            // console.log(obj);
            taxData         = null;

            nPopFirst       = 0;
            productData		= data.TB_ORDER_INFO;
            prodcutDetail	= data.TB_ORDER_DETAIL;
            options         = data.OPTION;
            taxData         = null;

            // 추가배달비 / 계산된거리 초기화
            CALC_DIST       = 0;
            CALC_ADDED_FEE  = 0;
            productData.ADDED_DELV_FEE  = CALC_ADDED_FEE;
            productData.ADDED_DELV_DIST = CALC_DIST;

            checkRadio();
            
            // 장바구니에서 넘어온 합계는 기본배달비만 추가되어 있음
            var _baseFee    = productData.BASE_DELV_FEE;
            var sumProduct  = productData.TOTAL_AMT - _baseFee;
            // 추가됨 : 2020-07-02 - 추가배달비 여부 체크
            // 무료배송 금액보다 작을 경우 추가 배달비 적용
            if( sumProduct < FREE_DELV_AMT  ){
                myApp.orderController.chkAddedDelvFee(
                    productData, 
                    {                
                        tel : localStorage.getItem("TEL"),
                        sum : productData.TOTAL_AMT ,
                        // addr: productData.ADDR1 ,
                        addr: localStorage.getItem("ADDR1"),
                    }
                );
            }
            
            // 주문완료 버튼 활성화
            // $(page).find(".btnOrderComplete").prop("disabled", false);

            // 배송메시지 알림(배송내용이 있을경우만 알림)
            areaMessage     = window.localStorage.getItem("AREA_MSG");
            if( !isNull(areaMessage) && areaMessage.length > 0 ){
                runWarningMessage(areaMessage, "info", 3500, "up");
            }

            gOption = {
                amt_level   : parseInt(productData.TOTAL_AMT),
            };

            // 세금계산서 비활성화 및 초기화
            $(page).find(".taxItem").css("display", "none");
            $(page).find("#corpname").removeAttr("required");
            $(page).find("#corpemail").removeAttr("required");
            $(page).find("#tax_number").removeAttr("required");

            // 주소 초기화
            ADDR1 = isNull(localStorage.getItem("ADDR1")) ? "" : localStorage.getItem("ADDR1") ;
            ADDR2 = isNull(localStorage.getItem("ADDR2")) ? "" : localStorage.getItem("ADDR2") ;

            $(page).find("#dlvaddr").val( ADDR1 );
            $(page).find("#dlvaddrDetail").val( ADDR2 );

            // 코인을 사용한 경우 체크
            // if( !isNull(productData.PAY_OPTION_COIN)){
            //     page.querySelector("ons-checkbox[name='payway_coin']").checked = true;
            //     page.querySelector("ons-checkbox[name='payway_coin']").disabled = true;
            // }else{
            //     page.querySelector("ons-checkbox[name='payway_coin']").checked = false;
            //     page.querySelector("ons-checkbox[name='payway_coin']").disabled = false;
            // }

            $giftSelect = $(page).find("#giftItem select");
            var serviceGift     = new withWSDL("api/gift/list");
            serviceGift.success = function(_jsonData){

                var jsonData    = _jsonData;
                // console.log(jsonData);

                if( jsonData.recordsTotal > 0){
                    $(jsonData.list).each(function(){
                        
                        pName   = getLang(this.PRODUCT_NM, this.PRODUCT_NM_VN, this.PRODUCT_NM_EN);
                        selmsg  = myApp.i18Next.getTrasTxt("ORDD", "ORD_GIFT");
                        
                        $giftSelect.html("<option value=''>" + selmsg + "</option>");
                        $giftSelect.append("<option data-corp_id='" + this.CORP_ID + "' data-qty='" + this.QTY + "' value='" + this.PRODUCT_CD + "'>" + pName + "</option>")
                    });

                }else{

                    var none    = myApp.i18Next.getTrasTxt("COMM", "NONE");
                    // 사은품이 없을 경우 필수값 제외
                    $giftSelect.html("<option value=''>" + none + "</option>");
                    $giftSelect.removeAttr("required");
                    $(page).find("#giftItem").removeAttr("required");
                    $(page).find(".giftMessage").show();

                    // 적립금 미취급시 적립금 메시지 hidden
                    if( IS_PAY_POINT != "Y") {
                        $(page).find(".giftMessage").hide();
                    }else{
                        $(page).find(".giftMessage").show();
                    }
                }
            };

            // 증정품(사은품) 조회
            serviceGift.ajaxGetData(gOption);


            // 영업종료로 인한 예약설정이 있는 경우 강제로 예약배송 설정으로 고정
            if( isNull( options.NEXT_DT) ){
                // 시간직접입력 숨기기
                $(page).find(".AddrTime2").hide();

                // 서버정지 일때 메시지
                if( !isNull( options.CONF_DT) && options.CONF_DT != "" ){

                    // 시간직접입력 숨기기
                    $(page).find(".msgForConfDiv").show();
                    var t_aft_proc  = myApp.i18Next.getTrasTxt("ORD", "AFTER_PROC");
                    var conf_msg    = options.CONF_DT + t_aft_proc;
                    
                    if( getLangNow().indexOf("ko") == -1){
                        conf_msg    = t_aft_proc + options.CONF_DT ;
                    }
                    $(page).find(".msgForConfig").html(conf_msg);
                    return;
                }else{
                    $(page).find(".msgForConfDiv").hide();
                }
                
            }else{
                $(page).find("ons-radio#AddrTime1").prop("disabled", true);
                $(page).find("ons-radio#AddrTime2").prop("checked", true);
                $(page).find("ons-radio#AddrTime2").trigger("click");
            }
        }

        page.onHide = function(){
            nPopFirst       = 0;
            productData		= null;
            prodcutDetail	= null;
            taxData         = null;
        }

        // 결제방식 체크 및 금액 계산 체크
        function checkRadio(){

            var tot_amt     = parseInt(productData.TOTAL_AMT);
            var coin_pay    = parseInt(productData.PAY_OPTION_COIN);
            var delv_pay    = parseInt(productData.BASE_DELV_FEE);
            var added_pay   = parseInt(productData.ADDED_DELV_FEE);
            var cal_amt     = tot_amt - delv_pay - added_pay;

            Array.prototype.forEach.call(page.querySelectorAll("ons-radio[name='payway']"), function(ele){

                if(ele.checked){
                    if( ele.value == "CASH"){
                        productData.PAY_OPTION = "CASH";
                        productData.PAY_OPTION_CARD = null;
                        productData.PAY_OPTION_CASH = cal_amt;
                    }else{
                        productData.PAY_OPTION = "CARD";
                        productData.PAY_OPTION_CARD = cal_amt;
                        productData.PAY_OPTION_CASH = null;
                    }
                    return true;
                }
            });
        }

        // 세금계산서 체크 및 계산서 필수값 체크
        function checkTax(){

            var isTaxChecked = page.querySelector("ons-checkbox[name='chkReqTax']").checked;

            if(isTaxChecked){
                $(page).find("#corpname").attr("required",true);
                //console.log( $(page).find("#corpname").prop("required") );
                $(page).find("#corpemail").attr("required",true);
                $(page).find("#tax_number").attr("required",true);
                $(page).find("#taxaddr").attr("required",true);

                taxData  = {

                    "TAX_COMP_NM"       : $(page).find("#corp_name").val(),
                    "TAX_CORP_EMAIL"    : $(page).find("#corp_email").val(),
                    "TAX_DUTY_NO"       : $(page).find("#taxnumber").val(),
                    "TAX_ADDR"          : $(page).find("#taxaddr").val()
                }

            }else{

                $(page).find("#corpname").removeAttr("required");
                $(page).find("#corpemail").removeAttr("required");
                $(page).find("#tax_number").removeAttr("required");
                $(page).find("#taxaddr").removeAttr("required");

                taxData  = null;
            }
        }



        // 현금 / 카드 사용여부 (체크박스체크 및 데이터 바인딩)
        Array.prototype.forEach.call(page.querySelectorAll("ons-checkbox[name='payway']"), function(element){
            element.onclick = function(){
                checkRadio();
            }
        });

        // 계산서 신청시 필수체크 및 계산서 데이터 값 바인딩
        page.querySelector("ons-checkbox[name='chkReqTax']").onclick = function(){

            $(page).find(".taxItem").toggle();

            var t_corpnm    = myApp.i18Next.getTrasTxt("ORDD", "CORP_NM");
            var t_email     = myApp.i18Next.getTrasTxt("ORDD", "CORP_EMAIL");
            var t_tax_num   = myApp.i18Next.getTrasTxt("ORDD", "TAX_NUM");
            var t_addr      = myApp.i18Next.getTrasTxt("CONF", "addr1");

            $(page).find("#corpname").attr("placeholder", t_corpnm);
            $(page).find("#corpemail").attr("placeholder", t_email);
            $(page).find("#taxnumber").attr("placeholder", t_tax_num);
            $(page).find("#taxaddr").attr("placeholder", t_addr);

            // console.log("ele.checked : " + this.checked );
            if( this.checked ){
                $(page).find("#corpname").attr("required",true);
                $(page).find("#corpemail").attr("required",true);
                $(page).find("#tax_number").attr("required",true);
                $(page).find("#taxaddr").attr("required",true);
                // var msg     =  myApp.i18Next.getTrasTxt("ORDD","TAX_WARNING");
                // runWarningMessage(msg, "info", 5000, "up");

            }else{
                $(page).find("#corpname").removeAttr("required");
                $(page).find("#corpemail").removeAttr("required");
                $(page).find("#tax_number").removeAttr("required");
                $(page).find("#taxaddr").removeAttr("required");
            }
        }
		// 주소 바인딩
		page.querySelector("#dlvaddr").value		= localStorage.getItem("ADDR1");
		page.querySelector("#phone_number").value	= localStorage.getItem("TEL");

        // 주문확인 모달창 닫기 클릭 이벤트
        Array.prototype.forEach.call(page.querySelectorAll(".btnCloseOrderPopup"), function(element){
            element.onclick = function(){
                orderConfirmPopup.hide({ "animation" : "none" });
            }
        });


        var standSt     = moment(new Date()).startOf("day").add(7, 'hours');
        var endSt       = moment(new Date()).startOf("day").add(1, 'days').add(30, 'minutes');
        var dDay        = moment(new Date())
        var diff_time   = 30;
        var day_date    = dDay.startOf('hour').add(1, 'hours');

        // console.log(standSt.format('YYYY-MM-DD HH:mm'), endSt.format('YYYY-MM-DD HH:mm'));
        for(var i=0; i < 10; i++){
            day_date    = day_date.add( diff_time , 'minutes' );
            md_date     = day_date.format("MM-DD");
            tm_end      = day_date.format("HH:mm");
            end_diff    = endSt.diff(day_date, 'minutes') ;

            if( end_diff == -30  ){
                day_date    = standSt.add(1, 'days');
                day_date    = day_date.startOf('hour');
                md_date     = day_date.format("MM-DD");
                tm_end      = day_date.add( 30, 'minutes' ).format("HH:mm");
            }

            $("#AddrTime select").append(
                "<option data-date='" + day_date.format("YYYY-MM-DD") + "' data-time='" + tm_end + "'>" +
                    "<span> 예약 : </span>" +
                     md_date + " " + tm_end + " 까지" +
                "</option>") ;
        }

        var minSt     = moment(new Date()).add(30, 'minutes').format();
        $(page).find("#AddrTime2").attr("min", minSt);
        
        // 시간 직접 입력 선택 클릭
        $(page).find("ons-radio[name='timeway']").click(function(){

            var radioId = $(this).attr("id") ;
            // console.log(radioId);

            // 직접
            if( radioId == "AddrTime1" ){
                timeCheck = true;
                $(page).find(".reserv").hide();
                // 시간 선택 selectbox 필수해제 / 직접입력 필수
            }else{

                timeCheck           = false;
                $(page).find(".reserv").show();

                var currentTime = moment();

                // 서버중지 종료 시간이 설정된 경우 : 최소 시간을 종료시간으로 초기화
                if( !isNull( options.CONF_DT) ){
                    currentTime = moment(options.CONF_DT).format("YYYY-MM-DDTHH:mm");
                }

                var reservTime  = moment(currentTime).add("31", 'minutes').format("YYYY-MM-DDTHH:mm");

                // 예약배송이 가능할때만 설정 : 최소시간을 업체 시작시간으로 초기화
                if( !isNull( options.NEXT_DT) ){
                    reservTime  = moment(options.NEXT_DT).format("YYYY-MM-DDTHH:mm");
                    // console.log(reservTime);
                }

                $(page).find("#AddrTime3").attr("min", reservTime);
                $(page).find("#AddrTime3").val(reservTime);
                productData.DLV_WISH_TIME = reservTime;

                $(page).find("#AddrTime3").trigger("change");
            }
        });

        $(page).find("#AddrTime3").change(function(){
            
            timeCheck           = false;
            var currentTime     = moment();
            var hour            = moment($(this).val()).format("HH");
            var min             = moment($(this).val()).format("mm");
            var time            = moment($(this).val());
            var diff            = currentTime.diff(time, 'minutes');
            
            if( diff > -30 ){
                runWarningMessage("현재시각대 보다 30분 후로 설정할수 있습니다", "warning", 2500, "up");
                timeCheck = false;
                return;
            }

            if( !isNull(hour) && !isNull(min) ){
               
                nHour   = parseInt(hour);
                nMin    = parseInt(min);
                if( nHour >= 0 && nHour <= 7 ){
                    if( nHour == 7 ){
                        if( nMin < 30 ){
                            runWarningMessage("배달가능시각은 07:30~12:30 사이입니다", "warning", 2500, "up");
                            timeCheck = false;
                            return;
                        }
                    }else if(nHour == 0) {
                        if( nMin > 30 ){
                            runWarningMessage("배달가능시각은 07:30~12:30 사이입니다", "warning", 2500, "up");
                            timeCheck = false;
                            return;
                        }
                    }else{
                        runWarningMessage("배달가능시각은 07:30~12:30 사이입니다", "warning", 2500, "up");
                        timeCheck = false;
                        return;
                    }
                }
                timeCheck = true;
            }

            timeCheck = true;
            productData.DLV_WISH_TIME       = currentTime.format("YYYY-MM-DD HH:mm");
        })

		// 주문하기 모달창 보이기
		$(page).find(".btnOrderComplete").click(function(){
            
            // 주문하기 버튼 비활성화
            // 중복 주문 방지
            // $(page).find(".btnOrderComplete").prop("disabled", true);
            
            // ---------- 예약 배달시간 밸리데이션 시간 -----------------------------------------------
            if( page.querySelector("#AddrTime1").checked ){
                timeCheck = true;
            }

            // 예약배송시간 설정 확인
            if( !timeCheck ){
                runWarningMessage("예약배송시간설정을 확인(현재시간보다 최소 +30분) 하여 주세요", "warning", 2500, "up");
                return false;
            }

            // ---------- 예약 배달시간 밸리데이션 완료 -----------------------------------------------            
            $(page).find("#orderConfirmPopup .orerList").empty();

            checkTax();
            checkRadio();

            // 예약배송일 경우
            if( page.querySelector("#AddrTime2").checked ){
                withDatetime = moment( $(page).find("#AddrTime3").val()).format("YYYY-MM-DD HH:mm");
                productData.DLV_WISH_TIME       = withDatetime;
            }else{
                productData.DLV_WISH_TIME       = moment().add( 30, 'minutes' ).format("YYYY-MM-DD HH:mm");
            }

            if( !fnValidationInput("orderDetailPage")){
                return false;
            }

            // 주문마스터 정보 (주소,메모)
            productData.ADDR1			= page.querySelector("#dlv_addr").value;
            productData.ADDR2			= page.querySelector("#dlvaddrDetail").value; ;
            productData.MEMO			= page.querySelector("#req_text").value;

            // 증정품 추가
            giftProductCd    = $(page).find("#giftItem select option:selected").val();
            if( !isNull(giftProductCd) ){
                obj.CORP_ID		= localStorage.getItem("CORP_ID");
                obj.USER_ID		= localStorage.getItem("USER_ID");
                obj.BRANCH_ID	= localStorage.getItem("BRANCH_ID");
                obj.PRODUCT_CD	= giftProductCd;
                obj.SALE_CORP_ID= $(page).find("#giftItem select option:selected").attr("data-corp_id");
                obj.DIV         = null;
                obj.BARCODE		= null;
                obj.PRODUCT_NM	= $(page).find("#giftItem select option:selected").text();
                obj.DISPLAY_NO	= null;
                obj.QTY			= $(page).find("#giftItem select option:selected").attr("data-qty");
                obj.UNIT		= 0;
                obj.RMK         = "GIFT";
                obj.IS_OUT_YN	= "Y";
            }else{
                obj = new Object();
            }
            //prodcutDetail.push(obj);
            // ----------------- 주문 직전 전체 확인 사항 팝업 내용 --------------------
            // 총금액
            $(page).find(".sValue").text( Number(productData.TOTAL_AMT).currency(0));

            // 배달시간
            $(page).find(".tValue").text( productData.DLV_WISH_TIME );

            // 배달주소
            $(page).find(".aValue").text( productData.ADDR1 + " " + productData.ADDR2 );

            var tempCalAmt  = productData.PAY_OPTION_CASH;
            if( productData.PAY_OPTION == "CARD"){
                tempCalAmt  = productData.PAY_OPTION_CARD;
            }
        
            // 위드유 현금/카드
            $(page).find(".cashValue").text( Number(tempCalAmt).currency(0) );

            // 위드유 페이
            $(page).find(".coinValue").text( Number(productData.PAY_OPTION_COIN * (-1)).currency(0) );

            // 결제
            $(page).find(".pValue").text( productData.PAY_OPTION );

            // 메모
            $(page).find(".pMemo").text( productData.MEMO );

            // 지불금액
            $(page).find(".billValue").text( Number(productData.TOTAL_AMT - productData.PAY_OPTION_COIN).currency() );
            

            // 기본배송비
            $(page).find(".pBaseFee").text( Number(productData.BASE_DELV_FEE).currency(0) );
            
            // 추가배송비/거리
            $(page).find(".pAddedFee").text( "( " + Number( parseInt(productData.ADDED_DELV_DIST) / 1000).toFixed(2) + " Km ) " + Number(productData.ADDED_DELV_FEE).currency(0));
            
            // 계산서 여부
            if( !isNull(taxData)){

                var t_corpnm    = myApp.i18Next.getTrasTxt("ORDD", "CORP_NM");
                var t_email     = myApp.i18Next.getTrasTxt("ORDD", "CORP_EMAIL");
                var t_tax_num   = myApp.i18Next.getTrasTxt("ORDD", "TAX_NUM");
                var t_tax_addr  = myApp.i18Next.getTrasTxt("ORDD", "TAX_ADDR");
                
                $(page).find(".taxValue1").text( t_corpnm + " : " + taxData.TAX_COMP_NM );
                $(page).find(".taxValue2").text( t_email  + " : " + taxData.TAX_CORP_EMAIL );
                $(page).find(".taxValue3").text( t_tax_num + " : " + taxData.TAX_DUTY_NO );
                $(page).find(".taxValue4").text( t_tax_addr + " : " + taxData.TAX_ADDR );
                $(page).find(".summaryTax").show();
            }else{
                $(page).find(".summaryTax").hide();
            }

            $list = $(page).find("#orderConfirmPopup .orerList");

            // 주문상품 목록 보여주기
            $(prodcutDetail).each(function(i){

                rmk         = isNull(this.RMK) ? "" : "("+ this.RMK + ")";

                if( !isNull(this.PRODUCT_NM) ){
                    pli         =   "<div class='summaryLi'>" +
                                    "   <span class='left'>"    + "<span class='oNumber'>" + (i+1) + ".</span> " + this.PRODUCT_NM + rmk + "</span>" +
                                    "   <span class='center'>"  + Number(this.UNIT).currency(0) + "</span>" +
                                    "   <span class='right'>"   + Number(this.QTY).format() + "</span>" +
                                    "</div>";

                    $list.append(pli);
                }
            });

            // 증점품이 있을 경우 보여주기
            if( !isNull(obj) && !isNull(obj.PRODUCT_CD) ){

                // 증정품도 보여주기
                gift    =   "<div class='summaryLi'>" +
                            "   <span class='left'>"    + obj.PRODUCT_NM + "(" + obj.RMK + ")" + "</span>" +
                            "   <span class='center'>"  + Number(obj.UNIT).currency(0) + "</span>" +
                            "   <span class='right'>"   + Number(obj.QTY).format() + "</span>" +
                            "</div>";

                $list.append(gift);
            }


            // 1차단계 줄임
            orderConfirmPopup.show(  { animation : "none" }  );
            //$(page).find(".btnExcuteOrder").trigger();
            //myApp.orderController.orderComplete(page, obj, productData, prodcutDetail, taxData);
        });

        // 취소 하기
        $(page).find(".btnCancelOrder").click(function(){
            orderConfirmPopup.hide(  { animation : "none" }  );
            $(page).find(".btnExcuteOrder").prop("disabled", false);
            // document.querySelector('#myNavigator').popPage();
            //document.getElementById('appTabbar').setActiveTab(0);
        });

        // 주문완료 하기
        $(page).find(".btnExcuteOrder").click(function(){

            $(page).find(".btnExcuteOrder").prop("disabled", true);
            // console.log("btnExcuteOrder");
            // 증정품 제일 마지막에 추가
            if( !isNull(obj)){
                prodcutDetail.push(obj);
            }
            // 증점품은 한번만 바인딩 되도록 함
            obj = null;
            obj = new Object();

            // 주문을 위한 웹서비시즈
            var serviceOrder    = new withWSDL("api/order/list");

            // console.log(
            //     {
            //         'TB_ORDER_INFO'     : productData,
            //         'TB_ORDER_DETAIL'   : prodcutDetail,
            //         'TB_TAX_INFO'       : taxData
            //     }
            // )

            // 주문정보 요청 성공
            serviceOrder.success = function(_jsonData){
                var jsonData    = _jsonData;
                // console.log(jsonData);

                if( jsonData.status == "success"){

                    orderConfirmPopup.hide();

                    usedCoin = productData.PAY_OPTION_COIN;
                    if( !isNull(usedCoin) && usedCoin > 0){
                        WITHU_PAY -= usedCoin;
                    }
                    
                    runWarningMessage(jsonData.message, "success", 1500, "up");

                    // 주문내역으로 이동
                    document.getElementById('appTabbar').setActiveTab(4);
                    myApp.navigator.popPage( { animation : "none" } );

                }else{
                    runWarningMessage(jsonData.message.PRODUCT_NM, "danger", 1500, "up");
                }
            };

            serviceOrder.ajaxPostData(
            {
                'TB_ORDER_INFO'     : productData,
                'TB_ORDER_DETAIL'   : prodcutDetail,
                'TB_TAX_INFO'       : taxData
            });

        });
	},

	orderComplete : function(page, obj, productData, prodcutDetail, taxData){

        // console.log("btnExcuteOrder");

        // 증정품 제일 마지막에 추가
        if( !isNull(obj)){
            prodcutDetail.push(obj);
        }
        // 증점품은 한번만 바인딩 되도록 함
        obj = null;

        // 주문을 위한 웹서비시즈
        var serviceOrder    = new withWSDL("api/order/list");

        // 주문정보 요청 성공
        serviceOrder.success = function(_jsonData){
            var jsonData    = _jsonData;
            
            if( jsonData.status == "success"){
                orderConfirmPopup.hide();
                runWarningMessage(jsonData.message, "success", 1500, "up");
                // 주문내역으로 이동
                usedCoin = productData.PAY_OPTION_COIN;

                if( !isNull(usedCoin) && usedCoin > 0){
                    WITHU_PAY -= usedCoin;
                    console.log(WITHU_PAY);
                }
                
                document.getElementById('appTabbar').setActiveTab(4);
                myApp.navigator.popPage( { animation : "none" } );

            }else{
                runWarningMessage(jsonData.message.PRODUCT_NM, "danger", 1500, "up");
            }
        };
        serviceOrder.ajaxPostData(
        {
            'TB_ORDER_INFO'     : productData,
            'TB_ORDER_DETAIL'   : prodcutDetail,
            'TB_TAX_INFO'       : taxData
        });   
    }, 

    chkAddedDelvFee : function(productData, paramOption){

        var svc = new withWSDL("api/basket/delvamt");

        // 추가 2020-07-02 : 추가배달비 
        var feeOption   = paramOption;

        // 상품군 목록 데이터 바인딩
        svc.success = function(_jsonData) {
            var data        = _jsonData;

            var addedFee    = data.fee;
            var dist        = data.dist;
            var cal_dist    = data.cal_dist;

            // 계산된 거리로 할려 했으나, 오해의 소지가 있으니 본 거리로 바로 보여줌
            CALC_DIST       = dist;
            CALC_ADDED_FEE  = addedFee;

            productData.ADDED_DELV_FEE  = CALC_ADDED_FEE;
            productData.ADDED_DELV_DIST = CALC_DIST;

            // 전체합계 추가 배달비 추가
            // console.log("before : " + productData.TOTAL_AMT);
            productData.TOTAL_AMT   = productData.TOTAL_AMT + CALC_ADDED_FEE;
            // console.log("after : " + productData.TOTAL_AMT);
            
        };

        svc.ajaxGetData(feeOption);
    },
    // 배달원 위치
	locationMapPage : function(page){

		document.addEventListener("deviceready", onDeviceReadyForMap, false);

        function onDeviceReadyForMap() {
             deviceExGeoLocation();
        }

        var data = page.pushedOptions.data;
        //console.log(data);

        var service		= new withWSDL("api/gps/list");

        var option = {
            corp_id		: data.CORP_ID,
            order_idx	: data.ORDER_ID
        };

        // 배달원정보 요청 성공
        service.success = function(_jsonData){
            var jsonData    = _jsonData;
            var gpsData		= jsonData.list;

            // console.log(gpsData);

            var option  = {
                isDraggable   : false,
                isShowCommnet : false
            };

            // 현재 위치정보를 수신하여 화면에 보여줌
            myApp.googleMapControllers.getCurrentGeoInfo( function(pos){
                myApp.googleMapControllers.showGoogleMap("#dlvMap", pos.lat, pos.lng, option );
                myApp.googleMapControllers.getCurrentAddr(pos);

                // 현재 지점 좌표를 가져온다

                var infowindow = new google.maps.InfoWindow();
                var marker;

                // console.log(gpsData.length);

                if( gpsData.length == 0){
                    runWarningMessage("현 주문에 대한 배달자의 위치를 읽어올수 없습니다", "warning", 1500, "up");
                    $(page).find('ons-progress-circular').hide();
                    return;
                }

                for (var i = 0; i < gpsData.length; i++) {

                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(gpsData[i]["D_LAT"], gpsData[i]["D_LNG"]),
                        icon : "img/map/bycle1.png",
                        map: myApp.googleMapControllers.map
                    });

                    if( gpsData.length == (i+1)){
                        marker.setAnimation(google.maps.Animation.BOUNCE);
                    }

                    google.maps.event.addListener(marker, 'click', (function(marker, i) {
                        return function() {
                            infowindow.setContent( gpsData[i]["GPS_DATE_DT"]);
                            infowindow.open(myApp.googleMapControllers.map, marker);
                        }

                    })(marker, i));
                    $(page).find('ons-progress-circular').hide();
                }
            });
        };

        service.ajaxGetData(option);
    },
}
