myApp.authControllers = {

    joinPage: function(page) {

        myApp.i18Next.changeLang();

        // 전화번호 필수 체크
        if( !showChkTel_Diag() ){
            return false;
        }

        var tel = localStorage.getItem("TEL");
        page.querySelector("#phone_number").value = !isNull(tel) ? tel : "";
        
        var error_div = $(page).find('.error_box');
        var erros_memo = error_div.find('ul');
        var eventList = ["keydown", "keyup", "focusout", "focusin"];

        var btnCheckEmail = page.querySelector("#btnCheckEmail");
        var btnCheckId = page.querySelector("#btnCheckId");

        var objUserId = page.querySelector('#userid input');
        var objEmail = page.querySelector('#email input');
        var objPassword = page.querySelector('#password input');
        var objPassword_confirm = page.querySelector('#passwordconfirm input');
        var objName = page.querySelector('#name input');
        var refferor = page.querySelector("#refferor");

        var agreeAll = page.querySelector("#agreeAll");
        var agreeUse = page.querySelector("#agreeUse");
        var agreePrivate = page.querySelector("#agreePrivate");
        var agreeAlram = page.querySelector("#agreeAlram");

        var btnJoin = page.querySelector(".btnJoin");
        var checkSuccess = false;
        var valiSuccess = false;
        var checkUserIdSuccess = false;
        var checkEmailSuccess = false;

        var t_aval_id   = myApp.i18Next.getTrasTxt("AUTH", "AVAL_ID");      // 사용가능한 아이디입니다
        var t_alrdy_id  = myApp.i18Next.getTrasTxt("AUTH", "ALRDY_ID");     // 이미 사용중인 아이디
        var t_warg_email= myApp.i18Next.getTrasTxt("AUTH", "EMAIL_USE_MSG");// 이메일은 계정 분실시 사용됨
        var t_aval_email= myApp.i18Next.getTrasTxt("AUTH", "EMAIL_AVABLE"); // 사용가능한 이메일
        var t_alrdyemail= myApp.i18Next.getTrasTxt("AUTH", "ALRDY_EMAIL");  // 이미 사용중인 이메일

        // 전체 체크
        var checkSum = function() {
             if (checkSuccess && valiSuccess && checkEmailSuccess && checkUserIdSuccess) {
                 btnJoin.removeAttribute("disabled");

             } else {
                 btnJoin.setAttribute("disabled");
             }
         };

         // 입력단 체크
         var erros_memo_change = function() {
            erros = $(page).find('.error_box ul');
            if (erros.find(".warning").length == 0) {
                erros.parent("div").addClass("success_box");
                valiSuccess = true;
                error_div.hide();
            } else {
                erros.parent("div").removeClass("success_box");
                valiSuccess = false;
                error_div.show();
            }

             checkSum();
         };

         // 체크박스 체크
         var erros_check_change = function() {
             Array.prototype.forEach.call(page.querySelectorAll(".subCheck"), function(element, i) {
                 if (element.hasAttribute("required")) {
                     if (!element.checked) {
                         return checkSuccess = false;
                     } else {
                         checkSuccess = true;
                     }
                 }
             });
             checkSum();
         };


         // 이메일 포키싱 : 이메일 주소 체크
         objEmail.onfocus = function() {

             runWarningMessage( t_warg_email, "info", 1000, "up");
             erros_memo.find(".emailChk").addClass("warning");
             erros_memo.find(".emailChk").removeClass("success");
             erros_memo_change();
         };

         // 아이디 체크
         btnCheckId.onclick = function() {

             myApp.commonController.chkUserduplication('user_id', objUserId.value, function(count) {

                if (count > 0) {
                    runWarningMessage( t_alrdy_id, "warning", 1500, "up");
                    erros_memo.find(".userIdChk").addClass("warning");
                    erros_memo.find(".userIdChk").removeClass("success");
                    checkUserIdSuccess = false;
                } else {
                    runWarningMessage( t_aval_id, "success", 1500, "up");
                    erros_memo.find(".userIdChk").removeClass("warning");
                    erros_memo.find(".userIdChk").addClass("success");
                    checkUserIdSuccess = true;

                }

                 erros_memo_change();
             });

        };

        // 이메일 중복 체크
        btnCheckEmail.onclick = function() {

             myApp.commonController.chkUserduplication('email', objEmail.value, function(count) {

                if (count > 0) {
                    runWarningMessage( t_alrdyemail , "warning", 1500, "up");
                    erros_memo.find(".emailChk").addClass("warning");
                    erros_memo.find(".emailChk").removeClass("success");
                    checkEmailSuccess = false;
                } else {
                    runWarningMessage( t_aval_email, "success", 1500, "up");
                    erros_memo.find(".emailChk").removeClass("warning");
                    erros_memo.find(".emailChk").addClass("success");
                    checkEmailSuccess = true;

                }

                 erros_memo_change();
             });
        };

         // 이메일 형식 체크 => 이메일 버튼 활성/비활성화
         for (event of eventList) {
             objEmail.addEventListener(event, function() {
                 if (validateEmail(objEmail.value)) {
                     erros_memo.find(".email").removeClass("warning");
                     erros_memo.find(".email").addClass("success");
                     btnCheckEmail.removeAttribute("disabled");
                 } else {
                     erros_memo.find(".email").addClass("warning");
                     erros_memo.find(".email").removeClass("success");
                     btnCheckEmail.setAttribute("disabled");
                 }
                 erros_memo_change();
             });
        };

        for (event of eventList) {
            objPassword.addEventListener(event, function() {
                if (ValidatePassWord(objPassword.value)) {
                    erros_memo.find(".password").removeClass("warning");
                    erros_memo.find(".password").addClass("success");
                } else {
                    erros_memo.find(".password").addClass('warning');
                    erros_memo.find(".password").removeClass("success");
                }

                if (objPassword.value.length >= 6) {
                    erros_memo.find(".length").removeClass('warning');
                    erros_memo.find(".length").addClass("success");
                } else {
                    erros_memo.find(".length").addClass("warning");
                    erros_memo.find(".length").removeClass("success");
                }

                if (objPassword.value === objPassword_confirm.value && objPassword_confirm.value.length >= 6) {
                    erros_memo.find(".confirm").removeClass('warning');
                    erros_memo.find(".confirm").addClass("success");
                } else {
                    erros_memo.find(".confirm").addClass('warning');
                    erros_memo.find(".confirm").removeClass("success");
                }
                erros_memo_change()
            });

            objPassword_confirm.addEventListener(event, function() {
                if (objPassword.value === objPassword_confirm.value && objPassword_confirm.value.length >= 6) {
                    erros_memo.find(".confirm").removeClass('warning');
                    erros_memo.find(".confirm").addClass("success");
                } else {
                    erros_memo.find(".confirm").addClass('warning');
                    erros_memo.find(".confirm").removeClass("success");
                }
                erros_memo_change();
            });
        };

        for (event of eventList) {
            objName.addEventListener(event, function() {
                if (objName.value.length >= 2) {
                    erros_memo.find(".name").removeClass('warning');
                    erros_memo.find(".name").addClass("success");
                } else {
                    erros_memo.find(".name").addClass('warning');
                    erros_memo.find(".name").removeClass("success");
                }
                erros_memo_change();
            });
        };

        for (event of eventList) {
            objUserId.addEventListener(event, function() {
                if (objUserId.value.length >= 2) {
                    erros_memo.find(".userId").removeClass('warning');
                    erros_memo.find(".userId").addClass("success");
                } else {
                    erros_memo.find(".userId").addClass('warning');
                    erros_memo.find(".userId").removeClass("success");
                }

                erros_memo_change();
            });
        };

        // 전체동의
        agreeAll.onclick = function() {
            Array.prototype.forEach.call(page.querySelectorAll("input[type='checkbox']"), function(element) {
                if (agreeAll.checked) {
                    element.checked = true;
                } else {
                    element.checked = false;
                }

                erros_check_change();
            });
        };

        Array.prototype.forEach.call(page.querySelectorAll(".subCheck"), function(element, i) {
            element.onclick = function() {
                if (!element.checked) {
                    agreeAll.checked = false;
                }
                if (element.checked) {
                    var nAllCheck = 0;
                    Array.prototype.forEach.call(page.querySelectorAll(".subCheck"), function(ele) {
                        if (ele.checked) {
                            nAllCheck++;
                            if (nAllCheck == 3) {
                                agreeAll.checked = true;
                            }
                        }

                    });
                }
                erros_check_change();
            };
        });

        // 개인정보 보호정책
        Array.prototype.forEach.call(document.querySelectorAll('.goPolicy'), function(ele) {
            ele.onclick = function() {
                myApp.navigator.pushPage('html/etc/policy.html', { animation : "none"} );
            };
        });

        btnJoin.onclick = function() {

            // 다 완료하고 이메일 변경 방지 => 다시 한번 체크            
            data = $("#joinForm").serializeObject();

            // 로그인 서비스 호출
            service.success = function(_jsonData) {
                var jsonData = _jsonData;
                if (typeof serviceModel.joinMember === 'undefined') {
                    $.getScript("js/service/auth.js", function() {
                        serviceModel.joinMember(page, jsonData);
                    });
                } else {
                    serviceModel.joinMember(page, jsonData);
                }
            };

            var option = {

                'user_id'       : data.USER_ID,
                'deviceToken'   : localStorage.getItem("FCM_KEY"), //'43jkl4fjkdo4lro4fdsafsafasfdasfdasfdasdfasdfsa3fsafsa',
                'password'      : data.PASS_WD,
                'name'          : data.USER_NM,
                'email'         : data.EMAIL,
                'phone_number'  : data.TEL, //localStorage.getItem("TEL"),

                'email_isuse'       : 'Y',
                'phone_number_isuse': 'Y',

                'CORP_ID'       : localStorage.getItem("CORP_ID"),
                'ADDR1'         : localStorage.getItem("ADDR1"),
                'ADDR2'         : localStorage.getItem("ADDR2"),
                'APP_ID'        : localStorage.getItem("APP_ID"),
                'REF_ID'        : data.REF_ID,
                'FCM_TOKEN'     : localStorage.getItem("FCM_TOKEN"),
                'LNG'           : localStorage.getItem("LNG"),
                'LAT'           : localStorage.getItem("LAT")
            };

            localStorage.setItem('joinMemberData', JSON.stringify(option));

            // 회원가입시 사용
            if( !isNull( localStorage.getItem('joinMemberData') ) ){
                myApp.configControllers.exeJoinMember( { ADDR1 : "", ADDR2 : "", BRANCH_ID : null, AREA_ID : "ALL"} );
                return;
            }
        }

    },

    loginPage: function(page) {

        myApp.i18Next.changeLang();

        var lsAutoLogin = localStorage.getItem("IS_AUTO_LOGIN"); // 자동로그인
        var lsSaveIdPw  = localStorage.getItem("IS_SAVE_IDPW"); // 계정저장

        var btnFindId   = page.querySelector("#btnFindId");
        var btnFindPw   = page.querySelector("#btnFindPw");

        var btnKakaoLo  = page.querySelector(".btnLoginKakao");
        var btnNaverLo  = page.querySelector(".btnLoginNaver");
        var btnAppleLo  = page.querySelector(".btnLoginApple");
        var tel         = localStorage.getItem("TEL");
        
        // 전화번호 수정
        var fabTel              = page.querySelector("#fabTel");
        var dialogTel           = page.querySelector("#phonenumber-dialog");
        var btnDiagTelCancel    = page.querySelector(".diagTelCancel");
        var btnDiagTelOk        = page.querySelector(".diagTelOk");
        var btnPhoneNumber      = page.querySelector("#btnCheckTelNumber");

        var t_iTel  = myApp.i18Next.getTrasTxt("AUTH", "INP_TEL");
        var t_mTel  = myApp.i18Next.getTrasTxt("DIAG", "I_VAL_10_20");

        fabTel.onclick = function(){
            if( !showChkTel_Diag() ){
                return false;
            }

            $(page).find(".hide").toggle("slow");
        };
                
        // 클릭 : 전화번호 변경 다이어로그
        btnPhoneNumber.onclick = function(){
            var tel     = page.querySelector("#phone_number").value;
            $(page).find(".changeTelNum").text(tel);

            if ( isNull(tel) || tel == "" ) {
                runWarningMessage(t_iTel, "warning", 1500, "up");
            } else if( tel.length < 10 || tel.length >= 12){
                runWarningMessage(t_mTel, "warning", 1500, "up");
            } else{
                dialogTel.show( { "animation": "none" } );
            }
        };

        // 전화번호 변경 확인 
        btnDiagTelOk.onclick = function(){
            var tel     = page.querySelector("#phone_number").value;
            var id      = localStorage.getItem("ID");
            
            // 추천인 등록
            var servicePhoneNum = new withWSDL("api/userm/phonenumber/" + id );
            servicePhoneNum.success = function(_jsonData) {

                var jsonData = _jsonData;
                if (jsonData.status == 200) {
                    localStorage.setItem("TEL", tel);
                    runWarningMessage(jsonData.message, "success", 2000, "up");
                    $(".myTel").text(tel);
                    $(page).find(".hide").hide("slow");
                    
                } else {
                    runWarningMessage(jsonData.message, "danger", 2000, "up");
                }
            };

            var data = {
                "id"            : id,
                "phone_number"  : tel,
            };

            if ( isNull(tel) || tel == "" ) {
                runWarningMessage(t_iTel, "warning", 1500, "up");

            } else if( tel.length < 10 || tel.length >= 12){
                runWarningMessage(t_mTel, "warning", 1500, "up");
            }else {
                servicePhoneNum.ajaxPutData(data);
                dialogTel.hide();
            }
        }

        // 전화번호 변경 다이어로그 닫기
        btnDiagTelCancel.onclick = function(){
            dialogTel.hide();
        }

        // 모바일웹 버전 / 코도바 분기
        if( !ons.platform.isWebView() ){

            $(page).find(".btnSns").hide();
            $(page).find(".btnSnsWeb").show();

            // Kakao.init(KAKAO_SDK_KEY);
            // // 카카오 로그인 버튼을 생성합니다.
            // Kakao.Auth.createLoginButton({
            //     container: '#kakao-login-btn',
            //     success: function(){
            //         if( Kakao.Auth.getAccessToken() ){
            //             myApp.authControllers.fnSuccessKakaoLogin(page);        
            //         }
            //     },
            //     fail: function(err) {
            //         alert(JSON.stringify(err));
            //     }
            // });
        }else{
            $(page).find(".btnSns").show();
            $(page).find(".btnSnsWeb").hide();
        }

        document.addEventListener("deviceready", onDeviceReadySns, false);

        function onDeviceReadySns() {

            btnKakaoLo.onclick = () => { 

                LOGIN_CLB = () => { callLoginKakao(page); } ;
                if( !showChkTel_Diag() ){
                    return false;
                }else{
                    LOGIN_CLB();
                }
                
            };
            btnNaverLo.onclick = () => { 

                LOGIN_CLB = () => { callLoginNaver(page); } ;
                if( !showChkTel_Diag() ){
                    return false;
                }else{
                    LOGIN_CLB();
                }
            };

            btnAppleLo.onclick = () => { 
                LOGIN_CLB = () => { callLoginApple(page); } ;
                if( !showChkTel_Diag() ){
                    return false;
                }else{
                    LOGIN_CLB();
                }
            };
        }

        function callLoginApple(page){

            window.cordova.plugins.SignInWithApple.signin(
                { requestedScopes: [0, 1] },
                function(succ){
                  myApp.authControllers.fnSuccessAppleResponse(page, succ, "APPLE");
                },
                function(err){
                  console.error(err);
                  console.log(JSON.stringify(err))
                }
            );
        }

        function callLoginKakao(page){

            KakaoCordovaSDK.login(  0, 
                function(response){
                    myApp.authControllers.fnSuccessKakaoResponse(page, response, "KAKAO");
                },
                function(error) {
                    console.log("fail");
                    console.log(JSON.stringify(error));
                }
            );
        }

        function callLoginNaver(page){

            if( !showChkTel_Diag() ){
                return false;
            }

            Naver.login( 
                function(response) {
                    var accessToken = response.accessToken;
                    Naver.requestMe( 
                        function(response){
                            myApp.authControllers.fnSuccessNaverResponse(page, response, accessToken, "NAVER"), function(response){
                                console.log("Naver Fail Login");
                                var res     = JSON.stringify(response);
                                console.log(res);
                                return;
                            }
                        }, 
                        function(error) {
                            console.log("fail");
                            console.log(JSON.stringify(error));
                    })
                }, function(error) {
                    console.log("fail");
                    console.log(JSON.stringify(error));
                }
            );
        }

        // 아이디 찾기 이동
        btnFindId.onclick = function() {
            var data = { 'index': 0 };
            myApp.navigator.pushPage('html/auth/find_tabbar.html', { data: data });
        };

        // 비밀번호 찾기 이동
        btnFindPw.onclick = function() {
            var data = { 'index': 1 };
            myApp.navigator.pushPage('html/auth/find_tabbar.html', { data: data });
        };


        page.onShow = function() {
            myApp.i18Next.changeLang();
            tel = localStorage.getItem("TEL");
            page.querySelector("#phone_number").value = !isNull(tel) ? tel : "";
           
            $(fabTel).show();
        };

        function fnSetAutoLogin() {
            if (lsAutoLogin === true || lsAutoLogin == "true") {
                document.getElementById('isAutoLogin').checked = true;
            } else {
                document.getElementById('isAutoLogin').checked = false;
            }
        }

        function fnSetIdPw() {
            // 자동로그인 세팅에 따른 컨트롤 설정
            if (lsSaveIdPw === true || lsSaveIdPw == "true") {
                var tempId = window.localStorage.getItem("LOGIN_ID"); // 로그인 아이디
                var tempPw = window.localStorage.getItem("USER_PW"); // 로그인 비밀번호
                // page.querySelector('isSaveIdPw').checked = true;
                page.querySelector("#user_id").val(tempId);
                page.querySelector("#pass_wd").val(tempPw);
            } else {
                // document.getElementById('isSaveIdPw').checked = false;
                page.querySelector("#user_id").val("");
                page.querySelector("#pass_wd").val("");
            }
        }

        // 이벤트 세팅 =====================================================================
        // 로그인 버튼 클릭 및 로그인
        page.querySelector(".btnLogin").onclick = function() {

            localStorage.setItem('joinMemberData', null);
            if (fnValidationInput(page.id)) {
                var user_id = page.querySelector("#user_id input").value;
                var password = page.querySelector("#pass_wd input").value;

                var option = {
                    "user_id": user_id,
                    "password": password,
                    'phone_number': localStorage.getItem("TEL"),
                };

                var service = new withWSDL("api/userm/login");
                // 로그인 서비스 호출
                service.success = function(_jsonData) {
                    var jsonData = _jsonData;
                    if (typeof serviceModel.login === 'undefined') {
                        $.getScript("js/service/auth.js", function() {
                            serviceModel.login(page, jsonData);
                        });
                    } else {
                        serviceModel.login(page, jsonData);
                    }
                };

                service.ajaxPostData(option);
            }
        }

        // 회원가입 페이지 이동
        page.querySelector('.btnJoin').onclick = function() {

            LOGIN_CLB = () => { 
                myApp.navigator.pushPage('html/auth/join.html');
            };
            if( !showChkTel_Diag() ){
                return false;
            }else{
                LOGIN_CLB();
            }
        };

    },

    findIdPwTabbarPage: function(page) {
        myApp.i18Next.changeLang();
        var findIdPwTabbar = page.querySelector("#findIdPwTabbar");
        var data = page.data;
        var index = isNaN(data.index) ? 0 : parseInt(data.index);

        // 온센 UI 버그로 인한 setTimeOut 적용
        setTimeout(function() {
            findIdPwTabbar.setActiveTab(index);
        }, 200);
    },

    findIdPage: function(page) {
        myApp.i18Next.changeLang();
        page.querySelector(".btnSendEmail").onclick = function() {

            var email = page.querySelector("ons-input[name='email']").value;

            var option = {
                "email": email,
                "view": 1
            };

            // 전송완료되면 버튼 비활성화
            var service = new withWSDL("api/user/sendemail");
            service.success = function(_jsonData) {
                var jsonData = _jsonData;
                runWarningMessage(jsonData.message , "success", 2500, "up");
                page.querySelector(".btnSendEmail").setAttribute("disabled", "disabled");
            };

            service.ajaxGetData(option);
        }
    },

    findPwPage: function(page) {
        myApp.i18Next.changeLang();
        page.querySelector(".btnSendEmail").onclick = function() {

            var email = page.querySelector("ons-input[name='email']").value;
            var userid = page.querySelector("ons-input[name='userid']").value;
            var phonenumber = page.querySelector("ons-input[name='phonenumber']").value;

            var option = {
                "email": email,
                "userid": userid,
                "phonenumber": phonenumber,
                "view": 2
            };

            // 전송완료되면 버튼 비활성화
            var service = new withWSDL("api/user/sendemail");
            service.success = function(_jsonData) {
                $(page).find(".after-list").hide({ "animation": "none" });

                var jsonData = _jsonData;
                runWarningMessage(jsonData.message, "success", 2500, "up");
                page.querySelector(".btnSendEmail").setAttribute("disabled", "disabled");
            };

            service.ajaxGetData(option);
        }
    },


    profilePage: function(page) {

        myApp.i18Next.changeLang();

        var error_div = $(page).find('.error_box');
        var erros_memo = error_div.find('ul');
        var eventList = ["keydown", "keyup", "focusout", "focusin"];

        var objPassword = page.querySelector('#password input');
        var objPassword_confirm = page.querySelector('#passwordconfirm input');
        var objName = page.querySelector('#name input');

        var btnSave = page.querySelector("#btnSave");
        var btnLogout = page.querySelector("#btnLogout");
        var btnOffServer = page.querySelector("#btnOffService")
        var btnChangePw = page.querySelector(".btnChangePw");

        var btnRef  = page.querySelector("#btnReferer");

        var dialog = document.querySelector('#my-dialog');
        var serviceDialog = document.querySelector('#service-dialog');
        var btnDialogOK = document.querySelector(".btnDialogOK");
        var btnDialogCancel = document.querySelector("#btnMyDiagHide");
        
        var refDialog   = page.querySelector("#ref-dialog");
        var btnRefDialogOK = page.querySelector(".refDialogOK");
        var btnRefDialogCancel = page.querySelector(".refDiagClose");

        // 전화번호 수정
        var dialogTel       = page.querySelector("#phonenumber-dialog");
        var btnDiagTelCancel   = page.querySelector(".diagTelCancel");
        var btnDiagTelOk   = page.querySelector(".diagTelOk");
        var btnPhoneNumber = page.querySelector("#btnCheckTelNumber");

        var valiSuccess = false;

        btnOffServer.onclick = function(){

            if (!serviceDialog) {
                ons.createElement('dialog1.html', { append: true }).then(function(dialog) {});
            }
            serviceDialog.show( { "animation": "none" } );
        }

        page.querySelector('.diagServicecCancel').onclick = function(){
            serviceDialog.hide( { "animation": "none" } );
        }

        document.addEventListener("deviceready", onDeviceReadyProfile, false);

        if( !ons.platform.isWebView() ){
            // Kakao.init(KAKAO_SDK_KEY);
            btnDialogOK.onclick = function() {
                dialog.hide();
                var sns_type    = localStorage.getItem("SNS_TYPE");
                if( !isNull(sns_type) ){
                    if (!Kakao.Auth.getAccessToken()) {
                        console.log('Not logged in.');
                    }

                    Kakao.Auth.logout(function() {
                        console.log("Logout Success : ", Kakao.Auth.getAccessToken());
                    });
                }
                myApp.authControllers.fnSetStorageNull4Logout();
                location.href="index.html";
            };
        }

        function onDeviceReadyProfile() {

            // 클릭 : 서비스 끊기
            page.querySelector(".diagServiceOffOk").onclick = function(){
                dialog.hide();
                var sns_type    = localStorage.getItem("SNS_TYPE");

                if( !isNull(sns_type) ){
                    if( sns_type == "KAKAO"){
                        KakaoCordovaSDK.unlinkApp( function() {
                            myApp.authControllers.fnSetStorageNull4Logout();
                            location.href="index.html";
                        });
                    }else if( sns_type == "NAVER"){
                        Naver.logoutAndDeleteToken(function() {
                            myApp.authControllers.fnSetStorageNull4Logout();
                            location.href="index.html";
                        }, function(){

                        });

                    }else if( sns_type == "APPLE"){
                        Naver.logoutAndDeleteToken(function() {
                            myApp.authControllers.fnSetStorageNull4Logout();
                            location.href="index.html";
                        }, function(){

                        });

                    }else{

                    }
                }else{
                    myApp.authControllers.fnSetStorageNull4Logout();
                    location.href="index.html";
                }
            };

            // 클릭 : 로그아웃
            btnDialogOK.onclick = function() {
                
                dialog.hide();
                var sns_type    = localStorage.getItem("SNS_TYPE");

                if( !isNull(sns_type) ){
                    if( sns_type == "KAKAO"){
                        KakaoCordovaSDK.logout( function() {
                            myApp.authControllers.fnSetStorageNull4Logout();
                            location.href="index.html";
                        });
                    }else if( sns_type == "NAVER"){
                        Naver.logout(function() {
                            myApp.authControllers.fnSetStorageNull4Logout();
                            location.href="index.html";
                        }, function(){

                        })
                    }else if( sns_type == "APPLE"){
                        myApp.authControllers.fnSetStorageNull4Logout();
                        location.href="index.html";

                    }else{

                    }
                }else{
                    myApp.authControllers.fnSetStorageNull4Logout();
                    location.href="index.html";
                }
            }
        }

        // 클릭 : 코인사용내역 조회 페이지 이동
        page.querySelector(".btnCoinList").onclick = function() { myApp.navigator.pushPage('html/auth/coin.html'); };

        var option = {
            "CORP_ID"   : localStorage.getItem("CORP_ID"),
            "ID"        : localStorage.getItem("ID")

        };
        var service = new withWSDL("api/userm/list/" + option.ID);

        service.success = function(_jsonData) {
            var jsonData = _jsonData;

            // TODO 사용자정보가 없을 경우 : iOS 적용 필요
            if( isNull(jsonData.data) || jsonData.data === null){
                myApp.authControllers.fnSetStorageNull4Logout();
                location.href="index.html";
                return;
            }
            
            if (typeof serviceModel.login === 'undefined') {
                $.getScript("js/service/auth.js", function() {
                    serviceModel.userInfo(page, jsonData);
                });
            } else {
                serviceModel.userInfo(page, jsonData);
            }

            // 지역설정 바로가기
            page.querySelector(".goLocSetting").onclick = function(){ 
                myApp.navigator.pushPage('html/auth/user_location.html',  { animation: "none" , data: jsonData }); 
            };
        };


        // 사용자정보 / 코인수량 갱신
        service.ajaxGetData(option);

        page.onShow = function() {

            // 사용자정보 / 코인수량 갱신
            service.ajaxGetData(option);

            // 에러박스 숨기기
            $(page).find("[type='password']").val("");
            error_div.hide();
        };


        // 전체 체크
        var checkSum = function() {
            if (valiSuccess) {
                btnChangePw.removeAttribute("disabled");
                $(page).find('.error_box').fadeOut();
            } else {
                $(page).find('.error_box').show();
                btnChangePw.setAttribute("disabled");
            }
        };

        // 입력단 체크
        var erros_memo_change = function() {

            erros = $(page).find('.error_box ul');
            if (erros.find(".warning").length == 0) {
                erros.parent("div").addClass("success_box");
                valiSuccess = true;
            } else {
                erros.parent("div").removeClass("success_box");
                valiSuccess = false;
            }
            checkSum();
        }


        // 비밀번호 체크
        for (event of eventList) {
            objPassword.addEventListener(event, function() {
                if (ValidatePassWord(objPassword.value)) {
                    erros_memo.find(".password").removeClass("warning");
                } else {
                    erros_memo.find(".password").addClass('warning');
                }

                if (objPassword.value.length >= 6) {
                    erros_memo.find(".length").removeClass('warning');
                } else {
                    erros_memo.find(".length").addClass("warning");
                }

                if (objPassword.value === objPassword_confirm.value && objPassword_confirm.value.length >= 6) {
                    erros_memo.find(".confirm").removeClass('warning');
                } else {
                    erros_memo.find(".confirm").addClass('warning');
                }
                erros_memo_change()
            });


            objPassword_confirm.addEventListener(event, function() {
                if (objPassword.value === objPassword_confirm.value && objPassword_confirm.value.length >= 6) {
                    erros_memo.find(".confirm").removeClass('warning');
                } else {
                    erros_memo.find(".confirm").addClass('warning');
                }
                erros_memo_change();
            });
        }

        // 이름체크
        for (event of eventList) {
            objName.addEventListener(event, function() {
                if (objName.value.length >= 2) {
                    // erros_memo.find(".name").removeClass('warning');
                    btnSave.removeAttribute("disabled");
                } else {
                    // erros_memo.find(".name").addClass('warning');
                    btnSave.setAttribute("disabled");
                }
            });
        }

        // 클릭 : 비밀번호 변경
        btnChangePw.onclick = function() {

            var service = new withWSDL("api/userm/list/" + option.ID);

            var data = {
                ID      : option.ID,
                PASS_WD : objPassword.value,
                TEL     : localStorage.getItem("TEL"),
                MODE    : 'PASS',
            };

            service.success = function(_jsonData) {
                // var jsonData = _jsonData;
                var t_pw_changed    = myApp.i18Next.getTrasTxt("AUTH", "PW_CHANGED");
                runWarningMessage(t_pw_changed, "success", 1000, "up");
                $(page).find("[type='password']").val("");
            };

            service.ajaxPutData(data);
        }

        btnSave.onclick = function() {

            var service = new withWSDL("api/userm/list/" + option.ID);

            var data = {
                ID      : option.ID,
                USER_NM : objName.value,
                TEL     : localStorage.getItem("TEL"),
                MODE    : 'NAME',
            };

            service.success = function(_jsonData) {
                var jsonData = _jsonData;
                var t_nick_changed  = myApp.i18Next.getTrasTxt("AUTH", "NICK_CHANGED");

                localStorage.setItem("USER_NM", objName.value);
                $(".profile .userName").text(objName.value);

                //console.log(jsonData);

                if (jsonData.message == 'success') {
                    runWarningMessage(t_nick_changed, "success", 1000, "up");
                } else {
                    runWarningMessage("Change failed..", "fail", 1500, "up");
                }
            };

            service.ajaxPutData(data);

        }


        // 클릭 : 다이어로그(로그아웃 여부)
        btnLogout.onclick = function() {

            if (!dialog) {
                ons.createElement('dialog.html', { append: true }).then(function(dialog) {});
            }

            $(dialog).find(".dialogMessage").attr("data-i18n", "AUTH:questLogout");
            $('.translation').i18n();
            dialog.show( { "animation": "none" } );
        }

        
        // 클릭 : 추천인 다이어로그
        btnRef.onclick = function(){
            if(refDialog){
                refDialog.show( { "animation": "none" } );
            }
        }

        // 클릭 : 전화번호 변경 다이어로그
        btnPhoneNumber.onclick = function(){
            var tel         = page.querySelector("#tel").value;
            var t_inTel     = myApp.i18Next.getTrasTxt("AUTH", "INP_TEL");
            var t_val_tel   = myApp.i18Next.getTrasTxt("DIAG", "I_VAL_10_20");

            $(page).find(".changeTelNum").text(tel);

            if ( isNull(tel) || tel == "" ) {
                runWarningMessage(t_inTel, "warning", 1500, "up");
            } else if( tel.length < 10 || tel.length >= 12){
                runWarningMessage(t_val_tel, "warning", 1500, "up");
            } else{
                dialogTel.show( { "animation": "none" } );
            }
        }

        // 전화번호 변경 확인 
        btnDiagTelOk.onclick = function(){
            var tel     = page.querySelector("#tel").value;
            var id      = localStorage.getItem("ID");
            
            var t_inTel     = myApp.i18Next.getTrasTxt("AUTH", "INP_TEL");
            var t_val_tel   = myApp.i18Next.getTrasTxt("DIAG", "I_VAL_10_20");

            // 추천인 등록
            var servicePhoneNum = new withWSDL("api/userm/phonenumber/" + id );
            servicePhoneNum.success = function(_jsonData) {

                var jsonData = _jsonData;
                if (jsonData.status == 200) {
                    localStorage.setItem("TEL", tel);
                    runWarningMessage(jsonData.message, "success", 1000, "up");
                    $(".myTel").text(tel);
                    
                } else {
                    runWarningMessage(jsonData.message, "danger", 1000, "up");
                }
                
            };

            var data = {
                "id"            : id,
                "phone_number"  : tel,
            };

            if ( isNull(tel) || tel == "" ) {
                runWarningMessage(t_inTel, "warning", 1500, "up");

            } else if( tel.length < 10 || tel.length >= 12){
                runWarningMessage(t_val_tel, "warning", 1500, "up");
            }else {
                servicePhoneNum.ajaxPutData(data);
                dialogTel.hide();
            }
        }

        // 전화번호 변경 다이어로그 닫기
        btnDiagTelCancel.onclick = function(){
            dialogTel.hide();
        }


        // 추천하기
        btnRefDialogOK.onclick = function(){

            var id      = localStorage.getItem("ID");
            var ref     = $(page).find("#refferor").val();

            // 추천인 아이디를 입력하세요
            var t_inRef = myApp.i18Next.getTrasTxt("AUTH", "INPUT_REF");

            // 추천인 등록
            var serviceRef = new withWSDL("api/userm/ref/" + id );
            serviceRef.success = function(_jsonData) {

                var jsonData = _jsonData;
                if (jsonData.status == 200) {
                    runWarningMessage(jsonData.message, "success", 1000, "up");
                    $(page).find("#refferor").prop("readonly", true);
                    $(page).find("#refferor input").prop("readonly", true);
                    $(page).find("#btnReferer").prop("disabled", true);
                    refDialog.hide();
                } else {
                    runWarningMessage(jsonData.message, "danger", 1000, "up");
                }
                refDialog.hide();
            };

            var data = {
                "REF_ID"    : ref,
            };

            if ( isNull(ref) || ref == "" || ref == 0) {
                runWarningMessage(t_inRef, "warning", 1500, "up");
            } else {
                serviceRef.ajaxPutData(data);
            }
        }

        // 추천인 닫기
        btnRefDialogCancel.onclick = function(){
            refDialog.hide();
        }

         // 클릭 : 로그아웃 다이어로그 닫기
         btnDialogCancel.onclick = function() {
            dialog.hide();
        };
       
    },

    coinPage: function(page) {

        $(page).find("#allQTY").attr("data-i18n", "[placeholder]BSK:MY_COIN");
        
        myApp.i18Next.changeLang();
        
        var option = {
            "user_id": localStorage.getItem("ID"),
            "new": null,
            "start": 0
        };

        var service = new withWSDL("api/coin/list");
        service.success = function(_jsonData) {
            var jsonData = _jsonData;
            if (typeof serviceModel.login === 'undefined') {
                $.getScript("js/service/auth.js", function() {
                    serviceModel.coinInfoList(page, jsonData, option);
                });
            } else {
                serviceModel.coinInfoList(page, jsonData, option);
            }
        };

        searchList(option);

        // 전체/획득/사용 라디오버튼 클릭 이벤트
        var radioClick = true;
        Array.prototype.forEach.call(page.querySelectorAll("ons-radio[name=radCoins]"), function(ele) {

            ele.onclick = function() {

                // 중복 클릭 체크
                //if (radioClick) {
                // 1. 구현부 
                if (ele.value == "in") {
                    option.is_inout = 'I';
                } else if (ele.value == "out") {
                    option.is_inout = "O";
                } else {
                    delete option.is_inout;
                }

                option.new = true;
                option.start = 0;
                searchList(option);

                // // 2. 중복방지 부
                // radioClick = !radioClick;

                // // 3. 중복방지 부 : 타이밍 추가
                // setTimeout(function () {
                //     radioClick = true;
                // }, 1000)  
                //} 
            }
        });

        // 더보기 스크롤(10개씩 스크롤)
        page.onInfiniteScroll = function(done) {
            if (page.querySelector(".emptyOrNoData.card") === null) {
                setTimeout(function() {
                    // 이벤트 정보 가져오기
                    option.start += 10;
                    searchList(option);
                    done();
                }, 300);
            }
        };

        // 더보기 클릭
        $(page).find(".card").on("click", ".after-list", function() {
            option.start += 10;
            option.new = 'N';
            // 이벤트 목록 호출
            searchList(option);
        });

        // 리스트 정보 호출
        function searchList(option) {
            service.ajaxGetData(option);
        }
    },

    // 이메일 체크
    fnCheckEmail: function(data, callback, callback2) {
        // console.log(data);
        var option = data;
        var service = new withWSDL();

        service.success = function(_jsonData, cl, cl2) {
            cl = callback;
            c2 = callback2;
            var bTrue = true;
            var jsonData = $.parseJSON(_jsonData);
            if (typeof serviceModel.login === 'undefined') {
                $.getScript("js/service/auth.js", function() {
                    bTrue = serviceModel.chkEmail(jsonData);
                    if (typeof cl === 'function') {
                        cl(bTrue);
                    }
                    if (typeof cl2 === 'function') {
                        cl2();
                    }
                });
            } else {
                bTrue = serviceModel.chkEmail(jsonData);
                if (typeof cl === 'function') {
                    cl(bTrue);
                }
                if (typeof cl2 === 'function') {
                    cl2();
                }
            }
        };
    }, 

    fnSetStorageNull4Logout : function(){

        localStorage.setItem("IS_LOGIN_YN", "N");    // 로그인 여부
        localStorage.setItem("USER_ID", null);       // 사용자 아이디(KEY)
        // localStorage.setItem("ID", null);            // 사용자 아이디
        localStorage.setItem("USER_NM", null);       // 사용자명
        localStorage.setItem("EMAIL", null);         // 이메일
        // localStorage.setItem("TEL", null);           // 전화번호
        
        localStorage.setItem("MY_COIN", null);       // 위드유페이 코인 
        // window.localStorage.setItem("ROLE", null);          // 권한
        // window.localStorage.setItem("CORP_ID", null);    // 회사아이디
        // window.localStorage.setItem("BRANCH_ID", null);  // 호점(지점)코드
        localStorage.setItem("PRODUCT_CORP_ID", null); // 상품회사 아이디
        localStorage.setItem("PRODUCT_CORP_NM", null); // 상품회사명
        localStorage.setItem("REQUEST_AT", null);     // 요청일
        localStorage.setItem("joinMemberData", null);     // 요청일

        // window.localStorage.setItem("ADDR1", null);      // 주소1
        // window.localStorage.setItem("ADDR2", null);      // 주소2
        // window.localStorage.setItem("JOIN_DT", null);       // 가입일시

        localStorage.setItem("SNS_TYPE",   null ); 
        localStorage.setItem("PROFILE_IMG",null ); 
        localStorage.setItem("NICK_NM",    null ); 
        localStorage.setItem("AGE_RANGE",  null ); 
               
    }, 
    fnSuccessNaverResponse : function(page, response, accessToken, sns_type){

        // var res     = JSON.stringify(response);
        // console.log(res);

        var res         = response;
        var message     = res.message;
        var resultCode  = res.resultCode;
        var account     = res.response;

        if( message && account ){

            var obj = {
                'nickname'    : account.nickname,
                'p_image'     : account.profileImage,
                'email'       : account.email,
                'age_range'   : account.age,
                'birth'       : null,
                'gender'      : null,
                'accessToken' : accessToken,
                'connect_at'  : moment().format("YYYY-MM-DD HH:mm:ss"),
                'phone_number': localStorage.getItem("TEL"),
                'sns_type'    : sns_type,
            }

            localStorage.setItem("AGE_RANGE", obj.age_range);
            localStorage.setItem("SNS_TYPE", obj.sns_type);

            var service = new withWSDL("api/userm/sns_login");
            // 로그인 서비스 호출
            service.success = function(_jsonData) {
                var jsonData = _jsonData;
                
                if (typeof serviceModel.loginNaver === 'undefined') {
                    $.getScript("js/service/auth.js", function() {
                        serviceModel.loginNaver(page, jsonData, obj);
                    });
                } else {
                    serviceModel.loginNaver(page, jsonData, obj);
                }
            };

            service.ajaxPostData(obj);
        }
    },

    fnSuccessKakaoResponse : function(page, response, sns_type){
        
        var res     = JSON.stringify(response);
        // console.log(res);
        var profile = response.properties;
        var account = response.kakao_account;

        if( profile && account ){

            var obj = {
                'nickname'      : profile.nickname,
                'p_image'       : profile.thumbnail_image,
                'email'         : account.email,
                'has_age_range' : account.has_age_range,
                'age_range'     : account.age_range,
                'birth'         : account.birthday,
                'gender'        : account.gender,
                'accessToken'   : response.accessToken,
                'connect_at'    : response.connected_at,
                'phone_number'  : localStorage.getItem("TEL"),
                'sns_type'      : sns_type,
            }

            localStorage.setItem("AGE_RANGE", obj.age_range);
            localStorage.setItem("SNS_TYPE", obj.sns_type);

            var service = new withWSDL("api/userm/sns_login");

            // 로그인 서비스 호출
            service.success = function(_jsonData) {
                var jsonData = _jsonData;
                
                if (typeof serviceModel.loginKaKao === 'undefined') {
                    $.getScript("js/service/auth.js", function() {
                        serviceModel.loginKaKao(page, jsonData, obj);
                    });
                } else {
                    serviceModel.loginKaKao(page, jsonData, obj);
                }
            };

            service.ajaxPostData(obj);
        }

    }, 
    fnSuccessKakaoLogin : function(page){
        Kakao.API.request({
            url : "/v2/user/me",
            success: function(response) {
                myApp.authControllers.fnSuccessKakaoResponse(page, response, "KAKAO");
            },
            fail: function(error) {
              console.log(error);
            },
        });
    },

    fnSuccessAppleResponse : function(page, response, sns_type){
        
        var res     = (response);
        var profile = res;

        var email   = typeof res.email == 'undefined'? "" : res.email;

        if( profile ){

            var obj = {

                'nickname'      : email,
                'p_image'       : null,
                'email'         : email,

                'has_age_range' : null,
                'age_range'     : null,
                'birth'         : null,
                'gender'        : null,
                'accessToken'   : profile.identityToken,
                'connect_at'    : moment().format("YYYY-MM-DD HH:mm"),
                'phone_number'  : localStorage.getItem("TEL"),
                'sns_type'      : sns_type,
            }

            localStorage.setItem("AGE_RANGE", obj.age_range);
            localStorage.setItem("SNS_TYPE", obj.sns_type);

            var service = new withWSDL("api/userm/sns_login");

            // 로그인 서비스 호출
            service.success = function(_jsonData) {
                var jsonData = _jsonData;
                
                if (typeof serviceModel.loginApple === 'undefined') {
                    $.getScript("js/service/auth.js", function() {
                        serviceModel.loginApple(page, jsonData, obj);
                    });
                } else {
                    serviceModel.loginApple(page, jsonData, obj);
                }
            };

            service.ajaxPostData(obj);
        }

    }, 

    userLocationPage : function(page){
        
        var data    = page.pushedOptions.data.data;
        var uID     = localStorage.getItem("ID");

        var addr1   = "";
        var addr2   = "";

        page.onShow = function(){

            addr1   = localStorage.getItem("ADDR1");
            addr2   = localStorage.getItem("ADDR2");

            page.querySelector("ons-input[name='addr1']").value   = isNull(addr1) ? "" : addr1;
            page.querySelector("ons-input[name='addr2']").value   = isNull(addr2) ? "" : addr2;
        };
        
        if( isNull(data)){
            
            data = {
                area_id     : localStorage.getItem("AREA_ID"),
                CNTNT_ID    : localStorage.getItem("CNTNT_ID"),
                NATION_ID   : localStorage.getItem("NATION_ID"),
                ADDR1       : addr1,
                ADDR2       : addr2,
            };
            
            var svcUserM = new withWSDL("api/userm/list/" + uID);
            svcUserM.success = function(_jsonData) {

                var dt  = _jsonData.data;
                data = {
                    area_id     : data.area_id,
                    CNTNT_ID    : data.CNTNT_ID,
                    NATION_ID   : data.NATION_ID,
                    ADDR1       : dt.ADDR1,
                    ADDR2       : dt.ADDR2,
                };

                getAreaInfo();
            };

            if( !isNull(uID)){
                svcUserM.ajaxGetData({});
            }else{
                getAreaInfo();    
            }

        }else{
            getAreaInfo();
        }
       
        function getAreaInfo(){
            var sOption  = {
                "area_id"   : data.area_id,
                "cntnt_id"  : data.CNTNT_ID,
                "nation_id" : data.NATION_ID,
                "kind"      : "ALL",
                // "id"        : localStorage.getItem("ID"),
            };
    
            page.querySelector("ons-input[name='addr1']").value = data.ADDR1;
            page.querySelector("ons-input[name='addr2']").value = data.ADDR2;
    
            // 사용자 지역/주소정보 가져오기
            var svcCntnt = new withWSDL("api/areaconf/list");
            svcCntnt.success = function(_jsonData) {
    
                var jsonData = _jsonData;
                if (typeof serviceModel.setDepth === 'undefined') {
                    $.getScript("js/service/area.js", function(){
                        if( sOption.kind == "ALL"){
                            serviceModel.setAreaInfo(page, jsonData, sOption);
                        }else if(sOption.kind == "NATION"){
                            serviceModel.setAreaInfo(page, jsonData, sOption);
                        }else if(sOption.kind == "CITY"){
                            serviceModel.setAreaInfo(page, jsonData, sOption);
                        }
                    });    
                }else{
                    if( sOption.kind == "ALL"){
                        serviceModel.setAreaInfo(page, jsonData, sOption);
                    }else if(sOption.kind == "NATION"){
                        serviceModel.setAreaInfo(page, jsonData, sOption);
                    }else if(sOption.kind == "CITY"){
                        serviceModel.setAreaInfo(page, jsonData, sOption);
                    }
                }

                
            };
            
            svcCntnt.ajaxGetData(sOption);
    
            // 대륙,국가,도시 선택
            $select1    = $(page).find("ons-select[name='sCntnt']");
            $select2    = $(page).find("ons-select[name='sNation']");
            $select3    = $(page).find("ons-select[name='sCity']");
    
            $select1.change(function(){
                
                var _id = $(this).find("select option:selected").val();
                
                if( _id == "" ){
                    $select2.find("option[value!='']").remove();
                    return;
                }
                sOption.cntnt_id = _id,
                sOption.kind     = "NATION",
                
                svcCntnt.ajaxGetData(sOption);
            });
    
            $select2.change(function(){
                
                var _id = $(this).find("select option:selected").val();
                
                if( _id == "" ){
                    $select3.find("option[value!='']").remove();
                    return;
                }
                sOption.nation_id = _id,
                sOption.kind     = "CITY",
                
                svcCntnt.ajaxGetData(sOption);
            });
        }
        
        // 주소설정 바로가기
        page.querySelector(".goAddr").onclick = function(){ myApp.navigator.pushPage('html/config/config_addr_tabbar.html' ); };

        // 저장
        page.querySelector(".btnSaveLoc").onclick = function(){ 
            
            var cntnt_id= $select1.find("select option:selected").val();
            var naton_id= $select2.find("select option:selected").val();
            var area_id = $select3.find("select option:selected").val();
            var area_nm = $select3.find("select option:selected").text();
            var addr1   = page.querySelector("ons-input[name='addr1']").value;
            var addr2   = page.querySelector("ons-input[name='addr2']").value;

            var t_mod_ok= myApp.i18Next.getTrasTxt("COMM", "MOD_COMPLTE");

            var option  = {
                "ID"        : localStorage.getItem("ID"),
                "ADDR1"     : addr1,
                "ADDR2"     : addr2,
                "AREA_ID"   : area_id,
                "MODE"      : "AREA",
            };

            // 지역아이디 변경여부 ON
            IS_CHANGE_AREA = true;
             
            var svcmuser  = new withWSDL("api/userm/list/" + option.ID);
            svcmuser.success = function(_jsonData) {

                localStorage.setItem("ADDR1", addr1);
                localStorage.setItem("ADDR2", addr2);
                localStorage.setItem("AREA_NM", area_nm);
                localStorage.setItem("AREA_ID", area_id);
                localStorage.setItem("CNTNT_ID", cntnt_id);
                localStorage.setItem("NATION_ID", naton_id);
                
                document.querySelector('#myNavigator').popPage( 
                    { 
                        animation   : "none" , 
                        callback    : function(){
                            runWarningMessage(t_mod_ok, "success", 2000, "up");
                        }
                    }
                );
            };

            if( isNull( uID ) ){

                localStorage.setItem("ADDR1", addr1);
                localStorage.setItem("ADDR2", addr2);
                localStorage.setItem("AREA_NM", area_nm);
                localStorage.setItem("AREA_ID", area_id);
                localStorage.setItem("CNTNT_ID", cntnt_id),
                localStorage.setItem("NATION_ID", naton_id),

                document.querySelector('#myNavigator').popPage( 
                    { 
                        animation   : "none" , 
                        callback    : function(){
                            runWarningMessage(t_mod_ok, "success", 2000, "up");
                        }
                    }
                );

            }else{
                svcmuser.ajaxPutData(option);
            }
        };

    }
}
