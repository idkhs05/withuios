myApp.basket = {

    getSummaryCount : function(){

        var summaryCount = 0;
        $.each(window.baskets, function(index, obj){ 
            summaryCount += !isNaN( obj.qty ) ? parseInt(obj.qty) : 0;
        });

        return summaryCount;
    },

    getList : function(itemName){
		var retrievedObject = localStorage.getItem(itemName);
        return JSON.parse(retrievedObject);
    },

    setItem : function(itemName, objData){
        localStorage.setItem(itemName, JSON.stringify(objData));
    }, 

    removeObjectItem : function(itemName, objData){
        
        if( window.baskets.length > 0 ){
            $.each(window.baskets, function(index, obj){ 
                // console.log("index : " + index);
                if( obj.corpCode == objData.corpCode && obj.categoryCode == objData.categoryCode && obj.productCode == objData.productCode){
                    
                    if( isNaN(obj.qty) ) {
                        obj.qty = parseInt(0);
                    }
                    if( obj.qty >= 1){
                        obj.qty--;
                    }else{
                        // console.log(index);
                        window.baskets.slice(index);         
                    }

                    return false;
                }
            });
        }

        localStorage.setItem(itemName, JSON.stringify(baskets));
    },

    addObjectItem : function(data){

        var product_cd  = data.REF_PRODUCT_CD;
        var now_qty     = data.NOW_QTY;

        // console.log(data);

		// 장바구니에 상품 추가/삭제 : UPDATE
		var service	= new withWSDL("api/basket/list/0");

		// 장바구니 서비스 바인딩
		service.success = function(_jsonData){
            var jsonData    = _jsonData;

            // 메시지가 안 떠 있는 경우만 보이게.
            if( $("ons-toast[id!=toast]").length == 0){

                if( jsonData.status == 'success'){
                    if( now_qty == 0){
                        $("#basketPage ons-list-item[data-product_cd='" + product_cd + "']").remove();
                    }
                    runWarningMessage(jsonData.message, "success", 1000, "up");
                }else{
                    runWarningMessage(jsonData.message, "danger", 1000, "up");
                }
            }
        };

        service.error = function(x, t, m){
            var errors = $.parseJSON(x.responseText);
            var ul = $("<ul></ul>");

            $.each(errors.message, function (key, val) {
                ul.append("<li><i class='fa fa-warning'></i> " +  key[0] + "</li>");
            });

            ul.find("li").css("list-style", "none");
            runWarningMessage( ul.html(), "warning", 3000, "up");
        }

        if( isNull( data.TEL ) ){

            // "전화번호를 읽어올수 없습니다. 관리자에게 문의하세요"
            var t_err_auth  = myApp.i18Next.getTrasTxt("SYS", "ERR_TEL_AUTH");
            runWarningMessage(t_err_auth, "danger", 2000, "up");
        }else{
            service.ajaxPutData(data);
        }
    },

    setProductBasket : function(page, basketData){

        var data = {
            ID					: localStorage.getItem("ID"),
            TEL                 : localStorage.getItem("TEL"),
            PRODUCT_CORP_ID     : basketData.p_corp_id,
            REF_PRODUCT_CD      : basketData.productCd,
            QTY                 : basketData.qty,
            NOW_QTY             : basketData.now_qty
        }

        if( basketData.isEnable ){
            myApp.basket.addObjectItem(data);
        }else{
            return false;
        }
    }


}